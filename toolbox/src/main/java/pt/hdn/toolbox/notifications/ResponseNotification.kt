package pt.hdn.toolbox.notifications

import android.app.Notification.Builder
import android.app.Notification.CATEGORY_MESSAGE
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.NotificationManager
import android.content.Context
import android.view.View.VISIBLE
import android.widget.RemoteViews
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Channel.Companion.RESPONSE
import pt.hdn.toolbox.annotations.Duration.Companion.x60
import pt.hdn.toolbox.annotations.Notification.Companion.REPLY
import pt.hdn.toolbox.annotations.Notification.Companion.WARNING
import pt.hdn.toolbox.resources.Resources

class ResponseNotification(
    context: Context,
    private val res: Resources,
    private val remoteViews: RemoteViews,
    private val notificationManager: NotificationManager
): BaseNotification(context) {

    init {
        this.builder = Builder(context, RESPONSE)
            .setVisibility(VISIBILITY_PUBLIC)
            .setCategory(CATEGORY_MESSAGE)
            .setAutoCancel(true)
            .setTimeoutAfter(x60)
            .setCustomContentView(remoteViews)
    }

    fun cancel() { notify(R.drawable.ic_notification_negative, R.string.cancelled, R.string.requestCancelled, WARNING) }

    fun unavailable() { notify(R.drawable.ic_cancel, R.string.unavailable, R.string.unavailableSpecialist, WARNING) }

    fun reject() { notify(R.drawable.ic_notification_negative, R.string.rejected, R.string.requestRejected, REPLY) }

    fun accept() { notify(R.drawable.ic_notification_positive, R.string.accept, R.string.requestAccepted, REPLY) }

    fun clear() { notificationManager.cancel(WARNING); notificationManager.cancel(REPLY) }

    private fun notify(@DrawableRes iconId: Int, @StringRes titleId: Int, @StringRes subtitleId: Int, id: Int) {
        remoteViews
            .apply {
                setImageViewResource(R.id.img_notifyLogo, iconId)
                setTextViewText(R.id.lbl_notifyTitle, string(titleId))
                setTextViewText(R.id.lbl_notifySubtitle, string(subtitleId))
                setViewVisibility(R.id.lbl_notifySubtitle, VISIBLE)

                builder.setSmallIcon(iconId)

                notificationManager.notify(id, builder.build())
            }
    }

    private fun string(@StringRes id: Int) = res.string(id)
}