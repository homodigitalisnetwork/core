package pt.hdn.toolbox.notifications

import android.app.Notification.Builder
import android.content.Context
import android.util.TypedValue
import androidx.annotation.ColorInt
import pt.hdn.toolbox.R
import pt.hdn.toolbox.misc.getColorAttr

abstract class BaseNotification(
    context: Context
) {

    //region vars
    protected lateinit var builder: Builder
    @ColorInt protected val colorPrimary: Int = context.getColorAttr(R.attr.colorPrimary)
    @ColorInt protected val colorOnPrimary: Int = context.getColorAttr(R.attr.colorOnPrimary)
    //endregion vars

}