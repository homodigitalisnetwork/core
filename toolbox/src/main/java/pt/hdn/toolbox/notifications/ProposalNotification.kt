package pt.hdn.toolbox.notifications

import android.app.Notification.*
import android.app.NotificationManager
import android.content.Context
import android.view.View.VISIBLE
import android.widget.RemoteViews
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.google.maps.android.SphericalUtil.computeDistanceBetween
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Channel.Companion.REQUEST
import pt.hdn.toolbox.annotations.Notification.Companion.PROPOSAL
import pt.hdn.toolbox.annotations.Notification.Companion.REPLY
import pt.hdn.toolbox.annotations.Notification.Companion.WARNING
import pt.hdn.toolbox.business.proposal.DefaultProposal
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.resources.Resources

class ProposalNotification(
    context: Context,
    private val res: Resources,
    private val genericRemoteViews: RemoteViews,
    private val proposalRemoteViews: RemoteViews,
    private val notificationManager: NotificationManager
): BaseNotification(context) {

    init { this.builder = Builder(context, REQUEST).setVisibility(VISIBILITY_PUBLIC).setCategory(CATEGORY_MESSAGE).setLocalOnly(true) }

    fun cancel() { notificationManager.cancel(PROPOSAL); notify(R.drawable.ic_notification_negative, R.string.cancelled, R.string.proposalCancelled) }

    fun reject() { notify(R.drawable.ic_notification_negative, R.string.rejected, R.string.proposalRejected) }

    fun propose(proposal: DefaultProposal, position: Position) {
        with(proposal) {
            genericRemoteViews
                .apply {
                    setImageViewResource(R.id.img_notifyLogo, R.drawable.ic_partnership)
                    setTextViewText(R.id.lbl_notifyTitle, string(R.string.contract))
                    setTextViewText(R.id.lbl_notifySubtitle, string(R.string.contractProposal))
                }

            proposalRemoteViews
                .apply {
                    setTextViewText(R.id.lbl_notifyProposalTitle, string(R.string.contract))
                    setTextViewText(R.id.lbl_notifyProposalSubtitle, string(R.string.contractProposal))
                    setTextViewText(R.id.lbl_notifyProposalNameValue, buyerName)
                    setTextViewText(R.id.lbl_notifyProposalRatingValue, "%.2f".format(buyerRating))
                    setTextViewText(R.id.lbl_notifyProposalRequestValue, specialitiesNames)
                    setTextViewText(R.id.lbl_notifyProposalLocationValue, distance(buyerPosition, position))
                }

            builder
                .apply {
                    setSmallIcon(R.drawable.ic_partnership)
                    setAutoCancel(true)
                    setOnlyAlertOnce(true)
                    setCustomContentView(genericRemoteViews)
                    setCustomBigContentView(proposalRemoteViews)
                    setCustomHeadsUpContentView(proposalRemoteViews)

                    notificationManager.notify(uuid.toString(), PROPOSAL, build())
                }
        }
    }

    fun accept() { notify(R.drawable.ic_notification_positive, R.string.accept, R.string.proposalAccepted) }

    fun clear() { notificationManager.cancel(WARNING); notificationManager.cancel(REPLY) }

    private fun notify(@DrawableRes iconId: Int, @StringRes titleId: Int, @StringRes subtitleId: Int) {
        genericRemoteViews
            .apply {
                setImageViewResource(R.id.img_notifyLogo, iconId)
                setTextViewText(R.id.lbl_notifyTitle, string(titleId))
                setTextViewText(R.id.lbl_notifySubtitle, string(subtitleId))
                setViewVisibility(R.id.lbl_notifySubtitle, VISIBLE)

                builder
                    .apply {
                        setSmallIcon(iconId)
                        setCustomBigContentView(null)
                        setCustomHeadsUpContentView(null)

                        notificationManager.notify(REPLY, build())
                    }
            }
    }

    private fun distance(from: Position, to: Position): String {
        return computeDistanceBetween(from.toLatLng(), to.toLatLng())
            .let {
                when {
                    100000.0 <= it  -> "%.0f km".format(it / 1000.0)
                    10000.0 <= it  && it < 100000.0 -> "%.1f km".format(it / 1000.0)
                    1000.0 <= it  && it < 10000.0 -> "%.2f km".format(it / 1000.0)
                    else -> "%.0f m".format(it)
                }
            }
    }

    private fun string(@StringRes id: Int) = res.string(id)
}