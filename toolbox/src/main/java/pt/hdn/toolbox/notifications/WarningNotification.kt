package pt.hdn.toolbox.notifications

import android.app.Notification.Builder
import android.app.Notification.CATEGORY_MESSAGE
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.NotificationManager
import android.content.Context
import android.view.View.VISIBLE
import android.widget.RemoteViews
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Channel.Companion.TRANSACTION
import pt.hdn.toolbox.annotations.Duration.Companion.x60
import pt.hdn.toolbox.annotations.Notification.Companion.WARNING
import pt.hdn.toolbox.resources.Resources

class WarningNotification(
    context: Context,
    private val res: Resources,
    private val remoteViews: RemoteViews,
    private val notificationManager: NotificationManager
): BaseNotification(context) {

    init {
        this.builder = Builder(context, TRANSACTION)
            .setVisibility(VISIBILITY_PUBLIC)
            .setCategory(CATEGORY_MESSAGE)
            .setAutoCancel(true)
            .setTimeoutAfter(x60)
            .setCustomContentView(remoteViews)
    }

    fun play() { warn(R.drawable.ic_start, R.string.work, R.string.running) }

    fun pause() { warn(R.drawable.ic_pause, R.string.work, R.string.paused) }

    fun abort() { warn(R.drawable.ic_abort, R.string.aborted, R.string.requestAborted) }

    fun finish() { warn(R.drawable.ic_check, R.string.finished, R.string.workFinished) }

    fun clear() { notificationManager.cancel(WARNING) }

    private fun warn(@DrawableRes iconId: Int, @StringRes titleId: Int, @StringRes subtitleId: Int) {
        with(remoteViews) {
            setImageViewResource(R.id.img_notifyLogo, iconId)
            setTextViewText(R.id.lbl_notifyTitle, string(titleId))
            setTextViewText(R.id.lbl_notifySubtitle, string(subtitleId))
            setViewVisibility(R.id.lbl_notifySubtitle, VISIBLE)

            builder.setSmallIcon(iconId)

            notificationManager.notify(WARNING, builder.build())
        }
    }

    private fun string(@StringRes id: Int) = res.string(id)
}