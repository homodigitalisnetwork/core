package pt.hdn.toolbox.notifications

import android.app.Notification.Builder
import android.app.Notification.CATEGORY_MESSAGE
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.NotificationManager
import android.content.Context
import android.widget.RemoteViews
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Channel.Companion.TRANSACTION
import pt.hdn.toolbox.annotations.Duration.Companion.x10
import pt.hdn.toolbox.annotations.Notification.Companion.WARNING

class RingNotification(
    context: Context,
    remoteViews: RemoteViews,
    private val notificationManager: NotificationManager
): BaseNotification(context) {

    init {
        this.builder = Builder(context, TRANSACTION)
            .setVisibility(VISIBILITY_PUBLIC)
            .setCategory(CATEGORY_MESSAGE)
            .setAutoCancel(true)
            .setTimeoutAfter(x10)
            .setCustomContentView(remoteViews)
            .setSmallIcon(R.drawable.ic_ring_bell)
    }

    fun ring() { notificationManager.notify(WARNING, builder.build()) }
}