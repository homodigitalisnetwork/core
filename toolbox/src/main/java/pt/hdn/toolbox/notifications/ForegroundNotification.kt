package pt.hdn.toolbox.notifications

import android.app.Notification.*
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.app.Service
import android.content.Context
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.RemoteViews
import androidx.annotation.StringRes
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Channel.Companion.GPS
import pt.hdn.toolbox.annotations.Notification.Companion.FOREGROUND
import pt.hdn.toolbox.resources.Resources

class ForegroundNotification(
    private val service: Service,
    private val res: Resources,
    private val remoteViews: RemoteViews,
    context: Context
): BaseNotification(context) {

    init {
        this.builder = context
            .packageManager
            .getLaunchIntentForPackage(context.packageName)!!
            .setPackage(null)
            .setFlags(FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
            .let { PendingIntent.getActivity(context, 0, it, FLAG_UPDATE_CURRENT) }
            .let { Builder(context, GPS).setVisibility(VISIBILITY_PUBLIC).setCategory(CATEGORY_SERVICE).setSmallIcon(res.logoId).setCustomContentView(remoteViews).setContentIntent(it) }
    }

    fun notify(searchable: Boolean, location: Boolean) {
        with(remoteViews) {
//            setInt(R.id.layout_notifyRoot, SET_BACKGROUND_COLOR, colorPrimary)

            setImageViewResource(R.id.img_notifyLogo, res.logoId)
//            setInt(R.id.img_notifyLogo, SET_COLOR_FILTER, colorOnPrimary)

            if (searchable) { setTextViewText(R.id.lbl_notifySubtitle, string(if (location) R.string.broadcastGPS else R.string.broadcastHome))/*; setTextColor(R.id.lbl_notifySubtitle, colorOnPrimary)*/; R.string.online to VISIBLE }
            else { R.string.offline to GONE }
                .run { setTextViewText(R.id.lbl_notifyTitle, string(first)); setViewVisibility(R.id.lbl_notifySubtitle, second) }

//            setTextColor(R.id.lbl_notifyTitle, colorOnPrimary)
        }

        service.startForeground(FOREGROUND, builder.build())
    }

    private fun string(@StringRes id: Int) = res.string(id)
}