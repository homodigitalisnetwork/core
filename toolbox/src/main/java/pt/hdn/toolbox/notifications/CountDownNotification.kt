package pt.hdn.toolbox.notifications

import android.app.Notification.Builder
import android.app.Notification.CATEGORY_MESSAGE
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.NotificationManager
import android.app.PendingIntent.*
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import androidx.annotation.StringRes
import com.google.maps.android.SphericalUtil.computeDistanceBetween
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.annotations.Action.Companion.ACCEPT
import pt.hdn.toolbox.annotations.Action.Companion.REJECT
import pt.hdn.toolbox.annotations.Channel.Companion.REQUEST
import pt.hdn.toolbox.annotations.Duration.Companion.x1
import pt.hdn.toolbox.annotations.Duration.Companion.x20
import pt.hdn.toolbox.annotations.Duration.Companion.x60
import pt.hdn.toolbox.annotations.Field.Companion.IS_BUYER_SIDE
import pt.hdn.toolbox.annotations.Notification.Companion.TRANSACTION
import pt.hdn.toolbox.annotations.Notification.Companion.WARNING
import pt.hdn.toolbox.annotations.Request.Companion.ACCEPT_TRANSACTION
import pt.hdn.toolbox.annotations.Request.Companion.REJECT_TRANSACTION
import pt.hdn.toolbox.countdown.CountDownSingletonTimer
import pt.hdn.toolbox.foreground.ForegroundService
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.transaction.util.DefaultTransaction
import java.util.*

class CountDownNotification(
    private val context: Context,
    private val res: Resources,
    private val countDownSingletonTimer: CountDownSingletonTimer,
    private val genericRemoteViews: RemoteViews,
    private val smallRemoteViews: RemoteViews,
    private val bigRemoteViews: RemoteViews,
    private val notificationManager: NotificationManager
) {

    //region vars
    private val builder: Builder = Builder(context, REQUEST).setCategory(CATEGORY_MESSAGE).setVisibility(VISIBILITY_PUBLIC).setLocalOnly(true)
    //endregion vars

    fun start(millisInFuture: Long, countDowInternal: Long, transaction: DefaultTransaction, position: Position) {
        with(transaction) {
            Intent(ACCEPT.toString(), null, context, ForegroundService::class.java)
                .putExtra(Field.TRANSACTION, this)
                .let { getService(context, ACCEPT_TRANSACTION, it, FLAG_UPDATE_CURRENT or FLAG_IMMUTABLE) }
                .also {
                    smallRemoteViews
                        .apply {
                            setTextViewText(R.id.lbl_notifyTransactionSmallCountDown, "%2d".format(x20 / x1))
                            setOnClickPendingIntent(R.id.layout_notifyTransactionSmallRoot, it)
                        }

                    bigRemoteViews
                        .apply {
                            setTextViewText(R.id.lbl_notifyTransactionBigCountDownValue, "%2d".format(x20 / x1))
                            setTextViewText(R.id.lbl_notifyTransactionBigRatingValue, "%.2f".format(buyerRating))
                            setTextViewText(R.id.lbl_notifyTransactionBigNameValue, specialitiesNames)
                            setOnClickPendingIntent(R.id.layout_notifyTransactionBigRoot, it)
                        }

                    Intent(REJECT.toString(), null, context, ForegroundService::class.java)
                        .putExtra(Field.TRANSACTION, this)
                        .putExtra(IS_BUYER_SIDE, false)
                        .let { getService(context, REJECT_TRANSACTION, it, FLAG_UPDATE_CURRENT or FLAG_IMMUTABLE) }
                        .also {
                            builder
                                .apply {
                                    setSmallIcon(R.drawable.ic_work)
                                    setDeleteIntent(it)
                                    setAutoCancel(true)
                                    setOnlyAlertOnce(true)
                                    setCustomContentView(smallRemoteViews)
                                    setCustomBigContentView(bigRemoteViews)
                                    setCustomHeadsUpContentView(bigRemoteViews)

                                    countDownObserver {
                                        tick {
                                            "%2d".format(it / x1)
                                                .let {
                                                    smallRemoteViews.setTextViewText(R.id.lbl_notifyTransactionSmallCountDown, it)

                                                    bigRemoteViews.setTextViewText(R.id.lbl_notifyTransactionBigCountDownValue, it)

                                                    notificationManager.notify(uuid.toString(), TRANSACTION, build())
                                                }
                                        }

                                        finish { notificationManager.cancel(uuid.toString(), TRANSACTION); it.send() }

                                        cancel {
                                            notificationManager.cancel(uuid.toString(), TRANSACTION)

                                            genericRemoteViews
                                                .apply {
                                                    setImageViewResource(R.id.img_notifyLogo, R.drawable.ic_cancel)
                                                    setTextViewText(R.id.lbl_notifyTitle, string(R.string.cancelled))
                                                    setTextViewText(R.id.lbl_notifySubtitle, string(R.string.requestCancelled))

                                                    setSmallIcon(R.drawable.ic_cancel)
                                                    setDeleteIntent(null)
                                                    setAutoCancel(true)
                                                    setOnlyAlertOnce(true)
                                                    setTimeoutAfter(x60)
                                                    setCustomContentView(this)
                                                    setCustomBigContentView(null)
                                                    setCustomHeadsUpContentView(null)

                                                    notificationManager.notify(WARNING, build())
                                                }
                                        }

                                        abort { notificationManager.cancel(uuid.toString(), TRANSACTION) }
                                    }.let { me ->
                                        bigRemoteViews.setTextViewText(R.id.lbl_notifyTransactionBigLocationValue, distance(buyerPosition, position))

                                        notificationManager.notify(uuid.toString(), TRANSACTION, build())

                                        countDownSingletonTimer.start(millisInFuture, countDowInternal, me)
                                    }
                                }
                        }
                }
        }
    }

    fun cancel(uuid: UUID) { notificationManager.findWith { id == TRANSACTION && tag == uuid.toString() }?.let { countDownSingletonTimer.cancel() } }

    fun finish() { countDownSingletonTimer.finish() }

    fun abort() { countDownSingletonTimer.abort() }

    private fun string(@StringRes id: Int) = res.string(id)

    private fun distance(from: Position, to: Position): String {
        return computeDistanceBetween(from.toLatLng(), to.toLatLng())
            .let {
                when {
                    100000.0 <= it  -> "%.0f km".format(it / 1000.0)
                    10000.0 <= it  && it < 100000.0 -> "%.1f km".format(it / 1000.0)
                    1000.0 <= it  && it < 10000.0 -> "%.2f km".format(it / 1000.0)
                    else -> "%.0f m".format(it)
                }
            }
    }
}