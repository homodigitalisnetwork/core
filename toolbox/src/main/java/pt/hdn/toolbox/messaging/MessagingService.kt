package pt.hdn.toolbox.messaging

import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.misc.viewModels
import javax.inject.Inject

/**
 * The [Service] responsible to handle all aspects related with Cloud Messaging
 */
@AndroidEntryPoint
class MessagingService : BaseMessagingService() {

    //region vars
    @Inject lateinit var messagingViewModelFactory: MessagingViewModelFactory
    private val messagingViewModel: MessagingViewModel by viewModels { messagingViewModelFactory }
    //endregion vars

    override fun onCreate() { super.onCreate(); messagingViewModel }

    override fun onNewToken(token: String) { messagingViewModel.onNewToken(token) }

    override fun onMessageReceived(remoteMessage: RemoteMessage) { remoteMessage.data.takeUnless { it.isEmpty() }?.let { messagingViewModel.onMessageReceived(it) } }
}