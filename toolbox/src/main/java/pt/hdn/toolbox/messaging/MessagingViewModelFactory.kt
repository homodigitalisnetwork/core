package pt.hdn.toolbox.messaging

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.Factory
import androidx.lifecycle.viewmodel.CreationExtras
import com.google.gson.Gson
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.notifications.ProposalNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources

class MessagingViewModelFactory(
    private val gson: Gson,
    private val sessionBus: SessionBus,
    private val dataBus: DataBus,
    private val repo: CoreRepo,
    private val resources: Resources,
    private val proposalNotification: ProposalNotification
): Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
       return if (modelClass.isAssignableFrom(MessagingViewModel::class.java)) {
           MessagingViewModel(
               gson = gson,
               sessionBus = sessionBus,
               dataBus = dataBus,
			   repo = repo,
			   res = resources,
               proposalNotification = proposalNotification
           ) as T
       } else {
           throw IllegalArgumentException("Unknown ViewModel class")
       }
    }
}