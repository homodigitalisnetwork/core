package pt.hdn.toolbox.messaging

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import pt.hdn.toolbox.annotations.*
import pt.hdn.toolbox.annotations.Action.Companion.ACCEPT
import pt.hdn.toolbox.annotations.Action.Companion.CANCEL
import pt.hdn.toolbox.annotations.Action.Companion.ENQUIRE
import pt.hdn.toolbox.annotations.Action.Companion.LOGOUT
import pt.hdn.toolbox.annotations.Action.Companion.REJECT
import pt.hdn.toolbox.annotations.Action.Companion.UNAVAILABLE
import pt.hdn.toolbox.annotations.Field.Companion.ACTION
import pt.hdn.toolbox.annotations.Field.Companion.TYPE
import pt.hdn.toolbox.application.CoreGson
import pt.hdn.toolbox.business.proposal.DefaultProposal
import pt.hdn.toolbox.communications.bus.*
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.notifications.ProposalNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.session.Session
import pt.hdn.toolbox.transaction.util.DefaultTransaction
import pt.hdn.toolbox.viewmodels.BasicViewModel
import java.util.*
import javax.inject.Inject

@SuppressLint("StaticFieldLeak")
@HiltViewModel
class MessagingViewModel @Inject constructor(
    @CoreGson private val gson: Gson,
    private val dataBus: DataBus,
    private val proposalNotification: ProposalNotification,
    sessionBus: SessionBus,
    repo: CoreRepo,
    res: Resources
): BasicViewModel<CoreRepo>(repo, res) {

    //region vars
    private var session: Session? = null
    private var deviceToken: String? = null
    //endregion vars

    init { sessionBus.observe(viewModelScope) { this.session = it?.apply { deviceToken?.let { sendToken(it) } } } }

    fun onMessageReceived(data: Map<String, String>) {
        val uuid: UUID = UUID.fromString(data[Field.UUID]!!)
        val action: Int = data[ACTION]!!.toInt()
        val isService: Boolean = data[TYPE].toBoolean()

        when {
            action == LOGOUT -> if (res.appUUID == uuid) dataBus.send(viewModelScope, Address.LOGOUT, Unit)
            isService -> {
                when (action) {
                    ENQUIRE /*seller side*/ -> dataBus.send(viewModelScope, Address.ENQUIRE, DefaultTransaction.from(gson, data, res))
                    UNAVAILABLE, REJECT/*, ACCEPT*/ /*buyer side*/, CANCEL /*seller side*/ -> dataBus.send(viewModelScope, Address.TRANS_COMM, action to uuid)
                }
            }
            else -> with(proposalNotification){
                with(DefaultProposal.from(gson, data, res)) {
                    when (action) {
                        REJECT -> reject()
                        CANCEL -> cancel()
                        ENQUIRE -> propose(this, session!!.position)
                        ACCEPT -> accept()
                    }

                    dataBus.send(viewModelScope, Address.PROPOSAL, uuid to action)
                }
            }
        }
    }

    fun onNewToken(token: String) { session?.apply { sendToken(token) } ?: run { this.deviceToken = token } }

    private fun Session.sendToken(token: String) {
        launch { repo.token(deputyUUID, res.appUUID, deviceToken ?: token).collect { this@MessagingViewModel.deviceToken = null;  it.error { _, _ -> } } }
    }
}