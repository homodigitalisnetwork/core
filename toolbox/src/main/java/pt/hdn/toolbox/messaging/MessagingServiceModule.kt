package pt.hdn.toolbox.messaging

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ServiceComponent
import dagger.hilt.android.scopes.ServiceScoped
import pt.hdn.toolbox.application.CoreGson
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.foreground.ForegroundService
import pt.hdn.toolbox.notifications.ProposalNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources

@Module
@InstallIn(ServiceComponent::class)
object MessagingServiceModule {

    /**
     * Provides a custom [Factory] for the [MessagingService] [ViewModel]
     * as a work around for not being able to create the [ViewModel] directly from [Hilt]
     */
    @ServiceScoped
    @Provides
    fun providesMessagingViewModelFactory(
        @CoreGson gson: Gson,
        dataBus: DataBus,
        sessionBus: SessionBus,
        repo: CoreRepo,
        resources: Resources,
        proposalNotification: ProposalNotification
    ): MessagingViewModelFactory {
        return MessagingViewModelFactory(
            gson = gson,
            dataBus = dataBus,
            sessionBus = sessionBus,
			repo = repo,
			resources = resources,
            proposalNotification = proposalNotification
        )
    }
}