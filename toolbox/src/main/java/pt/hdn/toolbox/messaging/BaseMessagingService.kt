package pt.hdn.toolbox.messaging

import android.content.Intent
import androidx.annotation.CallSuper
import androidx.annotation.MainThread
import androidx.annotation.StringRes
import androidx.lifecycle.*
import androidx.savedstate.SavedStateRegistry
import androidx.savedstate.SavedStateRegistryController
import androidx.savedstate.SavedStateRegistryOwner
import com.google.firebase.messaging.FirebaseMessagingService
import pt.hdn.toolbox.resources.Resources
import javax.inject.Inject

/**
 * An abstract class for [MessagingService] that inherits from [FirebaseMessagingService]
 * and implements [LifecycleOwner] to make lifecycle code dependent possible
 */
abstract class BaseMessagingService: FirebaseMessagingService(), LifecycleOwner, ViewModelStoreOwner, HasDefaultViewModelProviderFactory, SavedStateRegistryOwner {

    //region vars
    @Inject lateinit var res: Resources
    override val savedStateRegistry: SavedStateRegistry get() = savedStateRegistryController.savedStateRegistry
    private val dispatcher: ServiceLifecycleDispatcher = ServiceLifecycleDispatcher(this)
    private val viewModelStore: ViewModelStore = ViewModelStore()
    private val savedStateRegistryController = SavedStateRegistryController.create(this)
    private val factory: ViewModelProvider.Factory = SavedStateViewModelFactory(application, this, null)
    //endregion vars

    @CallSuper
    override fun onCreate() { dispatcher.onServicePreSuperOnCreate(); super.onCreate() }

    @CallSuper
    override fun onStart(intent: Intent?, startId: Int) { dispatcher.onServicePreSuperOnStart(); super.onStart(intent, startId) }

    @CallSuper
    override fun onDestroy() { dispatcher.onServicePreSuperOnDestroy(); super.onDestroy() }

    override fun getLifecycle(): Lifecycle = dispatcher.lifecycle

    override fun getViewModelStore(): ViewModelStore = viewModelStore

    override fun getDefaultViewModelProviderFactory(): ViewModelProvider.Factory = factory

    fun string(@StringRes id: Int) = res.string(id)

    fun string(format: String, vararg args: Any): String = res.string(format, args)
}