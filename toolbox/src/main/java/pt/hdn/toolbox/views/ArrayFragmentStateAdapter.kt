package pt.hdn.toolbox.views

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.*
import androidx.lifecycle.Lifecycle.Event.ON_DESTROY
import androidx.viewbinding.ViewBinding
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import androidx.viewpager2.widget.ViewPager2.SCROLL_STATE_IDLE
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.misc.OnFocusRequestListener
import pt.hdn.toolbox.misc.OnPageChanged

class ArrayFragmentStateAdapter(
    private val fragmentManager: FragmentManager,
    private val lifecycle: Lifecycle,
    viewPager2: ViewPager2,
    supplier: (OnPageChanged.() -> Unit)?
) : FragmentStateAdapter(fragmentManager, lifecycle), Iterable<BindingFragment<ViewBinding>>, OnFocusRequestListener {

    //region vars
    private var newId: Long = 0L
    private var viewPager2: ViewPager2? = viewPager2
    private val invokers: MutableList<Pair<Long, Invoker>> = mutableListOf()
    private val onPageChangeCallback: OnPageChangeCallback? = supplier?.let { OnPageChanged(viewPager2).apply(it).build() }
    val lastIndex: Int; get() = itemCount - 1
    //endregion vars

    override fun createFragment(position: Int): Fragment {
        return invokers[position].second.invoke(bundleOf(Field.POSITION to position)).apply { onFocusRequestListener = this@ArrayFragmentStateAdapter }
    }

    override fun getItemId(position: Int): Long = invokers[position].first

    override fun containsItem(itemId: Long): Boolean = invokers.any { it.first == itemId }

    override fun getItemCount(): Int = invokers.size

    override fun onFocusRequest(index: Int) { viewPager2?.setCurrentItem(index, true) }

    override fun iterator(): Iterator<BindingFragment<ViewBinding>> = ArrayFragmentStateAdapterIterator()

    fun initiate() {
        lifecycle
            .addObserver(
                LifecycleEventObserver { _, event ->
                    if (event == ON_DESTROY) { this.viewPager2 = viewPager2?.run { onPageChangeCallback?.let { unregisterOnPageChangeCallback(it) }; adapter = null; null } }
                }
            )

        viewPager2?.apply { adapter = this@ArrayFragmentStateAdapter; offscreenPageLimit = 1; onPageChangeCallback?.let { registerOnPageChangeCallback(it) } }
    }

    fun remove(position: Int) { removeBetween(position, position + 1) }

    fun removeFrom(position: Int) { removeBetween(position, invokers.size) }

    fun removeBetween(from: Int, to: Int) {
        var idx = 0

        with(invokers.iterator()) { while (hasNext() && idx < to) { next(); if (from <= idx++) remove() } }

        if (to - from > 0) { notifyItemRangeRemoved(from, to - from) }
    }

    operator fun get(position: Int): BindingFragment<ViewBinding> = fragmentManager.findFragmentByTag("f${invokers[position].first}") as BindingFragment<ViewBinding>

    fun add(supplier: () -> BindingFragment<ViewBinding>) { add(Invoker(supplier)) }

    fun add(function: (arguments: Bundle) -> BindingFragment<ViewBinding>, arguments: Bundle) { add(Invoker(function, arguments)) }

    fun add(invoker: Invoker) { add(invokers.size, invoker) }

    fun add(position: Int, supplier: () -> BindingFragment<ViewBinding>) { add(position, Invoker(supplier)) }

    fun add(position: Int, function: (arguments: Bundle) -> BindingFragment<ViewBinding>, arguments: Bundle) { add(position, Invoker(function, arguments)) }

    fun add(position: Int, invoker: Invoker) { invokers.add(position, newId++ to invoker); notifyItemInserted(position) }

    fun addAll(vararg suppliers: () -> BindingFragment<ViewBinding>) { addAllPairs(suppliers.map { newId++ to Invoker(it) }) }

    fun addAll(vararg invokers: Invoker) { addAllPairs(invokers.map { newId++ to it }) }

    fun addAll(invokers: List<Invoker>) { addAllPairs(invokers.map { newId++ to it }) }

    fun addAll(position: Int, vararg suppliers: () -> BindingFragment<ViewBinding>) { addAllPairs(position, suppliers.map { newId++ to Invoker(it) }) }

    fun addAll(position: Int, vararg invokers: Invoker) { addAllPairs(position, invokers.map { newId++ to it }) }

    fun addAll(position: Int, invokers: List<Invoker>) { addAllPairs(position, invokers.map { newId++ to it }) }

    private fun addAllPairs(list: List<Pair<Long, Invoker>>) { addAllPairs(invokers.size, list) }

    private fun addAllPairs(position: Int, list: List<Pair<Long, Invoker>>) { invokers.addAll(position, list); notifyItemRangeInserted(position, invokers.size) }

    class Invoker private constructor() {

        //region vars
        private var function: ((arguments: Bundle) -> BindingFragment<ViewBinding>)? = null
        private var supplier: (() -> BindingFragment<ViewBinding>)? = null
        private var arguments: Bundle? = null
        //endregion

        constructor(supplier: () -> BindingFragment<ViewBinding>) : this() { this.supplier = supplier }

        constructor(function: (arguments: Bundle) -> BindingFragment<ViewBinding>, arguments: Bundle) : this() { this.function = function; this.arguments = arguments }

        fun invoke(args: Bundle? = null): BindingFragment<ViewBinding> {
            return if (arguments == null) supplier!!.invoke().apply { args?.let { arguments?.apply { putAll(it) } ?: run { arguments = it } } }
            else function!!.invoke(arguments!!.apply { args?.let { putAll(it) } })
        }
    }

    private inner class ArrayFragmentStateAdapterIterator: Iterator<BindingFragment<ViewBinding>> {
        private var index: Int = 0

        override fun hasNext(): Boolean = index < invokers.size

        override fun next(): BindingFragment<ViewBinding> = fragmentManager.findFragmentByTag("f${invokers[index++].first}") as BindingFragment<ViewBinding>
    }
}