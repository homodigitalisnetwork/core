package pt.hdn.toolbox.views

import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy.NONE
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.RequestOptions.circleCropTransform
import pt.hdn.toolbox.R
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.databinding.ViewIntroBinding
import pt.hdn.toolbox.resources.Resources

class IntroView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    //region vars
    lateinit var resources: Resources
    var data: Seller? = null; set(value) { field = value; compile(value) }
    private val binding: ViewIntroBinding = ViewIntroBinding.inflate(LayoutInflater.from(context), this)
    //endregion vars

    fun setPhoto() { binding.setImage(data?.photo?.uri) }

    private fun compile(value: Seller?){
        with(binding) {
            value
                ?.apply {
                    setImage(photo?.uri)

                    lblIntroName.text = partyName
                    lblIntroLanguages.text = buildString {
                        resources.languages.associateBy { it.id }.let { map -> languages!!.forEach { map[it]?.run { append(", ").append(name) } } }

                        if (length > 1) delete(0, 2)
                    }
                }
                ?: run { imgIntroSpot.setImageDrawable(context.getDrawable(R.drawable.ic_spot_logo)); lblIntroName.text = null; lblIntroLanguages.text = null }
        }
    }

    private fun ViewIntroBinding.setImage(uri: Uri?) {
        uri
            ?.let {
                Glide
                    .with(context)
                    .load(it)
                    .apply(RequestOptions().diskCacheStrategy(NONE).skipMemoryCache(true).apply(circleCropTransform()))
                    .error(context.getDrawable(R.drawable.ic_spot_logo))
                    .into(binding.imgIntroSpot)
            }
            ?: run { imgIntroSpot.setImageDrawable(context.getDrawable(R.drawable.ic_spot_logo)) }
    }
}