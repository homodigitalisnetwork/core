package pt.hdn.toolbox.views

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.MotionEvent.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class EnhanceMapView(
    context: Context,
    attributeSet: AttributeSet?
) : MapView(context, attributeSet) {

    //region vars
    private val mutableIntercepting: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val isIntercepting: StateFlow<Boolean> = mutableIntercepting
    private var uiSettings: UiSettings? = null
    private val isGesturesEnable: Boolean; get() = uiSettings?.isScrollGesturesEnabled == true
    //endregion vars

    override fun onDestroy() { this.uiSettings = null; super.onDestroy() }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (isGesturesEnable) {
            ev?.actionMasked
                ?.also {
                    when (it) {
                        ACTION_POINTER_DOWN -> mutableIntercepting.tryEmit(true)
                        ACTION_POINTER_UP -> mutableIntercepting.tryEmit(false)
                    }
                }
        }

        return super.dispatchTouchEvent(ev)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?) = isGesturesEnable && super.onInterceptTouchEvent(ev)

    override fun onTouchEvent(event: MotionEvent?) = isGesturesEnable && super.onTouchEvent(event)

    fun getMapAsyncWithContext(onMapCallback: OnMapCallback) {
        super.getMapAsync { this.uiSettings =  it.uiSettings.apply { isZoomGesturesEnabled = true; isScrollGesturesEnabled = true }; with(onMapCallback) { it.onMapReady() } }
    }
}