package pt.hdn.toolbox.views

import android.content.Context
import android.graphics.DashPathEffect
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.graphics.ColorUtils.*
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition.RIGHT_TOP
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition.RIGHT_BOTTOM
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis.XAxisPosition.BOTTOM
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.google.android.material.textview.MaterialTextView
import pt.hdn.toolbox.R
import pt.hdn.toolbox.databinding.ViewExchangesBinding
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.getColorAttr
import pt.hdn.toolbox.report.adapter.exchange.ExchangeDataSet
import pt.hdn.toolbox.resources.Resources

class ExchangesView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): ConstraintLayout(context, attrs, defStyleAttr) {

    //region vars
    var res: Resources? = null
    var dataSet: ExchangeDataSet? = null; set(value) { field = value?.also { it.compile() } }
    private val binding: ViewExchangesBinding = ViewExchangesBinding.inflate(LayoutInflater.from(context), this)
//    private val dashPathEffect: DashPathEffect = DashPathEffect(floatArrayOf(20f, 10f), 0f)
//    private val colorTransparent: Int = context.getColor(android.R.color.transparent)
//    private val colorError: Int = context.getColorAttr(R.attr.colorError)
//    private val colorOnPrimary: Int = context.getColorAttr(R.attr.colorOnPrimary)
    private val colorPrimaryContainer: Int = context.getColorAttr(R.attr.colorPrimaryContainer)
//    private val colorOnPrimaryContainer: Int = context.getColorAttr(R.attr.colorOnPrimaryContainer)
//    private val colorTertiary: Int = context.getColorAttr(R.attr.colorTertiary)
    private val colorTertiaryContainer: Int = context.getColorAttr(R.attr.colorTertiaryContainer)
//    private val colorPrimary: Int = context.getColorAttr(R.attr.colorPrimary)
//    private val colorSecondary: Int = context.getColorAttr(R.attr.colorSecondary)
//    private val colorSecondaryContainer: Int = context.getColorAttr(R.attr.colorSecondaryContainer)
//    private val colorBackground: Int = context.getColorAttr(android.R.attr.colorBackground)
    private val colorOnBackground: Int = context.getColorAttr(R.attr.colorOnBackground)
//    private val OFFSET: Float = 30f
    //endregion vars

    init {
        with(binding) {
            chartExchangesRevenue
                .apply {
                    setBarChart()

                    this.extraBottomOffset = 5f

                    legend.isEnabled = false

                    xAxis.apply { this.yOffset = 15f; this.textSize = 14f; this.textColor = colorOnBackground; this.position = BOTTOM }
                }

            chartExchangesCost
                .apply {
                    setBarChart()

                    legend.apply { this.textSize = 14f; this.textColor = colorOnBackground }

                    xAxis.setDrawLabels(false)

                    axisLeft.isInverted = true
                }
        }
    }

    private fun ExchangeDataSet.compile() {
        with(binding) {
            val stackColor: List<Int> = colorShades(colorPrimaryContainer, colorTertiaryContainer, order.size)

            lblExchangesDisplay.apply { if (order.isEmpty()) text = res?.string(R.string.nothingToReport) else visibility = GONE }

            if (order.isEmpty()) { chartExchangesCost.visibility = INVISIBLE; chartExchangesRevenue.visibility = INVISIBLE }
            else {
                chartExchangesRevenue
                    .apply {
                        xAxis.valueFormatter = res?.weekDayLabels?.let { WeekValueFormatter(it) }

                        axisLeft.axisMaximum = revenueDataSet.yMax.coerceAtLeast(costDataSet.yMax) + 5f

                        data = revenueDataSet.run{ setBarDataSet(stackColor, axisLeft, RIGHT_TOP); BarData(this).apply { this.barWidth = 0.9f } }
                    }

                chartExchangesCost
                    .apply {
                        axisLeft.axisMaximum = revenueDataSet.yMax.coerceAtLeast(costDataSet.yMax) + 10f

                        data = costDataSet.run { setBarDataSet(stackColor, axisLeft, RIGHT_BOTTOM); BarData(this).apply { this.barWidth = 0.9f } }
                    }
            }
        }
    }

    private fun BarChart.setBarChart() {
        setNoDataText(null)
        setDrawValueAboveBar(false)
        setTouchEnabled(true)
        setPinchZoom(false)
        setScaleEnabled(false)
        isDoubleTapToZoomEnabled = false
        description.isEnabled = false
        xAxis.apply { this.setDrawGridLines(false); this.spaceMax = 2f }
        axisLeft.apply { this.setDrawLabels(false); this.setDrawAxisLine(false); this.setDrawGridLines(false); this.axisMinimum = 0f; this.setDrawLimitLinesBehindData(true) }
        axisRight.isEnabled = false
    }

    private fun BarDataSet.setBarDataSet(colors: List<Int>, yAxis: YAxis, position: LimitLabelPosition) {
        this.valueTextSize = 12f
        this.valueTextColor = colorOnBackground
        this.valueFormatter = StackValueFormatter()
        this.colors = colors

        values
            .forEachWith {
                positiveSum
                    .takeIf { it > 0f }
                    ?.let {
                        yAxis
                            .addLimitLine(
                                LimitLine(it, "%.2f".format(it)).apply { this.lineColor = colorOnBackground; this.textColor = colorOnBackground; this.labelPosition = position; this.textSize = 12f }
                            )
                    }
            }
    }

    private fun colorShades(startColor: Int, endColor: Int, shades: Int): List<Int> {
        return buildList(shades) {
            if (shades == 1) add(blendARGB(startColor, endColor, 0.5f))
            else for (step in 0 until shades) add(blendARGB(startColor, endColor, step.toFloat() / (shades - 1)))
        }
    }

    inner class AmountMarker(
        context: Context
    ): MarkerView(context, R.layout.view_exchange_marker) {

        //region vars
        private val lblExchangeTitle: MaterialTextView = findViewById(R.id.lbl_exchangeTitle)
        //endregion vars

        override fun refreshContent(e: Entry?, highlight: Highlight?) {
            super.refreshContent(e, highlight)

            chartView.highlightValues(null)

            lblExchangeTitle.text = "Hello"

//        label.text = highlight?.stackIndex?.let { (e as? BarEntry)?.yVals?.getOrNull(it)?.toString() }
        }
    }

    inner class StackValueFormatter: ValueFormatter() {
        override fun getFormattedValue(value: Float): String  = if (value == 0f) ""  else "%.2f".format(value)
    }

    inner class WeekValueFormatter(
        private val weekDayLabels: List<String>
    ): ValueFormatter() {
        override fun getAxisLabel(value: Float, axis: AxisBase?): String = weekDayLabels.getOrElse(value.toInt()) { "" }
    }
}