package pt.hdn.toolbox.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TableLayout
import android.widget.TableRow
import androidx.annotation.StringRes
import com.google.android.material.textview.MaterialTextView
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.databinding.ViewSchemasBinding
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal

class SchemasView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : TableLayout(context, attrs) {

    //region vars
    lateinit var resources: Resources
    var data: List<Schema>? = null; set(value) { field = value; compile() }
    private val binding: ViewSchemasBinding = ViewSchemasBinding.inflate(LayoutInflater.from(context), this)
    private val tinyMargin: Int = context.resources.getDimension(R.dimen.t_size).toInt()
    //endregion vars

    private fun compile() {
        binding.tbSchemasContainer.removeAllViews()

        data
            ?.forEachWith {
                addDescription(resources.schemas[id]!!)

                when (this) {
                    is FixSchema -> addParameter(R.string.value, fix)
                    is RateSchema -> { addParameter(R.string.over, resources.sources[source!!]); addParameter(R.string.value, rate) }
                    is CommissionSchema -> {
                        addParameter(R.string.over, resources.sources[source!!])
                        addParameter(R.string.cut, cut)
                        lowerBound?.let { addParameter(R.string.lowerBound, it) }
                        upperBound?.let { addParameter(R.string.upperBound, it) }
                    }
                    is ObjectiveSchema -> {
                        addParameter(R.string.over, resources.sources[source!!])
                        addParameter(R.string.bonus, bonus)
                        lowerBound?.let { addParameter(R.string.lowerBound, it) }
                        upperBound?.let { addParameter(R.string.upperBound, it) }
                    }
                    is ThresholdSchema -> {
                        addParameter(R.string.over, resources.sources[source!!])
                        addParameter(R.string.bonus, bonus)
                        addParameter(if (isAbove!!) R.string.aboveLimit else R.string.belowLimit, threshold)
                    }
                }
            }
    }

    private fun addDescription(value: String) {
        TableRow(context)
            .apply {
                layoutParams = LayoutParams().apply { setMargins(0, tinyMargin, 0, 0) }

                with(MaterialTextView(context)) {
                    layoutParams = TableRow.LayoutParams()
                    text = value

                    addView(this)
                }

                binding.tbSchemasContainer.addView(this)
            }
    }

    private fun addParameter (@StringRes field: Int, value: BigDecimal?) { addParameter(field, value?.toString()) }

    private fun addParameter (@StringRes field: Int, value: String?) {
        TableRow(context)
            .apply {
                layoutParams = LayoutParams().apply { setMargins(tinyMargin, tinyMargin, tinyMargin, 0) }

                with(MaterialTextView(context)) {
                    layoutParams = TableRow.LayoutParams().apply { setMargins(0, 0, tinyMargin, 0) }
                    textAlignment = TEXT_ALIGNMENT_TEXT_END
                    text = string(field)

                    addView(this)
                }

                with(MaterialTextView(context)) {
                    layoutParams = TableRow.LayoutParams(0, WRAP_CONTENT, 1f).apply { setMargins(tinyMargin, 0, 0, 0) }
                    textAlignment = TEXT_ALIGNMENT_TEXT_START
                    text = value

                    addView(this)
                }

                binding.tbSchemasContainer.addView(this)
            }
    }

    private fun string(@StringRes idRes: Int): String = resources.string(idRes)
}