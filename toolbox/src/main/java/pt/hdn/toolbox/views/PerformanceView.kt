package pt.hdn.toolbox.views

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import pt.hdn.toolbox.R
import pt.hdn.toolbox.databinding.ViewPerformanceBinding
import pt.hdn.toolbox.misc.getColorAttr
import pt.hdn.toolbox.partnerships.dealership.resume.util.Performance
import pt.hdn.toolbox.performance.AxisLabelValueFormatter
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.statistics.Statistics.tScore

class PerformanceView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): ConstraintLayout(context, attrs, defStyleAttr) {

    //region vars
    var res: Resources? = null; set(value) { field = value; this.labelValueFormatter = value?.let { AxisLabelValueFormatter(it.labels) } }
    var data: Performance? = null; set(value) { field = value; this.radarDataSet = value?.compileData(); setView() }
    private var radarDataSet: RadarDataSet? = null
    private var labelValueFormatter: AxisLabelValueFormatter? = null
    private val binding: ViewPerformanceBinding = ViewPerformanceBinding.inflate(LayoutInflater.from(context), this)
    private val TEXT_SIZE = 14f
    private val colorPrimaryContainer: Int = context.getColorAttr(R.attr.colorPrimaryContainer)
    private val colorOnPrimaryContainer: Int = context.getColorAttr(R.attr.colorOnPrimaryContainer)
    //endregion vars

    init {
        with(binding.chartPerformanceRadar) {
            xAxis.apply { textSize = TEXT_SIZE; webLineWidthInner = 1f; webLineWidth = 1f }

            yAxis.apply { axisMinimum = 0f; axisMaximum = 10f; setDrawLabels(false) }

            legend.isEnabled = false
            description.isEnabled = false
            setNoDataText(null)
            isRotationEnabled = false
            setTouchEnabled(false)
            isClickable = false
        }
    }

    private fun Performance.compileData(): RadarDataSet {
        return with(populationStats) {
            listOf(
                tScore(stats?.revenuePerTime, revenuePerTimeStats),
                tScore(stats?.revenuePerDistance, revenuePerDistanceStats),
                tScore(stats?.revenuePerFuel, revenuePerFuelStats),
                tScore(stats?.efficiency, efficiencyStats),
                tScore(stats?.consistency, consistencyStats)
            )
        }
            .map { RadarEntry(it) }
            .let { RadarDataSet(it, null) }
            .apply {
                isHighlightEnabled = false
                setDrawHighlightIndicators(false)
                lineWidth = 2f
                setDrawFilled(true)
                valueTextSize = TEXT_SIZE
                color = colorOnPrimaryContainer
                fillColor = colorPrimaryContainer
                valueTextColor = colorOnPrimaryContainer
            }
    }

    private fun setView() {
        with(binding) {
            lblPerformanceTitle.apply { visibility = if (data?.type != null) VISIBLE else GONE; text = data?.type?.name }

            chartPerformanceRadar
                .apply {
                    xAxis.apply { labelValueFormatter?.let { valueFormatter = it }; textColor = colorOnPrimaryContainer }

                    yAxis.apply { setLabelCount(6, true); /*res?.run { typeface = font }*/ }

                    data = radarDataSet?.let { RadarData(it) }

                    invalidate()
                }
        }
    }
}