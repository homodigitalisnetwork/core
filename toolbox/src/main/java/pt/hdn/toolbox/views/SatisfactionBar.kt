package pt.hdn.toolbox.views

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import pt.hdn.toolbox.R
import pt.hdn.toolbox.databinding.ViewSatisfactionBinding

class SatisfactionBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
): ConstraintLayout(context, attrs, defStyleAttr, defStyleRes), OnClickListener {

    //region vars
    var onSatisfactionBarChangeListener: OnSatisfactionBarChangeListener? = null
    private val binding: ViewSatisfactionBinding = ViewSatisfactionBinding.inflate(LayoutInflater.from(context), this)
    private var satisfaction: Int? = null
    private var colorAccent: Int = TypedValue().also { context.theme.resolveAttribute (R.attr.colorAccent, it, true) }.data
    //endregion vars

    interface OnSatisfactionBarChangeListener { fun onSatisfactionChange(satisfaction: Int?) }

    init {
        with(binding) {
            imgSatisfaction1.setOnClickListener(this@SatisfactionBar)
            imgSatisfaction2.setOnClickListener(this@SatisfactionBar)
            imgSatisfaction3.setOnClickListener(this@SatisfactionBar)
            imgSatisfaction4.setOnClickListener(this@SatisfactionBar)
            imgSatisfaction5.setOnClickListener(this@SatisfactionBar)
        }
    }

    override fun onClick(v: View?) {
        v?.id
            ?.also {
                with(binding) {
                    clearTint()

                    when (it) {
                        R.id.img_satisfaction1 -> 1 to imgSatisfaction1
                        R.id.img_satisfaction2 -> 2 to imgSatisfaction2
                        R.id.img_satisfaction3 -> 3 to imgSatisfaction3
                        R.id.img_satisfaction4 -> 4 to imgSatisfaction4
                        R.id.img_satisfaction5 -> 5 to imgSatisfaction5
                        else -> null
                    }?.run{ evaluateSatisfaction(first, second) }
                }

                onSatisfactionBarChangeListener?.onSatisfactionChange(satisfaction)
            }
    }

    private fun ViewSatisfactionBinding.clearTint() {
        imgSatisfaction1.clearColorFilter()
        imgSatisfaction2.clearColorFilter()
        imgSatisfaction3.clearColorFilter()
        imgSatisfaction4.clearColorFilter()
        imgSatisfaction5.clearColorFilter()
    }

    private fun evaluateSatisfaction(satisfaction: Int, imageView: ImageView) {
        this.satisfaction = if (this.satisfaction == satisfaction) null else { imageView.setColorFilter(colorAccent); satisfaction }
    }
}