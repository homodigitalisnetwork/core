package pt.hdn.toolbox.views

import android.content.Context
import android.graphics.Paint.Style.STROKE
import android.graphics.Paint.Style.FILL
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition.LEFT_TOP
import pt.hdn.toolbox.R
import com.github.mikephil.charting.components.XAxis.XAxisPosition.BOTTOM
import com.github.mikephil.charting.data.*
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.databinding.ViewTradeBinding
import pt.hdn.toolbox.misc.getColorAttr
import pt.hdn.toolbox.misc.setCompoundTopDrawable
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.dealership.util.SchemaTrade

class TradeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    //region vars
    var res: Resources? = null
    var data: SchemaTrade? = null; set(value) { field = value; setView(value) }
    var errorMessage: String? = null
    private val binding: ViewTradeBinding = ViewTradeBinding.inflate(LayoutInflater.from(context), this)
    private val colorTertiary: Int = context.getColorAttr(R.attr.colorTertiary)
    private val colorOnBackground: Int = context.getColorAttr(R.attr.colorOnBackground)
    private val colorOnSurfaceVariant: Int = context.getColorAttr(R.attr.colorOnSurfaceVariant)
    private val colorPrimary: Int = context.getColorAttr(R.attr.colorPrimary)
    private val colorSecondary: Int = context.getColorAttr(R.attr.colorSecondary)
    private val TEXT_SIZE = 14f
    //endregion vars

    init {
        with(binding) {
            chartTradeCandle
                .apply {
                    legend.isEnabled = false
                    description.isEnabled = false
                    setNoDataText(null)

                    xAxis.apply { setDrawGridLines(false); textSize = TEXT_SIZE; textColor = colorOnBackground; position = BOTTOM; labelRotationAngle = 45f }

                    axisRight.isEnabled = false

                    axisLeft
                        .apply {
                            textSize = TEXT_SIZE
                            textColor = colorOnBackground
                            granularity = 0.01f
                            isGranularityEnabled = true
                            axisMinimum = 0f
                            enableGridDashedLine(10f, 5f, 0f)
                        }
                }

            chartTradeBar
                .apply {
                    legend.isEnabled = false
                    description.isEnabled = false
                    setNoDataText(null)

                    xAxis.apply { setDrawGridLines(false); textSize = TEXT_SIZE; textColor = colorOnBackground; position = BOTTOM; labelRotationAngle = 45f }

                    axisRight.isEnabled = false

                    axisLeft
                        .apply {
                            textSize = TEXT_SIZE
                            textColor = colorOnBackground
                            granularity = 1f
                            isGranularityEnabled = true
                            axisMinimum = 0f
                            enableGridDashedLine(10f, 5f, 0f)
                        }
                }
        }
    }

    private fun setView(schemaTrade: SchemaTrade?) {
        with(binding) {
            schemaTrade
                ?.apply {
                    with(candleDataSet) set@{
                        isHighlightEnabled = false
                        increasingColor = colorPrimary
                        increasingPaintStyle = STROKE
                        neutralColor = colorOnSurfaceVariant
                        decreasingColor = colorTertiary
                        decreasingPaintStyle = STROKE
                        shadowColor = colorOnBackground
                        setDrawValues(false)

                        with(chartTradeCandle) {
                            xAxis.apply { setDrawGridLines(false); valueTextSize = TEXT_SIZE; valueFormatter = formatter; axisMinimum = xMin - 0.5f; axisMaximum = xMax + 0.5f }

                            axisLeft
                                .apply {
                                    textColor = colorOnBackground

                                    removeAllLimitLines()

                                    schema
                                        ?.apply {
                                            when (this) {
                                                is FixSchema -> fix
                                                is RateSchema -> rate
                                                is CommissionSchema -> cut
                                                is ObjectiveSchema -> bonus
                                                is ThresholdSchema -> bonus
                                                else -> null
                                            }
                                                ?.toFloat()
                                                ?.let {
                                                    LimitLine(it)
                                                        .apply {
                                                            enableDashedLine(2f, 2f, 0f)
                                                            label = "(%s) %.2f".format(res?.schemas?.get(id), it)
                                                            labelPosition = LEFT_TOP
                                                            lineColor = colorSecondary
                                                            textColor = colorSecondary
                                                            textSize = 12f

                                                            addLimitLine(this)
                                                        }
                                                }
                                        }
                                }

                            data = CandleData(this@set)
                            invalidate()
                        }
                    }

                    with(barDataSet) set@{
                        isHighlightEnabled = false
                        valueTextColor = colorOnBackground
                        setDrawValues(false)

                        with(chartTradeBar) {
                            xAxis.apply { setDrawGridLines(false); valueFormatter = formatter; axisMinimum = xMin - 0.5f; axisMaximum = xMax + 0.5f }

                            data = BarData(this@set)
                            invalidate()
                        }
                    }
                }
                ?: lblTradeDisplay.run { visibility = VISIBLE; text = errorMessage; setCompoundTopDrawable(R.drawable.ic_sad_face) }
        }
    }
}