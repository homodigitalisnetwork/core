package pt.hdn.toolbox.views

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView
import pt.hdn.toolbox.R

class EnhanceRecyclerView(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int
) : RecyclerView(context, attrs, defStyleAttr) {

    //region vars
    var isUserInputEnabled: Boolean = false
    //endregion vars

    constructor(context: Context, attrs: AttributeSet?): this(context, attrs, R.attr.recyclerViewStyle)

    constructor(context: Context): this(context, null)

    override fun onInterceptTouchEvent(e: MotionEvent?): Boolean = isUserInputEnabled && super.onInterceptTouchEvent(e)

    override fun onTouchEvent(e: MotionEvent?): Boolean = isUserInputEnabled && super.onTouchEvent(e)
}