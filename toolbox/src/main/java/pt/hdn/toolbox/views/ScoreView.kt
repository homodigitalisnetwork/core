package pt.hdn.toolbox.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.textview.MaterialTextView
import pt.hdn.toolbox.R
import pt.hdn.toolbox.databinding.ViewScoreBinding
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.util.Score

class ScoreView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    //region vars
    lateinit var res: Resources
    var data: Score? = null; set(value) { field = value; compile(value) }
    private val binding: ViewScoreBinding = ViewScoreBinding.inflate(LayoutInflater.from(context), this)
    //endregion vars

    private fun compile(value: Score?){
        with(binding) {
            value
                ?.apply {
                    lblScoreRating.apply { text = "%.2f".format(rating); typeface = res.font }

                    setTrophy(lblScoreRating, lblScoreRatingCounter, countRating)
                    setTrophy(lblScorePro, lblScoreProCounter, countPro)
                    setTrophy(imgScoreOverachieve, lblScoreOverachieveCounter, countOverachiever)
                    setTrophy(imgScoreWellManner, lblScoreWellMannerCounter, countWellManner)
                    setTrophy(imgScoreCleanWork, lblScoreCleanWorkCounter, countCleanWork)
                    setTrophy(imgScoreQuickWork, lblScoreQuickWorkCounter, countQuickWork)
                }
                ?: run {
                    lblScoreRating.text = null

                    setTrophy(lblScoreRating, lblScoreRatingCounter)
                    setTrophy(lblScorePro, lblScoreProCounter)
                    setTrophy(imgScoreOverachieve, lblScoreOverachieveCounter)
                    setTrophy(imgScoreWellManner, lblScoreWellMannerCounter)
                    setTrophy(imgScoreCleanWork, lblScoreCleanWorkCounter)
                    setTrophy(imgScoreQuickWork, lblScoreQuickWorkCounter)
                }
        }
    }

    private fun setTrophy(view: View, materialTextView: MaterialTextView, count: Int = 0){
        view.background = drawable(if (count > 0) R.drawable.bg_circle_enable else R.drawable.bg_circle_disable)

        materialTextView.apply { visibility = if (count > 0) VISIBLE else INVISIBLE; text = "x%d".format(count) }
    }

    private fun drawable(@DrawableRes id: Int): Drawable? = context?.getDrawable(id)
}