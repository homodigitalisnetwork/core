package pt.hdn.toolbox.views

import android.content.Context
import android.graphics.Color.WHITE
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.graphics.ColorUtils.blendARGB
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import pt.hdn.toolbox.R
import pt.hdn.toolbox.databinding.ViewActivityBinding
import pt.hdn.toolbox.misc.getColorAttr
import pt.hdn.toolbox.report.adapter.activity.ActivityDataSet
import pt.hdn.toolbox.resources.Resources

class ActivityView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): FrameLayout(context, attrs, defStyleAttr) {

    //region vars
    var res: Resources? = null
    var dataSet: ActivityDataSet? = null; set(value) { field = value?.also { it.compile() } }
    private val binding: ViewActivityBinding = ViewActivityBinding.inflate(LayoutInflater.from(context), this)
    private val colorTransparent: Int = context.getColor(android.R.color.transparent)
    private val colorPrimaryContainer: Int = context.getColorAttr(R.attr.colorPrimaryContainer)
    private val colorTertiaryContainer: Int = context.getColorAttr(R.attr.colorTertiaryContainer)
//    private val colorBackground: Int = context.getColorAttr(android.R.attr.colorBackground)
    private val colorOnBackground: Int = context.getColorAttr(R.attr.colorOnBackground)
    //region vars

    init {
        with(binding.chartActivityChart) {
            setNoDataText(null)
            isHighlightPerTapEnabled = false
            description.isEnabled = false
            legend.apply { this.textColor = colorOnBackground; this.textSize = 14f; this.xEntrySpace = 15f; this.isWordWrapEnabled = true }
            setDrawEntryLabels(false)
            setUsePercentValues(true)
            setCenterTextSize(14f)
            setCenterTextColor(colorOnBackground)
            holeRadius = 40f
            setHoleColor(colorTransparent)
            transparentCircleRadius = 40f
        }
    }

    private fun ActivityDataSet.compile() {
        with(binding) {
            lblActivityDisplay.apply { if (this@compile.isEmpty()) text = res?.string(R.string.nothingToReport) else visibility = GONE }

            chartActivityChart
                .apply {
                    data = dataSet
                        .run {
                            sliceSpace = 2f
                            colors = colorShades(colorPrimaryContainer, colorTertiaryContainer, entryCount)
                            valueTextColor = colorOnBackground
                            valueTextSize = 14f
                            selectionShift = 0f
                            valueFormatter = ActivityFormatter()

                            PieData(this)
                        }
                }
        }
    }

    private fun colorShades(startColor: Int, endColor: Int, shades: Int): List<Int> {
        return buildList(shades) {
            if (shades == 1) add(blendARGB(startColor, endColor, 0.5f))
            else for (step in 0 until shades) add(blendARGB(startColor, endColor, step.toFloat() / (shades - 1)))
        }
    }

    inner class ActivityFormatter: ValueFormatter() {
        override fun getPieLabel(value: Float, pieEntry: PieEntry?): String = "%d (%.2f%%)".format(pieEntry?.value?.toInt(), value)
    }
}