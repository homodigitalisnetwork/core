package pt.hdn.toolbox.views

import com.google.android.gms.maps.GoogleMap

interface OnMapCallback {
    fun GoogleMap.onMapReady()
}