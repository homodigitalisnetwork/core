package pt.hdn.toolbox.chat

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.Expose
import pt.hdn.toolbox.annotations.Field.Companion.MESSAGE
import pt.hdn.toolbox.annotations.Field.Companion.SENDER
import pt.hdn.toolbox.annotations.Field.Companion.TIMESTAMP
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * The default implementation of [ChatMessage] with a static function to create an object from [DataSnapshot] and [UUID]
 */
data class DefaultChatMessage(
    @Expose override val senderUUID: UUID = UUID.randomUUID(),
    @Expose override val message: String = "",
    @Expose override val timestamp: String = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss").withZone(ZoneId.systemDefault()).format(Instant.now())
) : ChatMessage {

    //region vars
    override var key: String? = null
    override var receiverUUID: UUID? = null
    //endregion vars

    companion object {
        fun from(snapshot: DataSnapshot, receiverUUID: UUID): DefaultChatMessage {
            return with(snapshot.value as Map<String, Any?>) {
                DefaultChatMessage(senderUUID = UUID.fromString(this[SENDER] as String), message = this[MESSAGE] as String, timestamp = this[TIMESTAMP] as String)
                    .apply { this.key = snapshot.key; this.receiverUUID =  receiverUUID }
            }
        }
    }

    override fun toMap(): Map<String, String> = mapOf(SENDER to senderUUID.toString(), MESSAGE to message, TIMESTAMP to timestamp)
}