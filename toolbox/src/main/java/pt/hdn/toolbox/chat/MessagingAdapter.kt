package pt.hdn.toolbox.chat

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.viewbinding.ViewBinding
import pt.hdn.toolbox.resources.Resources
import java.util.*

/**
 * Abstract class that inherits from [Adapter] that translate a [MutableList] to [MessagingViewHolder].
 * It provides common [MutableList] like functionality to enable visual synchronization with the [View] it is attached to.
 */
abstract class MessagingAdapter<SVH: MessagingViewHolder<ViewBinding, T>, EVH: MessagingViewHolder<ViewBinding, T>, T: ChatMessage>(
    protected val res: Resources,
    val source: MutableList<T> = mutableListOf()
) : Adapter<MessagingViewHolder<ViewBinding, T>>() {

    //region vars
    var lastKey: String? = null; private set
    //endregion vars

    companion object {
        private const val START: Int = 0
        private const val END: Int = 1
    }

    abstract fun onCreateStartViewHolder(resources: Resources, parent: ViewGroup): SVH

    abstract fun onCreateEndViewHolder(resources: Resources, parent: ViewGroup): EVH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagingViewHolder<ViewBinding, T> {
        return when (viewType) { START -> onCreateStartViewHolder(res, parent); else /*END*/ -> onCreateEndViewHolder(res, parent) }
    }

    override fun onBindViewHolder(holder: MessagingViewHolder<ViewBinding, T>, position: Int) { with(holder) { source[position].setViewHolder() } }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemCount(): Int = source.size

    override fun getItemViewType(position: Int): Int = if (source[position].side) END else START

    fun removeAt(position: Int): T = source.removeAt(position).also { notifyItemRemoved(position) }

    operator fun get(position: Int): T = source[position]

    fun add(t: T): Boolean = source.add(t).also { this.lastKey = t.key; notifyItemInserted(source.size - 1) }

    fun set(position: Int, t: T) { source.set(position, t).also { notifyItemChanged(position) }}

    fun append(list: List<T>) { var count: Int = 0; list.forEach { if (!source.contains(it) && source.add(it)) count++ }; if (count > 0) notifyItemRangeInserted(source.size - count - 1, count)}

    fun clear() { source.clear(); notifyDataSetChanged()}
}