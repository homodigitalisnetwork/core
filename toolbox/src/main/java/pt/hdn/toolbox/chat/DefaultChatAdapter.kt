package pt.hdn.toolbox.chat

import android.view.ViewGroup
import pt.hdn.toolbox.resources.Resources

/**
 * Default implementation of [MessagingAdapter]
 */
class DefaultChatAdapter(
    resources: Resources
) : MessagingAdapter<DefaultStartViewHolder, DefaultEndViewHolder, ChatMessage>(resources) {
    override fun onCreateStartViewHolder(resources: Resources, parent: ViewGroup): DefaultStartViewHolder = DefaultStartViewHolder(res, parent)

    override fun onCreateEndViewHolder(resources: Resources, parent: ViewGroup): DefaultEndViewHolder = DefaultEndViewHolder(res, parent)
}