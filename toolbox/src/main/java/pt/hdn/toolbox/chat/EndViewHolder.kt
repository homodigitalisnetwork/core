package pt.hdn.toolbox.chat

import android.view.ViewGroup
import pt.hdn.toolbox.databinding.ViewholderChatBubbleEndBinding
import pt.hdn.toolbox.resources.Resources

/**
 * Abstract class that inherits from [MessagingViewHolder] and sets up the [ViewBinding] bubble of the screen' end side
 */
abstract class EndViewHolder<T: ChatMessage>(
	res: Resources,
	parent: ViewGroup
) : MessagingViewHolder<ViewholderChatBubbleEndBinding, T>(res, parent, ViewholderChatBubbleEndBinding::inflate)