package pt.hdn.toolbox.chat

import android.view.ViewGroup
import pt.hdn.toolbox.resources.Resources

/**
 * Default implementation of [StartViewHolder]
 * used by [DefaultChatAdapter] for the in app chat service during a transaction
 */
class DefaultStartViewHolder(
    res: Resources,
    parent: ViewGroup
) : StartViewHolder<ChatMessage>(res, parent) {
    override fun ChatMessage.setViewHolder() { with(binding) { lblStartBubbleMessage.setTextView(message); lblStartBubbleTimestamp.setTextView(timestamp) } }
}