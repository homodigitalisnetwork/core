package pt.hdn.toolbox.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.google.android.material.textview.MaterialTextView
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.resources.Resources

/**
 * Abstract class that inherits from [BindingViewHolder] that handles the presentation on the [ChatMessage] to the user
 */
abstract class MessagingViewHolder<out VB: ViewBinding, in T: ChatMessage>(
    res: Resources,
    parent: ViewGroup,
    inflate: (LayoutInflater, ViewGroup, Boolean) -> VB
) : BindingViewHolder<VB>(res, parent, inflate) {
    abstract fun T.setViewHolder()

    protected fun MaterialTextView.setTextView(value: String) { text = null; text = value }
}