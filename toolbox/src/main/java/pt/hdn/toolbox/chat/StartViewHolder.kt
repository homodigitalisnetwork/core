package pt.hdn.toolbox.chat

import android.view.ViewGroup
import pt.hdn.toolbox.databinding.ViewholderChatBubbleStartBinding
import pt.hdn.toolbox.resources.Resources

/**
 * Abstract class that inherits from [MessagingViewHolder] and sets up the [ViewBinding] bubble of the screen' start side
 */
abstract class StartViewHolder<T: ChatMessage>(
	res: Resources,
	parent: ViewGroup
) : MessagingViewHolder<ViewholderChatBubbleStartBinding, T>(res, parent, ViewholderChatBubbleStartBinding::inflate)