package pt.hdn.toolbox.chat

import java.util.*

/**
 * An interface that defines a chat message on the chat service that is available during a service transaction
 */
interface ChatMessage {
    val senderUUID: UUID
    val message: String
    val timestamp: String
    var key: String?
    var receiverUUID: UUID?
    val side: Boolean; get() = senderUUID == receiverUUID

    fun toMap(): Map<String, String>
}