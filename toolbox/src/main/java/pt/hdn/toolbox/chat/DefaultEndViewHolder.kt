package pt.hdn.toolbox.chat

import android.view.ViewGroup
import pt.hdn.toolbox.resources.Resources

/**
 * Default implementation of [EndViewHolder]
 * used by [DefaultChatAdapter] for the in app chat service during a transaction
 */
class DefaultEndViewHolder(
    res: Resources,
    parent: ViewGroup
) : EndViewHolder<ChatMessage>(res, parent) {
    override fun ChatMessage.setViewHolder() { with(binding) { lblEndBubbleMessage.setTextView(message); lblEndBubbleTimestamp.setTextView(timestamp) } }
}