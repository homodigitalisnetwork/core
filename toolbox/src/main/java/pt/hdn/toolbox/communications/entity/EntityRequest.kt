package pt.hdn.toolbox.communications.entity

import android.net.Uri
import com.google.gson.annotations.Expose
import pt.hdn.contract.schemas.Schema
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.entity.util.Entity
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.preferences.Preferences
import pt.hdn.toolbox.security.Clearance
import java.util.*

/**
 * Request used in [ProfileViewModel] and [RegisterViewModel] to control the [Entity] object that represent the user
 */
data class EntityRequest(
    @Expose @Command val command: Int,
    @Expose val collectionUUID: UUID? = null,
    @Expose val appUUID: UUID? = null,
    @Expose val deputyID: String? = null,
    @Expose val partyUUID: UUID? = null,
    @Expose val sessionUUID: UUID? = null,
    @Expose val deputyUUID: UUID? = null,
    @Expose val entity: Entity?= null,
    @Expose val partyPassphrase: String? = null,
    @Expose val deputyPassphrase: String? = null,
    @Expose val contract: Contract? = null,
    @Expose val clearance: Clearance? = null,
    @Expose val schemas: List<Schema>? = null,
    @Expose val isNotSpot: Boolean? = null,
    @Action val action: Int? = null,
    val photo: Photo? = null,
    val preferences: Preferences? = null,
    val locale: String? = null
) {

    //region vars
    val photoUri: Uri; get() = photo!!.uri!!
    //endregion vars
}