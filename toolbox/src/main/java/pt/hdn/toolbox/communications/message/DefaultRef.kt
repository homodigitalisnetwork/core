package pt.hdn.toolbox.communications.message

import com.google.firebase.database.DataSnapshot
import com.google.gson.Gson
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Field.Companion.BUYER
import pt.hdn.toolbox.annotations.Field.Companion.SELLER
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.buyer.DefaultBuyer
import pt.hdn.toolbox.business.seller.DefaultSeller
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.misc.Position
import java.math.BigDecimal
import java.util.*

@Parcelize
data class DefaultRef(
    @Expose override val buyer: Buyer? = null,
    @Expose override val seller: Seller? = null
) : Ref {
    companion object {
        fun from(snapshot: DataSnapshot) : DefaultRef {
            return DefaultRef(
                buyer = DefaultBuyer.from(snapshot.child(BUYER)),
                seller = DefaultSeller.from(snapshot.child(SELLER))
            )
        }
    }

    override fun toMap(): Map<String, Any> {
        val ignoreExtraField: Boolean = false

        return mapOf(BUYER to buyer!!.toMap(ignoreExtraField), SELLER to seller!!.toMap(ignoreExtraField))
    }
}