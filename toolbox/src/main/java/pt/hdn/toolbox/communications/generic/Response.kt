package pt.hdn.toolbox.communications.generic

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.annotations.Err

data class Response(
    @Expose @Err val error: Int,
    @Expose val errorMessage: String
)
