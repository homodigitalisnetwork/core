package pt.hdn.toolbox.communications.partnership

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.communications.message.Message
import pt.hdn.toolbox.partnerships.util.Partnership
import java.util.*

data class PartnershipsRequest(
    @Expose @Command val command: Int,
    @Expose val deputyUUID: UUID? = null,
    @Expose val partyUUID: UUID? = null,
     @Expose val partnership: Partnership? = null,
    @Expose val partnershipUUID: UUID? = null,
    @Expose val contractUUID: UUID? = null,
    @Expose val contract: Contract? = null,
    @Expose val isNotSpot: Boolean? = null,
    @Action val action: Int,
    val message: Message? = null
 )
