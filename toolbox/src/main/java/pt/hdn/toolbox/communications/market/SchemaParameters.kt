package pt.hdn.toolbox.communications.market

import com.google.gson.annotations.Expose
import pt.hdn.contract.annotations.SchemaType
import pt.hdn.contract.annotations.SourceType

data class SchemaParameters(
    @Expose @SchemaType val schema: Int,
    @Expose @SourceType val source: Int
)