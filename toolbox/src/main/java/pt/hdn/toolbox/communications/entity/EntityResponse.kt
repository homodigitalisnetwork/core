package pt.hdn.toolbox.communications.entity

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.entity.util.Entity
import java.util.*

/**
 * Response from [EntityRequest]
 */
data class EntityResponse(
    @Expose val sessionUUID: UUID? = null,
    @Expose val token: String? = null,
    @Expose val partnership: Partnership? = null,
    @Expose val entity: Entity? = null,
    @Expose val isSuccessful: Boolean? = null
) {
    lateinit var request: EntityRequest
}
