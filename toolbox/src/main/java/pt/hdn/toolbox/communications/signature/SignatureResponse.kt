package pt.hdn.toolbox.communications.signature

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.Contract

data class SignatureResponse(
    @Expose val signedContract: Contract
)
