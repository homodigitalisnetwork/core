package pt.hdn.toolbox.communications.market

import com.google.gson.annotations.Expose
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.specialities.dealership.util.Trade
import pt.hdn.toolbox.misc.WindowBounds

data class TradesResponse(
    @Expose val trades: List<Trade>/*? = null*/,
    @Expose val windowBounds: WindowBounds,
    @Expose val schemas: List<Schema>
)
