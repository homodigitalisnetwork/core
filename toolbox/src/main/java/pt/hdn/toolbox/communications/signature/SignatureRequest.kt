package pt.hdn.toolbox.communications.signature

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.Contract
import java.util.*

data class SignatureRequest(
    @Expose val contract: Contract,
    @Expose val partyUUID: UUID,
    @Expose val partyPassphrase: String,
    @Expose val deputyUUID: UUID,
    @Expose val deputyPassphrase: String,
    @Expose val isBuyerSide: Boolean
)
