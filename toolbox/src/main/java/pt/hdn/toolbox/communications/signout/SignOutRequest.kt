package pt.hdn.toolbox.communications.signout

import com.google.gson.annotations.Expose

data class SignOutRequest(
	@Expose val sessionUUID: String
)
