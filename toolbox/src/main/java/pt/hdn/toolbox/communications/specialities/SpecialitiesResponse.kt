package pt.hdn.toolbox.communications.specialities

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.specialities.util.Speciality
import java.util.*

data class SpecialitiesResponse(
	@Expose val uuid: UUID? = null,
	@Expose val specialities: MutableList<Speciality>? = null,
	@Expose val isSuccessful: Boolean? = null
) {
    lateinit var request: SpecialitiesRequest
}
