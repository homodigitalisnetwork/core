package pt.hdn.toolbox.communications.performance

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.statistics.DeputyStats
import pt.hdn.toolbox.statistics.PopulationStats
import pt.hdn.toolbox.statistics.Progression
import java.util.*

data class PerformanceResponse(
    @Expose val populationStats: PopulationStats,
    @Expose val deputiesStats: List<DeputyStats>,
    @Expose val progressionsStats: Map<UUID, List<Progression>>?
)
