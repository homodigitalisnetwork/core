package pt.hdn.toolbox.communications.resumes

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.misc.WindowBounds
import java.util.*

data class ResumesRequest(
    @Expose val deputyUUID: UUID,
    @Expose val partyUUID: UUID? = null,
    @Expose val specialityUUID: UUID? = null
) {
    //region vars
    @Expose val windowBounds: WindowBounds = WindowBounds.create()
    val renew: Boolean = specialityUUID != null || partyUUID != null
    //endregion vars
}
