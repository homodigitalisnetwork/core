package pt.hdn.toolbox.communications.bus

import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import pt.hdn.toolbox.session.Session
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * A specialized [DataBus] type class to be used across the app to provide a [Session] object
 */
object SessionBus {

    //region vars
    private val mutableFlow: MutableStateFlow<Session?> = MutableStateFlow(null)
    val flow: StateFlow<Session?> = mutableFlow
    //endregion vars

    fun send(session: Session? = null) { mutableFlow.tryEmit(session) }
}

fun SessionBus.observe(owner: LifecycleOwner, context: CoroutineContext = EmptyCoroutineContext, collector: FlowCollector<Session?>) { observe(owner.lifecycleScope, context, collector) }

fun SessionBus.observe(scope: CoroutineScope, context: CoroutineContext = EmptyCoroutineContext, collector: FlowCollector<Session?>) { scope.launch(context) { flow.collect(collector) } }