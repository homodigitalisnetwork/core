package pt.hdn.toolbox.communications.report

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.report.adapter.ReportData
import java.util.*

data class ReportResponse(
    @Expose val datas: List<ReportData>
)
