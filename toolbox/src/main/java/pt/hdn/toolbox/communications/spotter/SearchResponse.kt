package pt.hdn.toolbox.communications.spotter

import com.google.android.gms.maps.model.Marker
import com.google.gson.annotations.Expose
import pt.hdn.toolbox.business.seller.Seller

data class SearchResponse(
    val sellers: List<Seller>? = null,
    var markers: MutableList<Marker>
)