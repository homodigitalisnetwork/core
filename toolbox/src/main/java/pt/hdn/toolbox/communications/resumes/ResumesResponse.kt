package pt.hdn.toolbox.communications.resumes

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.partnerships.dealership.resume.util.Resume

data class ResumesResponse(
    @Expose val resumes: MutableList<Resume>
) {
    //region vars
    lateinit var request: ResumesRequest
    //endregion vars
}
