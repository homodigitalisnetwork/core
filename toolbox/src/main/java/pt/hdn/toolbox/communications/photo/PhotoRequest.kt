package pt.hdn.toolbox.communications.photo

import java.util.*

data class PhotoRequest(
    val deputyUUID: UUID
)