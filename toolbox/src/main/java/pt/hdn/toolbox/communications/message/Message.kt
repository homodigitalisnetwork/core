package pt.hdn.toolbox.communications.message

import pt.hdn.toolbox.annotations.Field
import java.util.*

data class Message(
    val receiverUUID: UUID,
    val payload: MutableMap<String, Any>
) {
    fun updateUUID(uuid: UUID) { payload[Field.UUID] = uuid.toString() }
}