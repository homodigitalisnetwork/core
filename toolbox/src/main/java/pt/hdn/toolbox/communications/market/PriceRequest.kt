package pt.hdn.toolbox.communications.market

import com.google.gson.annotations.Expose
import pt.hdn.contract.schemas.Schema
import pt.hdn.contract.annotations.Market
import pt.hdn.toolbox.misc.WindowBounds
import pt.hdn.toolbox.misc.Position
import java.util.*

data class PriceRequest(
	@Expose @Market val market: Int,
	@Expose val specialityType: UUID,
	@Expose val position: Position,
	@Expose val schemas: List<Schema>
) {
	@Expose val windowBounds: WindowBounds = WindowBounds.create()
}
