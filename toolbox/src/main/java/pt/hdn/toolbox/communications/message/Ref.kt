package pt.hdn.toolbox.communications.message

import android.os.Parcelable
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.misc.Position
import java.math.BigDecimal
import java.util.*

interface Ref : Parcelable {
	val buyer: Buyer?
	val seller: Seller?
	val sellerSpecialityServiceSchemasUUID: UUID; get() = seller!!.specialityServiceSchemasUUID
	val buyerRating: Float; get() = buyer!!.rating
	val buyerPartyUUID: UUID; get() = buyer!!.partyUUID
	val buyerPartyName: String; get() = buyer!!.partyName
	val buyerDeputyName: String; get() = buyer!!.deputyName
	val buyerDeputyUUID: UUID; get() = buyer!!.deputyUUID
	val buyerAppUUID: UUID; get() = buyer!!.partyAppUUID
	val buyerPosition: Position; get() = buyer!!.position!!
	val sellerPartyUUID: UUID; get() = seller!!.partyUUID
	val sellerDeputyName: String; get() = seller!!.deputyName
	val sellerDeputyUUID: UUID; get() = seller!!.deputyUUID
	val sellerSpecialityUUID: UUID; get() = seller!!.specialityUUID
	val sellerSpecialityTaxValue: BigDecimal; get() = seller!!.specialityTaxValue
	val sellerSpecialityTaxUUID: UUID; get() = seller!!.specialityTaxUUID

	fun toMap(): Map<String, Any>
}