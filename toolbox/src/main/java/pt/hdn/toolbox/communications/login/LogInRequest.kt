package pt.hdn.toolbox.communications.login

import com.google.gson.annotations.Expose
import java.util.*

data class LogInRequest(
    @Expose val userName: String,
    @Expose val password: String,
    @Expose val command: Boolean,
    @Expose val appUUID: UUID,
    @Expose val sessionUUID: UUID = UUID.randomUUID(),
    val deputyPassphrase: String? = null
)