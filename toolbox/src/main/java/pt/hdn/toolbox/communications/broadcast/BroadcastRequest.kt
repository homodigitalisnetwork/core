package pt.hdn.toolbox.communications.broadcast

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.misc.Position
import java.util.*

/**
 * Request used in [LocationProvider] to control the user's location associated with his visibility to the markets
 */
data class BroadcastRequest(
    @Expose @Command val command: Int,
    @Expose val deputyUUID: UUID,
    @Expose val partyUUID: UUID,
    @Expose val position: Position? = null
)