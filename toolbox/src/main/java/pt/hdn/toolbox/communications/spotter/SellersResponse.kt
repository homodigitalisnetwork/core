package pt.hdn.toolbox.communications.spotter

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.business.seller.Seller

data class SellersResponse(
    @Expose val sellers: List<Seller>
)
