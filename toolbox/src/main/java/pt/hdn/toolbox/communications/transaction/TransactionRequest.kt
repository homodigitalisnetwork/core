package pt.hdn.toolbox.communications.transaction

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.transaction.util.Chrono
import pt.hdn.toolbox.transaction.util.DefaultTransaction
import java.math.BigDecimal
import java.util.*

data class TransactionRequest(
	@Expose val deputyUUID: UUID,
	@Expose val transactionUUID: UUID,
	@Expose val sellerUUID: UUID,
	@Expose val buyerUUID: UUID,
	@Expose val serviceSchemasUUID: UUID,
    @Expose val taxUUID: UUID,
	@Expose val contract: Contract,
    @Expose val action: Int,
	@Expose val position: Position,
	@Expose val chrono: Chrono? = null,
	@Expose val subTotal: BigDecimal,
    @Expose val tax: BigDecimal,
	@Expose val isBuyerSide: Boolean,
	@Expose val rating: Int? = null,
	@Expose val trophy: Int? = null
) {
    companion object {
        fun from(transaction: DefaultTransaction): TransactionRequest {
            return with(transaction) {
                TransactionRequest(
                    deputyUUID = deputyUUID,
                    transactionUUID = uuid,
                    sellerUUID = sellerUUID,
                    buyerUUID = buyerUUID,
                    serviceSchemasUUID = serviceSchemasUUID,
                    taxUUID = taxUUID,
                    contract = contract!!,
                    action = action,
                    position = buyerPosition,
                    chrono = chrono,
                    subTotal = subTotal,
                    tax = tax * subTotal,
                    isBuyerSide = isBuyerSide,
                    rating = rating,
                    trophy = trophy
                )
            }
        }
    }
}
