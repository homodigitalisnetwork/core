package pt.hdn.toolbox.communications.market

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.specialities.dealership.util.Price

data class PriceResponse(
	@Expose val price: Price? = null
)
