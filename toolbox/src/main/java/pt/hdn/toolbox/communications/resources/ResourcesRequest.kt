package pt.hdn.toolbox.communications.resources

import com.google.gson.annotations.Expose

data class ResourcesRequest(
    @Expose val types: List<String>
)
