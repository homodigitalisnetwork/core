package pt.hdn.toolbox.communications.singles

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Command
import java.util.*

data class SinglesRequest(
    @Expose val partyUUID: UUID
)
