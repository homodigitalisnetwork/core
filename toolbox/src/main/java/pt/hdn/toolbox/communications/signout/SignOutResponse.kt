package pt.hdn.toolbox.communications.signout

import com.google.gson.annotations.Expose

data class SignOutResponse(
    @Expose val isSuccessful: Boolean
)
