package pt.hdn.toolbox.communications.login

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.partnerships.util.Partnership
import java.util.*

data class SignInResponse(
    @Expose val uuid: UUID,
    @Expose val id: String?,
    @Expose val token: String?
) {

    //region vars
    @Expose var partnerships: List<Partnership> = emptyList(); private set
    val partnership: Partnership; get() = partnerships[index]
    var index: Int = 0
    lateinit var request: LogInRequest
    //endregion vars
}