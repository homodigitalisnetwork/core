package pt.hdn.toolbox.communications.bus

import android.app.Service
import androidx.lifecycle.*
import androidx.lifecycle.Lifecycle.State
import androidx.lifecycle.Lifecycle.State.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.misc.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * An internal communication class used for more complex communications
 * like a binary combination of [Service], [Fragment], nested [Fragment], [Activity] and being them be in this library or an app that uses this library.
 * Also, to allow "startFragmentForResult" like behaviour in [NavGraph]s
 * and enable the usage of the same [Fragment] in multiple [NavGraph]s and still to be able to communicate the inside of the [NavGraph]s.
 */
object DataBus {

    //region vars
    private val mutableFlow: MutableSharedFlow<Pair<@Address Int, Any?>> = MutableSharedFlow()
    val flow: Flow<Pair<@Address Int, Any?>> = mutableFlow
    //endregion vars

    fun trySend(@Address address: Int, value: Any? = null) { mutableFlow.tryEmit(address to value) }

    suspend fun send(@Address address: Int, value: Any? = null) { mutableFlow.emit(address to value) }

    fun send(owner: LifecycleOwner, @Address address: Int, value: Any? = null) { send(owner.lifecycleScope, address, value) }

    fun send(scope: CoroutineScope, @Address address: Int, value: Any? = null) { scope.launch { mutableFlow.emit(address to value) } }

    fun send(owner: LifecycleOwner, @Address address: Int, vararg values: Any) { owner.lifecycleScope.launch { values.forEach { mutableFlow.emit(address to it) } } }

    fun using(owner: LifecycleOwner, block: DataBusLifecycleObservers.() -> Unit) { DataBusLifecycleObservers(this, owner).apply(block) }

    fun using(scope: CoroutineScope, block: DataBusScopeObservers.() -> Unit) { DataBusScopeObservers(this, scope).apply(block) }
}

inline fun <reified R: Any?> DataBus.observe(owner: LifecycleOwner, @Address address: Int, context: CoroutineContext = EmptyCoroutineContext, state: State = if (owner is Service) STARTED else RESUMED, crossinline block: CoroutineScope.(value: R) -> Unit): Job {
    return with(owner) { lifecycleScope.launch(context) { repeatOnLifecycle(state) { flow.filterWith { address == first && second is R }.map { it.second as R }.collect { this.block(it) } } } }
}

inline fun <reified R: Any?> DataBus.observeWith(owner: LifecycleOwner, @Address address: Int, context: CoroutineContext = EmptyCoroutineContext, state: State = if (owner is Service) STARTED else RESUMED, crossinline block: R.() -> Unit): Job {
    return observe<R>(owner, address, context, state) { it.block() }
}

inline fun <reified R: Any?> DataBus.observe(scope: CoroutineScope, @Address address: Int, context: CoroutineContext = EmptyCoroutineContext, flowCollector: FlowCollector<R>): Job {
    return scope.launch(context) { flow.filterWith { address == first && second is R }.map { it.second as R }.collect(flowCollector) }
}

inline fun <reified R: Any?> DataBus.observeWith(scope: CoroutineScope, @Address address: Int, context: CoroutineContext = EmptyCoroutineContext, crossinline block: R.() -> Unit): Job {
    return observe<R>(scope, address, context) { it.block() }
}