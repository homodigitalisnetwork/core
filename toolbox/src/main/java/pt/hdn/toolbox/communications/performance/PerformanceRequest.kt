package pt.hdn.toolbox.communications.performance

import com.google.gson.annotations.Expose
import java.util.*

data class PerformanceRequest(
    @Expose val uuid: UUID,
    @Expose val specialityUUID: UUID,
    @Expose val isDeputy: Boolean
)
