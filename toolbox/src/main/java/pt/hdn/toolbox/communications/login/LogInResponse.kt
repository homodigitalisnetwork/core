package pt.hdn.toolbox.communications.login

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.session.Session
import java.util.*

data class LogInResponse(
    @Expose val specialities: Map<UUID, UUIDNameMarketData>,
    @Expose val locale: String,
    @Expose val session: Session
)
