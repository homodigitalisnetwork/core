package pt.hdn.toolbox.communications.photo

import pt.hdn.toolbox.misc.Photo

data class PhotoResponse(
    val photo: Photo
)