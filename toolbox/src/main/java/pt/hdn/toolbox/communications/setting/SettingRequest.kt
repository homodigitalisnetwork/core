package pt.hdn.toolbox.communications.setting

data class SettingRequest(
	val deputyUUID: String,
	val value: Boolean
)