package pt.hdn.toolbox.communications.bus

import androidx.lifecycle.*
import androidx.lifecycle.Lifecycle.State.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.*
import pt.hdn.toolbox.annotations.Address

/**
 * A specialized [DataBus] type class to be used by Dealership type classes on validating the class they are generating
 */
class ErrorBus {

    //region vars
    private val mutableFlow: MutableSharedFlow<Pair<@Address Int, Int>> = MutableSharedFlow(extraBufferCapacity = 1, onBufferOverflow = DROP_OLDEST)
    val defaultAddress: Int = Address.ADDRESS_1
    val flow: Flow<Pair<@Address Int, Int>> = mutableFlow
    //endregion vars

    fun trySend(@Address address: Int = defaultAddress, value: Int) { mutableFlow.tryEmit(address to value) }

    suspend fun send(@Address address: Int = defaultAddress, value: Int) { mutableFlow.emit(address to value) }

    fun send(owner: LifecycleOwner, @Address address: Int = defaultAddress, value: Int) { send(owner.lifecycleScope, address, value) }

    fun send(scope: CoroutineScope, @Address address: Int = defaultAddress, value: Int) { scope.launch { mutableFlow.emit(address to value) } }
}

inline fun ErrorBus.observe(owner: LifecycleOwner, @Address address: Int = defaultAddress, crossinline block: (err: Int) -> Unit) {
    with(owner) { lifecycleScope.launch { repeatOnLifecycle(STARTED) { flow.filter { it.first == address }.collect { block(it.second) } } } }
}