package pt.hdn.toolbox.communications.singles

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.singles.adapter.Single

data class SinglesResponse(
    @Expose val singles: MutableList<Single>
)
