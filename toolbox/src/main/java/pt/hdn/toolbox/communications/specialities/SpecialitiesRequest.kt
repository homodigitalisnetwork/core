package pt.hdn.toolbox.communications.specialities

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.specialities.util.Speciality
import java.util.*

data class SpecialitiesRequest(
    @Expose @Command val command: Int,
    @Expose val deputyUUID: UUID? = null,
    @Expose val partyUUID: UUID? = null,
    @Expose val uuid: UUID? = null,
    @Expose val speciality: Speciality? = null,
    @Action val action: Int
)