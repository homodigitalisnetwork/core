package pt.hdn.toolbox.communications.spotter

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.misc.Position
import java.util.*

data class SearchRequest(
    @Expose val deputyUUID: UUID,
    @Expose val partyUUID: UUID,
    @Expose val specialityUUID: UUID,
    @Expose val range: Float,
    @Expose val position: Position
)
