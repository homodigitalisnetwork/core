package pt.hdn.toolbox.communications.responses

import com.google.gson.Gson
import pt.hdn.toolbox.annotations.Err
import java.io.Reader

class ErrorResponse(
    @Err val error: Int?,
    val errorMessage: String?
) {
    companion object {
        fun from(reader: Reader): ErrorResponse = Gson().fromJson(reader, ErrorResponse::class.java)
    }

    override fun toString() = "ErrorMessage(error: $error, errorMessage: $errorMessage)"
}
