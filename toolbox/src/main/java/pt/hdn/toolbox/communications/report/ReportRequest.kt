package pt.hdn.toolbox.communications.report

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.misc.WindowBounds
import java.util.*

data class ReportRequest(
    @Expose val partyUUID: UUID,
    @Expose val appUUID: UUID,
    @Expose val windowBounds: WindowBounds
)
