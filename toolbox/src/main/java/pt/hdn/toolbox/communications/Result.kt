package pt.hdn.toolbox.communications

sealed class Result<out T> {
    data class Success<out T>(val response: T) : Result<T>()

    data class Error(val code: Int, val response: Throwable): Result<Nothing>()

    object Unknown : Result<Nothing>()
}

inline fun <T> Result<T>.success(block: T.() -> Unit): Result<T> { if (this is Result.Success) block(response); return this }

inline fun <T> Result<T>.error(block: (Int, Throwable) -> Unit): Result<T> { if (this is Result.Error) block(code, response); return this }