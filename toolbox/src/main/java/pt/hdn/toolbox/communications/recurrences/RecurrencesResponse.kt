package pt.hdn.toolbox.communications.recurrences

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.recurrences.util.Recurrence

data class RecurrencesResponse(
    @Expose val recurrences: MutableList<Recurrence>
)
