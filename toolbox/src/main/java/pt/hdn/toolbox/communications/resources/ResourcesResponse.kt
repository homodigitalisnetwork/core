package pt.hdn.toolbox.communications.resources

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.data.UUIDBrandModelData

data class ResourcesResponse(
    @Expose val resources: List<UUIDBrandModelData>? = null
)
