package pt.hdn.toolbox.communications.partnership

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.partnerships.util.Partnership
import java.util.*

data class PartnershipsResponse(
	@Expose val partnership: Partnership? = null,
	@Expose val partnerships: MutableMap<UUID, Partnership>? = null,
	@Expose val partnershipUUID: UUID? = null,
	@Expose val contractUUID: UUID? = null
) {
    lateinit var request: PartnershipsRequest
}
