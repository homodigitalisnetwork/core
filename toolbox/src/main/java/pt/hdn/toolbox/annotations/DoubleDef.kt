package pt.hdn.toolbox.annotations

import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Allow annotations of the type [Double]
 */
@Retention(SOURCE)
annotation class DoubleDef
