package pt.hdn.toolbox.annotations

import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Miscellaneous values used across the app
 */
@Retention(SOURCE)
@DoubleDef
annotation class Value {
    companion object {
        const val RING_RADIUS = 30.0
        const val NEXT_RADIUS = 15.0
        const val RANGE_OFFSET = 2500.0
        const val RANGE_MAX = 10000.0
        const val SQRT2 = 1.41421356237
    }
}