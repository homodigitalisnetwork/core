package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.*

/**
 * States that [Chrono] can reach
 */
@Target(PROPERTY, TYPE, PROPERTY, VALUE_PARAMETER, TYPE)
@Retention(SOURCE)
@IntDef
annotation class StopWatch {
    companion object {
        const val CREATED = 0
        const val RUNNING = 1
        const val PAUSED = 2
        const val STOPPED = 3
    }
}