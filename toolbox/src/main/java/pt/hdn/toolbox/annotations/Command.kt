package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Flag to indicate what type of operation is to be executed by the server
 */
@Retention(SOURCE)
@IntDef
annotation class Command {
    companion object {
        const val REMOVE = -2
        const val ROLLBACK = -1
        const val GET = 0
        const val ADD = 1
        const val SET = 2
        const val RECOVER = 3
    }
}