package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Behavioral modes available for any [ListAdapter] or [MapAdapter] classes
 */
@Retention(SOURCE)
@IntDef
annotation class Mode {
    companion object {
        const val NOT_EXPANDABLE = -1
        const val SINGLE_SELECTION = 0
        const val MULTIPLE_SELECTION = 1
    }
}