package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Available request flags to be used on [PendingIntent]s attach to notifications
 */
@Retention(SOURCE)
@StringDef
annotation class Request {
    companion object {
        const val ACCEPT_TRANSACTION = 1
        const val REJECT_TRANSACTION = 2
    }
}