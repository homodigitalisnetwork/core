package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.*

/**
 * Flag used on a transaction publisher to indicate that side of the code belongs to and execute accordingly
 */
@Target(PROPERTY, VALUE_PARAMETER, TYPE)
@Retention(SOURCE)
@IntDef
annotation class Side {
    companion object {
        const val BUYER = 1
        const val SELLER = 2
        const val BOTH = 3
    }
}