package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Trophies IDs used on the last step on a transaction to identify which trophy was selected
 */
@Retention(SOURCE)
@IntDef
annotation class Trophy {
    companion object {
        const val PRO = 1
        const val OVERACHIEVER = 2
        const val WELL_MANNER = 3
        const val CLEAN_WORK = 4
        const val QUICK_WORK = 5
    }
}