package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * In-app destinations catalogue to be used to validate the last path segment in a deep link
 */
@Retention(SOURCE)
@StringDef
annotation class Destination {
    companion object {
            const val REGISTRY = "registry"
    }
}