package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

@Target(PROPERTY, VALUE_PARAMETER)
@Retention(SOURCE)
@IntDef
annotation class ThresholdId() {
    companion object {
        const val BALANCE = 0
        const val CAPITAL_RESERVE = 1
        const val ASSET = 2
    }
}
