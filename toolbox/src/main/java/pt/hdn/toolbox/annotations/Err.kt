package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.*

/**
 * Error catalogue to be used across the app
 */
@Target(PROPERTY, LOCAL_VARIABLE, VALUE_PARAMETER, FUNCTION, TYPE)
@Retention(SOURCE)
@IntDef
annotation class Err {
    companion object {
        const val NONE = 100
        const val UNKNOWN = 101
        const val USER = 102
        const val INVALID  = 103
        const val SESSION  = 104
        const val PASSPHRASE  = 105
        const val COMMAND  = 106
        const val EXEC  = 107
        const val API  = 108
        const val CREDENTIALS  = 109
        const val FIREBASE  = 110
        const val FIRESTORE  = 111
        const val LOCATION  = 112
        const val NO_CHANGE  = 113
        const val SCHEMA  = 114
        const val SOURCE  = 115
        const val LOWER_BOUND  = 116
        const val UPPER_BOUND  = 117
        const val THRESHOLD  = 118
        const val IS_ABOVE  = 119
        const val DIFFERENCE  = 120
        const val NO_BOUND  = 121
        const val FIRST_NAME  = 122
        const val MIDDLE_NAMES  = 123
        const val LAST_NAME  = 124
        const val PHONE  = 125
        const val EMAIL  = 126
        const val WEB  = 127
        const val LANGUAGES  = 128
        const val TIN  = 129
        const val STREET  = 130
        const val DOOR  = 131
        const val ZIP  = 132
        const val POSITION  = 133
        const val PHOTO  = 134
        const val USER_NAME  = 135
        const val PASSWORD  = 136
        const val GDPR  = 137
        const val PP  = 138
        const val TIMEOUT  = 139
        const val PLATE  = 140
        const val TYPE  = 141
        const val DEBT  = 142
        const val IMEI  = 143
        const val AMOUNT  = 144
        const val DEADLINE  = 145
        const val ACCOUNT  = 146
        const val BRAND_AND_MODEL  = 147
        const val DAY  = 148
        const val MONTH  = 149
        const val DEPENDENTS  = 150
        const val CIVIL_STATUS  = 151
        const val REGIONS  = 152
        const val CENTER  = 153
        const val RADIUS  = 154
        const val TAXES  = 155
        const val TIERS  = 156
        const val TYPE_1  = 157
        const val TYPE_2  = 158
        const val TYPE_3  = 159
        const val TYPE_4  = 160
        const val TYPE_5  = 161
        const val TYPE_6  = 162
        const val TYPE_7  = 163
        const val TYPE_8  = 164
        const val TYPE_9  = 165
        const val TYPE_10  = 166
        const val SPECIALITIES  = 167
        const val COUNTRY  = 168
        const val HANDICAPPED = 169
    }
}