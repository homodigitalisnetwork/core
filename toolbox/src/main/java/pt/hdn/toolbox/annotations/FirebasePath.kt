package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Child or path names found in Firebase Database
 */
@Retention(SOURCE)
@StringDef
annotation class FirebasePath {
    companion object {
        const val ABORT = "abort"
        const val APPS = "apps"
        const val ACKNOWLEDGE = "ack"
        const val BUYER = "buyer"
        const val CATALOGUE_LOCALE = "catalogue/locale"
//        const val CATALOGUE_SERVICE = "catalogue/service"
        const val CATALOGUE_SPECIALITIES = "catalogue/specialities"
        const val CHATS = "chats"
        const val CHRONO = "chrono"
//        const val DEBUG_DIR = "debug"
        const val DEPUTIES = "deputies"
        const val CONTRACT = "contract"
//        const val CONTRACTS = "contracts"
//        const val DEVICE = "device"
        const val DISTANCE = "distance"
//        const val INSURANCES = "insurances"
        const val KILL = "kill"
        const val LEVEL = "level"
        const val LOCALE = "locale"
        const val LOCATION = "location"
        const val LOGIN = "login"
//        const val MARKET_ACCESS = "marketAccess"
        const val NAME = "name"
        const val NOTIFICATIONS = "notifications"
        const val PARTIALS = "partials"
//        const val PARTNERSHIP = "partnership"
//        const val PARTNERSHIPS = "partnerships"
        const val PAUSE = "pause"
        const val PAUSE_LENGTH = "pauseLength"
        const val POSITION = "position"
        const val RATING = "rating"
//        const val RESPONSE = "response"
        const val RING = "ring"
        const val SEARCHABLE = "searchable"
        const val SELLER = "seller"
//        const val SESSION = "session"
        const val SHIFT = "shift"
        const val SHIFTS = "shifts"
        const val START = "start"
        const val STOP = "stop"
        const val THUMBNAILS = "thumbnails"
        const val TRANSACTION = "transaction"
        const val TRANSACTIONS = "transactions"
        const val VARIABLES = "variables"
        const val USERS = "users"
        const val UUID = "uuid"
    }
}