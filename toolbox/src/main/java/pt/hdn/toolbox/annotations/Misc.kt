package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Miscellaneous information for general usage in the app
 */
@Retention(SOURCE)
@StringDef
annotation class Misc {
    companion object {
        const val DEFAULT_LOCALE = "en_GB"
        const val JPEG_SUFFIX = ".jpg"
        const val PATTERN = "dd/MM"
        const val PICS_PATH = "pics"
        const val GOOGLE_MAPS_URL = "https://www.google.com/maps/dir/?api=1&destination=%f%%2C%f&travelmode=driving&dir_action=navigate"
        const val WAZE_URL = "https://www.waze.com/ul?ll=%f%%2C%f&navigate=yes&zoom=17"
        const val SPOT_URL = "https://play.google.com/store/apps/details?id=pt.hdn.spot"
    }
}