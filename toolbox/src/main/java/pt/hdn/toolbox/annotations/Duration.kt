package pt.hdn.toolbox.annotations

import androidx.annotation.LongDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Durations catalogue to used across the app
 */
@Retention(SOURCE)
@LongDef
annotation class Duration {
    companion object {
        const val x1 = 1000L
        const val x5 = 5000L
        const val x10 = 10000L
        const val x20 = 20000L
        const val x30 = 30000L
        const val x60 = 60000L
        const val x3600 = 3600000L
    }
}