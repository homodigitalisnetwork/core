package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

/**
 * Annotation to indicate what kind of action is desired to be execute while interacting with any database
 */
@Target(PROPERTY, VALUE_PARAMETER)
@Retention(SOURCE)
@IntDef
annotation class Action {
    companion object {
        const val ROLLBACK = -10
        const val PENDING = -9
        const val REMOVE = -8
        const val UNKNOWN = -7
        const val ERROR = -6
        const val LOGOUT = -5
        const val ABORT = -4
        const val CANCEL = -3
        const val UNAVAILABLE = -2
        const val REJECT = -1
        const val ENQUIRE = 0
        const val ACCEPT = 1
        const val CONCLUDE = 2
        const val RECOVER = 3
        const val ADD = 4
        const val SET = 5
        const val GET = 8
    }
}