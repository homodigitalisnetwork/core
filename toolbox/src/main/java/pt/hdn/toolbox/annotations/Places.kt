package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * IDs for In-app structural location to be used in [Clearance]
 */
@Retention(SOURCE)
@IntDef
annotation class Places {
    companion object {
        const val VISIBILITY = 0
        const val PROFILE = 1
        const val PARTNERSHIPS = 2
        const val ASSETS = 3
        const val EXPENDITURES = 4
        const val SPOTTER = 5
        const val TODO = 6
        const val REPORT = 7
        const val SINGLE = 8
        const val RECURRENCE = 9
        const val SHIFT = 10
        const val BALANCE = 11
        const val SPECIALITIES = 12
        const val ACCOUNTING = 13
        const val PERFORMANCE = 14
    }
}