package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * UUIDs of different report types that are available to be request from the server
 */
@Retention(SOURCE)
@StringDef
annotation class Report {
    companion object {
        const val ACTIVITY = "3ff6e654-a09d-11ec-960f-ac1f6bab0326"
        const val EXCHANGE = "5dc85396-a09d-11ec-960f-ac1f6bab0326"
    }
}