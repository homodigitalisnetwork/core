package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER
import kotlin.annotation.AnnotationTarget.FUNCTION

/**
 * View type indicator used in [SellerAdapter]
 */
@Target(PROPERTY, VALUE_PARAMETER, FUNCTION)
@Retention(SOURCE)
@IntDef
annotation class Seller {
    companion object {
        const val INTRO = 0
        const val SCORE = 1
        const val PERSON_MARKET = 2
        const val SERVICE_MARKET = 3
    }
}