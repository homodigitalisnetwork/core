package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

@Target(PROPERTY, VALUE_PARAMETER)
@Retention(SOURCE)
@StringDef
annotation class SpecialityId() {
    companion object {
        const val CAR_DRIVER = "0x11ec49e4427a4fe29aeeac1f6bab0326"
        const val CEO = "0x11ec49e4427a4ed19aeeac1f6bab0326"
    }
}
