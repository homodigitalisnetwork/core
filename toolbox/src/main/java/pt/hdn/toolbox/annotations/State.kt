package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * States that the [LocationProvider] can reach
 */
@Retention(SOURCE)
@IntDef
annotation class State {
    companion object {
        const val IDLE = 0
        const val RUNNING = 1
        const val TO_DB = 2
        const val TO_FB = 3
    }
}