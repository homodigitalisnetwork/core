package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Notification IDs used in the app
 */
@Retention(SOURCE)
@IntDef
annotation class Notification {
    companion object {
        const val BELL = 1
        const val FOREGROUND = 2
        const val PROPOSAL = 3
        const val REPLY = 4
        const val TRANSACTION = 5
        const val WARNING = 6
    }
}