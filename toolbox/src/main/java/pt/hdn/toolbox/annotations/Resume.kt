package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER
import kotlin.annotation.AnnotationTarget.FUNCTION

/**
 * View types indicator to be uses in [ResumeAdapter]
 */
@Target(PROPERTY, VALUE_PARAMETER, FUNCTION)
@Retention(SOURCE)
@IntDef
annotation class Resume {
    companion object {
        const val SCORE = 0
        const val PERFORMANCE = 1
        const val SCHEMAS = 2
        const val TRADE = 3
    }
}