package pt.hdn.toolbox.annotations

import kotlin.annotation.AnnotationRetention.SOURCE

@Retention(SOURCE)
@DoubleDef
annotation class Media {
    companion object {
        const val THUMBNAIL_SIZE = 512.0
    }
}