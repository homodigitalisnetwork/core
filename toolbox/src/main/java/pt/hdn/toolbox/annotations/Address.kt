package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER
import kotlin.annotation.AnnotationTarget.TYPE

/**
 * DataBus addresses
 */
@Target(PROPERTY, VALUE_PARAMETER, VALUE_PARAMETER, TYPE)
@Retention(SOURCE)
@IntDef
annotation class Address {
    companion object {
        const val SWIPE: Int = 28
        const val PROPOSAL: Int = 27
        const val LOCATION: Int = 26
        const val PERMISSION: Int = 25
        const val ADDRESS_5: Int = 24
        const val ADDRESS_4: Int = 23
        const val ADDRESS_3: Int = 22
        const val ADDRESS_2: Int = 21
        const val ADDRESS_1: Int = 20
        const val VIEW_3: Int = 19
        const val CLICK_3: Int = 18
        const val TAXES_VERSION =17
        const val CLICK_2: Int = 16
        const val TRANS_COMM: Int = 15
        const val UPDATE: Int = 14
        const val SCHEMA: Int = 13
        const val VIEW_2: Int = 12
        const val VIEW_1: Int = 11
        const val CLICK_1: Int = 10
        const val MENU: Int = 10
        const val PARTNERSHIP: Int = 9
        const val PAGEABLE: Int = 8
        const val SPECIALITY: Int = 7
        const val LOGIN: Int = 6
        const val CONTRACT: Int = 5
        const val ABORT: Int = 4
        const val ENQUIRE: Int = 3
        const val TRANSACTION = 2
        const val CANCEL = 1
        const val LOGOUT = 0
    }
}