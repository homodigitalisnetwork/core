package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.*

/**
 * Levels to be used by any publisher type class
 */
@Target(PROPERTY, VALUE_PARAMETER, TYPE)
@Retention(SOURCE)
@IntDef
annotation class Level {
    companion object {
        const val ESCAPED = -800
        const val ESCAPING = -700
        const val CANCELED = -600
        const val CANCELING = -500
        const val TERMINATED = -400
        const val TERMINATING = -300
        const val ABORTED = -200
        const val ABORTING = -100
        const val INITIATING = 0
        const val INITIATED = 100
        const val PROPOSE = 200
        const val PROPOSED = 300
        const val REQUESTING = 400
        const val REQUESTED = 500
        const val REQUIRING = 600
        const val REQUIRED = 700
        const val NAVIGATING = 800
        const val ONROUTE = 900
        const val NEAR = 1000
        const val IN = 1100
        const val NAVIGATED = 1200
        const val PREPARING = 1300
        const val PREPARED = 1400
        const val WORKING = 1500
        const val WORKED = 1600
        const val FINISHING = 1700
        const val FINISHED = 1800
        const val CONCLUDING = 1900
        const val CONCLUDED = 2000
        const val EXITING = 2100
        const val EXITED = 2200
    }
}
