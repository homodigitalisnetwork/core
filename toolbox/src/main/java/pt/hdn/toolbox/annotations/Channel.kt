package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * Channel names for notifications
 */
@Retention(SOURCE)
@StringDef
annotation class Channel {
    companion object {
        const val GPS = "gps"
        const val REQUEST = "request"
        const val RESPONSE = "response"
        const val RING = "ring"
        const val TRANSACTION = "important"
    }
}