package pt.hdn.toolbox.annotations

import androidx.annotation.IntDef
import kotlin.annotation.AnnotationRetention.SOURCE

@Retention(SOURCE)
@IntDef
annotation class Visibility {
    companion object {
        const val GONE = -1
        const val DISABLE = 0
        const val ENABLE = 1
    }
}