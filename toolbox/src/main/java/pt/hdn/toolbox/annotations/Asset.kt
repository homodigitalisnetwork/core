package pt.hdn.toolbox.annotations

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

//@Target(PROPERTY, VALUE_PARAMETER)
//@Retention(SOURCE)
//@StringDef
//annotation class Asset {
//    companion object {
//        const val VEHICLE = "0x11ec64be4e83cbea960fac1f6bab0326"
//        const val CELLPHONE = "0x11ec64be4e83ce04960fac1f6bab0326"
//        const val SIM_CARD = "0x11ec64be4e83ce04960fac1f6bab0326"
//        const val REAL_ESTATE = "0x11ec64be4e83cf8d960fac1f6bab0326"
//    }
//}