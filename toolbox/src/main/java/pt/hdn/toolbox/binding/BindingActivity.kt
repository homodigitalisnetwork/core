package pt.hdn.toolbox.binding

import android.os.Bundle
import android.view.Gravity.TOP
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEach
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.*
import androidx.navigation.fragment.NavHostFragment
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_INDEFINITE
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.Flow
import pt.hdn.toolbox.R
import pt.hdn.toolbox.application.Connectivity
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.misc.Keyboard
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.resources.Resources
import javax.inject.Inject

/**
 * Base [Activity] class that handles the [ViewBinding] associated with it and provides basic standardise resources and navigation functionality
 */
abstract class BindingActivity<out VB: ViewBinding>(private val inflate: (LayoutInflater) -> VB) : AppCompatActivity() {

    //region vars
    @Inject lateinit var res: Resources
    @Inject @Connectivity lateinit var connectivity: Flow<Boolean>
    protected var snackbar: Snackbar? = null
    private var nullableBinding: VB? = null
    val binding: VB; get() = nullableBinding!!
    var navController: NavController? = null
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.nullableBinding = inflate.invoke(layoutInflater).apply { root.let { setContentView(it); recursiveSearch(it as ViewGroup) } }

        connectivity.observe(this) { this@BindingActivity.snackbar = if (it) snackbar?.run { dismiss(); null } else createSnackBar(binding.root) }
    }

    override fun onDestroy() { this.nullableBinding = null; super.onDestroy() }

    protected fun dimen(@DimenRes id: Int): Int = res.dimen(id)

    protected fun color(@ColorRes id: Int): Int = res.color(id)

    protected fun string(@StringRes id: Int) = res.string(id)

    protected fun string(format: String, vararg args: Any?): String = res.string(format, args)

    protected fun toast(@StringRes id: Int) { Toast.makeText(this, string(id), LENGTH_LONG).show() }

    fun navigateUp() = navController?.navigateUp()

    fun navigateTo(directions: NavDirections, navOptions: NavOptions? = null) {
        navController?.takeIf { it.currentDestination?.run { getAction(directions.actionId)?.destinationId?.let { it != id } } == true }?.navigate(directions, navOptions)
    }

    fun popBack() = navController?.popBackStack()

    fun popBackTo(@IdRes destinationId: Int, inclusive: Boolean = true) = navController?.popBackStack(destinationId, inclusive)

    private fun createSnackBar(v: View): Snackbar {
        return Snackbar.make(v, string(R.string.noInternet), LENGTH_INDEFINITE)
            .apply { view.layoutParams = (view.layoutParams as FrameLayout.LayoutParams).apply { gravity = TOP }; show() }
    }

    private fun recursiveSearch(viewGroup: ViewGroup): Boolean {
        if (viewGroup is FragmentContainerView) { assignNavController(viewGroup.id); return true }
        else {
            viewGroup.forEach { if (it is FragmentContainerView) { assignNavController(it.id); return true } else if (it is ViewGroup) { if (recursiveSearch(it)) { return true } } }

            return false
        }
    }

    private fun assignNavController(@IdRes id: Int) { this.navController = (supportFragmentManager.findFragmentById(id) as NavHostFragment).navController }
}