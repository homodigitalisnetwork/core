package pt.hdn.toolbox.binding

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.activity.OnBackPressedDispatcher
import androidx.annotation.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_INDEFINITE
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.Flow
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.application.Connectivity
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.misc.Keyboard
import pt.hdn.toolbox.misc.OnFocusRequestListener
import pt.hdn.toolbox.misc.getColorAttr
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.resources.Resources
import javax.inject.Inject

/**
 * Base [Fragment] class that handles the [ViewBinding] associated with it and provides basic standardise resources and navigation functionality
 */
abstract class BindingFragment<out VB : ViewBinding>(private val inflate: (LayoutInflater) -> VB) : Fragment() {

    //region vars
    @Inject lateinit var res: Resources
    @Inject @Connectivity lateinit var connectivity: Flow<Boolean>
    @Inject lateinit var inputMethodManager: InputMethodManager
    @Inject lateinit var dataBus: DataBus
    private var mutableKeyboard: Keyboard? = null
    protected var snackbar: Snackbar? = null
    protected var index: Int = -1
    private var nullableBinding: VB? = null; private set
    var onFocusRequestListener: OnFocusRequestListener? = null
    val keyboard: Keyboard; get() = mutableKeyboard!!
    val binding: VB?; get() = nullableBinding
    val onBackPressedDispatcher: OnBackPressedDispatcher; get() = requireActivity().onBackPressedDispatcher
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState); this.index = arguments?.getInt(Field.POSITION) ?: -1 }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflate(inflater).also { this.nullableBinding = it }.root.also { this.mutableKeyboard = Keyboard(inputMethodManager, it) }
    }

    override fun onDestroyView() { this.nullableBinding = null; this.mutableKeyboard = null; super.onDestroyView() }

    protected fun dimen(@DimenRes id: Int): Int = res.dimen(id)

    protected fun color(@ColorRes id: Int): Int = res.color(id)

    protected fun drawable(@DrawableRes id: Int): Drawable? = requireContext().getDrawable(id)

    protected fun colorAttr(@AttrRes id: Int): Int = requireContext().getColorAttr (id)

    protected fun string(@StringRes id: Int) = res.string(id)

    protected fun string(format: String, vararg args: Any?): String = res.string(format, args)

    protected fun stringArray(@ArrayRes id: Int): Array<String> = res.stringArray(id)

    protected fun toast(@StringRes id: Int) { Toast.makeText(requireContext(), string(id), LENGTH_LONG).show() }

    protected fun createSnackBar(view: View): Snackbar = Snackbar.make(view, string(R.string.noInternet), LENGTH_INDEFINITE).apply { show() }

    protected fun handleError(view: View) { onFocusRequestListener?.onFocusRequest(index); view.requestFocus() }

    fun Fragment.navigateUp() { findNavController().navigateUp() }

    fun Fragment.navigateTo(directions: NavDirections?, navOptions: NavOptions? = null) {
        directions?.let { findNavController().takeIf { it.currentDestination?.run { getAction(directions.actionId)?.destinationId?.let { it != id } } == true }?.navigate(it, navOptions) }
    }

    fun Fragment.popBack() { findNavController().popBackStack() }

    fun Fragment.popBackTo(@IdRes destinationId: Int, inclusive: Boolean = true) { findNavController().popBackStack(destinationId, inclusive) }
}