package pt.hdn.toolbox.binding

import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.KeyEvent.ACTION_UP
import android.view.KeyEvent.KEYCODE_BACK
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.activity.OnBackPressedDispatcher
import androidx.annotation.*
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavGraph
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_INDEFINITE
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.Flow
import pt.hdn.toolbox.R
import pt.hdn.toolbox.application.Connectivity
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.misc.Keyboard
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.resources.Resources
import javax.inject.Inject

/**
 * Base [DialogFragment] class that handles the [ViewBinding] associated with it and provides basic standardise resources and navigation functionality
 */
abstract class BindingDialogFragment<out VB : ViewBinding>(private val inflate: (LayoutInflater) -> VB, private val inset: Int) : DialogFragment() {

    //region vars
    @Inject lateinit var res: Resources
    @Inject @Connectivity lateinit var connectivity: Flow<Boolean>
    @Inject lateinit var inputMethodManager: InputMethodManager
    @Inject lateinit var dataBus: DataBus
    private var mutableKeyboard: Keyboard? = null
    private var nullableBinding: VB? = null
    protected var snackbar: Snackbar? = null
    val keyboard: Keyboard; get() = mutableKeyboard!!
    val binding: VB?; get() = nullableBinding
    val onBackPressedDispatcher: OnBackPressedDispatcher; get() = requireActivity().onBackPressedDispatcher
    //endregion vars

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflate.invoke(inflater).also { this.nullableBinding = it }.root.also { this.mutableKeyboard = Keyboard(inputMethodManager, it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        connectivity.observe(viewLifecycleOwner) { this@BindingDialogFragment.snackbar = if (it) snackbar?.run { dismiss(); null } else createSnackBar(view) }
    }

    override fun onStart() {
        super.onStart()

        dialog
            ?.apply {
                setCancelable(false)
                setCanceledOnTouchOutside(false)
                setOnKeyListener { _, code, event -> if (code == KEYCODE_BACK && event.action == ACTION_UP) { this@BindingDialogFragment.onBackPressed(); true} else false } }
            ?.window
            ?.apply { setLayout(MATCH_PARENT, MATCH_PARENT); setBackgroundDrawable(InsetDrawable(getDrawable(requireContext(), R.drawable.bg_dialog), 40, inset, 40, inset)) }
    }

    override fun onDestroyView() { this.nullableBinding = null; this.mutableKeyboard = null; super.onDestroyView() }

    protected fun dimen(@DimenRes id: Int): Int = res.dimen(id)

    protected fun color(@ColorRes id: Int): Int = res.color(id)

    protected fun string(@StringRes id: Int) = res.string(id)

    protected fun string(format: String, vararg args: Any?): String = res.string(format, args)

    protected fun toast(@StringRes id: Int) { Toast.makeText(requireContext(), string(id), LENGTH_LONG).show() }

    private fun createSnackBar(view: View): Snackbar = Snackbar.make(view, string(R.string.noInternet), LENGTH_INDEFINITE).apply { show() }

    fun Fragment.navigateTo(directions: NavDirections, navOptions: NavOptions? = null) {
        findNavController().takeIf { it.currentDestination?.run { getAction(directions.actionId)?.destinationId?.let { it != id } } == true }?.navigate(directions, navOptions)
    }

    fun Fragment.popBack() { findNavController().popBackStack() }

    fun Fragment.navGraph(): NavGraph = findNavController().graph

    fun Fragment.popBackTo(@IdRes destinationId: Int, inclusive: Boolean = true) { findNavController().popBackStack(destinationId, inclusive) }

    open fun onBackPressed() { popBack() }
}