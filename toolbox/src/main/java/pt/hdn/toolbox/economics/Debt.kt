package pt.hdn.toolbox.economics

import java.math.BigDecimal
import java.time.ZonedDateTime

interface Debt: Cloneable {
	var debt: BigDecimal?
	var deadline: ZonedDateTime?
	var payback: BigDecimal?
}