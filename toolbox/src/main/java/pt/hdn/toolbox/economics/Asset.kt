package pt.hdn.toolbox.economics

import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.data.UUIDBrandModelData

interface Asset : Cloneable, Debt {
	var uuid: String?
	val resourceUUID: String?
	var modelUUID: String?
	var type: UUIDBrandModelData?

	public override fun clone(): Asset

	@Err fun validate(asset: Asset?): Int

	infix fun reducedBy(asset: Asset?): Asset?
}