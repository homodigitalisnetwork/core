package pt.hdn.toolbox.economics

import pt.hdn.toolbox.annotations.Err
import java.math.BigDecimal
import java.time.ZonedDateTime

interface Expenditure : Cloneable {
	var uuid: String?
	val name: String
	val amount: BigDecimal
	val expirationDate: ZonedDateTime?

	public override fun clone(): Expenditure

	@Err fun validate(expenditure: Expenditure?): Int

	infix fun reduce(expenditure: Expenditure?): Expenditure?
}