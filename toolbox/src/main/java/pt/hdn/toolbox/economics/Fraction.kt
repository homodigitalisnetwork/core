package pt.hdn.toolbox.economics

import com.google.gson.annotations.Expose
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

data class Fraction(
    @Expose val uuid: String = "",
    @Expose val id: Int = -1,
    @Expose val amount: BigDecimal = ZERO
)
