package pt.hdn.toolbox.economics

import android.os.Parcelable
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.misc.Reducible
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.util.*

interface Tax: Parcelable, Cloneable, Reducible<Tax> {
    var uuid: UUID
    var value: BigDecimal?

    public override fun clone(): Tax

    fun toMap(): Map<String, Any>

    @Err fun validate(tax: Tax? = null): Int {
        return when {
            this == tax -> Err.NO_CHANGE
            value == null -> Err.TYPE_1
            value!! < ZERO -> Err.INVALID
            else -> Err.NONE
        }
    }
}