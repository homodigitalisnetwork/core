package pt.hdn.toolbox.economics

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.annotations.Field.Companion.VALUE
import pt.hdn.toolbox.misc.takeUnlessWith
import java.math.BigDecimal
import java.util.*

@Parcelize
data class DefaultTax(
    @Expose override var uuid: UUID = UUID.randomUUID(),
    @Expose override var value: BigDecimal? = null
): Tax {
    companion object {
        fun from(snapshot: DataSnapshot) : DefaultTax{
            return with(snapshot.value as Map<String, Any?>) { DefaultTax(uuid = UUID.fromString(this[Field.UUID] as String), value = (this[VALUE] as String).toBigDecimal()) }
        }
    }

    override fun clone(): Tax = copy()

    override fun reducedBy(other: Tax?): Tax? { other?.also { return DefaultTax(uuid = it.uuid, value = takeUnless(value, it.value)).takeUnlessWith { value == null } }; return this }

    override fun toMap(): Map<String, Any> = mapOf(Field.UUID to uuid.toString(), VALUE to value!!.toString())
}