package pt.hdn.toolbox.transaction.util

import androidx.lifecycle.Lifecycle.State
import androidx.lifecycle.Lifecycle.State.RESUMED
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.CoroutineScope
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.CONCLUDING
import pt.hdn.toolbox.annotations.Side
import pt.hdn.toolbox.annotations.Side.Companion.BOTH

@DslMarker
annotation class TransactionScopeSubscribersDsl

@DslMarker
annotation class TransactionLifecycleSubscribersDsl

@TransactionScopeSubscribersDsl
class TransactionScopeSubscribers(
    val transactionPublisher: DefaultTransactionPublisher,
    val scope: CoroutineScope
) {

    fun subscribe(@Level left: Int, @Level right: Int = left, leftInclusive: Boolean = true, rightInclusive: Boolean = (left == right || right == CONCLUDING), callOnce: Boolean = true, @Side callOnSide: Int = BOTH, executeOnAborting: Boolean = (left >= CONCLUDING || right <= ABORTING), supplier: Observer.() -> Unit) {
        transactionPublisher.subscribe(scope, left, right, leftInclusive, rightInclusive, callOnce, callOnSide, executeOnAborting, supplier)
    }
}

@TransactionLifecycleSubscribersDsl
class TransactionLifecycleSubscribers(
    val transactionPublisher: DefaultTransactionPublisher,
    val owner: LifecycleOwner
) {
    fun subscribe(state: State = RESUMED, @Level left: Int, @Level right: Int = left, leftInclusive: Boolean = true, rightInclusive: Boolean = (left == right || right == CONCLUDING), callOnce: Boolean = true, @Side callOnSide: Int = BOTH, executeOnAborting: Boolean = (left >= CONCLUDING || right <= ABORTING), supplier: Observer.() -> Unit) {
        transactionPublisher.subscribe(owner, state, left, right, leftInclusive, rightInclusive, callOnce, callOnSide, executeOnAborting, supplier)
    }
}