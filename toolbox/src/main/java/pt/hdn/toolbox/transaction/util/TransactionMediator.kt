package pt.hdn.toolbox.transaction.util

import androidx.navigation.NavDirections
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.StopWatch
import pt.hdn.toolbox.annotations.Trophy
import pt.hdn.toolbox.communications.message.DefaultRef
import pt.hdn.toolbox.communications.message.Ref
import pt.hdn.toolbox.misc.Position
import java.math.BigDecimal
import java.util.*

interface TransactionMediator {

    //region vars
    val position: Flow<LatLng>
    val distance: Flow<Double>
    val buyerLatLng: LatLng
    val isBuyerSide: Boolean
    val transactionUUID: UUID
    val buyerPosition: Position
    val receiverName: String
    val buyerAppUUID: UUID
    val isRated: Boolean
    var rating: Int?
    var buyerAddress: Flow<String>
    @Trophy var trophy: Int?
    val state: StateFlow<Pair<@StopWatch Int, Long>>
    val elapsedTimeSeconds: Long
    @StopWatch val previousState: Int
    //endregion vars

    suspend fun enquire()
    suspend fun cancel()
    suspend fun reject()
    suspend fun accept()
    suspend fun contract(contract: Contract)
    fun signatureDialog(): NavDirections?
    suspend fun level(@Level level: Int)
    suspend fun attachLeveListener(block: (level: Int) -> Unit)
    suspend fun attachContractListener()
    suspend fun attachRingListener()
    suspend fun attachPositionListener()
    suspend fun ring()
    suspend fun attachChronoListeners()
    suspend fun pause()
    suspend fun work()
    suspend fun finish()
//    suspend fun conclude(abort: Boolean)
    suspend fun positionToAddress()
    fun invoice(abort: Boolean, revenue: BigDecimal, block: (details: List<Pair<String?, BigDecimal>>, commission: BigDecimal, tax: BigDecimal, total: BigDecimal) -> Unit)
}