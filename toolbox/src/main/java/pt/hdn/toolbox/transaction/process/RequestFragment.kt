package pt.hdn.toolbox.transaction.process

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.FINISHING
import pt.hdn.toolbox.annotations.Level.Companion.NAVIGATING
import pt.hdn.toolbox.annotations.Level.Companion.PREPARING
import pt.hdn.toolbox.annotations.Level.Companion.INITIATING
import pt.hdn.toolbox.annotations.Level.Companion.WORKING
import pt.hdn.toolbox.annotations.Side.Companion.BUYER
import pt.hdn.toolbox.transaction.TransactionViewModel
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentRequestBinding
import pt.hdn.toolbox.misc.navHolderViewModel

@AndroidEntryPoint
class RequestFragment : BindingFragment<FragmentRequestBinding>(FragmentRequestBinding::inflate) {

    //region vars
    private val transactionViewModel: TransactionViewModel by navHolderViewModel()
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState); onBackPressedDispatcher.addCallback(this) { transactionViewModel.confirmationDialog(requireContext()) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(transactionViewModel) {
                lblRequestMessage.text = string(if (transactionMediator.isBuyerSide) R.string.requesting else R.string.responding)

                transactionPublisher
                    .using(viewLifecycleOwner) {
                        subscribe(left = ABORTING) { execute { _, _ -> navigateTo(RequestFragmentDirections.actionRequestFragmentToFinishFragment()) } }

                        subscribe(left = INITIATING) { execute { _, _ -> navigateTo(transactionMediator.signatureDialog()) } }

                        subscribe(left = NAVIGATING, right = PREPARING) { execute { _, _ -> navigateTo(RequestFragmentDirections.actionRequestFragmentToNavigationFragment()) } }

                        subscribe(left = WORKING, right = FINISHING) { execute { _, _ -> navigateTo(RequestFragmentDirections.actionRequestFragmentToWorkFragment()) } }

                        subscribe(left = FINISHING) { execute { _, _ -> navigateTo(RequestFragmentDirections.actionRequestFragmentToFinishFragment()) } }
                    }
            }
        }
    }
}