package pt.hdn.toolbox.transaction

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.*
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Action.Companion.ACCEPT
import pt.hdn.toolbox.annotations.Action.Companion.CANCEL
import pt.hdn.toolbox.annotations.Action.Companion.ENQUIRE
import pt.hdn.toolbox.annotations.Action.Companion.RECOVER
import pt.hdn.toolbox.annotations.Action.Companion.REJECT
import pt.hdn.toolbox.annotations.Action.Companion.UNAVAILABLE
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.CANCELING
import pt.hdn.toolbox.annotations.Level.Companion.CONCLUDING
import pt.hdn.toolbox.annotations.Level.Companion.ESCAPING
import pt.hdn.toolbox.annotations.Level.Companion.EXITING
import pt.hdn.toolbox.annotations.Level.Companion.FINISHING
import pt.hdn.toolbox.annotations.Level.Companion.NAVIGATING
import pt.hdn.toolbox.annotations.Level.Companion.PREPARING
import pt.hdn.toolbox.annotations.Level.Companion.REQUESTING
import pt.hdn.toolbox.annotations.Level.Companion.TERMINATING
import pt.hdn.toolbox.annotations.Level.Companion.WORKING
import pt.hdn.toolbox.annotations.Side.Companion.BUYER
import pt.hdn.toolbox.annotations.Side.Companion.SELLER
import pt.hdn.toolbox.annotations.StopWatch.Companion.PAUSED
import pt.hdn.toolbox.annotations.StopWatch.Companion.RUNNING
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.communications.bus.*
import pt.hdn.toolbox.communications.generic.Response
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.communications.transaction.TransactionRequest
import pt.hdn.toolbox.foreground.ForegroundService
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.observeWith
import pt.hdn.toolbox.notifications.ResponseNotification
import pt.hdn.toolbox.notifications.WarningNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.transaction.util.*
import pt.hdn.toolbox.viewmodels.CoreViewModel
import java.util.*
import javax.inject.Inject

/**
 *
 */
@SuppressLint("StaticFieldLeak")
@HiltViewModel
class TransactionViewModel @Inject constructor(
    @pt.hdn.toolbox.transaction.Action @Action private val action: Int,
    private val responseNotification: ResponseNotification,
    private val warningNotification: WarningNotification,
    val chatMediator: ChatMediator,
    val transactionPublisher: TransactionPublisher,
    val transactionMediator: TransactionMediator,
    foregroundServiceFlow: Flow<ForegroundService?>,
    transaction: DefaultTransaction,
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
 ) : CoreViewModel(dataBus, sessionBus, repo, resources), DefaultLifecycleObserver {

    //region vars
    private var foregroundService: ForegroundService? = null
    private var isFirstTime: Boolean = true
    var isForeground: Boolean = false
    val request: MutableSharedFlow<TransactionRequest> = MutableSharedFlow()
    val response: Flow<Result<Response>>
    //endregion vars

    init {
        launch(Default) { chatMediator.setChat() }

        launch(Default) { transactionMediator.positionToAddress() }

        with(transactionPublisher) {
            this@TransactionViewModel.response = request
                .flatMapLatest { repo.transaction(it) }
                .onEach { it.success { foregroundService?.resumeBroadcast(); exit() } }
                .shareIn(viewModelScope, Lazily)

            dataBus
                .using(viewModelScope) {
                    observe<Contract?>(Address.CONTRACT) { it?.let { transactionMediator.contract(it); nextLevel() } ?: run { previousLevel() } }
                    observeWith<Pair<@Action Int, UUID>>(Address.TRANS_COMM) {
                        if (second == transactionMediator.transactionUUID) {
                            with(responseNotification) {
                                when (first) {
    //                                LOGOUT -> exit()
                                    CANCEL /*seller side*/ -> { cancel(); terminate() }
                                    UNAVAILABLE /*buyer side*/ -> { unavailable(); terminate() }
                                    REJECT /*buyer side*/ -> { reject(); terminate() }
                                    ACCEPT /*buyer side*/ -> { accept() }
                                    else -> null
                                }
                            }
                        }
                    }
                }

            using(viewModelScope) {
                subscribe(left = ESCAPING, right = TERMINATING, rightInclusive = true) { execute { _, _ -> foregroundService?.resumeBroadcast() } }

                subscribe(left = CANCELING, callOnSide = BUYER) { execute { _, _ -> transactionMediator.cancel() } }

                subscribe(left = CANCELING, callOnSide = SELLER) { execute { _, _ -> transactionMediator.reject() } }

                subscribe(left = ABORTING, callOnSide = SELLER) { execute { level, _ -> if (level == WORKING) transactionMediator.finish() } }

                subscribe(left = ABORTING) { execute { _, _ -> if (!isForeground) warningNotification.abort() } }

                subscribe(left = REQUESTING, right = PREPARING, callOnSide = SELLER) {
                    execute { _, _ -> dataBus.observe<Boolean>(this@execute, Address.PERMISSION) { if (!it) abort() } }
                }

                subscribe(left = REQUESTING, callOnSide = BUYER) { execute { _, _ -> transactionMediator.enquire() } }

                subscribe(left = REQUESTING, right = FINISHING, callOnSide = BUYER) {
                    execute { _, _ -> with(transactionMediator) { launch { attachLeveListener { transactionPublisher.level = it } }; launch { attachContractListener() } } }
                }

                subscribe(left = NAVIGATING, callOnSide = SELLER) { execute { _, _ -> transactionMediator.accept() } }

                subscribe(left = NAVIGATING, right = PREPARING, callOnSide = BUYER) { execute { _, _ -> transactionMediator.attachPositionListener() } }

                subscribe(left = NAVIGATING, right = PREPARING, callOnSide = SELLER) {
                    execute { _, _ -> with(transactionMediator) { launch { attachPositionListener() }; distance.observe(this@execute) { transactionPublisher.levelByDistance(it) } } }
                }

                subscribe(left = NAVIGATING, right = PREPARING, callOnSide = BUYER) { execute { _, _ -> transactionMediator.attachRingListener() } }

                subscribe(left = NAVIGATING, right = PREPARING, callOnSide = SELLER) {
                    execute { _, _ -> with(transactionMediator) { foregroundService?.startBroadcast(transactionUUID, buyerPosition) } }

                    finally { foregroundService?.holdBroadcast() }
                }

                subscribe(left = NAVIGATING, right = FINISHING, rightInclusive = true, callOnSide = SELLER, callOnce = false) {
                    execute { level, _ -> transactionMediator.level(level) }
                }

                subscribe(left = NAVIGATING, right = FINISHING) {
                    execute { _, _ -> repo.abortListener(transactionMediator.transactionUUID).collect { it.success { if (this) abort(false) } } }
                }

//            subscribe(left = IN, callOnce = false, callOnSide = BUYER) { /*TODO: notificacao "nas proximidades"*/ }

                subscribe(left = PREPARING, callOnSide = SELLER, callOnce = false) { execute { _, _ -> transactionMediator.pause() } }

                subscribe(left = PREPARING, right = FINISHING, rightInclusive = true, callOnSide = BUYER) {
                    execute { _, _ ->
                        with(transactionMediator) {
                            attachChronoListeners()

                            state.observeWith(this@execute) { if (!isForeground) { with(warningNotification) { when (first) { PAUSED -> pause(); RUNNING -> play() } } } }
                        }
                    }
                }

                subscribe(left = WORKING, callOnSide = SELLER, callOnce = false) { execute { _, _ -> transactionMediator.work() } }

                subscribe(left = FINISHING, callOnSide = BUYER) { execute { _, _ -> if (!isForeground) warningNotification.finish() } }

                subscribe(left = FINISHING, callOnSide = SELLER) { execute { _, _ -> transactionMediator.finish() } }

                subscribe(left = CONCLUDING, callOnce = false) { execute { _, aborting -> if (transactionMediator.isRated || aborting) { request.emit(TransactionRequest.from(transaction)) } } }

                subscribe(left = EXITING, callOnce = false) { execute { _, _ -> warningNotification.clear(); responseNotification.clear() } }
            }

            foregroundServiceFlow
                .observe(viewModelScope) {
                    this@TransactionViewModel.foregroundService = it
                        ?.apply {
                            with(transactionPublisher) {
                                when (action) {
//                                    UNKNOWN -> {}
//                                    ERROR -> {}
                                    ENQUIRE /*buyer*/, ACCEPT /*seller*/ -> initiate()
                                    RECOVER -> recover()
                                }
                            }
                        }
                }
        }
    }

    override fun onResume(onLifecycleOwner: LifecycleOwner) { this.isForeground = true }

    override fun onPause(onLifecycleOwner: LifecycleOwner) { this.isForeground = false }

    fun confirmationDialog(context: Context) {
        MaterialAlertDialogBuilder(context)
            .setTitle(string(R.string.confirmation))
            .setMessage(string(R.string.areYouSure))
            .setPositiveButton(string(R.string.yes)) { dialog, _ -> launch { transactionPublisher.abort() }; dialog.dismiss() }
            .setNegativeButton(string(R.string.no), null)
            .show()
    }

    fun isFirstTime(): Boolean = if (isFirstTime) { this.isFirstTime = false; true } else false

    fun abort() { launch { transactionPublisher.abort() } }

    fun sendMessage(message: String) { launch { chatMediator.send(message) } }
}