package pt.hdn.toolbox.transaction.process

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentVoyageBinding

class VoyageFragment: BindingFragment<FragmentVoyageBinding>(FragmentVoyageBinding::inflate), OnClickListener {

    //region vars
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}