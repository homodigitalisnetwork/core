package pt.hdn.toolbox.transaction.util

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Duration.Companion.x1
import pt.hdn.toolbox.annotations.Duration.Companion.x3600
import pt.hdn.toolbox.annotations.Field.Companion.PAUSE
import pt.hdn.toolbox.annotations.Field.Companion.PAUSE_LENGTH
import pt.hdn.toolbox.annotations.Field.Companion.START
import pt.hdn.toolbox.annotations.Field.Companion.STOP
import pt.hdn.toolbox.annotations.StopWatch
import pt.hdn.toolbox.annotations.StopWatch.Companion.CREATED
import pt.hdn.toolbox.annotations.StopWatch.Companion.PAUSED
import pt.hdn.toolbox.annotations.StopWatch.Companion.RUNNING
import pt.hdn.toolbox.annotations.StopWatch.Companion.STOPPED
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.time.Instant
import kotlin.math.round

@Parcelize
data class Chrono(
    @Expose var start: Long?  = null,
    @Expose var pauseLength: Long = 0L,
    @Expose var pause: Long? = null,
    @Expose var stop: Long? = null
) : Parcelable {

    //region vars
    @StopWatch private val status: Int; get() = when { stop != null -> STOPPED; pause != null -> PAUSED; start != null -> RUNNING else -> CREATED }
    private val mutableState: MutableStateFlow<Pair<@StopWatch Int, Long>> = MutableStateFlow(status to elapsedTimeSeconds)
    val state: StateFlow<Pair<@StopWatch Int, Long>> = mutableState
    val elapsedTime: BigDecimal; get() = start?.let { ((stop ?: pause ?: Instant.now().toEpochMilli()) - it - pauseLength).toDouble() / x3600 }?.toBigDecimal() ?: ZERO
    val elapsedTimeSeconds: Long; get() = start?.let { round(((stop ?: pause ?: Instant.now().toEpochMilli()) - it - pauseLength).toDouble() / x1).toLong() } ?: 0L
    @StopWatch var previousState: Int = status; private set
    //endregion vars

    fun resume(pauseLength: Long? = null): Long {
        updatePreviousStatus()

        return run {
            (pauseLength ?: pause?.let { Instant.now().toEpochMilli() - it + this.pauseLength })?.also { this.pause = null; this.pauseLength = it } ?: this.pauseLength
        }.also { emitNewState() }
    }

    fun start(start: Long = Instant.now().toEpochMilli()): Long = start.also { updatePreviousStatus(); this.start = it; emitNewState() }

    fun pause(pause: Long = Instant.now().toEpochMilli()): Long = pause.also { updatePreviousStatus(); this.pause = it; emitNewState() }

    fun stop(stop: Long = Instant.now().toEpochMilli()): Long = stop.also { updatePreviousStatus(); this.stop = it; emitNewState() }

    fun toMap(): Map<String, Long> = mutableMapOf<String, Long>().apply { start?.let { put(START, it) }; put(PAUSE_LENGTH, pauseLength); pause?.let { put(PAUSE, it) }; stop?.let { put(STOP, it) } }

    private fun updatePreviousStatus() { this.previousState = status }

    private fun emitNewState() { mutableState.tryEmit(status to elapsedTimeSeconds) }
}