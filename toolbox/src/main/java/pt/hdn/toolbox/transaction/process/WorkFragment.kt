package pt.hdn.toolbox.transaction.process

import android.os.Bundle
import android.text.format.DateUtils.formatElapsedTime
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.animation.Animation
import android.view.animation.AnimationUtils.loadAnimation
import androidx.activity.addCallback
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.transaction.TransactionViewModel
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Duration.Companion.x1
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.FINISHING
import pt.hdn.toolbox.annotations.Level.Companion.PREPARING
import pt.hdn.toolbox.annotations.StopWatch.Companion.PAUSED
import pt.hdn.toolbox.annotations.StopWatch.Companion.RUNNING
import pt.hdn.toolbox.annotations.StopWatch.Companion.STOPPED
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentWorkBinding
import pt.hdn.toolbox.misc.navHolderViewModel
import pt.hdn.toolbox.misc.observeWith

@AndroidEntryPoint
class WorkFragment: BindingFragment<FragmentWorkBinding>(FragmentWorkBinding::inflate) {

    //region vars
    private val transactionViewModel: TransactionViewModel by navHolderViewModel()
    private lateinit var rotateForever: Animation
    private lateinit var runnable: Runnable
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);

        onBackPressedDispatcher.addCallback(this) { transactionViewModel.confirmationDialog(requireContext()) }

        this.rotateForever = loadAnimation(requireContext(), R.anim.rotate_forever)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding!!) {
            with(transactionViewModel) {
                this@WorkFragment.runnable = Runnable { binding?.lblWorkChrono?.apply { postDelayed(runnable, x1); text = formatElapsedTime(transactionMediator.elapsedTimeSeconds) } }

                lblWorkPaused.text = string(R.string.paused)

//                    connectivity
//                        .observe(viewLifecycleOwner) {
//                            if (it) { if (transactionMediator.previousState == RUNNING) work() }
//                            else { if (transactionMediator.state.value.first == RUNNING) pause() }
//                        }

                transactionPublisher
                    .using(viewLifecycleOwner) {
                        subscribe(left = ABORTING) { execute { _, _ -> navigateTo(WorkFragmentDirections.actionWorkFragmentToFinishFragment()) } }

                        subscribe(left = PREPARING, right = FINISHING) {
                            execute { _, _ ->
                                fun holdAnimation(time: Long) { lblWorkChrono.apply { removeCallbacks(runnable); text = formatElapsedTime(time) }; imgWorkLogo.clearAnimation() }

                                transactionMediator
                                    .state
                                    .observeWith(this) {
                                        when (first) {
                                            STOPPED -> holdAnimation(second)
                                            PAUSED -> { holdAnimation(second); lblWorkPaused.visibility = VISIBLE }
                                            RUNNING -> { lblWorkChrono.postDelayed(runnable, x1); imgWorkLogo.startAnimation(rotateForever); lblWorkPaused.visibility = GONE }
                                        }
                                    }
                            }
                        }

                        subscribe(left = FINISHING) { execute { _, _ -> navigateTo(WorkFragmentDirections.actionWorkFragmentToFinishFragment()) } }
                    }
            }
        }
    }
}