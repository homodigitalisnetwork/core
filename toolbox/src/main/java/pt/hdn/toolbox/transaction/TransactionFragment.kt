package pt.hdn.toolbox.transaction

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.View.VISIBLE
import android.view.animation.Animation
import android.view.animation.AnimationUtils.loadAnimation
import androidx.fragment.app.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.CONCLUDING
import pt.hdn.toolbox.annotations.Level.Companion.ESCAPING
import pt.hdn.toolbox.annotations.Level.Companion.EXITING
import pt.hdn.toolbox.annotations.Level.Companion.FINISHING
import pt.hdn.toolbox.annotations.Level.Companion.IN
import pt.hdn.toolbox.annotations.Level.Companion.NAVIGATING
import pt.hdn.toolbox.annotations.Level.Companion.NEAR
import pt.hdn.toolbox.annotations.Level.Companion.ONROUTE
import pt.hdn.toolbox.annotations.Level.Companion.PREPARING
import pt.hdn.toolbox.annotations.Level.Companion.TERMINATING
import pt.hdn.toolbox.annotations.Level.Companion.WORKING
import pt.hdn.toolbox.annotations.Side.Companion.BUYER
import pt.hdn.toolbox.annotations.Side.Companion.SELLER
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.databinding.FragmentTransactionBinding
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.onBottomSheetCallback

/**
 * A [Fragment] that inherits from [BindingFragment] and is responsible for the visual behaviour of the entire transaction process
 */
@AndroidEntryPoint
class TransactionFragment : BindingFragment<FragmentTransactionBinding>(FragmentTransactionBinding::inflate), OnClickListener {

    //region vars
    private val transactionViewModel: TransactionViewModel by viewModels()
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<FragmentContainerView>
    private lateinit var pulseAnimation: Animation
    private var activeButton1: FloatingActionButton? = null
    private var activeButton2: FloatingActionButton? = null
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState); this.pulseAnimation = loadAnimation(requireContext(), R.anim.pulse) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * There was no other way to initiate the [Fragment] responsible for the in-app chat service. It was desired to initiate on the xml file and avoid the boilerplate code
         */
        with(childFragmentManager) { findFragmentById(R.id.fcv_transactionChat) ?: run { beginTransaction().add(R.id.fcv_transactionChat, ChatFragment()).commit() } }

        with(binding!!) {
            with(transactionViewModel) {
                this@TransactionFragment.bottomSheetBehavior = from(fcvTransactionChat)
                    .apply {
                        addBottomSheetCallback(
                            onBottomSheetCallback {
                                onStateChanged { _, newState ->
                                    when (newState) {
                                        STATE_COLLAPSED -> keyboard.hide()
                                        STATE_EXPANDED -> { keyboard.show(); this@TransactionFragment.launch { chatMediator.readMessage() } }
                                    }
                                }
                            }
                        )

                        state = STATE_COLLAPSED
                    }

                lifecycle.addObserver(transactionViewModel)

                btnTransactionRing.setOnClickListener(this@TransactionFragment)
                btnTransactionChat.setOnClickListener(this@TransactionFragment)
                btnTransactionWork.setOnClickListener(this@TransactionFragment)
                btnTransactionPause.setOnClickListener(this@TransactionFragment)
                btnTransactionAbort.setOnClickListener(this@TransactionFragment)
                btnTransactionNext.setOnClickListener(this@TransactionFragment)
                btnTransactionFinish.setOnClickListener(this@TransactionFragment)
                btnTransactionConclude.setOnClickListener(this@TransactionFragment)

                response.observe(viewLifecycleOwner) { it.error { _, _ -> toast(R.string.failExe) } }

                transactionPublisher
                    .using(viewLifecycleOwner) {
//                        subscribe(viewLifecycleOwner, left = ESCAPING, callOnSide = BUYER) { execute { _, _ -> popBack() } }

                        subscribe(left = ESCAPING, right = TERMINATING, rightInclusive = true) { execute { _, _ -> popBack() } }

                        subscribe(left = ABORTING) { execute { _, _ -> clearActiveButton1(); clearActiveButton2(); btnTransactionAbort.hide(); btnTransactionConclude.show() } }

//                        subscribe(left = NAVIGATING, right = FINISHING, callOnSide = SELLER) {
//                            execute { _, _ ->
//                                connectivity.collectLatest {
//                                    if (it) { btnTransactionAbort.show(); btnTransactionChat.show(); activeButton1?.show(); activeButton2?.show(); btnTransactionConclude.show() }
//                                    else { btnTransactionAbort.hide(); btnTransactionChat.hide(); activeButton1?.hide(); activeButton2?.hide(); btnTransactionConclude.hide() }
//                                }
//                            }
//                        }

                        subscribe(left = NAVIGATING, right = FINISHING, rightInclusive = true, callOnce = false, callOnSide = SELLER) {
                            execute { level, _ ->
                                when (level) {
                                    ONROUTE -> { clearActiveButton1(); clearActiveButton2() }
                                    NEAR -> { btnTransactionRing.also { this@TransactionFragment.activeButton1 = it }.show(); clearActiveButton2() }
                                    IN -> { btnTransactionRing.also { this@TransactionFragment.activeButton1 = it }.show(); btnTransactionNext.also { this@TransactionFragment.activeButton2 = it }.show() }
                                    PREPARING -> { clearActiveButton1(); clearActiveButton2(); btnTransactionWork.also { this@TransactionFragment.activeButton1 = it }.show() }
                                    WORKING -> { clearActiveButton1(); btnTransactionPause.also { this@TransactionFragment.activeButton1 = it }.show(); btnTransactionFinish.show() }
                                    FINISHING -> { clearActiveButton1(); btnTransactionFinish.hide(); btnTransactionAbort.hide(); btnTransactionConclude.show() }
                                }
                            }
                        }

                        subscribe(left = NAVIGATING, right = CONCLUDING, executeOnAborting = true) {
                            execute { _, _ ->
                                chatMediator
                                    .newMessage
                                    .observe(this) {
                                        with(imgTransactionChat) { if ((bottomSheetBehavior.state == STATE_COLLAPSED) && it) startAnimation(pulseAnimation) else clearAnimation() }
                                    }
                            }
                        }

                        subscribe(left = NAVIGATING, right = CONCLUDING, executeOnAborting = true) {
                            execute { _, _ -> btnTransactionChat.show(); imgTransactionChat.visibility = VISIBLE }
                        }

                        subscribe(left = FINISHING, callOnSide = BUYER) { execute { _, _ -> btnTransactionAbort.hide(); btnTransactionConclude.show() } }

                        subscribe(left = CONCLUDING, callOnce = false) { execute { _, aborting -> if (!(transactionMediator.isRated || aborting)) toast(R.string.classificationRequired) } }

                        subscribe(left = EXITING, callOnce = false) { execute { _, _ -> popBack() } }
                    }
            }
        }
    }

    override fun onClick(view: View?) {
        view?.id
            ?.let {
                with(transactionViewModel) {
                    with(transactionPublisher) {
                        when (it) {
                            R.id.btn_transactionAbort -> confirmationDialog(requireContext())
                            R.id.btn_transactionChat -> with (bottomSheetBehavior) {
                                when (state) { STATE_COLLAPSED -> state = STATE_EXPANDED; STATE_EXPANDED -> state = STATE_COLLAPSED }
                            }
                            R.id.btn_transactionRing -> this@TransactionFragment.launch { transactionMediator.ring() }
                            R.id.btn_transactionNext -> prepare()
                            R.id.btn_transactionWork -> work()
                            R.id.btn_transactionPause -> pause()
                            R.id.btn_transactionFinish -> finish()
                            R.id.btn_transactionConclude -> conclude()
                            else -> null
                        }
                    }
                }
            }
    }

    private fun clearActiveButton1() { this.activeButton1 = activeButton1?.run { hide(); null } }

    private fun clearActiveButton2() { this.activeButton2 = activeButton2?.run { hide(); null } }
}