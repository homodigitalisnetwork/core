package pt.hdn.toolbox.transaction.util

import android.location.Geocoder
import androidx.navigation.NavDirections
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil.computeDistanceBetween
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import pt.hdn.contract.annotations.SourceType.Companion.COMPANY_REVENUE
import pt.hdn.contract.annotations.SourceType.Companion.DEPARTMENT_REVENUE
import pt.hdn.contract.annotations.SourceType.Companion.DISTANCE
import pt.hdn.contract.annotations.SourceType.Companion.INDIVIDUAL_REVENUE
import pt.hdn.contract.annotations.SourceType.Companion.NONE
import pt.hdn.contract.annotations.SourceType.Companion.TEAM_REVENUE
import pt.hdn.contract.annotations.SourceType.Companion.TIME
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Action.Companion.ACCEPT
import pt.hdn.toolbox.annotations.Action.Companion.CANCEL
import pt.hdn.toolbox.annotations.Action.Companion.ENQUIRE
import pt.hdn.toolbox.annotations.Action.Companion.REJECT
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.StopWatch
import pt.hdn.toolbox.annotations.StopWatch.Companion.CREATED
import pt.hdn.toolbox.annotations.StopWatch.Companion.RUNNING
import pt.hdn.toolbox.annotations.Trophy
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.notifications.RingNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.transaction.process.RequestFragmentDirections
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode.HALF_UP
import java.util.*

class DefaultTransactionMediator(
    private val res: Resources,
    private val repo: CoreRepo,
    private val transaction: DefaultTransaction,
    private val ringNotification: RingNotification,
    private val geocoder: Geocoder
): TransactionMediator {

    //region vars
    override val position: Flow<LatLng>
    override val distance: Flow<Double>
    override val isBuyerSide: Boolean; get() = transaction.isBuyerSide
    override val transactionUUID: UUID; get() = transaction.uuid
    override val buyerPosition: Position; get() =transaction.buyerPosition
    override val buyerLatLng: LatLng = buyerPosition.toLatLng()
    override val buyerAppUUID: UUID; get() = transaction.buyerAppUUID
    override val state: StateFlow<Pair<@StopWatch Int, Long>>; get() = chrono.state
    override val elapsedTimeSeconds: Long; get() = chrono.elapsedTimeSeconds
    override val receiverName: String; get() = if (isBuyerSide) sellerDeputyName else buyerDeputyName
    @StopWatch override val previousState: Int; get() = chrono.previousState
    private val mutablePosition: MutableSharedFlow<LatLng> = MutableSharedFlow(replay = 1)
    private val chrono: Chrono; get() = transaction.chrono
    private val mutableBuyerAddress: MutableStateFlow<String> = MutableStateFlow(res.string(R.string.unknownLocation))
    override val isRated: Boolean; get() = transaction.isRated
    @Trophy override var trophy: Int?; get() = transaction.trophy; set(value) { transaction.trophy = value }
    override var rating: Int?; get() = transaction.rating; set(value) { transaction.rating = value }
    override var buyerAddress: Flow<String> = mutableBuyerAddress
    private var contract: Contract; get() = transaction.contract!!; set(value) { transaction.contract = value }
    private val sellerDeputyName: String; get() = transaction.sellerDeputyName
    private val buyerDeputyName: String; get() = transaction.buyerDeputyName
    //endregion vars

    init {
        this.position = mutablePosition
            .distinctUntilChanged()
            .also { this.distance = it.map { computeDistanceBetween(buyerLatLng, it) }.onEach { transaction.setDistanceRecord(it) } }
    }

    override suspend fun cancel() { repo.notify(transaction.message(CANCEL)).collect { it.error { _, _ -> } } }

    override suspend fun reject() { repo.notify(transaction.message(REJECT)).collect { it.error { _, _ -> } } }

    override suspend fun accept() { repo.notify(transaction.message(ACCEPT)).collect { it.error { _, _ -> } } }

    override suspend fun enquire() { repo.notify(transaction.message(ENQUIRE)).collect { it.error { _, _ ->  } } }

    override fun signatureDialog(): NavDirections? {
        return with(contract) {
            if (hasSellerSigned && hasBuyerSigned) null
            else RequestFragmentDirections.actionRequestFragmentToSignatureDialogFragment(isBuyerSide, true, this)
        }
    }

    override suspend fun contract(contract: Contract) { this.contract = contract }

    override suspend fun level(@Level level: Int) { repo.level(transactionUUID, level).collect { it.error { _, _ ->  } } }

    override suspend fun attachLeveListener(block: (level: Int) -> Unit) { repo.levelListener(transactionUUID).collect { it.success { block(this) }.error { _, _ -> } } }

    override suspend fun attachContractListener() { repo.contractListener(transactionUUID).collectLatest { it.success { this@DefaultTransactionMediator.contract = this } } }

    override suspend fun attachPositionListener() { repo.positionListener(transactionUUID).collectLatest { it.success { mutablePosition.emit(toLatLng()) } } }

    override suspend fun attachRingListener() {
        with(transaction) { repo.ringListener(uuid).collectLatest { it.success { if (ring < this) { ring = this; ringNotification.ring() } }.error { _, _ -> } } }
    }

    override suspend fun ring() { with(transaction) { repo.ring(uuid, ++ring).collect() } }

    override suspend fun attachChronoListeners() {
        coroutineScope {
            with(repo) {
                with(chrono) {
                    launch { startListener(transactionUUID).collect { it.success { start(this) }.error { _, _ ->  } } }

                    launch { pauseListener(transactionUUID).collect { it.success { pause(this) }.error { _, _ ->  } } }

                    launch { resumeListener(transactionUUID).collect { it.success { resume(this) }.error { _, _ ->  } } }

                    launch { stopListener(transactionUUID).collect { it.success { stop(this) }.error { _, _ ->  } } }
                }
            }
        }
    }

    override suspend fun work() {
        with(chrono) { (if (state.value.first == CREATED) { repo.start(transactionUUID, start()) } else { repo.resume(transactionUUID, resume()) }).collect() }
    }

    override suspend fun pause() { with(chrono) { if (state.value.first == RUNNING) repo.pause(transactionUUID, pause()).collect() } }

    override suspend fun finish() { repo.stop(transactionUUID, chrono.stop()).collect() }

//    override suspend fun conclude(abort: Boolean) {
//        with(transaction) { if (rating != null || abort) repo.transaction(TransactionRequest.from(this)).collect { it.error { _, _ -> } } }
//    }

    override suspend fun positionToAddress() {
        with(buyerLatLng) {
            (try { geocoder.getFromLocation(latitude, longitude, 1) } catch (e: Exception) { null })
            ?.getOrNull(0)
            ?.getAddressLine(0)
            ?.let { mutableBuyerAddress.emit(it) }
        }
    }

    override fun invoice(abort: Boolean, revenue: BigDecimal, block: (details: List<Pair<String?, BigDecimal>>, commission: BigDecimal, tax: BigDecimal, total: BigDecimal) -> Unit) {
        with(transaction) {
            block(
                buildList {
                    contract
                        ?.tasks
                        ?.first()
                        ?.schemas
                        ?.forEachWith {
                            when (source) {
                                NONE, INDIVIDUAL_REVENUE, TEAM_REVENUE, DEPARTMENT_REVENUE, COMPANY_REVENUE -> calculate(revenue)
                                TIME -> calculate(chrono.elapsedTime)
                                DISTANCE -> calculate()
                                else -> ZERO
                            }.let { subTotal += it; add(res.schemas[id] to it) }
                        }

                    sortBy { it.first }
                },
                (commission * subTotal).setScale(2, HALF_UP),
                (subTotal * tax).setScale(2, HALF_UP),
                (subTotal + if (isBuyerSide) { subTotal * (commission + tax) } else ZERO).setScale(2, HALF_UP)
            )
        }
    }
}