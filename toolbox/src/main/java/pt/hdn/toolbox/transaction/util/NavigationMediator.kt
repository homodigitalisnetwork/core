package pt.hdn.toolbox.transaction.util

import android.animation.ObjectAnimator
import android.content.Context
import android.util.Property
import android.util.TypedValue
import androidx.annotation.ColorRes
import com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle
import com.google.maps.android.SphericalUtil.computeOffset
import com.google.maps.android.SphericalUtil.interpolate
import com.google.maps.android.ktx.addCircle
import com.google.maps.android.ktx.addMarker
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Duration.Companion.x1
import pt.hdn.toolbox.annotations.Field.Companion.POSITION
import pt.hdn.toolbox.annotations.Value.Companion.NEXT_RADIUS
import pt.hdn.toolbox.annotations.Value.Companion.RING_RADIUS
import pt.hdn.toolbox.annotations.Value.Companion.SQRT2
import pt.hdn.toolbox.misc.getColorAttr
import pt.hdn.toolbox.resources.Resources
import java.util.*
import kotlin.math.abs
import kotlin.math.sign

class NavigationMediator(
    private var googleMap: GoogleMap?,
    private val res: Resources,
    private val buyerPosition: LatLng,
    private var isFirstTime: Boolean,
    context: Context,
    isBuyerSide: Boolean,
    appUUID: UUID
) {

    //region vars
    private val buyerIcon: BitmapDescriptor
    private val sellerIcon: BitmapDescriptor
    private var sellerMarker: Marker? = null
    private var objectAnimator: ObjectAnimator? = null
    private val colorPrimaryContainer: Int = context.getColorAttr(R.attr.colorPrimaryContainer)
    private val colorOnPrimaryContainer: Int = context.getColorAttr(R.attr.colorOnPrimaryContainer)

    //endregion vars

    init {
        with(res.pins) {
            this@NavigationMediator.buyerIcon = (if (isBuyerSide) getOrDefault(appUUID, values.take(2).last()) else values.first()).bitmapDescriptor
            this@NavigationMediator.sellerIcon = (if (isBuyerSide) values.first() else getOrDefault(appUUID, values.take(2).last())).bitmapDescriptor

            googleMap
                ?.run {
                    uiSettings.apply { setAllGesturesEnabled(false); isMapToolbarEnabled = false }

                    mapType = MAP_TYPE_NORMAL

                    setMapStyle(loadRawResourceStyle(context, if (res.isDarkModeOn) R.raw.map_styling_dark else R.raw.map_styling_light))
                    setOnMarkerClickListener { true } //this is to avoid a marker to be center by double tapping in it

                    addCircle {
                        center(buyerPosition)
                        clickable(false)
                        strokeColor(colorOnPrimaryContainer)
                        strokePattern(listOf(Dash(10f), Gap(8f)))
                        strokeWidth(5f)
                        radius(RING_RADIUS)
                        zIndex(0f)
                    }

                    addCircle {
                        center(buyerPosition)
                        clickable(false)
                        strokeColor(colorOnPrimaryContainer)
                        strokeWidth(5f)
                        radius(NEXT_RADIUS)
                        zIndex(0f)
                    }

                    addMarker {
                        position(buyerPosition)
                        draggable(false)
                        icon(buyerIcon)
                        zIndex(0f)
                    }?.alpha = 0.75f
                }
        }
    }

    fun updateSellerPosition(position: LatLng) { googleMap?.updateSeller(position) }

    fun cancel() { objectAnimator?.cancel() }

    fun destroy() {cancel(); this.googleMap = googleMap?.run { clear(); null } }

    private fun color(@ColorRes id: Int): Int = res.color(id)

    private fun GoogleMap.updateSeller(position: LatLng) {
        when {
            sellerMarker == null -> {
                this@NavigationMediator.sellerMarker = addMarker {
                    position(position)
                    draggable(false)
                    icon(sellerIcon)
                    zIndex(1f)
                }

                focusGroup(position, isFirstTime())
            }
            objectAnimator == null -> {
                this@NavigationMediator.objectAnimator = ObjectAnimator
                    .ofObject(sellerMarker, Property.of(Marker::class.java, LatLng::class.java, POSITION), { fraction, start, end -> interpolate(start, end, fraction.toDouble()) }, position)
                    .apply { duration = x1; addUpdateListener { focusGroup(animatedValue as LatLng, false) }; start() }
            }
            else -> objectAnimator?.apply { cancel(); setObjectValues(position); start() }
        }
    }

    private fun isFirstTime(): Boolean = if (isFirstTime) { this.isFirstTime = false; true } else false

    private fun GoogleMap.focusGroup(sellerPosition: LatLng, smooth: Boolean) {
        val hypotenuse: Double = RING_RADIUS * SQRT2
        val buyerSWPosition: LatLng = computeOffset(buyerPosition, hypotenuse, 225.0)
        val buyerNEPosition: LatLng = computeOffset(buyerPosition, hypotenuse, 45.0)
        val swLat: Double = buyerSWPosition.latitude.coerceAtMost(sellerPosition.latitude)
        val neLat: Double = buyerNEPosition.latitude.coerceAtLeast(sellerPosition.latitude)
        var lngSWDelta: Double = buyerSWPosition.longitude - sellerPosition.longitude

        if (abs(lngSWDelta) > 180.0) lngSWDelta -= sign(lngSWDelta) * 360.0

        val swLon: Double = if (lngSWDelta >= 0.0) sellerPosition.longitude else buyerSWPosition.longitude
        var lngNEDelta: Double = buyerNEPosition.longitude - sellerPosition.longitude

        if (abs(lngNEDelta) > 180.0) lngNEDelta -= sign(lngNEDelta) * 360.0

        val neLon: Double = if (lngNEDelta >= 0.0) buyerNEPosition.longitude else sellerPosition.longitude

        (if (smooth) ::animateCamera else ::moveCamera).invoke(newLatLngBounds(LatLngBounds(LatLng(swLat, swLon), LatLng(neLat, neLon)), 250))
    }
}