package pt.hdn.toolbox.transaction.process

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri.parse
import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.activity.addCallback
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL
import com.google.android.gms.maps.model.*
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.transaction.TransactionViewModel
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.FINISHING
import pt.hdn.toolbox.annotations.Level.Companion.PREPARING
import pt.hdn.toolbox.annotations.Misc.Companion.GOOGLE_MAPS_URL
import pt.hdn.toolbox.annotations.Misc.Companion.WAZE_URL
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentNavigationBinding
import pt.hdn.toolbox.misc.navHolderViewModel
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.transaction.util.NavigationMediator
import pt.hdn.toolbox.views.OnMapCallback
import java.util.*
import java.util.Locale.US

@AndroidEntryPoint
class NavigationFragment: BindingFragment<FragmentNavigationBinding>(FragmentNavigationBinding::inflate), OnMapCallback, OnClickListener {

    //region vars
    private val transactionViewModel: TransactionViewModel by navHolderViewModel()
    private lateinit var navigationMediator: NavigationMediator
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState); onBackPressedDispatcher.addCallback(this) { transactionViewModel.confirmationDialog(requireContext()) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding!!) {
            with(transactionViewModel) {
                barNavigationLoading.visibility = VISIBLE

                if (!transactionMediator.isBuyerSide) {
                    layoutNavigationButtons.visibility = VISIBLE
                    btnNavigationWaze.setOnClickListener(this@NavigationFragment)
                    btnNavigationGoogleMaps.setOnClickListener(this@NavigationFragment)
                    transactionMediator.buyerAddress.observe(viewLifecycleOwner) { lblNavigationDestination.apply { visibility = VISIBLE; text = it } }
                }

                transactionPublisher
                    .using(viewLifecycleOwner) {
                        subscribe(left = ABORTING) { execute { _, _ -> navigateTo(NavigationFragmentDirections.actionNavigationFragmentToFinishFragment()) } }

                        subscribe(left = PREPARING, right = FINISHING) { execute { _, _ -> navigateTo(NavigationFragmentDirections.actionNavigationFragmentToWorkFragment()) } }

                    }

                mapNavigationMap.apply { onCreate(null); onResume(); getMapAsyncWithContext(this@NavigationFragment) }
            }
        }
    }

    override fun onResume() { super.onResume(); binding!!.mapNavigationMap.onResume() }

    override fun onPause() { binding?.mapNavigationMap?.onPause(); super.onPause() }

    override fun onStop() { navigationMediator.cancel(); binding?.mapNavigationMap?.onStop(); super.onStop() }

    override fun onSaveInstanceState(outState: Bundle) { binding?.mapNavigationMap?.onSaveInstanceState(outState); super.onSaveInstanceState(outState) }

    override fun onDestroyView() { navigationMediator.destroy(); super.onDestroyView() }

    override fun onLowMemory() { binding?.mapNavigationMap?.onLowMemory(); super.onLowMemory() }

    override fun GoogleMap.onMapReady() {
        with(transactionViewModel) {
            with(transactionMediator) {
                this@NavigationFragment.navigationMediator = NavigationMediator(this@onMapReady, res, buyerLatLng, isFirstTime(), requireContext(), isBuyerSide, buyerAppUUID)

                position.observe(viewLifecycleOwner) { binding!!.barNavigationLoading.visibility = GONE; navigationMediator.updateSellerPosition(it) }
            }
        }
    }

    override fun onClick(view: View?) { view?.id?.let { when (it) { R.id.btn_navigationWaze -> call(WAZE_URL); R.id.btn_navigationGoogleMaps -> call(GOOGLE_MAPS_URL) } } }

    private fun call(url: String) { with(transactionViewModel.transactionMediator.buyerLatLng) { startActivity(Intent(ACTION_VIEW, parse(url.format(US, latitude, longitude)))) } }
}