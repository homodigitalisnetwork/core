package pt.hdn.toolbox.transaction.util

import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

@DslMarker
annotation class ObserverDsl

@ObserverDsl
class Observer {

    //region vars
    var execute: Pair<CoroutineContext, suspend CoroutineScope.(level: Int, aborting: Boolean) -> Unit> = Pair(EmptyCoroutineContext, { _, _ -> }); private set
    var finally: Pair<CoroutineContext, suspend () -> Unit>? = null; private set
    //endregion vars

    fun execute(context: CoroutineContext = EmptyCoroutineContext, block: suspend CoroutineScope.(level: Int, aborting: Boolean) -> Unit) { this.execute = Pair(context, block) }

    fun finally(context: CoroutineContext = EmptyCoroutineContext, block: suspend () -> Unit) { this.finally = Pair(context, block) }
}