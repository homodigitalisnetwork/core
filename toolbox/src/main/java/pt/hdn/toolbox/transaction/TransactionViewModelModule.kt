package pt.hdn.toolbox.transaction

import android.app.NotificationManager
import android.content.Context
import android.location.Geocoder
import android.widget.RemoteViews
import androidx.lifecycle.SavedStateHandle
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.application.GenericNotificationView
import pt.hdn.toolbox.chat.DefaultChatAdapter
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.misc.remoteViews
import pt.hdn.toolbox.notifications.ResponseNotification
import pt.hdn.toolbox.notifications.RingNotification
import pt.hdn.toolbox.notifications.WarningNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.transaction.util.*
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class RingNotificationView

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class Action

@Module
@InstallIn(ViewModelComponent::class)
object TransactionViewModelModule {

    @ViewModelScoped
    @Provides
    fun providesResponseNotification(@ApplicationContext context: Context, res: Resources, @GenericNotificationView remoteViews: RemoteViews, manager: NotificationManager): ResponseNotification {
        return ResponseNotification(context, res, remoteViews, manager)
    }

    @ViewModelScoped
    @Provides
    fun providesWarningNotification(@ApplicationContext context: Context, res: Resources, @GenericNotificationView remoteViews: RemoteViews, manager: NotificationManager): WarningNotification {
        return WarningNotification(context, res, remoteViews, manager)
    }

    @ViewModelScoped
    @Provides
    fun providesRingNotification(@ApplicationContext context: Context, @RingNotificationView remoteViews: RemoteViews, manager: NotificationManager): RingNotification {
        return RingNotification(context, remoteViews, manager)
    }

    @ViewModelScoped
    @Provides
    fun providesTransaction(savedStateHandle: SavedStateHandle): DefaultTransaction = savedStateHandle.get<DefaultTransaction>(Field.TRANSACTION)!!

    @ViewModelScoped
    @Provides
    fun providesTransactionPublisher(repo: CoreRepo, transaction: DefaultTransaction): TransactionPublisher = DefaultTransactionPublisher(repo, transaction)

    @ViewModelScoped
    @Provides
    fun providesTransactionMediator(resources: Resources, repo: CoreRepo, transaction: DefaultTransaction, ringNotification: RingNotification, geocoder: Geocoder): TransactionMediator {
        return DefaultTransactionMediator(resources, repo, transaction, ringNotification, geocoder)
    }

    @Action
    @ViewModelScoped
    @Provides
    fun providesAction(savedStateHandle: SavedStateHandle): Int = savedStateHandle.get<Int>(Field.ACTION)!!

    @ViewModelScoped
    @Provides
    fun providesChatMediator(resources: Resources, transaction: DefaultTransaction, repo: CoreRepo, sessionBus: SessionBus): ChatMediator {
        return ChatMediator(resources, transaction, repo, sessionBus.flow.value!!.deputyUUID)
    }

    @RingNotificationView
    @ViewModelScoped
    @Provides
    fun providesRingNotificationView(@ApplicationContext context: Context, resources: Resources): RemoteViews {
        return remoteViews(context.packageName, R.layout.notification_ringbell) { setTextViewText(R.id.lbl_notifyRingBellTitle, resources.string(R.string.atYourDoor)) }
    }

    @ViewModelScoped
    @Provides
    fun providesChatAdapter(resources: Resources): DefaultChatAdapter = DefaultChatAdapter(resources)
}