package pt.hdn.toolbox.transaction.util

import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.business.proposal.Proposal
import java.math.BigDecimal
import java.util.*

/**
 * An interface that inherits from [Proposal] and holds all information necessary through out a transaction between parties,
 */
interface Transaction : Proposal {
    var contract: Contract?
    var isBuyerSide: Boolean
    var abort: Boolean
    var chrono: Chrono
    @Level var level: Int
    var distance: Double?
    var rating: Int?
    var ring: Int
    var subTotal: BigDecimal
    var trophy: Int?
    val commission: BigDecimal
    val deputyUUID: UUID; get() = with(ref) { if (isBuyerSide) buyerDeputyUUID else sellerDeputyUUID }
    val sellerUUID: UUID; get() = ref.sellerPartyUUID
    val buyerUUID: UUID; get() = ref.buyerPartyUUID
    val serviceSchemasUUID: UUID; get() = ref.sellerSpecialityServiceSchemasUUID
    val taxUUID: UUID; get() = ref.sellerSpecialityTaxUUID
    val buyerAppUUID: UUID; get() = ref.buyerAppUUID
    val buyerDeputyName: String; get() = ref.buyerDeputyName
    val sellerDeputyName: String; get() = ref.sellerDeputyName
    val tax: BigDecimal; get() = ref.sellerSpecialityTaxValue
    val isRated: Boolean; get() = rating != null
    @Action val action; get() = when { level == Level.REQUESTING && abort -> Action.CANCEL; abort -> Action.ABORT; else -> Action.CONCLUDE }
}