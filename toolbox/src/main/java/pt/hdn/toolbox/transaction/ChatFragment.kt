package pt.hdn.toolbox.transaction

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.inputmethod.EditorInfo.IME_ACTION_SEND
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentChatBinding
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.takeUnlessWith

/**
 * A [Fragment] that inherits from [BindingFragment] and handles
 */
@AndroidEntryPoint
class ChatFragment: BindingFragment<FragmentChatBinding>(FragmentChatBinding::inflate), OnClickListener, OnEditorActionListener {

    //region vars
    private val transactionViewModel: TransactionViewModel by viewModels({ requireParentFragment() })
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(transactionViewModel) {
                txtChatType.apply { hint = string(R.string.typeHere); setOnEditorActionListener(this@ChatFragment) }

                recChatLog.apply { setHasFixedSize(true); adapter = chatMediator.adapter }

                btnChatSend.setOnClickListener(this@ChatFragment)

                lblChatName.text = transactionMediator.receiverName
            }
        }
    }

    override fun onClick(v: View?) { send() }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean { if (actionId == IME_ACTION_SEND) send(); return false }

    private fun send() {
        with(binding!!) {
            txtChatType.apply { text?.takeUnlessWith { isBlank() }?.let { transactionViewModel.sendMessage(it.toString()) }; text = null }

            with(recChatLog) { scrollToPosition(adapter!!.itemCount - 1) }
        }
    }
}