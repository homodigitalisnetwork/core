package pt.hdn.toolbox.transaction.util

import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.CONCLUDING
import pt.hdn.toolbox.annotations.Side
import pt.hdn.toolbox.annotations.Side.Companion.BOTH
import kotlin.coroutines.CoroutineContext

@DslMarker
annotation class TopicDsl

@TopicDsl
data class Topic(
	@Level val left: Int,
	@Level val right: Int = left,
	val leftInclusive: Boolean = true,
	val rightInclusive: Boolean = (left == right || right == CONCLUDING),
	val callOnce: Boolean = true,
	@Side val callOnSide: Int = BOTH,
	val executeOnAborting: Boolean = (left >= CONCLUDING || right <= ABORTING),
	private val obs: Observer
) {

	//region vars
	var exec: Boolean = true; private set
	var conclude: Boolean = false; private set
	var execute: (obs: Observer, level: Int, aborting: Boolean) -> Unit = { _, _, _ -> }; private set
	var finally: (obs: Observer) -> Unit = { }; private set
	var cancel: () -> Unit = { }; private set
	var terminate: () -> Unit = { }; private set
	//endregion vars

	fun evaluate(hasFocus: Boolean, exit: Boolean, @Level level: Int, aborting: Boolean) {
		if (hasFocus && exec) { if (hasFocus && callOnce) this.exec = false; this.conclude = true; execute(obs, level, aborting) }

		if (!hasFocus && conclude) finally(obs)

		if (!hasFocus) cancel()

		if (exit) terminate()
	}

	fun execute(execute: (obs: Observer, level: Int, aborting: Boolean) -> Unit) { this.execute = execute }

	fun finally(finally: (obs: Observer) -> Unit) { this.finally = finally }

	fun cancel(cancel: () -> Unit) { this.cancel = cancel }

	fun terminate(terminate: () -> Unit) { this.terminate = terminate }
}