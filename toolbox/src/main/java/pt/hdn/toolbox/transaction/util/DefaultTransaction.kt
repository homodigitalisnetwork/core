package pt.hdn.toolbox.transaction.util

import com.google.firebase.database.DataSnapshot
import com.google.gson.Gson
import kotlinx.parcelize.Parcelize
import pt.hdn.contract.annotations.SourceType.Companion.DISTANCE
import pt.hdn.contract.annotations.SourceType.Companion.INDIVIDUAL_REVENUE
import pt.hdn.contract.annotations.SourceType.Companion.TIME
import pt.hdn.contract.schemas.CommissionSchema
import pt.hdn.contract.schemas.ObjectiveSchema
import pt.hdn.contract.schemas.RateSchema
import pt.hdn.contract.schemas.ThresholdSchema
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Action.Companion.ABORT
import pt.hdn.toolbox.annotations.Action.Companion.ACCEPT
import pt.hdn.toolbox.annotations.Action.Companion.CANCEL
import pt.hdn.toolbox.annotations.Action.Companion.CONCLUDE
import pt.hdn.toolbox.annotations.Action.Companion.ENQUIRE
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.annotations.Field.Companion.ACTION
import pt.hdn.toolbox.annotations.Field.Companion.CONTRACT
import pt.hdn.toolbox.annotations.Field.Companion.LEVEL
import pt.hdn.toolbox.annotations.Field.Companion.REF
import pt.hdn.toolbox.annotations.Field.Companion.RING
import pt.hdn.toolbox.annotations.Field.Companion.TYPE
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.Level.Companion.INITIATING
import pt.hdn.toolbox.annotations.Level.Companion.REQUESTING
import pt.hdn.toolbox.annotations.Value
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.communications.message.DefaultRef
import pt.hdn.toolbox.communications.message.Message
import pt.hdn.toolbox.communications.message.Ref
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.from
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.time.ZonedDateTime
import java.util.UUID

/**
 * The default implementation of [Transaction]
 */
@Parcelize
data class DefaultTransaction(
    override var uuid: UUID = UUID.randomUUID(),
    override val ref: Ref,
    override val specialitiesNames: String? = "",
    override var contract: Contract? = null,
    override var isBuyerSide: Boolean = false,
    override var abort: Boolean = false,
    override var chrono: Chrono = Chrono(),
    @Level override var level: Int = INITIATING,
    override var distance: Double? = null,
    override var rating: Int? = null,
    override var ring: Int = 0,
    override var subTotal: BigDecimal = ZERO,
    override var trophy: Int? = null,
    override val commission: BigDecimal = ZERO
) : Transaction {

    //region vars
    private val buyerDeputyUUID: UUID; get() = ref.buyerDeputyUUID
    private val sellerDeputyUUID: UUID; get() = ref.sellerDeputyUUID
    //endregion vars

    companion object {
        fun from(res: Resources, seller: Seller, buyer: Buyer): DefaultTransaction {
            seller
                .specialityServiceSchemas
                .forEachWith {
                    when (this) {
                        is RateSchema -> source = TIME
                        is CommissionSchema -> source = INDIVIDUAL_REVENUE
                        is ObjectiveSchema -> source = INDIVIDUAL_REVENUE
                        is ThresholdSchema -> { source = DISTANCE; threshold = Value.NEXT_RADIUS.toBigDecimal(); isAbove = true }
                    }
                }

            return DefaultTransaction(
                ref = DefaultRef(buyer, seller),
                specialitiesNames = res.specialities[seller.specialityUUID]?.name,
                contract = Contract(
                    tasks = mutableListOf(Task(res.specialities[seller.specialityUUID]!!, seller.specialityServiceSchemas)),
                    recurrence = ZonedDateTime.now().plusDays(1L).let { Recurrence(start = it.minusDays(1L), finish = it).apply { addDay(it.dayOfMonth); addMonth(it.monthValue) } },
                    buyerId = buyer.partyId,
                    buyerDeputyId = buyer.deputyId,
                    sellerId = seller.partyId,
                    sellerDeputyId = seller.deputyId
                ),
                isBuyerSide = true
            )
        }

        fun from(snapshot: DataSnapshot, gson: Gson, partyUUID: UUID): DefaultTransaction {
            return with(snapshot.value as Map<String, Any?>) {
                val ref: Ref = DefaultRef.from(snapshot.child(REF))

                DefaultTransaction(
                    uuid = UUID.fromString(snapshot.key),
                    ref = ref,
                    ring = (this[RING] as Long).toInt(),
                    level = (this[LEVEL] as Long).toInt(),
                    abort = this[Field.ABORT] as Boolean,
                    isBuyerSide = ref.buyerPartyUUID == partyUUID,
                    contract = gson.from<Contract>(this[CONTRACT] as String)
                )
            }
        }

        fun from(gson: Gson, map: Map<String, String>, res: Resources): DefaultTransaction {
            return with(gson) {
                from<Ref>(map[REF]!!)
                    .let {
                        DefaultTransaction(
                            uuid = UUID.fromString(map[Field.UUID]!!),
                            ref = it,
                            specialitiesNames = res.specialities[it.sellerSpecialityUUID]?.name,
                            contract = from(map[CONTRACT]!!),
                            isBuyerSide = false
                        )
                    }
            }
        }
    }

    fun message(@Action action: Int): Message {
        val receiverUUID: UUID = when (action) { CANCEL, ENQUIRE -> sellerDeputyUUID; else -> buyerDeputyUUID }

        val payload: MutableMap<String, Any> = mutableMapOf<String, Any>()
            .apply {
                this[TYPE] = true; this[ACTION] = action; this[Field.UUID] = uuid.toString()

                when (action) { ENQUIRE -> { this[REF] = ref!!.toMap(); this[CONTRACT] = contract!!.toJson(); this[LEVEL] = level }; ACCEPT -> this[CONTRACT] = contract!!.toJson() }
            }

        return Message(receiverUUID, payload)
    }

    fun setDistanceRecord(value: Double) { this.distance = distance?.coerceAtMost(value) ?: value }
}
