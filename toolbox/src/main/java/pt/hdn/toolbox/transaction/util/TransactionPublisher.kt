package pt.hdn.toolbox.transaction.util

import androidx.lifecycle.Lifecycle.State
import androidx.lifecycle.Lifecycle.State.RESUMED
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.CoroutineScope
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.CONCLUDING
import pt.hdn.toolbox.annotations.Side
import pt.hdn.toolbox.annotations.Side.Companion.BOTH

interface TransactionPublisher {

    //region vars
    @Level var level: Int
    //endregion vars

    fun using(scope: CoroutineScope, block: TransactionScopeSubscribers.() -> Unit)
    fun using(lifecycleOwner: LifecycleOwner, block: TransactionLifecycleSubscribers.() -> Unit)
    fun subscribe(lifecycleOwner: LifecycleOwner, state: State = RESUMED, @Level left: Int, @Level right: Int = left, leftInclusive: Boolean = true, rightInclusive: Boolean = (left == right || right == CONCLUDING), callOnce: Boolean = true, @Side callOnSide: Int = BOTH, executeOnAborting: Boolean = (left >= CONCLUDING || right <= ABORTING), supplier: Observer.() -> Unit): TransactionPublisher
    fun subscribe(scope: CoroutineScope, @Level left: Int, @Level right: Int = left, leftInclusive: Boolean = true, rightInclusive: Boolean = (left == right || right == CONCLUDING), callOnce: Boolean = true, @Side callOnSide: Int = BOTH, executeOnAborting: Boolean = (left >= CONCLUDING || right <= ABORTING), supplier: Observer.() -> Unit): TransactionPublisher
    fun recover()
    fun initiate()
    fun nextLevel()
    fun previousLevel()
    fun terminate()
    fun levelByDistance(distance: Double)
    fun prepare()
    fun work()
    fun pause()
    suspend fun abort(fromUser: Boolean = true)
    fun finish()
    fun conclude()
    fun exit()
}