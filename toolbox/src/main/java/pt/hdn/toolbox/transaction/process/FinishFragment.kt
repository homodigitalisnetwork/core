package pt.hdn.toolbox.transaction.process

import android.os.Bundle
import android.text.format.DateUtils
import android.view.Gravity.CENTER
import android.view.View
import android.view.View.*
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.RatingBar
import android.widget.RatingBar.OnRatingBarChangeListener
import android.widget.TableLayout
import android.widget.TableRow
import androidx.activity.addCallback
import androidx.lifecycle.viewModelScope
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.FINISHING
import pt.hdn.toolbox.annotations.Side.Companion.BUYER
import pt.hdn.toolbox.annotations.Side.Companion.SELLER
import pt.hdn.toolbox.annotations.Trophy
import pt.hdn.toolbox.annotations.Trophy.Companion.CLEAN_WORK
import pt.hdn.toolbox.annotations.Trophy.Companion.OVERACHIEVER
import pt.hdn.toolbox.annotations.Trophy.Companion.PRO
import pt.hdn.toolbox.annotations.Trophy.Companion.QUICK_WORK
import pt.hdn.toolbox.annotations.Trophy.Companion.WELL_MANNER
import pt.hdn.toolbox.transaction.TransactionViewModel
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentFinishBinding
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.navHolderViewModel
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.views.SatisfactionBar.OnSatisfactionBarChangeListener
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import kotlin.math.round

@AndroidEntryPoint
class FinishFragment: BindingFragment<FragmentFinishBinding>(FragmentFinishBinding::inflate), OnClickListener, OnSatisfactionBarChangeListener {

    //region vars
    private val transactionViewModel: TransactionViewModel by navHolderViewModel()
    private var tinyMargin: Int = 0
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState); onBackPressedDispatcher.addCallback(this) { launch { transactionViewModel.transactionPublisher.conclude() } }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding!!) {
            with(transactionViewModel) {
                this@FinishFragment.tinyMargin = dimen(R.dimen.t_size)

//                    lblFinishChrono.text = DateUtils.formatElapsedTime(transactionMediator.state.value.second)

                transactionMediator.state.observe(viewLifecycleOwner) { lblFinishChrono.text = DateUtils.formatElapsedTime(it.second) }

                lblFinishCommissionField.text = string(R.string.commission)
                lblFinishTaxField.text = string(R.string.tax)
                lblFinishTotalField.text = string(R.string.total)
                lblFinishOptional.text = string(R.string.optional)
                barFinishSatisfaction.onSatisfactionBarChangeListener = this@FinishFragment

                transactionPublisher
                    .using(viewModelScope) {
                        subscribe(left = ABORTING, callOnSide = SELLER) { execute { _, aborting -> invoice(aborting) } }

                        subscribe(left = ABORTING, callOnSide = BUYER) {
                            execute { _, aborting ->
                                trFinishCommission.visibility = VISIBLE
                                trFinishTax.visibility = VISIBLE

                                invoice(aborting)
                            }
                        }

                        subscribe(left = FINISHING) { execute { _, aborting -> invoice(aborting); barFinishSatisfaction.visibility = VISIBLE } }

                        subscribe(left = FINISHING, callOnSide = BUYER) {
                            execute { _, _ ->
                                trFinishCommission.visibility = VISIBLE
                                trFinishTax.visibility = VISIBLE

                                lblFinishPro.setOnClickListener(this@FinishFragment)
                                imgFinishOverachiever.setOnClickListener(this@FinishFragment)
                                imgFinishWellManner.setOnClickListener(this@FinishFragment)
                                imgFinishCleanWork.setOnClickListener(this@FinishFragment)
                                imgFinishQuickWork.setOnClickListener(this@FinishFragment)
                            }
                        }
                    }
            }
        }
    }

    override fun onSatisfactionChange(satisfaction: Int?) {
        with(transactionViewModel.transactionMediator) {
            this.rating = satisfaction

            binding!!.tbFinishTrophies.visibility = if (satisfaction == 5 && isBuyerSide) VISIBLE else { clearTrophy(); transactionViewModel.transactionMediator.trophy = null; GONE }
        }
    }

    override fun onClick(view: View?) {
        view
            ?.id
            ?.let {
                with(binding!!) {
                    clearTrophy()

                    when (it) {
                        R.id.lbl_finishPro -> setTrophy(PRO, lblFinishPro)
                        R.id.img_finishOverachiever -> setTrophy(OVERACHIEVER, imgFinishOverachiever)
                        R.id.img_finishWellManner -> setTrophy(WELL_MANNER, imgFinishWellManner)
                        R.id.img_finishCleanWork -> setTrophy(CLEAN_WORK, imgFinishCleanWork)
                        R.id.img_finishQuickWork -> setTrophy(QUICK_WORK, imgFinishQuickWork)
                    }
                }
            }
    }

    private fun invoice(abort: Boolean) {
        with(binding!!) {
            transactionViewModel
                .transactionMediator
                .invoice(abort, ZERO) { details, commission, tax, total ->
                    details.forEachWith { addParameter(first, second) }

                    lblFinishCommissionValue.text = "%.2f".format(commission)
                    lblFinishTaxValue.text = "%.2f".format(tax)
                    lblFinishTotalValue.text = "%.2f".format(total)
                }
        }
    }

    private fun clearTrophy() {
        with(binding!!) {
            drawable(R.drawable.bg_circle_disable)
                .let {
                    lblFinishPro.background = it
                    imgFinishOverachiever.background = it
                    imgFinishWellManner.background = it
                    imgFinishCleanWork.background = it
                    imgFinishQuickWork.background = it
                }
        }
    }

    private fun setTrophy(@Trophy trophy: Int, view: View) {
        with(transactionViewModel.transactionMediator) {
            this.trophy = if (this.trophy == trophy) null else { view.background = drawable(R.drawable.bg_circle_enable); trophy }
        }
    }

    private fun addParameter(field: String?, value: BigDecimal) {
        with(TableRow(requireContext())) {
            layoutParams = TableLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)

            with(MaterialTextView(requireContext())) {
                layoutParams = TableRow.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply { setMargins(tinyMargin, tinyMargin, tinyMargin, tinyMargin) }

                text = field

                addView(this)
            }

            with(MaterialTextView(requireContext())) {
                layoutParams = TableRow.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply { setMargins(tinyMargin, tinyMargin, tinyMargin, tinyMargin); gravity = CENTER }

                text = "%.2f".format(value)

                addView(this)
            }

            binding!!.tbFinishInvoice.addView(this, 0)
        }
    }
}