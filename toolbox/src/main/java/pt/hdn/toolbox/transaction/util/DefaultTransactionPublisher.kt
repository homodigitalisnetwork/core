package pt.hdn.toolbox.transaction.util

import androidx.lifecycle.*
import androidx.lifecycle.Lifecycle.State
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.Level.Companion.ABORTING
import pt.hdn.toolbox.annotations.Level.Companion.CANCELING
import pt.hdn.toolbox.annotations.Level.Companion.CONCLUDING
import pt.hdn.toolbox.annotations.Level.Companion.ESCAPING
import pt.hdn.toolbox.annotations.Level.Companion.EXITING
import pt.hdn.toolbox.annotations.Level.Companion.FINISHING
import pt.hdn.toolbox.annotations.Level.Companion.IN
import pt.hdn.toolbox.annotations.Level.Companion.INITIATING
import pt.hdn.toolbox.annotations.Level.Companion.NAVIGATING
import pt.hdn.toolbox.annotations.Level.Companion.NEAR
import pt.hdn.toolbox.annotations.Level.Companion.ONROUTE
import pt.hdn.toolbox.annotations.Level.Companion.PREPARING
import pt.hdn.toolbox.annotations.Level.Companion.REQUESTING
import pt.hdn.toolbox.annotations.Level.Companion.TERMINATING
import pt.hdn.toolbox.annotations.Level.Companion.WORKING
import pt.hdn.toolbox.annotations.Side
import pt.hdn.toolbox.annotations.Side.Companion.BUYER
import pt.hdn.toolbox.annotations.Side.Companion.SELLER
import pt.hdn.toolbox.annotations.Value.Companion.NEXT_RADIUS
import pt.hdn.toolbox.annotations.Value.Companion.RING_RADIUS
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.topic
import pt.hdn.toolbox.repo.CoreRepo
import java.util.*
import kotlin.Comparator
import kotlin.coroutines.resume

class DefaultTransactionPublisher(
    private val repo: CoreRepo,
    private val transaction: DefaultTransaction
): TransactionPublisher {

    //region vars
    private val isBuyerSide: Boolean; get() = transaction.isBuyerSide
    private val subscribers: MutableList<Topic> = mutableListOf()
    private val comparator: Comparator<Topic> = compareBy<Topic> { it.left }.thenBy { it.leftInclusive }.thenBy { it.right }.thenBy { !it.rightInclusive }
    @Side private val side: Int = if (isBuyerSide) BUYER else SELLER
    private val transactionUUID: UUID; get() = transaction.uuid
    private var abort: Boolean; get() = transaction.abort; set(value) { transaction.abort = value }
    @Level override var level: Int; get() = when { transaction.level == REQUESTING && abort -> CANCELING; abort -> ABORTING; else -> transaction.level }; set(value) {
        if (value >= INITIATING && !abort)  transaction.level = value else abort = true; subscribers.forEachWith { call(value, side, abort) }
    }
    //endregion vars

    override fun using(scope: CoroutineScope, block: TransactionScopeSubscribers.() -> Unit) { TransactionScopeSubscribers(this, scope).apply(block) }

    override fun using(owner: LifecycleOwner, block: TransactionLifecycleSubscribers.() -> Unit) { TransactionLifecycleSubscribers(this, owner).apply(block) }

    override fun subscribe(lifecycleOwner: LifecycleOwner, state: State, @Level left: Int, @Level right: Int, leftInclusive: Boolean, rightInclusive: Boolean, callOnce: Boolean, @Side callOnSide: Int, executeOnAborting: Boolean, supplier: Observer.() -> Unit): TransactionPublisher {
        with(lifecycleOwner) { lifecycleScope.launch { repeatOnLifecycle(state) { subscribe(this, left, right, leftInclusive, rightInclusive, callOnce, callOnSide, executeOnAborting, supplier) } } }; return this
    }

    override fun subscribe(scope: CoroutineScope, @Level left: Int, @Level right: Int, leftInclusive: Boolean, rightInclusive: Boolean, callOnce: Boolean, @Side callOnSide: Int, executeOnAborting: Boolean, supplier: Observer.() -> Unit): TransactionPublisher {
        scope.observeBlock(left, right, leftInclusive, rightInclusive, callOnce, callOnSide, executeOnAborting, Observer().apply(supplier)); return this
    }

    /*buyer side*/ override fun initiate() { this.level = INITIATING }

    /*seller side*/ override fun nextLevel() { this.level = if (isBuyerSide) REQUESTING else NAVIGATING }

    /*both side*/ override fun previousLevel() { this.level = if (isBuyerSide) ESCAPING else CANCELING }

    /*buyer side*/ override fun terminate() { this.level = TERMINATING }

    /*seller side*/ override fun levelByDistance(distance: Double) {
        when {
            RING_RADIUS < distance && level != ONROUTE -> this.level = ONROUTE
            NEXT_RADIUS < distance && distance <= RING_RADIUS && level != NEAR -> this.level = NEAR
            distance <= NEXT_RADIUS && level != IN -> this.level = IN
        }
    }

    /*seller side*/ override fun prepare() { this.level = PREPARING }

    /*seller side*/ override fun work() { this.level = WORKING }

    /*seller side*/ override fun pause() { this.level = PREPARING }

    /*both side*/ override suspend fun abort(fromUser: Boolean) {
        if (!fromUser) this.level = ABORTING else if (level in INITIATING..REQUESTING) this.level = CANCELING else repo.abort(transactionUUID).collect { it.error { _, _ -> } }
    }

    /*seller side*/ override fun finish() { this.level = FINISHING }

    /*seller side*/ override fun conclude() { this.level = CONCLUDING }

    /*both side*/ override fun exit() { this.level = EXITING }

    /*both side*/ override fun recover() { this.level = if (transaction.abort) { ABORTING.also { transaction.level = it } } else transaction.level }

    private fun Topic.call(@Level level: Int, @Side side: Int, aborting: Boolean) {
        evaluate(
            hasFocus = ((left < level || (left == level && leftInclusive)) && (level < right || (level == right && rightInclusive)) && (side and callOnSide) > 0 && (!aborting || executeOnAborting)),
            exit = level == EXITING,
            level = transaction.level,
            aborting = aborting
        )
    }

    private fun CoroutineScope.observeBlock(@Level left: Int, @Level right: Int, leftInclusive: Boolean, rightInclusive: Boolean, callOnce: Boolean, @Side callOnSide: Int, executeOnAborting: Boolean, obs: Observer) {
        launch {
            var job: Job? = null
            var topic: Topic? = null

            try {
                suspendCancellableCoroutine<Unit> { cont ->
                    topic = topic(left, right, leftInclusive, rightInclusive, callOnce, callOnSide, executeOnAborting, obs) {
                        with(Mutex()) {
                            execute { obs, level, aborting -> job?.cancel(); job = obs.execute.run { launch(first) { withLock { coroutineScope { this.second(level, aborting) } } } } }

                            finally { obs -> obs.finally?.run { launch(first) { withLock { coroutineScope { second() } } } } }

                            cancel { job = job?.run { cancel(); null } }

                            terminate { cont.resume(Unit) }
                        }
                    }.also { with(subscribers) { it.call(level, side, abort); add(it); sortWith(comparator) } }
                }
            }
            finally { job = job?.run { cancel(); null }; topic?.let { subscribers.remove(it) } }
        }
    }
}