package pt.hdn.toolbox.transaction.util

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import pt.hdn.toolbox.chat.DefaultChatAdapter
import pt.hdn.toolbox.chat.DefaultChatMessage
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import java.util.*

class ChatMediator(
    res: Resources,
    private val transaction: DefaultTransaction,
    private val repo: CoreRepo,
    private val deputyUUID: UUID
) {

    //region vars
    private val mutableNewMessage: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val newMessage: Flow<Boolean>; get() = mutableNewMessage
    val adapter: DefaultChatAdapter = DefaultChatAdapter(res)
    //endregion vars

    suspend fun setChat() {
        coroutineScope {
            launch { repo.chatListenOnce(transaction.uuid, deputyUUID).collect { it.success { adapter.append(this) } } }.cancel()

            launch { repo.chatOverwatch(transaction.uuid, deputyUUID).collect { it.success { this?.also { adapter.add(it) }?.takeUnless { it.side }?.let { mutableNewMessage.tryEmit(true) } } } }
        }
    }

    suspend fun send(message: String) { repo.message(transaction.uuid, DefaultChatMessage(deputyUUID, message)).collect { it.error { _, _ -> } } }

    fun readMessage() { mutableNewMessage.tryEmit(false) }
}