package pt.hdn.toolbox.business.seller

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field.Companion.DEPUTY
import pt.hdn.toolbox.annotations.Field.Companion.PARTY
import pt.hdn.toolbox.annotations.Field.Companion.SPECIALITY
import pt.hdn.toolbox.business.deputy.DefaultDeputy
import pt.hdn.toolbox.business.deputy.Deputy
import pt.hdn.toolbox.business.party.DefaultParty
import pt.hdn.toolbox.business.party.Party
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.specialities.util.DefaultSpeciality

/**
 * Default implementation on [Seller] with a static function to create an object from a [DataSnapshot]
 */
@Parcelize
data class DefaultSeller(
    @Expose(serialize = false) override val languages: List<Int>? = null,
    @Expose override val speciality: Speciality? = null,
    override var photo: Photo? = null,
    @Expose override val party: Party,
    @Expose override val deputy: Deputy,
    @Expose(serialize = false) override var position: Position? = null
) : Seller {
    companion object {
        fun from(snapshot: DataSnapshot): DefaultSeller {
            return with(snapshot) {
                DefaultSeller(party = DefaultParty.from(child(PARTY)), deputy = DefaultDeputy.from(child(DEPUTY)), speciality = DefaultSpeciality.from(child(SPECIALITY)))
            }
        }
    }

    override fun toMap(ignoreExtraField: Boolean): Map<String, Any> = mapOf(PARTY to party.toMap(), DEPUTY to deputy.toMap(), SPECIALITY to speciality!!.toMap(ignoreExtraField))
}