package pt.hdn.toolbox.business.party

import pt.hdn.toolbox.business.Entity
import java.util.*

/**
 * An interface that inherits from [Entity] and adds concept variables
 */
interface Party : Entity {
    val appUUID: UUID
}