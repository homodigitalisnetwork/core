package pt.hdn.toolbox.business

import android.os.Parcelable
import java.util.*

/**
 * An interface with the fundamental variables of what an entity is and to be inherit by other more specific interfaces/concepts like [Deputy] or [Party]
 */
interface Entity: Parcelable {
    val uuid: UUID
    val name: String
    val id: String?

    fun toMap(): Map<String, Any>
}