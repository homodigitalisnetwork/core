package pt.hdn.toolbox.business.buyer

import pt.hdn.toolbox.business.partner.Partner

/**
 * An interface that inherits from [Partner] to create a [Buyer] class with extra variables
 */
interface Buyer : Partner {
    override val rating: Float
}