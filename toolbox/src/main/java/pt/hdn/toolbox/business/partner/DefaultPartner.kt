package pt.hdn.toolbox.business.partner

import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field.Companion.DEPUTY
import pt.hdn.toolbox.annotations.Field.Companion.PARTY
import pt.hdn.toolbox.business.deputy.Deputy
import pt.hdn.toolbox.business.party.Party
import pt.hdn.toolbox.misc.Position

/**
 * The default implementation of [Partner]
 */
@Parcelize
data class DefaultPartner(
    @Expose override val deputy: Deputy? = null,
    @Expose override val party: Party? = null
) : Partner {

    //region vars
    override var position: Position? = null
    //endregion vars

    override fun toMap(ignoreExtraField: Boolean): Map<String, Any> {
        return buildMap { deputy?.let { this[DEPUTY] = it.toMap() }; party?.let { this[PARTY] = it.toMap() } }
    }
}
