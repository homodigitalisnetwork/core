package pt.hdn.toolbox.business.proposal

import com.google.gson.Gson
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.annotations.Field.Companion.ACTION
import pt.hdn.toolbox.annotations.Field.Companion.REF
import pt.hdn.toolbox.annotations.Field.Companion.TYPE
import pt.hdn.toolbox.communications.message.Ref
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.from
import pt.hdn.toolbox.resources.Resources
import java.util.*

/**
 * The default implementation of [Proposal]
 */
@Parcelize
data class DefaultProposal(
    override val uuid: UUID,
    override val ref: Ref,
    override val specialitiesNames: String? = ""
) : Proposal {
    companion object {
        fun from(gson: Gson, map: Map<String, String>, res: Resources): DefaultProposal {
            return gson
                .from<Ref>(map[REF]!!)
                .let { DefaultProposal(uuid = UUID.fromString(map[Field.UUID]!!), ref = it, specialitiesNames = res.specialities[it.sellerSpecialityUUID]?.name) }
        }
    }

//    fun toMap(@Action action: Int): Map<String, Any> = mapOf(TYPE to false, ACTION to action, Field.UUID to uuid.toString())
}
