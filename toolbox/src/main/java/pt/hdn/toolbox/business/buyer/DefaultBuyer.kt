package pt.hdn.toolbox.business.buyer

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ktx.getValue
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field.Companion.DEPUTY
import pt.hdn.toolbox.annotations.Field.Companion.PARTY
import pt.hdn.toolbox.annotations.Field.Companion.POSITION
import pt.hdn.toolbox.annotations.Field.Companion.RATING
import pt.hdn.toolbox.business.deputy.DefaultDeputy
import pt.hdn.toolbox.business.deputy.Deputy
import pt.hdn.toolbox.business.party.DefaultParty
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.business.party.Party
import pt.hdn.toolbox.misc.Position
import java.util.*

/**
 * The default implementation of [Buyer] interface with additional static functions
 * to create a class object from [Partnership] and [Position] or from a [DataSnapshot]
 */
@Parcelize
data class DefaultBuyer(
    @Expose override var position: Position? = null,
    @Expose override val rating: Float = 0f,
    @Expose override val party: Party? = null,
    @Expose override val deputy: Deputy? = null
): Buyer {
    companion object {
        fun from(partnership: Partnership, position: Position? = null): DefaultBuyer {
            return with(partnership) { with(partner) { DefaultBuyer (position ?: defaultPosition, rating, party, deputy) } }
        }

        fun from(snapshot: DataSnapshot): DefaultBuyer {
            return with(snapshot) {
                DefaultBuyer(
                    position = child(POSITION).getValue(Position::class.java),
                    party = DefaultParty.from(child(PARTY)),
                    deputy = DefaultDeputy.from(child(DEPUTY))
                )
            }
        }
    }

    override fun toMap(ignoreExtraField: Boolean): Map<String, Any> {
        return buildMap { deputy?.let { this[DEPUTY] = it.toMap() }; party?.let { this[PARTY] = it.toMap() }; position?.let { this[POSITION] = it.toMap() } }
    }
}
