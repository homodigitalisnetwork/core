package pt.hdn.toolbox.business.partner

import android.os.Parcelable
import pt.hdn.toolbox.business.deputy.Deputy
import pt.hdn.toolbox.business.party.Party
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.preferences.Preferences
import pt.hdn.toolbox.security.Clearance
import java.util.*

/**
 * An interface to create a [Partner] class with common assertation getters, regular getters, setters and functions
 * or to be inherit for by more specific interface like [Seller] or [Buyer]
 */
interface Partner: Parcelable {
    var position: Position?
    var preferences: Preferences; get() = deputy!!.preferences!!; set(value) { deputy!!.preferences = value }
    var location: Boolean; get() = preferences.location; set(value) { preferences.location = value }
    var searchable: Boolean; get() = preferences.searchable; set(value) { preferences.searchable = value }
    val party: Party?
    val deputy: Deputy?
    val partyId: String; get() = party!!.id!!
    val deputyId: String; get() = deputy!!.id!!
    val partyAppUUID: UUID; get() = party!!.appUUID
    val deputyUUID: UUID; get() = deputy!!.uuid
    val partyUUID: UUID; get() = party!!.uuid
    val deputyName: String; get() = deputy!!.name
    val partyName: String; get() = party!!.name
    val clearance: Clearance; get() = deputy!!.clearance!!
    val defaultPosition: Position; get() = preferences.position!!
    val rating: Float; get() = preferences.rating
    val locale: String; get() = preferences.locale

    fun toMap(ignoreExtraField: Boolean): Map<String, Any>
}