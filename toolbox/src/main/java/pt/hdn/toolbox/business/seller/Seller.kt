package pt.hdn.toolbox.business.seller

import pt.hdn.contract.annotations.Market
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.business.partner.Partner
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.specialities.util.Score
import java.math.BigDecimal
import java.util.*

interface Seller : Partner {
    val languages: List<Int>?
    val speciality: Speciality?
    var photo: Photo?
    val specialityUUID: UUID; get() = speciality!!.typeUUID!!
    val specialityServiceSchemasUUID: UUID; get() = speciality!!.serviceSchemasUUID!!
    val specialityPersonSchemas: MutableList<Schema>; get() = speciality!!.personSchemas!!
    val specialityServiceSchemas: MutableList<Schema>; get() = speciality!!.serviceSchemas!!
    val specialityTaxUUID: UUID; get() = speciality!!.taxUUID
    val specialityTaxValue: BigDecimal; get() = speciality!!.taxValue
    val specialityScore: Score; get() = speciality!!.score
    @Market val inMarketType: Int; get() = (speciality?.run { (personSchemas?.let { Market.PERSON } ?: Market.NONE) + (serviceSchemas?.let { Market.SERVICE } ?: Market.NONE) } ?: Market.NONE)
}