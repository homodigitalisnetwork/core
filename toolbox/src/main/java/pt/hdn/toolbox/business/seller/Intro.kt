package pt.hdn.toolbox.business.seller

import pt.hdn.toolbox.misc.Photo

data class Intro(
    val languages: List<Int>? = null,
    var photo: Photo? = null
)
