package pt.hdn.toolbox.business.party

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field.Companion.APP_UUID
import pt.hdn.toolbox.annotations.Field.Companion.NAME
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.business.deputy.Deputy
import java.util.*

/**
 * Default implementation of [Party] with static function to create an object class from [DataSnapshot]
 */
@Parcelize
data class DefaultParty(
    @Expose override val uuid: UUID,
    @Expose override val appUUID: UUID,
    @Expose override val name: String,
    @Expose override val id: String? = null
): Party {
    companion object {
        fun from(snapshot: DataSnapshot) : DefaultParty {
            return with(snapshot.value as Map<String, Any?>) {
                DefaultParty(
                    uuid = UUID.fromString(this[Field.UUID] as String),
                    appUUID = UUID.fromString(this[APP_UUID] as String),
                    name = this[NAME] as String
                )
            }
        }
    }

    override fun toMap(): Map<String, Any> = mapOf(Field.UUID to uuid.toString(), NAME to name, APP_UUID to appUUID.toString())
}