package pt.hdn.toolbox.business.deputy

import pt.hdn.toolbox.business.Entity
import pt.hdn.toolbox.security.Clearance
import pt.hdn.toolbox.preferences.Preferences

/**
 * An interface that inherits from [Entity] and adds concept variables
 */
interface Deputy : Entity {
    var preferences: Preferences?
    var clearance: Clearance?
}