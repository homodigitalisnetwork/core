package pt.hdn.toolbox.business

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.contract.annotations.Market
import pt.hdn.contract.util.UUIDNameData

/**
 * ON CONSTRUCTION
 */
@Parcelize
data class MarketsResponsibilities(
    @Expose var personResponsibilities: MutableList<UUIDNameData>? = null,
    @Expose var serviceResponsibilities: MutableList<UUIDNameData>? = null
) : Parcelable, Cloneable {
    public override fun clone(): MarketsResponsibilities = MarketsResponsibilities(personResponsibilities?.mapTo(mutableListOf()) { it.copy() }, serviceResponsibilities?.mapTo(mutableListOf()) { it.copy() })

    fun close(@Market market: Int) {
        when (market) {
            Market.PERSON -> personResponsibilities = null
            Market.SERVICE -> serviceResponsibilities = null
        }
    }

    fun open(@Market market: Int) {
        when (market) {
            Market.PERSON -> personResponsibilities = mutableListOf()
            Market.SERVICE -> serviceResponsibilities = mutableListOf()
        }
    }

    fun removeAt(@Market market: Int, index: Int) {
        when (market) {
            Market.PERSON -> personResponsibilities?.removeAt(index)
            Market.SERVICE -> serviceResponsibilities?.removeAt(index)
        }
    }

    operator fun get(@Market market: Int): List<UUIDNameData>? = when (market) { Market.PERSON -> personResponsibilities; Market.SERVICE -> serviceResponsibilities; else ->  null }

    fun add(@Market market: Int, uuidNameData: UUIDNameData) {
        when (market) {
            Market.PERSON -> personResponsibilities?.add(uuidNameData) == true
            Market.SERVICE -> serviceResponsibilities?.add(uuidNameData) == true
        }
    }
}