package pt.hdn.toolbox.business.deputy

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field.Companion.NAME
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.security.Clearance
import pt.hdn.toolbox.preferences.Preferences
import java.util.*

/**
 * Default implementation of [Deputy] with static function to create an object class from [DataSnapshot]
 */
@Parcelize
data class DefaultDeputy(
    @Expose override val uuid: UUID,
    @Expose override val name: String,
    @Expose override val id: String? = null,
    @Expose override var preferences: Preferences? = null,
    @Expose override var clearance: Clearance? = null
) : Deputy {
    companion object {
        fun from(snapshot: DataSnapshot) : DefaultDeputy {
            return with(snapshot.value as Map<String, Any?>) {
                DefaultDeputy(
                    uuid = UUID.fromString(this[Field.UUID] as String),
                    name = this[NAME] as String
                )
            }
        }
    }

    override fun toMap(): Map<String, Any> = mapOf(Field.UUID to uuid.toString(), NAME to name)

    override fun toString(): String = name
}