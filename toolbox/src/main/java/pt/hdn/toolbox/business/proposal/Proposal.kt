package pt.hdn.toolbox.business.proposal

import android.os.Parcelable
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.communications.message.Ref
import pt.hdn.toolbox.misc.Position
import java.util.*

/**
 * An interface that defines the participants on a proposal by using a [Ref] and what is being proposed
 */
interface Proposal : Parcelable {
    val uuid: UUID
    val ref: Ref
    val specialitiesNames: String?
    val buyerRating: Float; get() = ref.buyerRating
    val buyerPosition: Position; get() = ref.buyerPosition
    val buyerName: String; get() = ref.buyerPartyName

//    fun toMap(@Action action: Int): Map<String, Any>
}