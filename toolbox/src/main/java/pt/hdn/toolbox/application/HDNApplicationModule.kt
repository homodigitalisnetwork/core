package pt.hdn.toolbox.application

import android.app.Notification.*
import android.app.NotificationManager
import android.content.Context
import android.content.res.Configuration
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.view.inputmethod.InputMethodManager
import android.widget.RemoteViews
import com.bumptech.glide.load.engine.DiskCacheStrategy.NONE
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Channel.Companion.REQUEST
import pt.hdn.toolbox.annotations.Misc
import pt.hdn.toolbox.business.partner.Partner
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.deputy.Deputy
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.business.party.Party
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.communications.bus.*
import pt.hdn.toolbox.communications.message.Ref
import pt.hdn.toolbox.countdown.CountDownSingletonTimer
import pt.hdn.toolbox.economics.Tax
import pt.hdn.toolbox.entity.util.Entity
import pt.hdn.toolbox.misc.remoteViews
import pt.hdn.toolbox.misc.takeIfWith
import pt.hdn.toolbox.misc.typeToken
import pt.hdn.toolbox.misc.useWith
import pt.hdn.toolbox.notifications.*
import pt.hdn.toolbox.repo.PhotosDir
import pt.hdn.toolbox.report.adapter.ReportData
import pt.hdn.toolbox.report.adapter.exchange.SpecialityTrade
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.typeadapters.*
import java.io.File
import java.io.IOException
import java.math.BigDecimal
import java.net.InetSocketAddress
import java.net.Socket
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
import java.time.format.DateTimeFormatterBuilder
import java.util.*
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class GenericNotificationView

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class TransactionSmallNotificationView

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class TransactionBigNotificationView

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ProposalNotificationView

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class RequestNotificationBuilder

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class CoreGson

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class Connectivity

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    //region vars
    private const val PATTERN = "+HH:MM"
    private const val NO_OFFSET_TEXT = "+00:00"
    private const val OPEN = '['
    private const val CLOSE = ']'
    //endregion vars

    /**
     * Generic [RemoteViews] to be used by multiple notifications
     */
    @GenericNotificationView
    @Singleton
    @Provides
    fun providesGenericContentView(@ApplicationContext context: Context): RemoteViews = RemoteViews(context.packageName, R.layout.notification)

    /**
     * Special purpose [RemoteViews] to be used by the [ProposalNotification]
     */
    @ProposalNotificationView
    @Singleton
    @Provides
    fun providesProposalNotificationView(res: Resources): RemoteViews {
        return remoteViews(res.packageName, R.layout.notification_proposal)  {
            setImageViewResource(R.id.img_notifyProposalLogo, R.drawable.ic_partnership)
            setTextViewText(R.id.lbl_notifyProposalTitle, res.string(R.string.contract))
            setTextViewText(R.id.lbl_notifyProposalSubtitle, res.string(R.string.contractProposal))
            setTextViewText(R.id.lbl_notifyProposalPath, res.string(R.string.partnershipsPath))
            setTextViewText(R.id.lbl_notifyProposalNameField, res.string(R.string.name))
            setTextViewText(R.id.lbl_notifyProposalRatingField, res.string(R.string.clientWith))
            setTextViewText(R.id.lbl_notifyProposalRequestField, res.string(R.string.request))
            setTextViewText(R.id.lbl_notifyProposalLocationField, res.string(R.string.at))
        }
    }

    /**
     * Special purpose [RemoteViews] to be used by the [CountDownNotification]
     */
    @TransactionSmallNotificationView
    @Singleton
    @Provides
    fun providesTransactionSmallNotificationView(res: Resources): RemoteViews {
        return remoteViews(res.packageName, R.layout.notification_transaction_small) {
            setImageViewResource(R.id.img_notifyTransactionSmallLogo, R.drawable.ic_work)
            setTextViewText(R.id.lbl_notifyTransactionSmallTitle, res.string(R.string.work))
            setTextViewText(R.id.lbl_notifyTransactionSmallSubtitle, res.string(R.string.workProposal))
        }
    }

    /**
     * Special purpose [RemoteViews] to be used by the [CountDownNotification]
     */
    @TransactionBigNotificationView
    @Singleton
    @Provides
    fun providesTransactionBigNotificationView(res: Resources): RemoteViews {
        return remoteViews(res.packageName, R.layout.notification_transaction_big)  {
            setImageViewResource(R.id.img_notifyTransactionBigLogo, R.drawable.ic_work)
            setTextViewText(R.id.lbl_notifyTransactionBigTitle, res.string(R.string.work))
            setTextViewText(R.id.lbl_notifyTransactionBigSubtitle, res.string(R.string.workProposal))
            setTextViewText(R.id.lbl_notifyTransactionBigCountDownField, res.string(R.string.rejectingIn))
            setTextViewText(R.id.lbl_notifyTransactionBigRatingField, res.string(R.string.clientWith))
            setTextViewText(R.id.lbl_notifyTransactionBigNameField, res.string(R.string.request))
            setTextViewText(R.id.lbl_notifyTransactionBigLocationField, res.string(R.string.at))
            setTextViewText(R.id.lbl_notifyTransactionBigTap, res.string(R.string.tap))
            setTextViewText(R.id.lbl_notifyTransactionBigOr, res.string(R.string.or))
            setTextViewText(R.id.lbl_notifyTransactionBigSwipe, res.string(R.string.swipe))
        }
    }

    @Singleton
    @Provides
    fun providesProposalNotification(@ApplicationContext context: Context, res: Resources, @GenericNotificationView genericRemoteViews: RemoteViews, @ProposalNotificationView proposalRemoteViews: RemoteViews, manager: NotificationManager): ProposalNotification {
        return ProposalNotification(context, res, genericRemoteViews, proposalRemoteViews, manager)
    }

    @Singleton
    @Provides
    fun providesCountDownNotification(@ApplicationContext context: Context, res: Resources, timer: CountDownSingletonTimer, @GenericNotificationView generic: RemoteViews, @TransactionSmallNotificationView small: RemoteViews, @TransactionBigNotificationView big: RemoteViews, manager: NotificationManager): CountDownNotification {
        return CountDownNotification(context, res, timer, generic, small, big, manager)
    }

    @Singleton
    @Provides
    fun providesCountDownSingleton(): CountDownSingletonTimer = CountDownSingletonTimer


    @Singleton
    @Provides
    fun providesLocationManager(@ApplicationContext context: Context): LocationManager = context.getSystemService(LocationManager::class.java)

    /**
     * Provide a [NotificationManager] to all custom notification classes to notify them self
     */
    @Singleton
    @Provides
    fun providesNotificationManager(@ApplicationContext context: Context): NotificationManager = context.getSystemService(NotificationManager::class.java)

    /**
     * Provides a [InputMethodManager] to be injected in [BindingFragments] required for [Keyboard]
     */
    @Singleton
    @Provides
    fun providesInputMethodManager(@ApplicationContext context: Context): InputMethodManager = context.getSystemService(InputMethodManager::class.java)

    /**
     * Provides a [Geocoder] to[PositionFragment] so a GPS location can be obtain from an address
     */
    @Singleton
    @Provides
    fun providesGeocoder(@ApplicationContext context: Context): Geocoder = Geocoder(context)


    @Singleton
    @Provides
    fun providesConnectivityManager(@ApplicationContext context: Context): ConnectivityManager =  context.getSystemService(ConnectivityManager::class.java)

    /**
     * Provides a boolean flow of an internet connection availability
     */
    @Connectivity
    @Singleton
    @Provides
    fun providesConnectivity(connectivityManager: ConnectivityManager): Flow<Boolean> {
        return callbackFlow {
            with(connectivityManager) {
                val validNetworks: MutableSet<Network> = mutableSetOf()

                object : NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        getNetworkCapabilities(network)
                            ?.takeIfWith { hasCapability(NET_CAPABILITY_INTERNET) }
                            ?.let {
                                launch(IO) {
                                    try {
                                        Socket().useWith { connect(InetSocketAddress("8.8.8.8", 53), 1500) }

                                        validNetworks.apply { add(network); trySend(isNotEmpty()) }
                                    }
                                    catch (e: IOException) { trySend(validNetworks.isNotEmpty()) }
                                }
                            }
                    }

                    override fun onLost(network: Network) { validNetworks.apply { remove(network); trySend(isNotEmpty()) } }
                }.also { registerDefaultNetworkCallback(it); awaitClose { validNetworks.clear(); unregisterNetworkCallback(it) }}
            }
        }.stateIn(CoroutineScope(Default), WhileSubscribed(replayExpirationMillis = 0), false)
    }

    /**
     * Provides a [Gson] from a [GsonBuilder]
     */
    @CoreGson
    @Singleton
    @Provides
    fun providesCoreGson(gsonBuilder: GsonBuilder): Gson = gsonBuilder.create()

    @Singleton
    @Provides
    fun providesGsonBuilder(): GsonBuilder {
        return Contract
            .gsonBuilder
            .registerTypeAdapter(Seller::class.java, SellerTypeAdapter())
            .registerTypeAdapter(Buyer::class.java, BuyerTypeAdapter())
            .registerTypeAdapter(Party::class.java, PartyTypeAdapter())
            .registerTypeAdapter(Deputy::class.java, DeputyTypeAdapter())
            .registerTypeAdapter(Speciality::class.java, SpecialityTypeAdapter())
            .registerTypeAdapter(Partnership::class.java, PartnershipTypeAdapter())
            .registerTypeAdapter(BigDecimal::class.java, BigDecimalTypeAdapter())
            .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeTypeAdapter())
            .registerTypeAdapter(LocalDate::class.java, LocalDateTypeAdapter())
            .registerTypeAdapter(LatLng::class.java, LatLngTypeAdapter())
            .registerTypeAdapter(Tax::class.java, TaxTypeAdapter())
            .registerTypeAdapter(ReportData::class.java, ReportDataTypeAdapter())
            .registerTypeAdapter(typeToken<Map<String, SpecialityTrade>>(), MapTypeAdapter<String, SpecialityTrade>())
            .registerTypeAdapter(Partner::class.java, PartnerTypeAdapter())
            .registerTypeAdapter(Ref::class.java, RefTypeAdapter())
            .registerTypeAdapter(Entity::class.java, EntityTypeAdapter())
    }

    /**
     * Provides the apps pics directory
     */
    @PhotosDir
    @Singleton
    @Provides
    fun providesPicsDir(@ApplicationContext context: Context): File = File(context.filesDir, Misc.PICS_PATH).apply { if (!exists()) mkdir() }

    @Singleton
    @Provides
    fun providesConfiguration(@ApplicationContext context: Context): Configuration = Configuration(context.resources.configuration)

    @Singleton
    @Provides
    fun providesDateTimeFormatter(): DateTimeFormatter {
        return DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(ISO_LOCAL_DATE_TIME)
            .appendOffset(PATTERN, NO_OFFSET_TEXT)
            .optionalStart()
            .appendLiteral(OPEN)
            .parseCaseSensitive()
            .appendZoneRegionId()
            .appendLiteral(CLOSE)
            .toFormatter()
    }

    /**
     * Provides the [RequestOptions] used by [Glide]
     */
    @Singleton
    @Provides
    fun providesRequestOptions(): RequestOptions {
        return RequestOptions()
            .diskCacheStrategy(NONE)
            .skipMemoryCache(true)
//            .transform(RoundedCorners(64))
            .apply(RequestOptions.circleCropTransform())
    }

    @RequestNotificationBuilder
    @Singleton
    @Provides
    fun providesRequestNotificationBuilder(@ApplicationContext context: Context): Builder {
        return Builder(context, REQUEST)
            .setCategory(CATEGORY_MESSAGE)
            .setVisibility(VISIBILITY_PUBLIC)
            .setLocalOnly(true)
    }

    /**
     * Provides the [DataBus] used across the app
     */
    @Singleton
    @Provides
    fun providesCanBus(): DataBus = DataBus

    /**
     * Provides the [SessionBus] used across the app
     */
    @Singleton
    @Provides
    fun providesSession(): SessionBus = SessionBus
}
