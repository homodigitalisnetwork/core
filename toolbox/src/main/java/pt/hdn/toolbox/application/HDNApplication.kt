package pt.hdn.toolbox.application

import android.app.Application
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.Notification.VISIBILITY_SECRET
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import androidx.annotation.CallSuper
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Channel.Companion.GPS
import pt.hdn.toolbox.annotations.Channel.Companion.REQUEST
import pt.hdn.toolbox.annotations.Channel.Companion.RESPONSE
import pt.hdn.toolbox.annotations.Channel.Companion.RING
import pt.hdn.toolbox.annotations.Channel.Companion.TRANSACTION
import pt.hdn.toolbox.misc.notificationChannel

/**
 * Base [Application] class that creates the necessary notifications channels for the application to run
 */
open class HDNApplication : Application() {

	@CallSuper
	override fun onCreate() {
		super.onCreate()

		val foregroundServiceChannel: NotificationChannel = notificationChannel(GPS, getString(R.string.gps), IMPORTANCE_HIGH) {
			description = getString(R.string.gpsDescription)
			enableLights(false)
			enableVibration(false)
			setBypassDnd(true)
			setShowBadge(false)
			lightColor = getColor(R.color.hdn)
			lockscreenVisibility = VISIBILITY_SECRET
		}

		val transactionChannel: NotificationChannel = notificationChannel(TRANSACTION, getString(R.string.transactionStage), IMPORTANCE_HIGH) {
			description = getString(R.string.notificationTransaction)
			enableLights(true)
			enableVibration(true)
			setBypassDnd(true)
			setShowBadge(true)
			lightColor = getColor(android.R.color.holo_red_dark)
			lockscreenVisibility = VISIBILITY_PUBLIC
		}

		val responseChannel: NotificationChannel = notificationChannel(RESPONSE, getString(R.string.workResponse), IMPORTANCE_HIGH) {
			description = getString(R.string.notificationResponse)
			enableLights(true)
			enableVibration(true)
			setBypassDnd(true)
			setShowBadge(true)
			lightColor = getColor(android.R.color.holo_red_dark)
			lockscreenVisibility = VISIBILITY_PUBLIC
		}

		val requestChannel: NotificationChannel = notificationChannel(REQUEST, getString(R.string.workRequest), IMPORTANCE_HIGH) {
			description = getString(R.string.notificationRequest)
			enableLights(true)
			enableVibration(true)
			setBypassDnd(true)
			setShowBadge(true)
			lightColor = getColor(android.R.color.holo_red_dark)
			lockscreenVisibility = VISIBILITY_PUBLIC
		}

		val ringBellChannel: NotificationChannel = notificationChannel(RING, getString(R.string.ringBell), IMPORTANCE_HIGH) {
			description = getString(R.string.notificationResponse)
			enableLights(true)
			enableVibration(true)
			setBypassDnd(true)
			setShowBadge(true)
			lightColor = getColor(android.R.color.holo_red_dark)
			lockscreenVisibility = VISIBILITY_PUBLIC
		}

		with(getSystemService(NotificationManager::class.java)) {
            createNotificationChannel(ringBellChannel)
			createNotificationChannel(responseChannel)
			createNotificationChannel(requestChannel)
			createNotificationChannel(foregroundServiceChannel)
			createNotificationChannel(transactionChannel)
		}
	}
}