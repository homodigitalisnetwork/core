package pt.hdn.toolbox.specialities.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.resources.Resources

class SpecialitiesAdapter(
	specialities: MutableList<Speciality>,
	resources: Resources
) : ListAdapter<SpecialityViewHolder, Speciality>(specialities, resources) {
    override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): SpecialityViewHolder = SpecialityViewHolder(resources, parent)
}