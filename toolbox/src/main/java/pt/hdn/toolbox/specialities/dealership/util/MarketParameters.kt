package pt.hdn.toolbox.specialities.dealership.util

import com.google.gson.annotations.Expose
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.WindowBounds

data class MarketParameters(
    @Expose val specialityId: Int,
    @Expose val position: Position,
    @Expose val windowBounds: WindowBounds,
    @Expose val schemas: List<Schema>
)