package pt.hdn.toolbox.specialities.dealership

import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.annotation.IdRes
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.databinding.DialogDealershipBinding
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.specialities.SpecialitiesViewModel

@AndroidEntryPoint
class SpecialityDialogFragment: BindingDialogFragment<DialogDealershipBinding>(DialogDealershipBinding::inflate, 40), OnClickListener {

	//region vars
	private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
	//endregion vars

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		with(binding!!) {
			with(specialitiesViewModel) {
				response.observe(viewLifecycleOwner) { it.success { popBack() }.error { _, _ -> setViewVisibility(true); toast(R.string.failExe) } }

				dataBus
					.using(viewLifecycleOwner) {
						observe<Unit>(Address.SPECIALITY) { navigateTo(marketDialog()) }
						observeWith<Pair<Int, @Visibility Int>>(Address.VIEW_1) { setViewVisibility(first, second) }
					}

				errorBus.observe(viewLifecycleOwner) { setViewVisibility(true); if (it == Err.NO_CHANGE) toast(R.string.noChanges) }

				inMarket.observe(viewLifecycleOwner) { setViewVisibility(R.id.btn_dealershipDialogSubmit, if (it) Visibility.ENABLE else Visibility.GONE) }

				arrayFragmentStateAdapter(this@SpecialityDialogFragment, vpDealershipDialogContainer) { if (isNewBuild) add(::SelectionFragment) }
					.apply {
						marketType
							.observe(viewLifecycleOwner) {
								it?.let {
									removeFrom(1)

									add(MarketFragment.invoker(SERVICE))

//									when (it) {
//										PERSON -> add(MarketFragment.invoker(PERSON))
//										SERVICE -> add(MarketFragment.invoker(SERVICE))
//										else /*PERSON + SERVICE*/ -> addAll(MarketFragment.invoker(PERSON), MarketFragment.invoker(SERVICE))
//									}
								}
							}
					}

				tabLayoutMediator(viewLifecycleOwner, layoutDealershipDialogDots, vpDealershipDialogContainer)

				btnDealershipDialogRemove.setOnClickListener(this@SpecialityDialogFragment)
				btnDealershipDialogEdit.setOnClickListener(this@SpecialityDialogFragment)
				btnDealershipDialogSubmit.setOnClickListener(this@SpecialityDialogFragment)
			}
		}
	}

	override fun onClick(v: View?) {
		v?.id
			?.let {
				when (it) {
					R.id.btn_dealershipDialogSubmit -> { setViewVisibility(false); specialitiesViewModel.saveSpeciality() }
					else -> dataBus.send(viewLifecycleOwner, Address.CLICK_1, it)
				}
			}
	}

	override fun onBackPressed() { confirmationDialog() }

	private fun confirmationDialog() {
		MaterialAlertDialogBuilder(requireContext())
			.setTitle(string(R.string.confirmation))
			.setMessage(string(R.string.areYouSure))
			.setPositiveButton(string(R.string.yes)) { _, _ -> popBack() }
			.setNegativeButton(string(R.string.no), null)
			.show()
	}

	private fun setViewVisibility(isEnabled: Boolean) {
		with(binding!!) { barDealershipDialogLoading.visibility = if (isEnabled) GONE else VISIBLE; btnDealershipDialogSubmit.isEnabled = isEnabled }
	}

	private fun setViewVisibility(@IdRes id: Int, @Visibility state: Int) {
		with(binding!!) {
			when (id) {
				R.id.bar_dealershipDialogLoading -> with(barDealershipDialogLoading) { visibility = if (state == Visibility.GONE) GONE else VISIBLE }
				R.id.btn_dealershipDialogRemove -> with(btnDealershipDialogRemove) { if (state == Visibility.GONE) hide() else show() }
				R.id.btn_dealershipDialogAdd -> with(btnDealershipDialogAdd) { if (state == Visibility.GONE) hide() else show() }
				R.id.btn_dealershipDialogEdit -> with(btnDealershipDialogEdit) { if (state == Visibility.GONE) hide() else show() }
				R.id.btn_dealershipDialogSubmit -> with(btnDealershipDialogSubmit) { if (state == Visibility.GONE) hide() else show() }
			}
		}
	}
}