package pt.hdn.toolbox.specialities.util

import android.os.Parcelable
import pt.hdn.contract.annotations.Err as Err2
import pt.hdn.contract.annotations.Market
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.schemas.Schema
import pt.hdn.contract.util.Task
import pt.hdn.contract.util.UUIDNameData
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.economics.Tax
import pt.hdn.toolbox.misc.Reducible
import pt.hdn.toolbox.misc.contains
import java.math.BigDecimal
import java.util.*

interface Speciality : Parcelable, Cloneable, Reducible<Speciality> {
    var uuid: UUID?
    var score: Score
    var personSchemasUUID: UUID?
    var serviceSchemasUUID: UUID?
    var personSchemas: MutableList<Schema>?
    var serviceSchemas: MutableList<Schema>?
    var personResponsibilities: MutableList<UUIDNameData>?
    var serviceResponsibilities: MutableList<UUIDNameData>?
    var typeUUID: UUID?
    var type: UUIDNameMarketData?
    var tax: Tax?
    val taxValue: BigDecimal; get() = tax!!.value!!
    val taxUUID: UUID; get() = tax!!.uuid

    fun toMap(isNotSpot: Boolean): Map<String, Any>

    public override fun clone(): Speciality

    @Err @Err2 fun validate(speciality: Speciality?): Int {
        @Err @Err2 var err = Err.NONE
        val errors: Set<Int> = setOf(Err2.NONE, Err2.NO_CHANGE)

        return when {
            this == speciality -> Err.NO_CHANGE
            type == null -> Err.TYPE
            personSchemas == null  && serviceSchemas == null -> Err.TYPE_1
            personSchemas?.any { it.validate().also { err = it } !in errors } ?: false -> err
            serviceSchemas?.any { it.validate().also { err = it } !in errors } ?: false -> err
            else -> Err.NONE
        }
    }

    fun toTask(@Market market: Int): Task = Task(specialityType = type!!, schemas = (if (market == PERSON) personSchemas else serviceSchemas)!!)
}