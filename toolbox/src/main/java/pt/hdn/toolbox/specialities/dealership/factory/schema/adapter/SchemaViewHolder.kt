package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.view.ViewGroup
import com.google.android.material.chip.Chip
import pt.hdn.contract.annotations.SchemaType
import pt.hdn.contract.annotations.SchemaType.Companion.COMMISSION
import pt.hdn.contract.annotations.SchemaType.Companion.FIX
import pt.hdn.contract.annotations.SchemaType.Companion.OBJECTIVE
import pt.hdn.contract.annotations.SchemaType.Companion.RATE
import pt.hdn.contract.annotations.SchemaType.Companion.THRESHOLD
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.resources.Resources

class SchemaViewHolder(
    res: Resources,
    parent: ViewGroup,
    private val options: List<@SchemaType Int>,
    block: ((tag: Any?, isChecked: Boolean) -> Unit)? = null
) : ChipViewHolder<Schema>(res, parent) {

    init { this.isSingleSelection = true; this.onChipListener = block?.let { OnChipListener(it) } }

    @JvmName("setViewHolderWithoutContext")
    fun setViewHolder(schema: Schema?) {
        with(binding) {
            lblSelectorChipQuestion.apply { text = null; text = string(R.string.schemaQuestion) }

            xpgrpSelectorChipAnswer
                .apply {
                    removeAllViews()

                    options
                        .forEach {
                            with(Chip(context)) {
                                checkedIcon = drawable(R.drawable.ic_check)
                                isCheckedIconVisible = true
                                isCheckable = true
                                tag = it
                                isChecked = it == schema?.id
                                setOnCheckedChangeListener(this@SchemaViewHolder)

                                text = when (it) {
                                    FIX -> string(R.string.fix)
                                    RATE -> string(R.string.rate)
                                    COMMISSION -> string(R.string.commission)
                                    OBJECTIVE -> string(R.string.objective)
                                    THRESHOLD -> string(R.string.threshold)
                                    else -> null
                                }

                                addView(this)
                            }
                        }
                }
        }
    }

    //region NOT IN USE
    override fun Schema.setViewHolder() { }
    //endregion NOT IN USE
}