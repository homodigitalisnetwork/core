package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.view.ViewGroup
import com.google.android.material.chip.Chip
import pt.hdn.contract.annotations.SourceType.Companion.COMPANY_REVENUE
import pt.hdn.contract.annotations.SourceType.Companion.DISTANCE
import pt.hdn.contract.annotations.SourceType.Companion.TIME
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.resources.Resources

class SourceViewHolder(
    res: Resources,
    parent: ViewGroup,
    block: ((tag: Any?, isChecked: Boolean) -> Unit)? = null
): ChipViewHolder<Schema>(res, parent) {

    init { this.isSingleSelection = true; this.onChipListener = block?.let { OnChipListener(it) } }

    override fun Schema.setViewHolder() {
        with(binding) {
            lblSelectorChipQuestion.text = string(R.string.sourceQuestion)

            xpgrpSelectorChipAnswer
                .apply {
                    removeAllViews()

                    res.sources
                        .forEachWith {
//                            if ((this@setViewHolder !is RateSchema && key < DISTANCE) || (this@setViewHolder is RateSchema && key > COMPANY_REVENUE) ) {
                            if ((this@setViewHolder !is RateSchema && key < DISTANCE) || (this@setViewHolder is RateSchema && key == TIME) ) {
                                Chip(context)
                                    .apply {
                                        text = value
                                        checkedIcon = drawable(R.drawable.ic_check)
                                        isCheckedIconVisible = true
                                        isCheckable = true
                                        isChecked = source == key
                                        tag = key
                                        setOnCheckedChangeListener(this@SourceViewHolder)

                                        addView(this)
                                    }
                            }
                        }
                }
        }
    }
}