package pt.hdn.toolbox.specialities.util

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize

@Parcelize
data class Score(
    @Expose var rating: Float = 5f,
    @Expose var countRating: Int = 0,
    @Expose var countPro: Int = 0,
    @Expose var countOverachiever: Int = 0,
    @Expose var countWellManner: Int = 0,
    @Expose var countCleanWork: Int = 0,
    @Expose var countQuickWork: Int = 0
) : Parcelable
