package pt.hdn.toolbox.specialities.dealership.adapter

import android.view.ViewGroup
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.annotations.Mode.Companion.NOT_EXPANDABLE
import pt.hdn.toolbox.resources.Resources

class MarketAdapter(
    ref: MutableList<Schema>?,
    resources: Resources
) : ListAdapter<SchemaViewHolder, Schema>(ref?.mapTo(mutableListOf()) { it.clone() } ?: mutableListOf(), resources, NOT_EXPANDABLE) {
    override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): SchemaViewHolder = SchemaViewHolder(resources, parent)

    override fun clear() { super.clear(); sourceObservers(); sizeObservers() }
}