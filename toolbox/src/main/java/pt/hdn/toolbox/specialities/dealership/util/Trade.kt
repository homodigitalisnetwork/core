package pt.hdn.toolbox.specialities.dealership.util

import com.google.gson.annotations.Expose
import java.time.LocalDateTime

data class Trade(
    @Expose val schemasCandle: List<Candle>,
    @Expose val volume: Int,
    @Expose val date: LocalDateTime
)
