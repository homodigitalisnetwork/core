package pt.hdn.toolbox.specialities.dealership.util

import com.google.gson.annotations.Expose

data class Candle(
    @Expose val low: Float,
    @Expose val high: Float,
    @Expose val open: Float,
    @Expose val close: Float
)