package pt.hdn.toolbox.specialities.dealership.factory

import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.annotation.IdRes
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.annotations.Err as Err2
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.databinding.DialogDealershipBinding
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.specialities.SpecialitiesViewModel
import pt.hdn.toolbox.views.ArrayFragmentStateAdapter.Invoker

@AndroidEntryPoint
class MarketDialogFragment : BindingDialogFragment<DialogDealershipBinding>(DialogDealershipBinding::inflate, 80), OnClickListener {

    //region vars
    private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(specialitiesViewModel) {
                errorBus.observe(viewLifecycleOwner) { btnDealershipDialogSubmit.isEnabled = true; when (it) { Err.NO_CHANGE -> toast(R.string.noChanges); Err.NONE -> popBack() } }

                dataBus
                    .using(viewLifecycleOwner) {
                        observe<Unit>(Address.SCHEMA) { navigateTo(schemaDialog()) }
                        observeWith<Pair<Int, Int>>(Address.VIEW_2) { setViewVisibility(first, second) }
                        observe<Boolean>(Address.PERMISSION) { if (it) specialitiesViewModel.getOneShot() }
                    }

                positionResponse.observe(viewLifecycleOwner) { it.error { _, _ -> toast(R.string.failLocation) } }

                arrayFragmentStateAdapter(this@MarketDialogFragment, vpDealershipDialogContainer) {
                    onPageSelected { index, lastIndex ->
                        setViewVisibility(R.id.btn_dealershipDialogSubmit, if (index == lastIndex && lastIndex > 0) Visibility.ENABLE else Visibility.GONE)
                    }

                    add(::SchemasFragment)
                }.apply {
                    schemasAdapter
                        .source
                        .observe(viewLifecycleOwner) {
                            removeFrom(1)

                            it.takeIfWith { isNotEmpty() }?.run { addAll(map { Invoker(::ParameterFragment) }) }

                            if (it.isNotEmpty()) getPermission()
                        }
                }

                tabLayoutMediator(viewLifecycleOwner, layoutDealershipDialogDots, vpDealershipDialogContainer)

                btnDealershipDialogRemove.setOnClickListener(this@MarketDialogFragment)
                btnDealershipDialogEdit.setOnClickListener(this@MarketDialogFragment)
                btnDealershipDialogAdd.setOnClickListener(this@MarketDialogFragment)
                btnDealershipDialogSubmit.setOnClickListener(this@MarketDialogFragment)
            }
        }
    }

    override fun onClick(view: View?) {
        view?.id
            ?.let {
                when (it) {
                    R.id.btn_dealershipDialogSubmit -> { binding!!.btnDealershipDialogSubmit.isEnabled = false; specialitiesViewModel.saveMarket() }
                    else -> dataBus.send(viewLifecycleOwner, Address.CLICK_2, it)
                }
            }
    }

    override fun onBackPressed() { confirmationDialog() }

    private fun confirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(string(R.string.confirmation))
            .setMessage(string(R.string.areYouSure))
            .setPositiveButton(string(R.string.yes)) { _, _ -> popBack() }
            .setNegativeButton(string(R.string.no), null)
            .show()
    }

    private fun setViewVisibility(@IdRes id: Int, @Visibility state: Int) {
        with(binding!!) {
            when (id) {
                R.id.btn_dealershipDialogRemove -> with(btnDealershipDialogRemove) { if (state == Visibility.GONE) hide() else show() }
                R.id.btn_dealershipDialogAdd -> with(btnDealershipDialogAdd) { if (state == Visibility.GONE) hide() else show() }
                R.id.btn_dealershipDialogEdit -> with(btnDealershipDialogEdit) { if (state == Visibility.GONE) hide() else show() }
                R.id.btn_dealershipDialogSubmit -> with(btnDealershipDialogSubmit) { if (state == Visibility.GONE) hide() else show() }
            }
        }
    }
}