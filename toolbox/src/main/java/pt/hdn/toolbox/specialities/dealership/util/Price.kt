package pt.hdn.toolbox.specialities.dealership.util

import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.CandleDataSet
import com.github.mikephil.charting.data.CandleEntry
import com.google.gson.annotations.Expose
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.misc.DateValueFormatter
import pt.hdn.toolbox.misc.WindowBounds
import pt.hdn.toolbox.misc.forEachDay
import java.time.LocalDate
import java.time.temporal.ChronoUnit.DAYS

data class Price(
    @Expose private var trades: List<Trade>,
    @Expose private var windowBounds: WindowBounds,
    @Expose private var schemas: List<Schema>
) {

    //region vars
    private lateinit var schemasTrade: List<SchemaTrade>; private set
    //endregion vars

    operator fun get(index: Int): SchemaTrade = schemasTrade[index]

    fun init() {
        val barEntries: MutableList<BarEntry> = mutableListOf()
        val schemasCandleEntries: List<MutableList<CandleEntry>> = schemas.map { mutableListOf() }
        val lowerBound: LocalDate = windowBounds.lowerBound.toLocalDate()

        this.schemasTrade = with(trades) {
            if (isEmpty()) {
                windowBounds
                    .forEachDay { day ->
                        day
                            .toFloat()
                            .let { x -> schemasCandleEntries.forEach { it.add(CandleEntry(x, 0f, 0f, 0f, 0f)) }; barEntries.add(BarEntry(x, 0f)) }
                    }
            } else {
                var idx: Int = 0

                if (DAYS.between(lowerBound, this[idx].date) < 0) idx++

                windowBounds
                    .forEachDay { day ->
                        day
                            .toFloat()
                            .let { x ->
                                if (idx < size) {
                                    if (DAYS.between(lowerBound, this[idx].date) == day) {
                                        with(this[idx]) {
                                            schemasCandle.forEachIndexed { j, it -> schemasCandleEntries[j].add(CandleEntry(x, it.high, it.low, it.open, it.close)) }

                                            barEntries.add(BarEntry(x, volume.toFloat()))

                                            idx++
                                        }
                                    } else {
                                        if (idx - 1 < 0) schemasCandleEntries.forEach { it.add(CandleEntry(x, 0f, 0f, 0f, 0f)) }
                                        else this[idx - 1].schemasCandle.forEachIndexed { j, it -> schemasCandleEntries[j].add(CandleEntry(x, it.close, it.close, it.close, it.close)) }

                                        barEntries.add(BarEntry(x, 0f))
                                    }
                                } else {
                                    this[idx - 1].schemasCandle.forEachIndexed { j, it -> schemasCandleEntries[j].add(CandleEntry(x, it.close, it.close, it.close, it.close)) }

                                    barEntries.add(BarEntry(x, 0f))
                                }
                            }
                    }
            }

            schemasCandleEntries
                .mapIndexed { idx, it -> SchemaTrade(CandleDataSet(it, null), BarDataSet(barEntries, null), DateValueFormatter(lowerBound), schemas[idx]) }
        }
    }
}