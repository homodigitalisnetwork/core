package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import pt.hdn.contract.annotations.Parameter
import pt.hdn.contract.annotations.Parameter.Companion.LOWER_BOUND
import pt.hdn.contract.annotations.Parameter.Companion.UPPER_BOUND
import pt.hdn.contract.annotations.SchemaType
import pt.hdn.contract.annotations.SchemaType.Companion.COMMISSION
import pt.hdn.contract.annotations.SchemaType.Companion.FIX
import pt.hdn.contract.annotations.SchemaType.Companion.OBJECTIVE
import pt.hdn.contract.annotations.SchemaType.Companion.RATE
import pt.hdn.contract.annotations.SchemaType.Companion.THRESHOLD
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.adapter.LifecycleAdapter
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.misc.toInt
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.dealership.util.*
import java.math.BigDecimal.ZERO

class SchemaFactoryAdapter(
    val ref: Schema?,
    private val res: Resources,
    private val schemas: List<@SchemaType Int> =  listOf(FIX, RATE, COMMISSION, OBJECTIVE, THRESHOLD)
): LifecycleAdapter<BindingViewHolder<ViewBinding>>() {

    //region vars
    var schema: Schema? = ref?.clone(); private set
    @Parameter private lateinit var parameter: String
    private var length = 0
    private var lowerBoundPosition: Int? = null
    private var upperBoundPosition: Int? = null
    private val offset: Int = (ref == null).toInt()
    private val mutableSizeFlow: MutableStateFlow<Int> = MutableStateFlow(length)
    private val mutableSize: MutableLiveData<Int> = MutableLiveData(length)
    val size: LiveData<Int> = mutableSize
    val sizeFlow: StateFlow<Int> = mutableSizeFlow
    //endregion vars

    private companion object {
        private const val SCHEMA = 0
        private const val SOURCE = 1
        private const val BOUNDS = 2
        private const val THRES = 3
        private const val VALUE = 4
    }

    init {
        this.length = with(schema) {
            when (this) {
                is FixSchema -> { fix = null; 0 }
                is RateSchema -> { rate = null; 1 }
                is CommissionSchema -> { cut = null; 1 + (source != null).toInt() + (lowerBound != null).toInt() + (upperBound != null).toInt() }
                is ObjectiveSchema -> { bonus = null; 1 + (source != null).toInt() + (lowerBound != null).toInt() + (upperBound != null).toInt() }
                is ThresholdSchema -> { bonus = null; 1 + (source != null).toInt() + (isAbove != null).toInt() }
                else -> 0
            }
        } + offset
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<ViewBinding> {
        return when (viewType) {
            SCHEMA -> SchemaViewHolder(res, parent, schemas)  { tag, isChecked ->
                (tag as? @SchemaType Int)
                    ?.let {
                        this@SchemaFactoryAdapter.lowerBoundPosition = null
                        this@SchemaFactoryAdapter.upperBoundPosition = null

                        if (isChecked) {
                            this@SchemaFactoryAdapter.schema = when (it) {
                                FIX -> FixSchema()
                                RATE -> RateSchema()
                                COMMISSION -> CommissionSchema()
                                OBJECTIVE -> ObjectiveSchema()
                                THRESHOLD -> ThresholdSchema()
                                else -> null
                            }

                            this@SchemaFactoryAdapter.length
                                .let { size ->
                                    (it != FIX).toInt().let { this@SchemaFactoryAdapter.length = 1 + it; notifyItemSetted(-1 + it, size); notifyItemRemoveExcess(it, size) }

                                    setSize()
                                }
                        } else if (schema?.let { me -> me.id == it } == true) {
                            this@SchemaFactoryAdapter.schema = null
                            this@SchemaFactoryAdapter.length.let { size -> this@SchemaFactoryAdapter.length = 1; notifyItemRemoveExcess(0, size); setSize() }
                        }
                }
            }
            SOURCE -> SourceViewHolder(res, parent) { tag, isChecked ->
                (tag as? Int)
                    ?.let {
                        this@SchemaFactoryAdapter.lowerBoundPosition = null
                        this@SchemaFactoryAdapter.upperBoundPosition = null

                        this.schema
                            ?.apply {
                                when(this) {
                                    is CommissionSchema -> { lowerBound = null; upperBound = null }
                                    is ObjectiveSchema -> { lowerBound = null; upperBound = null }
                                    is ThresholdSchema -> { threshold = null; isAbove = null }
                                }

                                if (isChecked) {
                                    source = it

                                    this@SchemaFactoryAdapter.length
                                        .let { size ->
                                            if (this !is RateSchema) {
                                                this@SchemaFactoryAdapter.length = 2 + offset; notifyItemSetted(1, size); notifyItemRemoveExcess(2, size); setSize()
                                            }
                                        }
                                } else if (source?.let { me -> me == it } == true) {
                                    source = null

                                    this@SchemaFactoryAdapter.length.let { size -> this@SchemaFactoryAdapter.length = 1 + offset; notifyItemRemoveExcess(1, size); setSize() }
                                }
                            }
                }
            }
            BOUNDS -> BoundsChipViewHolder(res, parent) { tag, isChecked ->
                (tag as? @Parameter String)
                    ?.let {
                        when (it) {
                            LOWER_BOUND -> {
                                if (isChecked) {
                                    this@SchemaFactoryAdapter.parameter = LOWER_BOUND
                                    this@SchemaFactoryAdapter.length += 1
                                    this@SchemaFactoryAdapter.lowerBoundPosition = 2 + offset
                                    this@SchemaFactoryAdapter.upperBoundPosition?.let { this@SchemaFactoryAdapter.upperBoundPosition = 3 + offset }

                                    notifyItemInserted(2 + offset)
                                } else {
                                    schema?.let { when (it) {is CommissionSchema -> it.lowerBound = null; is ObjectiveSchema -> it.lowerBound = null } }

                                    this@SchemaFactoryAdapter.length -= 1
                                    this@SchemaFactoryAdapter.lowerBoundPosition = null
                                    this@SchemaFactoryAdapter.upperBoundPosition?.let { this@SchemaFactoryAdapter.upperBoundPosition = 2 + offset }

                                    notifyItemRemoved(2 + offset)
                                }
                            }
                            UPPER_BOUND -> {
                                if (isChecked) {
                                    this@SchemaFactoryAdapter.parameter = UPPER_BOUND
                                    this@SchemaFactoryAdapter.length += 1

                                    ((this@SchemaFactoryAdapter.lowerBoundPosition != null).toInt() + 2 + offset)
                                        .let {
                                            this@SchemaFactoryAdapter.upperBoundPosition = it

                                            notifyItemInserted(it)
                                        }
                                } else {
                                    schema?.let { when (it) {is CommissionSchema -> it.upperBound = null; is ObjectiveSchema -> it.upperBound = null } }

                                    this@SchemaFactoryAdapter.length -= 1
                                    this@SchemaFactoryAdapter.upperBoundPosition = null

                                    notifyItemRemoved((this@SchemaFactoryAdapter.lowerBoundPosition != null).toInt() + 2 + offset)
                                }
                            }
                        }

                        setSize()
                    }
            }
            THRES -> ThresholdViewHolder(res, parent) { tag, isChecked ->
                (tag as? Boolean)
                    ?.let {
                        with(schema as ThresholdSchema) {
                            threshold = null

                            if (isChecked) {
                                isAbove = it

                                this@SchemaFactoryAdapter.parameter = Parameter.THRESHOLD
                                this@SchemaFactoryAdapter.length.let { size -> this@SchemaFactoryAdapter.length = 3 + offset; notifyItemSetted(2, size) }
                            }
                            else if (isAbove?.let { me -> me == it } == true) { isAbove = null; this@SchemaFactoryAdapter.length = 2 + offset; notifyItemRemoved(2 + offset) }

                            setSize()
                        }
                    }
            }
            else /*VALUE*/-> ValueViewHolder(res, parent)
        }
    }

    override fun onBindViewHolder(bvh: BindingViewHolder<ViewBinding>, flatPosition: Int) {
        with(bvh) {
            when (this) {
                is SchemaViewHolder -> setViewHolder(schema)
                is SourceViewHolder -> schema?.setViewHolder()
                is BoundsChipViewHolder -> schema?.setViewHolder()
                is ThresholdViewHolder -> schema?.setViewHolder()
                is ValueViewHolder -> schema?.setViewHolder(parameter)
                else -> null
            }
        }
    }

    override fun getItemViewType(flatPosition: Int): Int {
        return when (flatPosition + (ref != null).toInt()) {
            0 -> SCHEMA
            1 -> SOURCE
            2 -> if (schema is ThresholdSchema) THRES else BOUNDS
            else -> VALUE
        }
    }

    override fun getItemCount(): Int = length

    @Err fun validate(): Int {
        return with(schema) {
           when (this) {
                null -> Err.SCHEMA
                ref -> Err.NO_CHANGE
                is FixSchema -> Err.NONE
                is RateSchema -> { when { source == null -> Err.SOURCE; else -> Err.NONE } }
                is CommissionSchema -> {
                    when {
                        source == null -> Err.SOURCE
                        lowerBound?.let { it < ZERO } == true -> Err.LOWER_BOUND
                        upperBound?.let { it < ZERO } == true -> Err.UPPER_BOUND
                        lowerBound?.let { me -> upperBound?.let { me >= it } } == true -> Err.INVALID
                        else -> Err.NONE
                    }
                }
                is ObjectiveSchema -> {
                    when {
                        source == null -> Err.SOURCE
                        lowerBound == null && upperBound == null -> Err.NO_BOUND
                        lowerBound?.let { it < ZERO } == true -> Err.LOWER_BOUND
                        upperBound?.let { it < ZERO } == true -> Err.UPPER_BOUND
                        lowerBound?.let { me -> upperBound?.let { me >= it } } == true -> Err.INVALID
                        else -> Err.NONE
                    }
                }
                is ThresholdSchema -> {
                    when {
                        source == null -> Err.SOURCE
                        isAbove == null -> Err.IS_ABOVE
                        threshold?.let { it < ZERO } == false -> Err.THRESHOLD
                        else -> Err.NONE
                    }
                }
                else -> Err.UNKNOWN
            }
        }
    }
    
    private fun setSize() { mutableSize.value = length; mutableSizeFlow.tryEmit(length) }

    private fun notifyItemRemoveExcess(value: Int, size: Int) { (value + offset).let { if (size > it) notifyItemRangeRemoved(it, size - it) } }

    private fun notifyItemSetted(value: Int, size: Int) { (if (size == (value + offset)) ::notifyItemInserted else ::notifyItemChanged).invoke(value + offset) }
}