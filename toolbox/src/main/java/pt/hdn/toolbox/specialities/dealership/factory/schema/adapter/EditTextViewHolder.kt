package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.text.TextWatcher
import android.view.ViewGroup
import pt.hdn.contract.annotations.Parameter
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.databinding.ViewholderSelectorEdittextBinding
import pt.hdn.toolbox.resources.Resources

abstract class EditTextViewHolder<T: Any>(
    resources: Resources,
    parent: ViewGroup
): BindingViewHolder<ViewholderSelectorEdittextBinding>(resources, parent, ViewholderSelectorEdittextBinding::inflate), TextWatcher {

    //region vars
    @Parameter protected lateinit var parameter: String
    protected lateinit var t: T
    //endregion vars

    protected abstract fun T.set()

    fun T.setViewHolder(@Parameter parameter: String) { this@EditTextViewHolder.t = this; this@EditTextViewHolder.parameter = parameter; set() }

    //region NOT IN USE
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
    //endregion NOT IN USE
}