package pt.hdn.toolbox.specialities

import android.annotation.SuppressLint
import androidx.activity.result.IntentSenderRequest
import androidx.lifecycle.*
import androidx.navigation.NavDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Eagerly
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.contract.annotations.Market
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.specialities.adapter.SpecialitiesAdapter
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Action.Companion.ADD
import pt.hdn.toolbox.annotations.Action.Companion.REMOVE
import pt.hdn.toolbox.annotations.Action.Companion.SET
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.ErrorBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.market.PriceRequest
import pt.hdn.toolbox.communications.specialities.SpecialitiesRequest
import pt.hdn.toolbox.communications.specialities.SpecialitiesResponse
import pt.hdn.toolbox.foreground.ForegroundService
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.specialities.dealership.util.SpecialityDealership
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.dealership.SpecialityDialogFragmentDirections
import pt.hdn.toolbox.specialities.dealership.factory.MarketDialogFragmentDirections
import pt.hdn.toolbox.specialities.dealership.factory.adapter.SchemasAdapter
import pt.hdn.toolbox.specialities.dealership.util.Price
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.viewmodels.CoreViewModel
import javax.inject.Inject

@SuppressLint("StaticFieldLeak")
@HiltViewModel
class SpecialitiesViewModel @Inject constructor(
    foregroundServiceFlow: Flow<ForegroundService?>,
    sessionBus: SessionBus,
    dataBus: DataBus,
    repo: CoreRepo,
    resources: Resources
) : CoreViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    var availableSpecialities: MutableList<UUIDNameMarketData>? = null; private set
    val specialitiesAdapter: SharedFlow<SpecialitiesAdapter?>
    //endregion vars

    init {
        this.specialitiesAdapter = flowOf(SpecialitiesRequest(command = Command.GET, partyUUID = session.partyUUID, action = Action.GET))
            .flatMapLatest { repo.specialities(it) }
            .map {
                var adapter: SpecialitiesAdapter? = null

                it.success {
                    specialities
                        ?.apply {
                            this@SpecialitiesViewModel.availableSpecialities = associateBy { it.typeUUID!! }
                                .let { map ->
                                    res.specialities.asSequence().filterWith { map[key]?.run { type = value; false } ?: true }.map { it.value }.filter { (it.market and SERVICE) == SERVICE }.toMutableList()
                                }

                            if (size > 1) sortWith(compareBy(res.collator) { it.type!!.name })

                            adapter = SpecialitiesAdapter(this, res)
                        }
                }

                adapter
            }
            .shareIn(viewModelScope, Lazily, 1)
    }

    fun remove() {
        launch {
            request.emit(
                SpecialitiesRequest(
                    command = Command.REMOVE,
                    deputyUUID = session.deputyUUID,
                    uuid = specialitiesAdapter.value()?.firstSelected?.uuid,
                    action = REMOVE
                )
            )
        }
    }

    fun setType(uuidNameMarketData: UUIDNameMarketData) { launch { specialityDealership.setType(uuidNameMarketData) } }

    fun specialityDialog(): NavDirections {
        this.specialityDealership = SpecialityDealership(res, specialitiesAdapter.value()?.firstSelected)

        return SpecialitiesFragmentDirections.actionSpecialitiesFragmentToSpecialityDialogFragment()
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------

    //region vars
    private var foregroundService: ForegroundService? = null
    private lateinit var specialityDealership: SpecialityDealership; private set
    private val positionRequest: MutableSharedFlow<ForegroundService> = MutableSharedFlow()
    private val priceRequest: MutableSharedFlow<PriceRequest> = MutableSharedFlow()
    private val request: MutableSharedFlow<SpecialitiesRequest> = MutableSharedFlow()
    private val mutablePermission: MutableSharedFlow<String> = MutableSharedFlow()
    val permission: Flow<String> = mutablePermission
    val priceResponse: Flow<Price?>
    val response: Flow<Result<SpecialitiesResponse>>
    val positionResponse: Flow<Result<Position>>
    val errorBus: ErrorBus; get() = specialityDealership.errorBus
    val inMarket: Flow<Boolean>; get() = specialityDealership.inMarket
    val isNewBuild: Boolean; get() = specialityDealership.isNewBuild
    val marketType: Flow<Int?>; get() = specialityDealership.marketType
    val speciality: Speciality; get() = specialityDealership.speciality
    val schemasAdapter: SchemasAdapter; get() = specialityDealership.schemasAdapter
    val schemaFactoryAdapter; get() = specialityDealership.schemaFactoryAdapter
    //endregion vars

    init {
        foregroundServiceFlow.observe(viewModelScope) { this.foregroundService = it }

        dataBus.observe<Boolean>(viewModelScope, Address.PERMISSION) { if (it) foregroundService?.run { positionRequest.emit(this@run) } }

        this.priceResponse = priceRequest
            .flatMapLatest { repo.price(it) }
            .map { var price: Price? = null; it.success { price = this.price?.apply { init() } }; price }
            .flowOn(Default)
            .shareIn(viewModelScope, Lazily, 1)

        this.response = request
            .flatMapLatest { repo.specialities(it) }
            .onEach {
                it.success {
                    with(specialitiesAdapter.value()!!) {
                        with(request) {
                            when (action) {
                                REMOVE -> { firstSelected!!.type!!.let { availableSpecialities?.apply { add(it); sortWith(compareBy(res.collator) { it.name }) } }; removeSelected() }
                                ADD -> specialityDealership.speciality.let{ it.uuid = this@success.uuid; addSorted(it); availableSpecialities?.remove(it.type!!) }
                                SET -> replaceSelected(specialityDealership.speciality)
                            }
                        }
                    }
                }
            }
            .shareIn(viewModelScope, Lazily)

        this.positionResponse = positionRequest
            .flatMapLatest { it.getOneShot() }
            .onEach { it.success { getMarket(this) } }
            .shareIn(viewModelScope, Lazily)
    }

    operator fun get(@Market market: Int) = specialityDealership[market]

    fun getPermission() { dataBus.send(viewModelScope, Address.PERMISSION, Unit) }

    fun getOneShot() { foregroundService?.run { launch { positionRequest.emit(this@run) } } }

    fun clearMarket(@Market market: Int) { specialityDealership.clearMarket(market) }

    fun marketDialog(): NavDirections = SpecialityDialogFragmentDirections.actionSpecialityDialogFragmentToMarketDialogFragment()

    fun schemaDialog(): NavDirections = MarketDialogFragmentDirections.actionMarketDialogFragmentToSchemaDialogFragment()

    fun prepareMarket(@Market market: Int) { specialityDealership.prepareMarket(market); dataBus.send(viewModelScope, Address.SPECIALITY, Unit) }

    fun saveMarket() { launch { specialityDealership.saveMarket() } }

    fun saveSpeciality() {
        launch {
            with(specialityDealership) {
                if (validate() == Err.NONE) {
                    request.emit(
                        SpecialitiesRequest(
                            command = if (isNewBuild) Command.ADD else Command.SET,
                            deputyUUID = session.deputyUUID,
                            partyUUID = session.partyUUID,
                            uuid = speciality.uuid,
                            speciality = reduce(),
                            action = if (isNewBuild) ADD else SET
                        )
                    )
                }
            }
        }
    }

    fun removeSchema() { specialityDealership.removeSchema() }

    fun prepareSchema() { specialityDealership.prepareSchema(); dataBus.send(viewModelScope, Address.SCHEMA, Unit) }

    fun validateSchema(@Address address: Int) { launch { specialityDealership.validateSchema(address) } }

    fun saveSchema() { specialityDealership.saveSchema() }

    private fun getMarket(position: Position) { launch { priceRequest.emit(specialityDealership.getMarketRequest(position)) } }
}
