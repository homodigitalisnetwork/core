package pt.hdn.toolbox.specialities.dealership.factory

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.*
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentParameterBinding
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.specialities.SpecialitiesViewModel
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ZERO

@AndroidEntryPoint
class ParameterFragment : BindingFragment<FragmentParameterBinding>(FragmentParameterBinding::inflate), TextWatcher {

    //region vars
    private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(specialitiesViewModel) {
                with(schemasAdapter[index - 1]) {
                    errorBus
                        .observe(viewLifecycleOwner) {
                            if (it == Err.INVALID) {
                                when (this) {
                                    is FixSchema -> fix
                                    is RateSchema -> rate
                                    is CommissionSchema -> cut
                                    is ObjectiveSchema -> bonus
                                    is ThresholdSchema -> bonus
                                    else -> null
                                }?.run { handleError(txtParameterAnswer); toast(R.string.invalidValue) }
                            }
                        }

                    priceResponse
                        .observe(viewLifecycleOwner) {
                            it
                                ?.apply {
                                    barParameterLoading.visibility = GONE

                                    tradeParameterCharts.apply { res = this@ParameterFragment.res; errorMessage = string(R.string.failToGetMarket); data = get(index - 1) }
                                }
                        }

                    viewLifecycleOwner.syncOnResumedObserver { setViewVisibility() }

                    lblParameterDisplay.apply { visibility = GONE; text = string(R.string.failToGetMarket) }

                    txtParameterAnswer.addTextChangedListener(this@ParameterFragment)

                    lblParameterDescription
                        .text = when (this) {
                            is FixSchema -> string(R.string.descriptionFix)
                            is RateSchema -> string(R.string.descriptionRate)
                            is CommissionSchema -> string(R.string.descriptionCut)
                            is ObjectiveSchema -> string(R.string.descriptionBonus)
                            is ThresholdSchema -> string(R.string.descriptionBonus)
                            else -> null
                        }

                    lblParameterQuestion
                        .text = when (this) {
                            is FixSchema -> fix?.let { "${string(R.string.questionFix)} (${string(R.string.previous)}: $it)" } ?: string(R.string.questionFix)
                            is RateSchema -> rate?.let { "${string(R.string.questionRate)} (${string(R.string.previous)}: $it)" } ?: string(R.string.questionRate)
                            is CommissionSchema -> cut?.let { "${string(R.string.questionCut)} (${string(R.string.previous)}: $it)" } ?: string(R.string.questionCut)
                            is ObjectiveSchema -> bonus?.let { "${string(R.string.questionBonus)} (${string(R.string.previous)}: $it)" } ?: string(R.string.questionBonus)
                            is ThresholdSchema -> bonus?.let { "${string(R.string.questionBonus)} (${string(R.string.previous)}: $it)" } ?: string(R.string.questionBonus)
                            else -> null
                        }
                }
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {
        with(specialitiesViewModel.schemasAdapter[index - 1]) {
            s?.toString()
                ?.takeIfWith { isNumber() }
                ?.toBigDecimal()
                ?.takeIf { it >= ZERO && if (this is CommissionSchema) it < ONE else true }
                .let {
                    when (this) {
                        is FixSchema -> fix = it
                        is RateSchema -> rate = it
                        is CommissionSchema -> cut = it
                        is ObjectiveSchema -> bonus = it
                        is ThresholdSchema -> bonus = it
                    }
                }
        }
    }

    private fun setViewVisibility() {
        dataBus.send(
            owner = viewLifecycleOwner,
            address = Address.VIEW_2,
            values = arrayOf(
                R.id.btn_dealershipDialogEdit to Visibility.GONE,
                R.id.btn_dealershipDialogRemove to Visibility.GONE,
                R.id.btn_dealershipDialogAdd to Visibility.GONE
            )
        )
    }

    //region NOT IN USE
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
    //region NOT IN USE
}