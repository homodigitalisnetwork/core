package pt.hdn.toolbox.specialities

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.databinding.FragmentViewerBinding
import pt.hdn.toolbox.misc.*

@AndroidEntryPoint
class SpecialitiesFragment : BindingFragment<FragmentViewerBinding>(FragmentViewerBinding::inflate) {

    //region vars
    private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(specialitiesViewModel) {
                lblViewerDisplay.apply { visibility = VISIBLE; setCompoundTopDrawable(R.drawable.ic_speciality) }

                response.observe(viewLifecycleOwner) { it.error { _, _ -> toast(R.string.failExe) }; setViewVisibility() }

                dataBus.observe<Int>(viewLifecycleOwner, Address.CLICK_1) { onClick(it) }

                specialitiesAdapter
                    .observe(viewLifecycleOwner) {
                        barViewerLoading.visibility = GONE

                        it
                            ?.apply {
                                recViewerContainer.apply { setHasFixedSize(true); adapter = it }

                                size.observe(this@observe) { lblViewerDisplay.apply { text = string(R.string.addASpeciality); visibility = if (it == 0) VISIBLE else GONE } }

                                highlighted.observe(this@observe) { setViewVisibility() }

                                viewLifecycleOwner.syncOnResumedObserver { setViewVisibility() }
                            }
                            ?: run { lblViewerDisplay.apply { setCompoundTopDrawable(R.drawable.ic_sad_face); text = string(R.string.somethingWrong) } }
                    }

                barViewerLoading.visibility = VISIBLE
            }
        }
    }

    private fun onClick(id: Int) {
        with(specialitiesViewModel) {
            when (id) {
                R.id.btn_definitionsRemove -> {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(string(R.string.confirmation))
                        .setMessage(string(R.string.areYouSure))
                        .setPositiveButton(string(R.string.yes)) { _, _ ->
                            dataBus.send(
                                owner = viewLifecycleOwner,
                                address = Address.VIEW_1,
                                values = arrayOf(
                                    R.id.btn_definitionsEdit to Visibility.GONE,
                                    R.id.btn_definitionsRemove to Visibility.GONE,
                                    R.id.btn_definitionsAdd to Visibility.GONE,
                                    R.id.btn_definitionsCheck to Visibility.GONE
                                )
                            )

                            remove()
                        }
                        .setNegativeButton(string(R.string.no), null)
                        .show()
                }
                R.id.btn_definitionsAdd, R.id.btn_definitionsEdit -> navigateTo(specialityDialog())
                else -> null
            }
        }
    }

    private fun setViewVisibility() {
        with(specialitiesViewModel) {
            dataBus.send(
                owner = viewLifecycleOwner,
                address = Address.VIEW_1,
                values = specialitiesAdapter
                    .value()
                    ?.firstSelected
                    ?.run {
                        arrayOf(
                            R.id.btn_definitionsCheck to Visibility.GONE,
                            R.id.btn_definitionsRemove to Visibility.ENABLE,
                            R.id.btn_definitionsAdd to Visibility.GONE,
                            R.id.btn_definitionsEdit to Visibility.ENABLE
                        )
                    } ?: run {
                        arrayOf(
                            R.id.btn_definitionsCheck to Visibility.GONE,
                            R.id.btn_definitionsRemove to Visibility.GONE,
                            R.id.btn_definitionsEdit to Visibility.GONE,
                            R.id.btn_definitionsAdd to if (specialitiesAdapter.isEmpty() || availableSpecialities?.isEmpty() == true) Visibility.GONE else Visibility.ENABLE
                        )
                    }
            )
        }
    }
}