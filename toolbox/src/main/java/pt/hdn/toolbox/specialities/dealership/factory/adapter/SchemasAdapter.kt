package pt.hdn.toolbox.specialities.dealership.factory.adapter

import android.view.ViewGroup
import pt.hdn.contract.annotations.Err as Err2
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.resources.Resources

class SchemasAdapter(
    private val reference: List<Schema>?,
    res: Resources
) : ListAdapter<SchemaSummaryViewHolder, Schema>(reference?.mapTo(mutableListOf()) { it.clone() } ?: mutableListOf(), res) {

    override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): SchemaSummaryViewHolder = SchemaSummaryViewHolder(resources, parent)

    override fun onBindViewHolder(holder: SchemaSummaryViewHolder, position: Int) {
        with(holder) { ref[position].setViewHolder(selectedPosition.containsKey(position) && ref.size > 1) }
    }

    @Err
    fun validate(): Int {
        with(ref) {
            return when {
                reference?.let { this == it || toSet() == it.toSet() } == true -> Err.NO_CHANGE
                isEmpty() -> Err.SCHEMA
                any { it.validate() != Err2.NONE } -> Err.INVALID
                else -> Err.NONE
            }
        }
    }
}