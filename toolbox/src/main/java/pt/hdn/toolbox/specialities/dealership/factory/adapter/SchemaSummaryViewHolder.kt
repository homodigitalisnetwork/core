package pt.hdn.toolbox.specialities.dealership.factory.adapter

import android.view.View.*
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TableLayout
import android.widget.TableRow
import androidx.annotation.StringRes
import com.google.android.material.textview.MaterialTextView
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.databinding.ViewholderSchemaSchemaBinding
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal

class SchemaSummaryViewHolder(
	resources: Resources,
	parent: ViewGroup
): HDNViewHolder<ViewholderSchemaSchemaBinding, Schema>(resources, parent, ViewholderSchemaSchemaBinding::inflate) {

	//region vars
	private val smallMargin: Int = dimen(R.dimen.s_size)
	private val tinyMargin: Int = dimen(R.dimen.t_size)
	//endregion vars

	override fun Schema.setViewHolder(isExpanded: Boolean) {
		with(binding) {
			root.isChecked = isExpanded

			lblSchemaTitle
				.apply {
					text = null
					text = when (this@setViewHolder) {
						is FixSchema -> string(R.string.fix)
						is RateSchema -> string(R.string.rate)
						is CommissionSchema -> string(R.string.commission)
						is ObjectiveSchema -> string(R.string.objective)
						is ThresholdSchema -> string(R.string.threshold)
						else -> null
					}
				}

			tlSchemaParameters
				.apply {
					removeAllViews()

					if (this@setViewHolder !is FixSchema) { visibility = VISIBLE; addParameter(smallMargin, R.string.over, res.sources[source]) }

					when (this@setViewHolder) {
						is CommissionSchema -> { lowerBound?.let { addParameter(smallMargin, R.string.lowerBound, it) }; upperBound?.let { addParameter(smallMargin, R.string.upperBound, it) } }
						is ObjectiveSchema -> { lowerBound?.let { addParameter(smallMargin, R.string.lowerBound, it) }; upperBound?.let { addParameter(smallMargin, R.string.upperBound, it) } }
						is ThresholdSchema -> { threshold?.let { addParameter(smallMargin, R.string.threshold, it) }; isAbove?.let { addParameter(smallMargin, R.string.aboveLimit, it) } }
					}
				}
		}
	}

	private fun addParameter (leftMargin: Int, @StringRes field: Int, value: BigDecimal?) { addParameter(leftMargin, field, value?.toString()) }

	private fun addParameter (leftMargin: Int, @StringRes field: Int, value: Boolean) { addParameter(leftMargin, field, string(if (value) R.string.yes else R.string.no)) }

	private fun addParameter (leftMargin: Int, @StringRes field: Int, value: String?) {
		with(TableRow(context)) {
			layoutParams = TableLayout.LayoutParams().apply { setMargins(0, tinyMargin, 0, 0) }

			with(MaterialTextView(context)) {
				layoutParams = TableRow.LayoutParams().apply { setMargins(leftMargin, 0, 0, 0) }
				textAlignment = TEXT_ALIGNMENT_TEXT_END
				text = string(field)

				addView(this)
			}

			with(MaterialTextView(context)) {
				layoutParams = TableRow.LayoutParams(0, WRAP_CONTENT, 1f).apply { setMargins(tinyMargin, 0, 0, 0) }
				textAlignment = TEXT_ALIGNMENT_TEXT_START
				text = value

				addView(this)
			}

			binding.tlSchemaParameters.addView(this)
		}
	}
}