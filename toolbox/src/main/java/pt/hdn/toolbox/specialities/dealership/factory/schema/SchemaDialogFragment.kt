package pt.hdn.toolbox.specialities.dealership.factory.schema

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import androidx.activity.addCallback
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.DialogSchemaBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.specialities.SpecialitiesViewModel

@AndroidEntryPoint
class SchemaDialogFragment : BindingDialogFragment<DialogSchemaBinding>(DialogSchemaBinding::inflate, 120), OnClickListener {

    //region vars
    private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(specialitiesViewModel) {
                errorBus
                    .observe(viewLifecycleOwner, Address.ADDRESS_2) {
                        when (it) {
                            Err.SCHEMA -> toast(R.string.missingSchema)
                            Err.NO_CHANGE -> toast(R.string.noChanges)
                            Err.SOURCE -> toast(R.string.missingSource)
                            Err.LOWER_BOUND -> toast(R.string.missingLowerBound)
                            Err.UPPER_BOUND -> toast(R.string.missingUpperBound)
                            Err.THRESHOLD -> toast(R.string.missingThreshold)
                            Err.IS_ABOVE -> toast(R.string.missingIsAbove)
                            Err.NO_BOUND -> toast(R.string.missingABound)
                            Err.INVALID -> toast(R.string.swapBoundValues)
                            Err.UNKNOWN -> toast(R.string.unknown)
                            else -> {
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(string(R.string.confirmation))
                                    .setMessage(string(R.string.schemasValuesReset))
                                    .setPositiveButton(string(R.string.yes)) { _, _ -> saveSchema(); popBack() }
                                    .setNegativeButton(string(R.string.no), null)
                                    .show()
                            }
                        }
                    }

                schemaFactoryAdapter
                    .let {
                        recSchemaFactoryContainer.apply { setHasFixedSize(true); adapter = it; it.sizeFlow.observe(viewLifecycleOwner) { if (it > 0) { smoothScrollToPosition(it - 1) } } }
                    }

                btnSchemaFactorySubmit.setOnClickListener(this@SchemaDialogFragment)
            }
        }
    }

    override fun onClick(v: View?) { v?.id?.also { when(it) {R.id.btn_schemaFactorySubmit -> specialitiesViewModel.validateSchema(Address.ADDRESS_2) } } }

    override fun onBackPressed() { confirmationDialog() }

    private fun confirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(string(R.string.confirmation))
            .setMessage(string(R.string.areYouSure))
            .setPositiveButton(string(R.string.yes)) { _, _ -> popBack() }
            .setNegativeButton(string(R.string.no), null)
            .show()
    }
}