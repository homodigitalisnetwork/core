package pt.hdn.toolbox.specialities.dealership.factory

import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.lifecycle.Lifecycle.State.RESUMED
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentSchemasBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.specialities.SpecialitiesViewModel

@AndroidEntryPoint
class SchemasFragment: BindingFragment<FragmentSchemasBinding>(FragmentSchemasBinding::inflate) {

    //region vars
    private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(specialitiesViewModel) {
                errorBus.observe(viewLifecycleOwner) { if (it == Err.SCHEMA) handleError(recSchemasContainer) }

                dataBus.observe<Int>(viewLifecycleOwner, Address.CLICK_2) { onClick(it) }

                schemasAdapter
                    .run {
                        recSchemasContainer.apply { setHasFixedSize(true); adapter = this@run }

                        size.observe(viewLifecycleOwner) { lblSchemasDisplay.visibility = if (it == 0) VISIBLE else GONE }

                        highlighted.observe(viewLifecycleOwner, state = RESUMED) { setViewVisibility() }

                        if (size.value == 0) onClick(R.id.btn_dealershipDialogAdd)
                    }
            }
        }
    }

    private fun onClick(id: Int) {
        with(specialitiesViewModel) {
            when (id) {
                R.id.btn_dealershipDialogRemove -> removeSchema()
                R.id.btn_dealershipDialogAdd, R.id.btn_dealershipDialogEdit -> prepareSchema()
            }
        }
    }

    private fun setViewVisibility() {
        with(specialitiesViewModel) {
            dataBus.send(
                owner = viewLifecycleOwner,
                address = Address.VIEW_2,
                values = schemasAdapter
                    .takeIf { it.size.value > 1 }
                    ?.firstSelected
                    ?.run {
                        arrayOf(
                            R.id.btn_dealershipDialogRemove to Visibility.ENABLE,
                            R.id.btn_dealershipDialogAdd to Visibility.GONE,
                            R.id.btn_dealershipDialogEdit to Visibility.ENABLE
                        )
                    } ?: run {
                        arrayOf(
                            R.id.btn_dealershipDialogRemove to Visibility.GONE,
                            R.id.btn_dealershipDialogEdit to Visibility.GONE,
                            R.id.btn_dealershipDialogAdd to Visibility.ENABLE
                        )
                    }
            )
        }
    }
}