package pt.hdn.toolbox.specialities.adapter

import android.view.View.*
import android.view.ViewGroup
import android.widget.*
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.databinding.ViewholderSpecialityBinding
import pt.hdn.toolbox.misc.takeIfWith
import pt.hdn.toolbox.misc.takeUnlessWith
import pt.hdn.toolbox.resources.Resources

class SpecialityViewHolder constructor(
    resources: Resources,
    parent: ViewGroup
) : HDNViewHolder<ViewholderSpecialityBinding, Speciality>(resources, parent, ViewholderSpecialityBinding::inflate) {

    init { with(binding) { schemasSpecialityService.resources = res; schemasSpecialityPerson.resources = res } }

    override fun Speciality.setViewHolder(isExpanded: Boolean) {
        with(binding) {
            with(score) score@{
                lblSpecialityTitle.apply { text = null; text = type?.name }

                layoutSpecialityBody.visibility = if (isExpanded) VISIBLE else GONE

                if (isExpanded) {
                    llSpecialityPerson.visibility = personSchemas?.takeUnless { it.isEmpty() }?.let { schemasSpecialityPerson.data = it; VISIBLE } ?: run { GONE }

                    llSpecialityService.visibility = serviceSchemas?.takeUnless { it.isEmpty() }?.let { schemasSpecialityService.data = it; VISIBLE } ?: run { GONE }

                    lblSpecialityService.text = string(R.string.independentMarket)

                    lblSpecialityPerson.text = string(R.string.dependentMarket)

                    scoreSpecialityContainer.apply { res = this@SpecialityViewHolder.res; data = this@score }
                }
            }
        }
    }
}