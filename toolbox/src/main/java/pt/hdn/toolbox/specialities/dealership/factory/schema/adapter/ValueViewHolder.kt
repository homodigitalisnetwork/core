package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.text.Editable
import android.view.ViewGroup
import pt.hdn.contract.annotations.Parameter
import pt.hdn.contract.annotations.Parameter.Companion.LOWER_BOUND
import pt.hdn.contract.annotations.Parameter.Companion.THRESHOLD
import pt.hdn.contract.annotations.Parameter.Companion.UPPER_BOUND
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.misc.isNumber
import pt.hdn.toolbox.resources.Resources

class ValueViewHolder(
    resources: Resources,
    parent: ViewGroup
): EditTextViewHolder<Schema>(resources, parent) {

    override fun Schema.set() {
        with(binding) {
            with(tilSelectorValue) {
                hint = null

                hint = when (parameter) {
                    LOWER_BOUND -> string(R.string.lowerBound)
                    UPPER_BOUND -> string(R.string.upperBound)
                    THRESHOLD -> string(R.string.threshold)
                    else -> null
                }
            }

            with(txtSelectorValue) {
                val value = when (this@set) {
                    is CommissionSchema -> when (parameter) { LOWER_BOUND -> lowerBound?.toString(); UPPER_BOUND -> upperBound?.toString(); else -> null }
                    is ObjectiveSchema -> when (parameter) { LOWER_BOUND -> lowerBound?.toString(); UPPER_BOUND -> upperBound?.toString(); else -> null }
                    is ThresholdSchema -> threshold?.toString()
                    else -> null
                }

                setText(value)
                addTextChangedListener(this@ValueViewHolder)
            }
        }
    }

    override fun afterTextChanged(editable: Editable?) {
        with(t) {
            editable
                ?.toString()
                ?.takeIf { it.isNumber() }
                ?.toBigDecimal()
                ?.let {
                    when (this) {
                        is FixSchema -> fix = it
                        is RateSchema -> rate = it
                        is CommissionSchema -> when (parameter) { LOWER_BOUND -> lowerBound = it; UPPER_BOUND -> upperBound = it }
                        is ObjectiveSchema -> when (parameter) { LOWER_BOUND -> lowerBound = it; UPPER_BOUND -> upperBound = it }
                        is ThresholdSchema -> threshold = it
                    }
                }
        }
    }
}