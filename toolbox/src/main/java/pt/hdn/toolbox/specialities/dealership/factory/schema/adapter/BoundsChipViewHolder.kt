package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.view.ViewGroup
import androidx.annotation.StringRes
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import pt.hdn.contract.annotations.Parameter
import pt.hdn.contract.annotations.Parameter.Companion.LOWER_BOUND
import pt.hdn.contract.annotations.Parameter.Companion.UPPER_BOUND
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.resources.Resources

class BoundsChipViewHolder(
    res: Resources,
    parent: ViewGroup,
    block: ((tag: Any?, isChecked: Boolean) -> Unit)? = null
) : ChipViewHolder<Schema>(res, parent) {

    init { this.onChipListener = block?.let { OnChipListener(it) } }

    override fun Schema.setViewHolder() {
        with(binding) {
            lblSelectorChipQuestion.text = string(R.string.boundsQuestion)

            xpgrpSelectorChipAnswer.removeAllViews()

            when (this@setViewHolder) {
                is CommissionSchema -> {
                    addChip(R.string.lowerBound, LOWER_BOUND, lowerBound != null)
                    addChip(R.string.upperBound, UPPER_BOUND, upperBound != null)
                }
                is ObjectiveSchema -> {
                    addChip(R.string.lowerBound, LOWER_BOUND, lowerBound != null)
                    addChip(R.string.upperBound, UPPER_BOUND, upperBound != null)
                }
            }
        }
    }

    private fun addChip(@StringRes id: Int, @Parameter parameter: String, checked: Boolean) {
        with(Chip(context)) {
            text = string(id)
            checkedIcon = drawable(R.drawable.ic_check)
            isCheckedIconVisible = true
            isCheckable = true
            isChecked = checked
            tag = parameter
            setOnCheckedChangeListener(this@BoundsChipViewHolder)

            binding.xpgrpSelectorChipAnswer.addView(this)
        }
    }
}