package pt.hdn.toolbox.specialities.util

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.Expose
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.contract.schemas.Schema
import pt.hdn.contract.util.UUIDNameData
import pt.hdn.toolbox.annotations.Field.Companion.SERVICE_SCHEMAS_UUID
import pt.hdn.toolbox.annotations.Field.Companion.TAX
import pt.hdn.toolbox.annotations.Field.Companion.TYPE_UUID
import pt.hdn.toolbox.economics.DefaultTax
import pt.hdn.toolbox.economics.Tax
import java.util.*

@Parcelize
data class DefaultSpeciality(
    @Expose override var uuid: UUID? = null,
    @Expose(serialize = false) override var score: Score = Score(),
    @Expose override var personSchemasUUID: UUID? = null,
    @Expose override var serviceSchemasUUID: UUID? = null,
    @Expose override var personSchemas: MutableList<Schema>? = null,
    @Expose override var serviceSchemas: MutableList<Schema>? = null,
    @Expose override var personResponsibilities: MutableList<UUIDNameData>? = null,
    @Expose override var serviceResponsibilities: MutableList<UUIDNameData>? = null,
    @Expose override var typeUUID: UUID? = null,
    @Expose override var tax: Tax? = null
) : Speciality {

    //region vars
    override var type: UUIDNameMarketData? = null; set(value) { field = value; this.typeUUID = value?.uuid }
    //endregion vars

    companion object {
        fun from(snapshot: DataSnapshot) : DefaultSpeciality {
            return with(snapshot.value as Map<String, Any?>) {
                DefaultSpeciality(
                    serviceSchemasUUID = UUID.fromString(this[SERVICE_SCHEMAS_UUID] as String),
                    typeUUID = UUID.fromString(this[TYPE_UUID] as String),
                    tax = DefaultTax.from(snapshot.child(TAX))
                )
            }
        }
    }

    override fun toMap(ignoreExtraField: Boolean): Map<String, Any> {
        return buildMap {
            typeUUID?.let { this[TYPE_UUID] = it.toString() }
            serviceSchemasUUID?.let { this[SERVICE_SCHEMAS_UUID] = it.toString() }
//            personSchemasUUID?.let { this[PERSON_SCHEMAS_UUID] = it.toString() }
            tax?.takeUnless { ignoreExtraField }?.let { this[TAX] = it.toMap() }
        }
    }

    override fun clone(): Speciality {
        return copy(
            personSchemas = personSchemas?.mapTo(mutableListOf()) { it.clone() },
            serviceSchemas = serviceSchemas?.mapTo(mutableListOf()) { it.clone() },
            personResponsibilities = personResponsibilities?.mapTo(mutableListOf()) { it.copy() },
            serviceResponsibilities = serviceResponsibilities?.mapTo(mutableListOf()) { it.copy() }
        ).apply { type = this@DefaultSpeciality.type }
    }

    override fun reducedBy(other: Speciality?): Speciality {
        return other
            ?.let {
                DefaultSpeciality(
                    score = it.score,
                    typeUUID = takeUnless(typeUUID, it.typeUUID),
                    personSchemas = personSchemas?.takeUnless { me -> me.toSet() == it.personSchemas?.toSet() },
                    serviceSchemas = serviceSchemas?.takeUnless { me -> me.toSet() == it.serviceSchemas?.toSet() },
                    personResponsibilities = personResponsibilities?.takeUnless { me -> me.toSet() == it.personResponsibilities?.toSet() },
                    serviceResponsibilities = serviceResponsibilities?.takeUnless { me -> me.toSet() == it.serviceResponsibilities?.toSet() }
                )
            } ?: this
    }
}