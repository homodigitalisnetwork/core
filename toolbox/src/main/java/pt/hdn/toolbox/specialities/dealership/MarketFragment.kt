package pt.hdn.toolbox.specialities.dealership

import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.core.os.bundleOf
import androidx.lifecycle.Lifecycle.State.RESUMED
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Field.Companion.MARKET
import pt.hdn.contract.annotations.Market
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentMarketBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.setCompoundTopDrawable
import pt.hdn.toolbox.specialities.SpecialitiesViewModel
import pt.hdn.toolbox.views.ArrayFragmentStateAdapter.Invoker

@AndroidEntryPoint
class MarketFragment : BindingFragment<FragmentMarketBinding>(FragmentMarketBinding::inflate) {

    //region vars
    private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
    @Market private var market: Int = 0
    //endregion vars

    companion object {
        private fun new(args: Bundle): MarketFragment = MarketFragment().apply { arguments = args }

        fun invoker(@Market market: Int): Invoker = Invoker(MarketFragment::new, bundleOf(MARKET to market))
    }

    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState); this.market = requireArguments().getInt(MARKET) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(specialitiesViewModel) {
                dataBus.observe<Int>(viewLifecycleOwner, Address.CLICK_1) { onClick(it) }

                lblMarketTitle
                    .apply {
                        when(market) {
                            PERSON -> R.drawable.ic_dependent_market to R.string.dependentMarket
                            SERVICE -> R.drawable.ic_independent_market to R.string.independentMarket
                            else -> null
                        }?.run { setCompoundTopDrawable(first); text = string(second) }
                    }

                lblMarketDisplay
                    .apply {
                        when(market) {
                            PERSON -> R.drawable.ic_dependent_market to R.string.addAPersonSchema
                            SERVICE -> R.drawable.ic_independent_market to R.string.addAServiceSchema
                            else -> null
                        }?.run { setCompoundTopDrawable(first); text = string(second) }
                    }

                this[market]
                    .let {
                        recMarketContainer.apply { setHasFixedSize(true); adapter = it }

                        it.size
                            .observe(viewLifecycleOwner, state = RESUMED) {
                                lblMarketTitle.visibility = if (it == 0) GONE else VISIBLE
                                lblMarketDisplay.visibility = if (it == 0) VISIBLE else GONE

                                setViewVisibility()
                            }
                    }
            }
        }
    }

    private fun onClick(id: Int) {
        with(specialitiesViewModel) {
            when (id) {
                R.id.btn_dealershipDialogRemove -> {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(string(R.string.confirmation))
                        .setMessage(string(R.string.areYouSure))
                        .setPositiveButton(string(R.string.yes)) { _, _ -> clearMarket(market) }
                        .setNegativeButton(string(R.string.no), null)
                        .show()
                }
                R.id.btn_dealershipDialogEdit -> prepareMarket(market)
                else -> null
            }
        }
    }

    private fun setViewVisibility() {
        dataBus.send(
            owner = viewLifecycleOwner,
            address = Address.VIEW_1,
            values = arrayOf(
                R.id.btn_dealershipDialogAdd to Visibility.GONE,
                R.id.btn_dealershipDialogEdit to Visibility.ENABLE,
                R.id.btn_dealershipDialogRemove to if (specialitiesViewModel[market].isEmpty()) Visibility.GONE else Visibility.ENABLE
            )
        )
    }
}