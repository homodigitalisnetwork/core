package pt.hdn.toolbox.specialities.dealership.util

import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.CandleDataSet
import pt.hdn.contract.schemas.Schema
import pt.hdn.toolbox.misc.DateValueFormatter
import java.math.BigDecimal

data class SchemaTrade(
    val candleDataSet: CandleDataSet,
    val barDataSet: BarDataSet,
    val formatter: DateValueFormatter,
    val schema: Schema? = null
)
