package pt.hdn.toolbox.specialities.dealership.util

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.merge
import pt.hdn.contract.annotations.Err as Err2
import pt.hdn.contract.annotations.Market
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.schemas.*
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.communications.bus.ErrorBus
import pt.hdn.toolbox.specialities.util.DefaultSpeciality
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.communications.market.PriceRequest
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.dealership.adapter.MarketAdapter
import pt.hdn.toolbox.specialities.dealership.factory.adapter.SchemasAdapter
import pt.hdn.toolbox.specialities.dealership.factory.schema.adapter.SchemaFactoryAdapter

class SpecialityDealership(
    private val res: Resources,
    private val ref: Speciality? = null
) {

    //region vars
    val speciality: Speciality = ref?.clone() ?: DefaultSpeciality()
    val personAdapter: MarketAdapter = MarketAdapter(speciality.personSchemas, res)
    val serviceAdapter: MarketAdapter = MarketAdapter(speciality.serviceSchemas, res)
    val inMarket: Flow<Boolean> = merge(personAdapter.size, serviceAdapter.size).mapLatest { it > 0 }
    val isNewBuild: Boolean = ref == null
    val errorBus: ErrorBus = ErrorBus()
    val marketType: Flow<Int?>; get() = mutableMarketType
    private val mutableMarketType: MutableStateFlow<Int?> = MutableStateFlow(speciality.type?.market)
    private lateinit var position: Position
    @Market var market: Int = 0; private set
    lateinit var schemasAdapter: SchemasAdapter
    lateinit var schemaFactoryAdapter: SchemaFactoryAdapter; private set
    //endregion vars

    operator fun get(@Market market: Int): MarketAdapter = if (market == PERSON) personAdapter else serviceAdapter

    @Err @Err2 suspend fun validate(): Int {
        with(speciality) {
            with(personAdapter.ref) { if (!(personSchemas == null && isEmpty())) personSchemas = this }

            with(serviceAdapter.ref) { if (!(serviceSchemas == null && isEmpty())) serviceSchemas = this }

            return validate(ref).also { errorBus.send(value = it) }
        }
    }

    fun reduce(): Speciality? = speciality reducedBy ref

    fun getMarketRequest(position: Position): PriceRequest { this.position = position; return PriceRequest(market, speciality.typeUUID!!, position, schemasAdapter.ref) }

    suspend fun setType(uuidNameMarketData: UUIDNameMarketData) {
        speciality.type = uuidNameMarketData

        personAdapter.clear()
        serviceAdapter.clear()

        mutableMarketType.emit(uuidNameMarketData.market)
    }

    fun clearMarket(@Market market: Int) { this[market].clear() }

    fun prepareMarket(@Market market: Int) { this.market = market; this.schemasAdapter= SchemasAdapter(this[market].ref, res) }

    suspend fun saveMarket() {
        with(schemasAdapter) { validate().let { if (it == Err.NONE) this@SpecialityDealership[market].ref = ref; errorBus.send(value = it) } }
    }

    fun removeSchema() { schemasAdapter.removeSelected() }

    fun prepareSchema() { this.schemaFactoryAdapter = SchemaFactoryAdapter(schemasAdapter.takeIf { it.size.value > 1 }?.firstSelected, res) }

    suspend fun validateSchema(@Address address: Int) { errorBus.send(address, schemaFactoryAdapter.validate()) }

    fun saveSchema() {
        with(schemasAdapter) {
            ref
                .forEachWith {
                    when (this) {
                        is FixSchema -> fix = null
                        is RateSchema -> rate = null
                        is CommissionSchema -> cut = null
                        is ObjectiveSchema -> bonus = null
                        is ThresholdSchema -> bonus = null
                    }
                }

            (if (size.value > 1) ::addOrReplaceSelected else ::add).invoke(schemaFactoryAdapter.schema!!)
        }
    }
}