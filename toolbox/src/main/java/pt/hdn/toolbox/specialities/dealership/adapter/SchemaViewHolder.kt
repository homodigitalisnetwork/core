package pt.hdn.toolbox.specialities.dealership.adapter

import android.view.View.TEXT_ALIGNMENT_TEXT_END
import android.view.View.TEXT_ALIGNMENT_TEXT_START
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TableLayout
import android.widget.TableRow
import androidx.annotation.StringRes
import com.google.android.material.textview.MaterialTextView
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.databinding.ViewholderSchemaBinding
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal

class SchemaViewHolder(
    resources: Resources,
    parent: ViewGroup
): HDNViewHolder<ViewholderSchemaBinding, Schema>(resources, parent, ViewholderSchemaBinding::inflate) {

    //region vars
    private var tinyMargin: Int = dimen(R.dimen.t_size)
    private var smallMargin: Int = dimen(R.dimen.s_size)
    //endregion vars

    override fun Schema.setViewHolder(isExpanded: Boolean) {
        binding.root.removeAllViews()

        addDescription(res.schemas[id]!!)

        when (this@setViewHolder) {
            is FixSchema -> addParameter(R.string.value, fix)
            is RateSchema -> {
                addParameter(R.string.over, res.sources[source!!])
                addParameter(R.string.value, rate)
            }
            is CommissionSchema -> {
                addParameter(R.string.over, res.sources[source!!])
                addParameter(R.string.cut, cut)
                lowerBound?.let { addParameter(R.string.lowerBound, it) }
                upperBound?.let { addParameter(R.string.upperBound, it) }
            }
            is ObjectiveSchema -> {
                addParameter(R.string.over, res.sources[source!!])
                addParameter(R.string.bonus, bonus)
                lowerBound?.let { addParameter(R.string.lowerBound, it) }
                upperBound?.let { addParameter(R.string.upperBound, it) }
            }
            is ThresholdSchema -> {
                addParameter(R.string.over, res.sources[source!!])
                addParameter(R.string.bonus, bonus)
                addParameter(if (isAbove!!) R.string.aboveLimit else R.string.belowLimit, threshold)
            }
        }
    }

    private fun addDescription(value: String) {
        TableRow(context)
            .apply {
                layoutParams = TableLayout.LayoutParams().apply { setMargins(0, tinyMargin, 0, 0) }

                with(MaterialTextView(context)) { layoutParams = TableRow.LayoutParams(); text = value; addView(this) }

                binding.root.addView(this)
            }
    }

    private fun addParameter(@StringRes field: Int, value: BigDecimal?) { addParameter(field, value?.toString()) }

    private fun addParameter(@StringRes field: Int, value: String?) {
        with(TableRow(context)) {
            layoutParams = TableLayout.LayoutParams().apply { setMargins(smallMargin, tinyMargin, tinyMargin, 0) }

            with(MaterialTextView(context)) {
                layoutParams = TableRow.LayoutParams().apply { setMargins(0, 0, tinyMargin, 0) }
                textAlignment = TEXT_ALIGNMENT_TEXT_END
                text = string(field)

                addView(this)
            }

            with(MaterialTextView(context)) {
                layoutParams = TableRow.LayoutParams(0, WRAP_CONTENT, 1f).apply { setMargins(tinyMargin, 0, 0, 0) }
                textAlignment = TEXT_ALIGNMENT_TEXT_START
                text = value

                addView(this)
            }

            binding.root.addView(this)
        }
    }
}