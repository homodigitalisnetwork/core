package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.CompoundButton.OnCheckedChangeListener
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.databinding.ViewholderSelectorChipBinding
import pt.hdn.toolbox.resources.Resources

abstract class ChipViewHolder<in T>(
    res: Resources,
    parent: ViewGroup
) : BindingViewHolder<ViewholderSelectorChipBinding>(res, parent, ViewholderSelectorChipBinding::inflate), OnCheckedChangeListener {

    //region vars
    var isSingleSelection: Boolean; get() = binding.xpgrpSelectorChipAnswer.isSingleSelection; set(value) { binding.xpgrpSelectorChipAnswer.isSingleSelection = value }
    var onChipListener: OnChipListener? = null
    //endregion vars

    abstract fun T.setViewHolder()

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) { onChipListener?.onCheckedChanged(buttonView?.tag, isChecked) }
}