package pt.hdn.toolbox.specialities

import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentSpecialitiesHolderBinding

@AndroidEntryPoint
class SpecialitiesHolderFragment: BindingFragment<FragmentSpecialitiesHolderBinding>(FragmentSpecialitiesHolderBinding::inflate)