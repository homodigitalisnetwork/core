package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

import android.view.ViewGroup
import androidx.annotation.StringRes
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import pt.hdn.contract.schemas.Schema
import pt.hdn.contract.schemas.ThresholdSchema
import pt.hdn.toolbox.R
import pt.hdn.toolbox.resources.Resources

class ThresholdViewHolder(
    res: Resources,
    parent: ViewGroup,
    block: ((tag: Any?, isChecked: Boolean) -> Unit)? = null
):  ChipViewHolder<Schema>(res, parent) {

    init { this.isSingleSelection = true; this.onChipListener = block?.let { OnChipListener(it) } }

    override fun Schema.setViewHolder() {
        this@setViewHolder as ThresholdSchema

        with(binding) {
            lblSelectorChipQuestion.text = string(R.string.boundsQuestion)

            xpgrpSelectorChipAnswer.removeAllViews()

            addChip(R.string.belowLimit, false, isAbove == false)
            addChip(R.string.aboveLimit, true, isAbove == true)
        }
    }

    private fun addChip(@StringRes id: Int, ref: Boolean, flag: Boolean) {
        with(Chip(context)) {
            text = string(id)
            checkedIcon = drawable(R.drawable.ic_check)
            isCheckedIconVisible = true
            isCheckable = true
            isChecked = flag
            tag = ref
            setOnCheckedChangeListener(this@ThresholdViewHolder)

            binding.xpgrpSelectorChipAnswer.addView(this)
        }
    }
}