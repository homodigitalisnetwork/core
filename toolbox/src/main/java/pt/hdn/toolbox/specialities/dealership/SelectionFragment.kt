package pt.hdn.toolbox.specialities.dealership

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.RadioGroup.LayoutParams
import android.widget.RadioGroup.OnCheckedChangeListener
import androidx.core.view.children
import androidx.lifecycle.Lifecycle.Event.ON_RESUME
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentSelectionBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.misc.syncObserve
import pt.hdn.toolbox.specialities.SpecialitiesViewModel

@AndroidEntryPoint
class SelectionFragment : BindingFragment<FragmentSelectionBinding>(FragmentSelectionBinding::inflate), OnCheckedChangeListener {

    //region vars
    private val specialitiesViewModel: SpecialitiesViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(specialitiesViewModel) {
                lblSelectionQuestion.text = string(R.string.whatSpeciality)

                rgSelectionContainer
                    .apply {
                        availableSpecialities
                            ?.forEach {
                                RadioButton(requireContext())
                                    .apply {
                                        id = View.generateViewId()
                                        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply { setMargins(0, 5, 0, 0) }
                                        text = it.name
                                        tag = it

                                        addView(this)
                                    }
                            }

                        clearCheck()
                        setOnCheckedChangeListener(this@SelectionFragment)
                    }

                viewLifecycleOwner.syncObserve { _, event -> if (event == ON_RESUME) setViewVisibility() }
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        with(specialitiesViewModel) {
            group
                ?.children
                ?.associate { it.id to it.tag as UUIDNameMarketData }
                ?.get(checkedId)
                ?.let {
                    speciality
                        .type
                        ?.run {
                            if (this != it) {
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(R.string.confirmation)
                                    .setMessage(R.string.areYouSure)
                                    .setPositiveButton(string(R.string.yes)) { dial, _ -> setType(it); dial.dismiss() }
                                    .setNegativeButton(string(R.string.no)) { dial, _ ->
                                        group.apply { setOnCheckedChangeListener(null); children.find { it.tag == this }?.id?.let { check(it) }; setOnCheckedChangeListener(this@SelectionFragment) }

                                        dial.dismiss()
                                    }
                                    .show()
                            }
                        }
                        ?: run { setType(it) }
                }
        }
    }

    private fun setViewVisibility() {
        dataBus.send(
            owner = viewLifecycleOwner,
            address = Address.VIEW_1,
            values = arrayOf(
                R.id.btn_dealershipDialogEdit to Visibility.GONE,
                R.id.btn_dealershipDialogRemove to Visibility.GONE,
                R.id.btn_dealershipDialogAdd to Visibility.GONE
            )
        )
    }
}