package pt.hdn.toolbox.specialities.dealership.factory.schema.adapter

fun interface OnChipListener {
    fun onCheckedChanged(tag: Any?, isChecked: Boolean)
}