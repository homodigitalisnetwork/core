package pt.hdn.toolbox.entity.shared

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.*
import android.view.inputmethod.EditorInfo.IME_ACTION_NEXT
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.databinding.FragmentEntityBinding
import pt.hdn.toolbox.entity.profile.ProfileFragment
import pt.hdn.toolbox.entity.profile.ProfileViewModel
import pt.hdn.toolbox.entity.register.RegisterFragment
import pt.hdn.toolbox.entity.register.RegisterViewModel
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.entity.util.EntityViewModel
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.onNextAction
import pt.hdn.toolbox.misc.setTextInput
import pt.hdn.toolbox.misc.viewModels

@AndroidEntryPoint
class EntityFragment : BindingFragment<FragmentEntityBinding>(FragmentEntityBinding::inflate), OnClickListener, OnFocusChangeListener, TextWatcher {

    //region vars
    private val entityViewModel: EntityViewModel by viewModels(mapOf(RegisterFragment::class to RegisterViewModel::class, ProfileFragment::class to ProfileViewModel::class))
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(entityViewModel) {
                if (res.isNotSpot) {
                    layoutEntityLanguages.visibility = GONE

                    txtEntityEmail.nextFocusDownId = R.id.til_entityWeb

                    txtEntityWeb.onNextAction { dataBus.send(this@EntityFragment, Address.SWIPE, Unit) }
                } else {
                    lstEntityLanguages.adapter = getLanguagesAdapter(requireContext()); btnEntityLanguages.setOnClickListener(this@EntityFragment)

                    txtEntityEmail.onNextAction { setLanguage(); keyboard.hide() }
                }

                if (parentFragment is RegisterFragment) {
                    if (res.isNotSpot) {
                        tilEntityFirstName.setTextInput(this@EntityFragment, this@EntityFragment, hint = string(R.string.fantasyName))
                        tilEntityMiddleNames.visibility = GONE
                        tilEntityLastName.setTextInput(this@EntityFragment, this@EntityFragment, hint = string(R.string.fullName))
                        tilEntityWeb.setTextInput(this@EntityFragment, this@EntityFragment)
                    } else {
                        tilEntityFirstName.setTextInput(this@EntityFragment, this@EntityFragment)
                        tilEntityMiddleNames.setTextInput(this@EntityFragment, this@EntityFragment)
                        tilEntityLastName.setTextInput(this@EntityFragment, this@EntityFragment)
                        tilEntityWeb.visibility = GONE
                    }

                    tilEntityPhone.setTextInput(this@EntityFragment, this@EntityFragment)
                    tilEntityEmail.setTextInput(this@EntityFragment, this@EntityFragment)
                } else {
                    tilEntityPhone.hint = string(R.string.cellphoneNumber)
                    tilEntityEmail.hint = string(R.string.email)

                    if (res.isNotSpot) {
                        tilEntityFirstName.hint = string(R.string.fantasyName)
                        tilEntityMiddleNames.visibility = GONE
                        tilEntityLastName.hint = string(R.string.fullName)
                        tilEntityWeb.hint = string(R.string.web)
                    } else {
                        tilEntityFirstName.hint = string(R.string.firstName)
                        tilEntityMiddleNames.hint = string(R.string.middleNames)
                        tilEntityLastName.hint = string(R.string.lastName)
                        tilEntityWeb.visibility = GONE
                    }

                    oldEntity
                        ?.observe(viewLifecycleOwner) {
                            it?.run {
                                tilEntityPhone.setTextInput(this@EntityFragment, this@EntityFragment, value = phone)
                                tilEntityEmail.setTextInput(this@EntityFragment, this@EntityFragment, value = email)

                                if (res.isNotSpot) {
                                    tilEntityFirstName.setTextInput(this@EntityFragment, this@EntityFragment, value = firstName)
                                    tilEntityLastName.setTextInput(this@EntityFragment, this@EntityFragment, value = lastName)
                                    tilEntityWeb.setTextInput(this@EntityFragment, this@EntityFragment, value = web)
                                } else {
                                    tilEntityFirstName.setTextInput(this@EntityFragment, this@EntityFragment, value = firstName)
                                    tilEntityMiddleNames.setTextInput(this@EntityFragment, this@EntityFragment, value = middleNames)
                                    tilEntityLastName.setTextInput(this@EntityFragment, this@EntityFragment, value = lastName)
                                }
                            }
                        }
                }

                errorBus
                    .observe(viewLifecycleOwner) {
                        when (it) {
                            Err.FIRST_NAME -> handleError(tilEntityFirstName)
                            Err.MIDDLE_NAMES -> handleError(tilEntityMiddleNames)
                            Err.LAST_NAME -> handleError(tilEntityLastName)
                            Err.PHOTO -> handleError(tilEntityPhone)
                            Err.EMAIL -> handleError(tilEntityEmail)
                            Err.WEB -> handleError(tilEntityWeb)
                            Err.LANGUAGES -> { handleError(lstEntityLanguages); toast(R.string.languages) }
                        }
                    }
            }
        }
    }

    override fun onClick(view: View?) { setLanguage() }

    override fun onFocusChange(view: View?, hasFocus: Boolean) { entityViewModel.fieldID = view?.takeIf { hasFocus }?.id ?: 0 }

    override fun afterTextChanged(editable: Editable?) { entityViewModel.setDetail(editable) }

    private fun setLanguage() {
        with(entityViewModel) {
            prepareLanguageSelection()

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(string(R.string.languages))
                .setCancelable(false)
                .setMultiChoiceItems(arrayLanguages, tempCheckLanguages) { _, position, isChecked -> (if (isChecked) ::addLanguage else ::removeLanguage).invoke(position) }
                .setPositiveButton(string(R.string.accept)) { _, _ -> saveLanguageSelection() }
                .setNegativeButton(string(R.string.cancel), null)
                .show()
        }
    }

    //region NOT IN USE
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    //endregion NOT IN USE
}