package pt.hdn.toolbox.entity.shared

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore.ACTION_IMAGE_CAPTURE
import android.text.Editable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.View.*
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts.TakePicture
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.databinding.FragmentAccountBinding
import pt.hdn.toolbox.entity.profile.ProfileFragment
import pt.hdn.toolbox.entity.profile.ProfileViewModel
import pt.hdn.toolbox.entity.register.RegisterFragment
import pt.hdn.toolbox.entity.register.RegisterViewModel
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.data.IdNameCodeData
import pt.hdn.toolbox.entity.util.EntityViewModel
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.onNextAction
import pt.hdn.toolbox.misc.setTextInput
import pt.hdn.toolbox.misc.viewModels

@AndroidEntryPoint
class AccountFragment : BindingFragment<FragmentAccountBinding>(FragmentAccountBinding::inflate), OnClickListener, OnItemClickListener, OnFocusChangeListener, TextWatcher {

    //region vars
    private val entityViewModel: EntityViewModel by viewModels(mapOf(RegisterFragment::class to RegisterViewModel::class, ProfileFragment::class to ProfileViewModel::class))
    private lateinit var requestPermission: ActivityResultLauncher<Uri>
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.requestPermission = registerForActivityResult(TakePicture()) { if (it) entityViewModel.setImage(binding!!.imgAccountProfile, true) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(entityViewModel) {
                imgAccountProfile.setOnClickListener(this@AccountFragment)

                tilAccountPassword.setTextInput(this@AccountFragment, this@AccountFragment)

                spnAccountLocale
                    .apply {
                        onItemClickListener = this@AccountFragment

                        ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, res.locales)
                            .let { setAdapter(it); setText(it.getItem(localeIndex).toString(), false) }
                    }

                if (parentFragment is RegisterFragment) {
                    if (res.isNotSpot) tilAccountPassword.visibility = GONE else txtAccountPassword.onNextAction { keyboard.hide(); ckboxAccountGDPR.requestFocus() }

                    ckboxAccountGDPR.setOnClickListener(this@AccountFragment)
                    ckboxAccountPP.setOnClickListener(this@AccountFragment)

                    tilAccountUserName.setTextInput(this@AccountFragment, this@AccountFragment)

                    lblAccountGDPR.movementMethod = LinkMovementMethod.getInstance()
                    lblAccountPP.movementMethod = LinkMovementMethod.getInstance()
                } else {
                    if (res.isNotSpot) tilAccountPassword.isEnabled = false

                    tilAccountPassword.hint = string(R.string.password)
                    tilAccountUserName.visibility = GONE
                    tlAccountLegal.visibility = GONE

                    oldEntity?.observe(viewLifecycleOwner) { it?.let { setImage(imgAccountProfile, false); if (res.isNotSpot) txtAccountPassword.setText(it.password) } }
                }

                errorBus
                    .observe(viewLifecycleOwner) {
                        when (it) {
                            Err.PHOTO -> handleError(imgAccountProfile)
                            Err.USER_NAME -> handleError(tilAccountUserName)
                            Err.PASSWORD -> handleError(tilAccountPassword)
                            Err.GDPR -> handleError(ckboxAccountGDPR)
                            Err.PP -> handleError(ckboxAccountPP)
                        }
                    }
            }
        }
    }

    override fun onClick(v: View?) {
        v?.id
            ?.let {
                with(binding!!) {
                    with(entityViewModel) {
                        when (it) {
                            R.id.img_accountProfile -> Intent(ACTION_IMAGE_CAPTURE).resolveActivity(requireContext().packageManager)?.run { requestPermission.launch(photo.uri) }
                            R.id.ckbox_accountGDPR -> isGDPRNotAccepted = !ckboxAccountGDPR.isChecked
                            R.id.ckbox_accountPP -> isPPNotAccepted = !ckboxAccountPP.isChecked
                            else -> null
                        }
                    }
                }
        }
    }

    override fun onFocusChange(view: View?, hasFocus: Boolean) { entityViewModel.fieldID = view?.takeIf { hasFocus }?.id ?: 0 }

    override fun afterTextChanged(editable: Editable?) { entityViewModel.setDetail(editable) }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        with(entityViewModel) { if (localeIndex != position) { localeIndex = position; parent?.adapter?.run { locale = (getItem(position) as IdNameCodeData).code } } }
    }

    //region NOT IN USE
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
    //endregion NOT IN USE
}