package pt.hdn.toolbox.entity.util

import android.content.Context
import android.location.Address
import android.text.Editable
import android.widget.ArrayAdapter
import android.widget.ImageView
import androidx.annotation.CallSuper
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlinx.coroutines.flow.*
import pt.hdn.toolbox.annotations.Address.Companion.PAGEABLE
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.ErrorBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.entity.EntityRequest
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.viewmodels.BasicViewModel

abstract class EntityViewModel constructor(
    private val requestOptions: RequestOptions,
    var photo: Photo,
    val dataBus: DataBus,
    val sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : BasicViewModel<CoreRepo>(repo, resources) {

    //region vars
    abstract val response: Flow<Result<Any>>
    val errorBus: ErrorBus; get() = entityDealership.errorBus
    val arrayLanguages: StringArray; get() = entityDealership.arrayLanguages
    val tempCheckLanguages: BooleanArray; get() = entityDealership.tempCheckLanguages
    var zoom: Float = 13f
    var oldEntity: SharedFlow<Entity?>? = null
    protected lateinit var entityDealership: EntityDealership
    protected var marker: Marker? = null
    protected var goFast: Boolean = false
    private var page: Int = 0
    var handicapped: Boolean; get() = entityDealership.handicapped; set(value) { entityDealership.handicapped = value }
    var fieldID: Int; get() = entityDealership.fieldID; set(value) { entityDealership.fieldID = value }
    var localeIndex: Int; get() = entityDealership.localeIndex; set(value) { entityDealership.localeIndex = value }
    var locale: String; get() = entityDealership.locale; set(value) { entityDealership.locale = value}
    var isGDPRNotAccepted: Boolean; get() = entityDealership.isGDPRNotAccepted; set(value) { entityDealership.isGDPRNotAccepted = value }
    var isPPNotAccepted: Boolean; get() = entityDealership.isPPNotAccepted; set(value) { entityDealership.isPPNotAccepted = value }
    protected val request: MutableSharedFlow<EntityRequest> = MutableSharedFlow()
    //endregion vars

    abstract fun removeEntity()

    abstract fun setEntity()

    @CallSuper
    override fun onCleared() { photo.delete() }

    fun addLanguage(position: Int) { entityDealership.addLanguage(position) }

    fun removeLanguage(position: Int) { entityDealership.removeLanguage(position) }

    fun saveLanguageSelection() { entityDealership.saveLanguageSelection() }

    fun getLanguagesAdapter(context: Context): ArrayAdapter<String> = entityDealership.getLanguagesAdapter(context)

    fun Address.setLocation() { entityDealership.apply { position = this@setLocation.toPosition(); countryCode = this@setLocation.countryCode } }

    fun setLocationDetail(value: LatLng?) { entityDealership.setLocationDetail(value) }

    fun setDetail(value: Editable?) { entityDealership.setDetail(value?.ifBlank { null }?.toString()) }

    fun setCivilStatus(status: Int) { entityDealership.setCivilStatus(status) }

    fun setImage(imageView: ImageView, createThumbnail: Boolean) {
        if (!createThumbnail || photo.toThumbnail()) with(imageView) { Glide.with(context).load(photo.uri!!).apply(requestOptions).into(this) }

        if (createThumbnail) entityDealership.notNewImage = false
    }

    fun prepareLanguageSelection() { entityDealership.prepareLanguageSelection() }

    fun increasePageable() { dataBus.send(viewModelScope, PAGEABLE, ++page) }

    fun decreasePageable() { dataBus.send(viewModelScope, PAGEABLE, (page - 1).coerceAtLeast(0).also { this.page = it });  }
}
