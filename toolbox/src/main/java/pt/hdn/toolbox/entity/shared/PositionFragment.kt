package pt.hdn.toolbox.entity.shared

import android.annotation.SuppressLint
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.View.*
import android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL
import com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle
import com.google.android.gms.maps.model.Marker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.*
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.databinding.FragmentPositionBinding
import pt.hdn.toolbox.entity.profile.ProfileFragment
import pt.hdn.toolbox.entity.profile.ProfileViewModel
import pt.hdn.toolbox.entity.register.RegisterFragment
import pt.hdn.toolbox.entity.register.RegisterViewModel
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.entity.util.EntityViewModel
import pt.hdn.toolbox.entity.util.HomeTarget
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.spotter.SpotterFragmentDirections
import pt.hdn.toolbox.views.OnMapCallback
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class PositionFragment : BindingFragment<FragmentPositionBinding>(FragmentPositionBinding::inflate), OnEditorActionListener, OnClickListener, OnMapCallback, OnMarkerDragListener, OnMarkerClickListener, OnCameraIdleListener, OnFocusChangeListener, TextWatcher {

    //region vars
    @Inject lateinit var geocoder: Geocoder
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>
    private var homeTarget: HomeTarget? = null
    private var googleMap: GoogleMap? = null
    private val entityViewModel: EntityViewModel by viewModels(mapOf(RegisterFragment::class to RegisterViewModel::class, ProfileFragment::class to ProfileViewModel::class))
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(entityViewModel) {
                this@PositionFragment.bottomSheetBehavior = from(layoutPositionSearch)
                    .apply {
                        addBottomSheetCallback(
                            onBottomSheetCallback {
                                onStateChanged { _, newState ->
                                    when (newState) {
                                        STATE_DRAGGING -> increasePageable()
                                        STATE_SETTLING -> decreasePageable()
                                        STATE_COLLAPSED -> keyboard.hide()
                                        STATE_EXPANDED -> keyboard.show()
                                    }
                                }
                            }
                        )

                        state = STATE_EXPANDED
                    }

                btnPositionSearch.setOnClickListener(this@PositionFragment)

                connectivity.observe(viewLifecycleOwner) { btnPositionSearch.apply { isEnabled = it; isClickable = it } }

                mapPositionMap
                    .apply {
                        isIntercepting.observe(viewLifecycleOwner) { if (it) increasePageable() else decreasePageable() }

                        onCreate(null)
                        onResume()
                        getMapAsyncWithContext(this@PositionFragment)
                    }

                if (parentFragment is RegisterFragment) {
                    tilPositionStreet.setTextInput(this@PositionFragment, this@PositionFragment)
                    tilPositionDoor.setTextInput(this@PositionFragment, this@PositionFragment)
                    tilPositionZip.setTextInput()
                } else { tilPositionStreet.hint = string(R.string.streetName); tilPositionDoor.hint = string(R.string.doorNumber); tilPositionZip.hint = string(R.string.zipCode) }

                errorBus
                    .observe(viewLifecycleOwner) {
                        when (it) {
                            Err.STREET -> handleError(tilPositionStreet)
                            Err.DOOR -> handleError(tilPositionDoor)
                            Err.ZIP -> handleError(tilPositionZip)
                            Err.POSITION -> handleError(mapPositionMap)
                            Err.COUNTRY -> handleError(mapPositionMap)
                        }
                    }

                keyboard.hide()
            }
        }
    }

    override fun onResume() { super.onResume(); binding!!.mapPositionMap.onResume() }

    override fun onPause() { binding?.mapPositionMap?.onPause(); super.onPause() }

    override fun onStop() { binding?.mapPositionMap?.onStop(); super.onStop() }

    override fun onSaveInstanceState(outState: Bundle) { binding?.mapPositionMap?.onSaveInstanceState(outState); super.onSaveInstanceState(outState) }

    override fun onDestroyView() { this.googleMap = googleMap?.run { clear(); null }; binding?.mapPositionMap?.onDestroy(); super.onDestroyView() }

    override fun onLowMemory() { binding?.mapPositionMap?.onLowMemory(); super.onLowMemory() }

    @SuppressLint("PotentialBehaviorOverride")
    override fun GoogleMap.onMapReady() {
        this@PositionFragment.googleMap = this

        uiSettings.apply { isRotateGesturesEnabled = false; isTiltGesturesEnabled = false; isMapToolbarEnabled = false }

        mapType = MAP_TYPE_NORMAL

        setMapStyle(loadRawResourceStyle(requireContext(), if (res.isDarkModeOn)  R.raw.map_styling_dark else R.raw.map_styling_light))
        setOnMarkerDragListener(this@PositionFragment)
        setOnCameraIdleListener(this@PositionFragment)

        with(binding!!) {
            with(entityViewModel) {
                oldEntity
                    ?.observe(viewLifecycleOwner) {
                        it?.run {
                            tilPositionStreet.setTextInput(this@PositionFragment, this@PositionFragment, value = streetName)
                            tilPositionDoor.setTextInput(this@PositionFragment, this@PositionFragment, value = doorNumber)
                            tilPositionZip.setTextInput(zipCode!!)

                            this@PositionFragment.homeTarget = HomeTarget(this@onMapReady, res, position!!.toLatLng())
                        }
                    }
            }
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean { homeTarget?.takeIf { it.equals(marker) }?.run { focus() }; return true }

    override fun onMarkerDragStart(p0: Marker) { entityViewModel.increasePageable() }

    override fun onMarkerDragEnd(marker: Marker) { with(entityViewModel) { marker.run { setLocationDetail(position); decreasePageable() } } }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean { if (actionId == IME_ACTION_SEARCH) search(); return false }

    override fun onCameraIdle() { googleMap?.cameraPosition?.run { entityViewModel.zoom = zoom } }

    override fun onClick(view: View?) { view?.run { with(bottomSheetBehavior) { when (state) { STATE_EXPANDED -> search(); STATE_COLLAPSED -> state = STATE_EXPANDED } } } }

    override fun onFocusChange(view: View?, hasFocus: Boolean) { entityViewModel.fieldID = view?.takeIf { hasFocus }?.id ?: 0 }

    override fun afterTextChanged(editable: Editable?) { entityViewModel.setDetail(editable) }

    private fun TextInputLayout.setTextInput(value: String? = null) {
        value
            ?.let {
                isHintAnimationEnabled = false
                editText
                    ?.apply {
                        setText(it)
                        onFocusChangeListener = this@PositionFragment
                        addTextChangedListener(this@PositionFragment)
                        setOnEditorActionListener(this@PositionFragment)
                    }
                isHintAnimationEnabled = true
            } ?: run {
                editText
                    ?.apply {
                        onFocusChangeListener = this@PositionFragment
                        addTextChangedListener(this@PositionFragment)
                        setOnEditorActionListener(this@PositionFragment)
                    }
            }
    }

    private fun search() {
        with(binding!!) {
            when {
                tilPositionStreet.editText?.text.isNullOrBlank() -> toast(R.string.missingStreet)
                tilPositionDoor.editText?.text.isNullOrBlank() -> toast(R.string.missingDoor)
                tilPositionZip.editText?.text.isNullOrBlank() -> toast(R.string.missingZip)
                else -> {
                    dataBus.send(this@PositionFragment, Address.VIEW_1, value = Visibility.ENABLE)

                    with(entityViewModel) {
                        try {
                            geocoder
                                .getFromLocationName(
                                    buildString { append(tilPositionStreet.editText?.text); append(" "); append(tilPositionDoor.editText?.text); append(" "); append(tilPositionZip.editText?.text) },
                                    1
                                ) ?.run {
                                    dataBus.send(this@PositionFragment, Address.VIEW_1, value = Visibility.GONE)

                                    if (isNullOrEmpty()) toast(R.string.failLocation)
                                    else {
                                        firstOrNull()
                                            ?.apply {
                                                toLatLng().let { homeTarget?.run { position = it } ?: run { this@PositionFragment.homeTarget = HomeTarget(googleMap!!, res, it) } }

                                                setLocation()

                                                toast(R.string.dragForPrecision)
                                            }
                                    }
                                }
                        } catch (e: IOException) { toast(R.string.failLocation) }
                    }
                }
            }

            bottomSheetBehavior.state = STATE_COLLAPSED
        }
    }

    //region NOT IN USE
    override fun onMarkerDrag(p0: Marker) { }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
    //endregion NOT IN USE
}