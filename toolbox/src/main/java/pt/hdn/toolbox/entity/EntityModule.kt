package pt.hdn.toolbox.entity

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import pt.hdn.toolbox.annotations.Misc
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.repo.PhotosDir
import pt.hdn.toolbox.resources.Resources
import java.io.File
import java.util.*
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PhotoFile

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PhotoUri

@Module
@InstallIn(ViewModelComponent::class)
object EntityModule {

    @PhotoFile
    @ViewModelScoped
    @Provides
    fun providesPicFile(@PhotosDir photosDir: File): File = File.createTempFile(UUID.randomUUID().toString(), Misc.JPEG_SUFFIX, photosDir)

    @PhotoUri
    @ViewModelScoped
    @Provides
    fun providesPicUri(@ApplicationContext context: Context, @PhotoFile photoFile: File, resources: Resources): Uri {
        return FileProvider.getUriForFile(context, resources.fileProviderAuth, photoFile)
    }

    @ViewModelScoped
    @Provides
    fun providesPic(@PhotoFile photoFile: File, @PhotoUri photoUri: Uri): Photo = Photo(photoUri).apply { file = photoFile }
}