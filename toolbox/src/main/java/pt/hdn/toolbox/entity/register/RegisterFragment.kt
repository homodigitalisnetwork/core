package pt.hdn.toolbox.entity.register

import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.activity.addCallback
import androidx.core.view.size
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle.State.RESUMED
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentGenericBinding
import pt.hdn.toolbox.entity.shared.*
import pt.hdn.toolbox.misc.arrayFragmentStateAdapter
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.swipeNext
import pt.hdn.toolbox.misc.tabLayoutMediator

/**
 * A [Fragment] that inherits from [BindingFragment] that holds and manage all fragment that work some aspect of the [Entity]
 */
@AndroidEntryPoint
class RegisterFragment : BindingFragment<FragmentGenericBinding>(FragmentGenericBinding::inflate), OnClickListener {

    //region vars
    private val registerViewModel: RegisterViewModel by viewModels()
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState); onBackPressedDispatcher.addCallback(this) { popBack() } }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(registerViewModel) {
                btnGenericRemove.visibility = GONE
                btnGenericAdd.visibility = INVISIBLE
                btnGenericEdit.visibility = INVISIBLE
                imgGenericDisplay.visibility = GONE
                barGenericLoading.visibility = GONE

                connectivity.observe(viewLifecycleOwner, state = RESUMED) { btnGenericSubmit.apply { isEnabled = it; isClickable = it } }

                response.observe(viewLifecycleOwner) { setViewVisibility(true); it.error { _, _ -> toast(R.string.failToRegister) } }

                dataBus
                    .using(viewLifecycleOwner) {
                        observe<Int>(Address.PAGEABLE) { vpGenericContainer.isUserInputEnabled = it == 0 }
                        observe<Unit>(Address.SWIPE) { vpGenericContainer.swipeNext() }
                    }

                errorBus.observe(viewLifecycleOwner) { setViewVisibility(it != Err.NONE) }

                arrayFragmentStateAdapter(this@RegisterFragment, vpGenericContainer) {
                    onPageSelected { index, lastIndex -> with(btnGenericSubmit) { if (index == lastIndex) show() else hide() } }

                    addAll(::EntityFragment, ::LegalFragment, ::PositionFragment, ::PassphraseFragment, ::AccountFragment)
                }

                tabLayoutMediator(viewLifecycleOwner, layoutGenericDots, vpGenericContainer)

                btnGenericSubmit.setOnClickListener(this@RegisterFragment)

                if (res.isNotSpot) { navigateTo(RegisterFragmentDirections.actionRegisterFragmentToLoginSpotDialogFragment()) }
            }
        }
    }

    override fun onClick(v: View?) { with(registerViewModel) { v?.id?.let { when (it) { R.id.btn_genericSubmit -> { setViewVisibility(false); setEntity() }; else -> null } } } }

    private fun setViewVisibility(isEnabled: Boolean) { with(binding!!) { barGenericLoading.visibility = if (isEnabled) GONE else VISIBLE; btnGenericSubmit.isEnabled = isEnabled } }
}