package pt.hdn.toolbox.entity.util

import com.google.android.gms.maps.CameraUpdateFactory.newLatLng
import com.google.android.gms.maps.CameraUpdateFactory.newLatLngZoom
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.ktx.addMarker
import pt.hdn.toolbox.resources.Resources

class HomeTarget(
    private val googleMap: GoogleMap,
    res: Resources,
    ll: LatLng
) {

    //region vars
    private val marker: Marker
    var position: LatLng; get() = marker.position; set(value) { with(marker) { position = value }; focus() }
    //endregion vars

    init { this@HomeTarget.marker = googleMap.addMarker { position(ll); draggable(true); icon(res.pins.values.first().bitmapDescriptor) }!!; focus() }

    override fun equals(other: Any?): Boolean = when (other) { is Marker -> marker == other; else -> super.equals(other) }

    fun focus() { googleMap.animateCamera(newLatLngZoom(position, 13f)) }

    fun center() { googleMap.animateCamera(newLatLng(position)) }
}