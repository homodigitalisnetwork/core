package pt.hdn.toolbox.entity.profile

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle.State.RESUMED
import androidx.viewbinding.ViewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Action.Companion.REMOVE
import pt.hdn.toolbox.annotations.Action.Companion.SET
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.entity.shared.*
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentGenericBinding
import pt.hdn.toolbox.misc.*

/**
 * A [Fragment] that inherits from [BindingFragment] and holds and manages all fragments that work some aspect of the [Entity] class
 */
@AndroidEntryPoint
class ProfileFragment : BindingFragment<FragmentGenericBinding>(FragmentGenericBinding::inflate), OnClickListener {

    //region vars
    private val profileViewModel: ProfileViewModel by viewModels()
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState); onBackPressedDispatcher.addCallback(this) { popBack() } }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(profileViewModel) {
                barGenericLoading.visibility = VISIBLE

                dataBus
                    .using(viewLifecycleOwner) {
                        observe<Int>(Address.PAGEABLE) { vpGenericContainer.isUserInputEnabled = it == 0 }
                        observe<Unit>(Address.SWIPE) { vpGenericContainer.swipeNext() }
                    }

                oldEntity
                    ?.observe(viewLifecycleOwner, state = RESUMED) {
                        barGenericLoading.visibility = GONE
                        imgGenericDisplay.visibility = GONE

                        it
                            ?.let {
                                connectivity
                                    .observe(this@ProfileFragment) { btnGenericSubmit.apply { isEnabled = it; isClickable = it }; btnGenericRemove.apply { isEnabled = it; isClickable = it } }

                                errorBus.observe(this@ProfileFragment) { setViewVisibility(true); if (it == Err.NO_CHANGE) toast(R.string.noChanges) }

                                response
                                    .observeWith(this) {
                                        setViewVisibility(true)

                                        success { with(request) { when (action) { REMOVE -> requireActivity().finish(); SET -> { toast(R.string.dataSaved); popBack() } } } }
                                        error { _, _ -> toast(R.string.failExe) }
                                    }

                                arrayFragmentStateAdapter(this@ProfileFragment, vpGenericContainer) {
                                    onPageSelected { index, lastIndex ->
                                        if (index == lastIndex) { btnGenericRemove.show(); btnGenericSubmit.show() } else { btnGenericRemove.hide(); btnGenericSubmit.hide() }
                                    }

                                    addAll(::EntityFragment, ::LegalFragment, ::PositionFragment, ::PassphraseFragment, ::AccountFragment)
                                }

                                tabLayoutMediator(viewLifecycleOwner, layoutGenericDots, vpGenericContainer)

                                btnGenericRemove.setOnClickListener(this@ProfileFragment)
                                btnGenericSubmit.setOnClickListener(this@ProfileFragment)
                            }
                            ?: run { toast(R.string.failAcquireData) }
                    }
            }
        }
    }

    override fun onClick(v: View?) {
        v?.id
            ?.let {
                with(profileViewModel) {
                    when (it) {
                        R.id.btn_genericRemove -> {
                            MaterialAlertDialogBuilder(requireContext())
                                .setTitle(string(R.string.confirmation))
                                .setMessage(string(R.string.areYouSure))
                                .setPositiveButton(string(R.string.yes)) { _, _ -> setViewVisibility(false); removeEntity() }
                                .setNegativeButton(string(R.string.no), null)
                                .show()
                        }
                        R.id.btn_genericSubmit -> { setViewVisibility(false); setEntity() }
                        else -> null
                    }
                }
            }
    }

    private fun setViewVisibility(isEnabled: Boolean) {
        with(binding!!) {
            barGenericLoading.visibility = if (isEnabled) GONE else VISIBLE
            btnGenericSubmit.isEnabled = isEnabled
            btnGenericRemove.isEnabled = isEnabled
        }
    }
}