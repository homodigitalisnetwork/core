package pt.hdn.toolbox.entity.util

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.FirebasePath.Companion.NAME
import pt.hdn.toolbox.annotations.FirebasePath.Companion.POSITION
import pt.hdn.toolbox.misc.Position

data class DefaultEntity(
    @Expose override var firstName: String? = null,
    @Expose override var middleNames: String? = null,
    @Expose override var lastName: String? = null,
    @Expose override var email: String? = null,
    @Expose override var phone: String? = null,
    @Expose override var web: String? = null,
    @Expose override var languages: MutableList<Int>? = null,
    @Expose override var tin: String? = null,
    @Expose override var dependents: String? = null,
    @Expose override var civilStatus: Int? = null,
    @Expose override var handicapped: Boolean? = null,
    @Expose override var streetName: String? = null,
    @Expose override var doorNumber: String? = null,
    @Expose override var zipCode: String? = null,
    @Expose override var countryCode: String? = null,
    @Expose override var position: Position? = null,
    @Expose override var userName: String? = null,
    @Expose override var password: String? = null
) : Entity {
    //region vars
    override val preferencesMap: MutableMap<String, Any>? = mutableMapOf<String, Any>()
        .run { firstName?.let { this[NAME] = it }; position?.let { this[POSITION] = it.toMap() }; if (isEmpty()) null else this }
    //endregion vars

    override fun clone(): Entity = copy(languages = languages?.toMutableList(), position = position?.clone())
}