package pt.hdn.toolbox.entity.register

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.View.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.fragment.app.activityViewModels
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.databinding.DialogLoginspotBinding
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.misc.observe

/**
 * A login [Fragment] that inherits from [BindingDialogFragment] and it is shown during the registry of a business
 * for the purpose of acquiring the business' owner information later on used to set a partnership with it
 */
@AndroidEntryPoint
class LoginSpotDialogFragment : BindingDialogFragment<DialogLoginspotBinding>(DialogLoginspotBinding::inflate, 40), OnClickListener, OnEditorActionListener {

    //region vars
    private val registerViewModel: RegisterViewModel by activityViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding!!) {
            with(registerViewModel) {
                setButton(btnLoginSpotPositive, R.string.login)
                setButton(btnLoginSpotNeutral, R.string.signUp)

                loginResponse
                    .observe(viewLifecycleOwner) {
                        barLoginSpotLoading.visibility = GONE
                        btnLoginSpotPositive.isEnabled = true
                        btnLoginSpotNeutral.isEnabled = true

                        it.success { toast(R.string.successfulLogin); popBack() }.error { _, _ -> toast(R.string.failLogin) }
                    }
            }
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean { if (actionId == EditorInfo.IME_ACTION_DONE) { eval() }; return false }

    override fun onBackPressed() { popBackTo(R.id.loginFragment, false) }

    override fun onClick(view: View?) { view?.id?.let { when (it) {R.id.btn_loginSpotPositive -> eval(); R.id.btn_loginSpotNeutral -> startActivity(registerViewModel.getSpotIntent()) } } }

    private fun setButton(materialButton: MaterialButton, @StringRes id: Int) { materialButton.apply { text = getString(id); setOnClickListener(this@LoginSpotDialogFragment) } }

    private fun eval() {
        with(binding!!) {
            val userName = txtLoginSpotUserName.text?.toString()
            val password = txtLoginSpotPassword.text?.toString()
            val passphrase = txtLoginSpotPassphrase.text?.toString()

            when {
                userName.isNullOrBlank() -> { txtLoginSpotUserName.requestFocus(); toast(R.string.missingUserName) }
                password.isNullOrBlank() -> { txtLoginSpotPassword.requestFocus(); toast(R.string.missingPassword) }
                passphrase.isNullOrBlank() -> { txtLoginSpotPassphrase.requestFocus(); toast(R.string.missingPassphrase) }
                else -> {
                    if (registerViewModel.setLogIn(userName, password, passphrase)) {
                        barLoginSpotLoading.visibility = VISIBLE
                        btnLoginSpotPositive.isEnabled = false
                        btnLoginSpotNeutral.isEnabled = false
                    }
                }
            }
        }
    }
}