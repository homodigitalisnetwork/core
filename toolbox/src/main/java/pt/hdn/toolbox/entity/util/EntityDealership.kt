package pt.hdn.toolbox.entity.util

import android.content.Context
import android.widget.ArrayAdapter
import com.google.android.gms.maps.model.LatLng
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.communications.bus.ErrorBus
import pt.hdn.toolbox.data.IdNameCodeData
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.StringArray
import pt.hdn.toolbox.misc.takeIfWith
import pt.hdn.toolbox.resources.Resources
import kotlin.collections.ArrayList

class EntityDealership (
    private val res: Resources,
    private val ref: Entity? = null,
    localePreference: String? = res.resourcesLocale
) {
    //region vars
    lateinit var languagesAdapter: ArrayAdapter<String>
    var notNewImage: Boolean = true
    var localeIndex: Int = res.locales.indexOfFirst { it.code == localePreference }.coerceAtLeast(0)
    var partyPassphrase: String? = null
    var fieldID: Int = 0
    var isGDPRNotAccepted = true
    var isPPNotAccepted = true
    var locale: String = res.resourcesLocale
    var tempCheckLanguages: BooleanArray = BooleanArray(res.languages.size)
    var checkLanguages: BooleanArray = BooleanArray(res.languages.size); private set
    var languagesKeys: MutableList<Int>? = null
    var handicapped: Boolean; get() = entity.handicapped!!; set(value) { entity.handicapped = value }
    var position: Position; get() = entity.position!!; set(value) { entity.position = value }
    var countryCode: String; get() = entity.countryCode!!; set(value) { entity.countryCode = value }
    private val languages: List<IdNameCodeData> = res.languages
    private val tempLanguagesKeys: MutableList<Int> = ArrayList()
    private val tempLanguagesNames: MutableList<String> = ArrayList()
    val entity: Entity = ref?.clone() ?: DefaultEntity()
    val arrayLanguages: StringArray = res.languages.map { it.name }.toTypedArray()
    val languagesNames: MutableList<String> = mutableListOf(res.string(R.string.spokenLanguages))
    val errorBus: ErrorBus = ErrorBus()
    val isNewLocale: Boolean = res.resourcesLocale != locale
    val isNewBuild: Boolean = ref == null
    //endregion vars

    init {
        ref
            ?.languages
            ?.let {
                languagesNames.clear()

                languages
                    .asSequence()
                    .withIndex()
                    .associateBy { it.value.id }
                    .let { ass -> this.languagesKeys = it.toMutableList().onEach { ass[it]?.let { checkLanguages[it.index] = true; languagesNames.add(it.value.name) } } }

                languagesNames.sortWith(compareBy(res.collator) { it })
            }
    }

    fun getLanguagesAdapter(context: Context): ArrayAdapter<String> = ArrayAdapter(context, android.R.layout.simple_list_item_1, languagesNames).also { this.languagesAdapter = it }

    fun addLanguage(position: Int) { with(languages[position]) { tempLanguagesKeys.add(id); tempLanguagesNames.add(name) } }

    fun removeLanguage(position: Int) { with(languages[position]) { tempLanguagesKeys.remove(id); tempLanguagesNames.remove(name) } }

    fun setLocationDetail(ll: LatLng?) { ll?.let { entity.position = Position.from(ll) } }

    fun setCivilStatus(status: Int) { entity.civilStatus = status }

    fun setDetail(value: String?) {
        with(entity) {
            when (fieldID) {
                R.id.txt_entityFirstName -> firstName = value
                R.id.txt_entityMiddleNames -> middleNames = value
                R.id.txt_entityLastName -> lastName = value
                R.id.txt_entityPhone -> phone = value
                R.id.txt_entityEmail -> email = value
                R.id.txt_entityWeb -> web = value
                R.id.txt_legalTIN -> tin = value
                R.id.txt_legalDependents -> dependents = value
                R.id.txt_passphrasePassphrase -> partyPassphrase = value
                R.id.txt_positionStreet -> streetName = value
                R.id.txt_positionDoor -> doorNumber = value
                R.id.txt_positionZip -> zipCode = value
                R.id.txt_accountUserName -> userName = value
                R.id.txt_accountPassword -> password = value
            }
        }
    }

    fun prepareLanguageSelection() {
        tempLanguagesKeys.clear()
        tempLanguagesNames.clear()

        languagesKeys?.takeIfWith { isNotEmpty() }?.apply { tempLanguagesKeys.addAll(this); tempLanguagesNames.addAll(languagesNames) }

        this.tempCheckLanguages = checkLanguages.copyOf(checkLanguages.size)
    }

    fun saveLanguageSelection() {
        this.checkLanguages = tempCheckLanguages.copyOf(tempCheckLanguages.size)

        languagesNames.apply { clear(); if (tempLanguagesKeys.isEmpty()) { add(res.string(R.string.spokenLanguages)) } else { addAll(tempLanguagesNames); sort() } }

        this.languagesKeys = tempLanguagesKeys.takeIfWith { isNotEmpty() }?.toMutableList()

        entity.languages = languagesKeys?.toMutableList()

        languagesAdapter.notifyDataSetChanged()
    }

    fun reduce(): Entity = entity reducedBy ref

    @Err suspend fun validate(): Int {
        return entity
            .validate(ref, res.isNotSpot)
            .let {
                when {
                    it != Err.NONE -> it
                    isNewBuild && partyPassphrase.isNullOrBlank() -> Err.PASSPHRASE
                    isNewBuild && notNewImage -> Err.PHOTO
                    !isNewBuild && entity == ref && notNewImage && !isNewLocale && partyPassphrase.isNullOrBlank() -> Err.NO_CHANGE
                    else -> Err.NONE
                }
            }
            .also { errorBus.send(value = it) }
    }
}