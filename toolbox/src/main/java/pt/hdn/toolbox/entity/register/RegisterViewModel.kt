package pt.hdn.toolbox.entity.register

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import androidx.lifecycle.*
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.flow.*
import pt.hdn.contract.annotations.Day.Companion.DAY_28
import pt.hdn.contract.annotations.MonthsPeriod.Companion.MONTHS_ALL
import pt.hdn.contract.annotations.Speciality.Companion.CEO
import pt.hdn.contract.schemas.FixSchema
import pt.hdn.contract.schemas.Schema
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.entity.util.EntityViewModel
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Misc.Companion.SPOT_URL
import java.util.*
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.login.LogInResponse
import pt.hdn.toolbox.communications.entity.EntityRequest
import pt.hdn.toolbox.communications.login.LogInRequest
import pt.hdn.toolbox.communications.login.SignInResponse
import pt.hdn.toolbox.entity.util.Entity
import pt.hdn.toolbox.entity.util.EntityDealership
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.preferences.DefaultPreferences
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.security.Clearance
import java.math.BigDecimal.ZERO
import java.time.ZonedDateTime
import javax.inject.Inject

/**
 * The register [ViewModel] that inherits from [EntityViewModel] and handles the addition of the [Entity] in the database
 */
@HiltViewModel
class RegisterViewModel @Inject constructor(
    requestOptions: RequestOptions,
    photo: Photo,
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : EntityViewModel(requestOptions, photo, dataBus, sessionBus, repo, resources){

    //region vars
    override val response: Flow<Result<LogInResponse>>
    private lateinit var signInResponse: SignInResponse
    private val loginRequest: MutableSharedFlow<LogInRequest> = MutableSharedFlow()
    val loginResponse: Flow<Result<SignInResponse>>
    //endregion vars

    init {
        this.entityDealership = EntityDealership(res = res, localePreference = Locale.getDefault().toString())

        this.response = request
            .flatMapLatest { repo.register(it) }
            .onEach { it.success { res.apply { resourcesLocale = this@success.locale; specialities = this@success.specialities }; sessionBus.send(session) } }
            .flowOn(Default)

        this.loginResponse = loginRequest
            .flatMapLatest { repo.firstLogin(it) }
            .onEach { it.success { this@RegisterViewModel.signInResponse = this } }
            .flowOn(Default)
    }

    override fun setEntity() {
        launch {
            with(entityDealership) {
                if (validate() == Err.NONE) {
                    val (contract: Contract?, schemas: List<Schema>?) = if (res.isNotSpot) generateContract(entity.tin!!, signInResponse.id!!) else null to null

                    request.emit(
                        EntityRequest(
                            command = Command.ADD,
                            collectionUUID = res.collectionUUID,
                            appUUID = res.appUUID,
                            deputyUUID = if (res.isNotSpot) signInResponse.uuid else null,
                            deputyID = if (res.isNotSpot) signInResponse.id else null,
                            sessionUUID = UUID.randomUUID(),
                            entity = entity,
                            clearance = if (res.isNotSpot) Clearance(true) else null,
                            contract = contract,
                            schemas = schemas,
                            partyPassphrase = partyPassphrase!!,
                            deputyPassphrase = if (res.isNotSpot) signInResponse.request.deputyPassphrase else null,
                            isNotSpot = res.isNotSpot,
                            preferences = DefaultPreferences(name = entity.firstName!!, position = entity.position!!, locale = locale, location = false),
                            photo = photo
                        )
                    )
                }
            }
        }
    }

    fun getSpotIntent(): Intent = Intent(ACTION_VIEW, Uri.parse(SPOT_URL)).setPackage(GOOGLE_PLAY_STORE_PACKAGE)

    fun setLogIn(userName: String, password: String, passphrase: String): Boolean {
        launch {
            loginRequest.emit(
                LogInRequest(
                    userName = userName,
                    password = password,
                    command = false,
                    appUUID = res.appUUID,
                    deputyPassphrase = passphrase
                )
            )
        }

        return true
    }

    private fun generateContract(buyerId: String, sellerId: String): Pair<Contract, List<Schema>> {
        return mutableListOf<Schema>(FixSchema(ZERO))
            .let {
                Contract(
                    tasks = mutableListOf(Task(specialityType = res.specialities[UUID.fromString(CEO)]!!, schemas = it)),
                    recurrence = Recurrence(start = ZonedDateTime.now()).apply { setMonthPeriod(MONTHS_ALL); addDay(DAY_28) },
                    buyerId = buyerId,
                    buyerDeputyId = sellerId,
                    sellerId = sellerId,
                    sellerDeputyId = sellerId
                ) to it
            }
    }

    //region NOT IN USE
    override fun removeEntity() {}
    //endregion NOT IN USE
}