package pt.hdn.toolbox.entity.shared

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.OnFocusChangeListener
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.databinding.FragmentPassphraseBinding
import pt.hdn.toolbox.entity.profile.ProfileFragment
import pt.hdn.toolbox.entity.profile.ProfileViewModel
import pt.hdn.toolbox.entity.register.RegisterFragment
import pt.hdn.toolbox.entity.register.RegisterViewModel
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.entity.util.EntityViewModel
import pt.hdn.toolbox.misc.setTextInput
import pt.hdn.toolbox.misc.viewModels

@AndroidEntryPoint
class PassphraseFragment : BindingFragment<FragmentPassphraseBinding>(FragmentPassphraseBinding::inflate), OnFocusChangeListener, TextWatcher {

    //region vars
    private val entityViewModel: EntityViewModel by viewModels(mapOf(RegisterFragment::class to RegisterViewModel::class, ProfileFragment::class to ProfileViewModel::class))
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(entityViewModel) {
                if (parentFragment is RegisterFragment) tilPassphrasePassphrase.setTextInput(this@PassphraseFragment, this@PassphraseFragment)
                else {
                    lblPassphraseTitle.text = string(R.string.readCarefully)
                    lblPassphraseWarning.text = string(if (res.isNotSpot) R.string.passphraseBusinessWarning else R.string.passphraseSpotWarning)
                    tilPassphrasePassphrase.hint = string(R.string.passphrase)
                }

                errorBus.observe(viewLifecycleOwner) { if (it == Err.PASSPHRASE) handleError(txtPassphrasePassphrase) }
            }
        }
    }

    override fun onFocusChange(view: View?, hasFocus: Boolean) { entityViewModel.fieldID = view?.takeIf { hasFocus }?.id ?: 0 }

    override fun afterTextChanged(editable: Editable?) { entityViewModel.setDetail(editable) }

    //region NOT IN USE
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {  }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {  }
    //endregion NOT IN USE
}