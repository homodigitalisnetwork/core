package pt.hdn.toolbox.entity.shared

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.OnFocusChangeListener
import android.widget.ArrayAdapter
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.databinding.FragmentLegalBinding
import pt.hdn.toolbox.entity.profile.ProfileFragment
import pt.hdn.toolbox.entity.profile.ProfileViewModel
import pt.hdn.toolbox.entity.register.RegisterFragment
import pt.hdn.toolbox.entity.register.RegisterViewModel
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.entity.util.EntityViewModel
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.onNextAction
import pt.hdn.toolbox.misc.setTextInput
import pt.hdn.toolbox.misc.viewModels

@AndroidEntryPoint
class LegalFragment : BindingFragment<FragmentLegalBinding>(FragmentLegalBinding::inflate), OnFocusChangeListener, TextWatcher {

    //region vars
    private val entityViewModel: EntityViewModel by viewModels(mapOf(RegisterFragment::class to RegisterViewModel::class, ProfileFragment::class to ProfileViewModel::class))
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(entityViewModel) {
                if (res.isNotSpot) {
                    tilLegalDependents.visibility = GONE
                    tilLegalCivilStatus.visibility = GONE
                    ckboxLegalHandicapped.visibility = GONE

                    txtLegalTIN.onNextAction { dataBus.send(viewLifecycleOwner, Address.SWIPE, Unit) }
                }
                else {
                    txtLegalTIN.nextFocusDownId = R.id.til_legalDependents
                    tilLegalDependents.nextFocusDownId = R.id.ckbox_legalHandicapped

                    txtLegalDependents.onNextAction { keyboard.hide(); txtLegalCivilStatus.performClick() }

                    txtLegalCivilStatus
                        .apply {
                            setAdapter(ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, stringArray(R.array.legal_civilStatus)))

                            setOnItemClickListener { _, _, position, _ -> setCivilStatus(position) }
                        }

                    ckboxLegalHandicapped.apply { text = string(R.string.handicapped); setOnCheckedChangeListener { _, isChecked -> handicapped = isChecked } }
                }

                if (parentFragment is RegisterFragment) {
                    tilLegalTIN.setTextInput(this@LegalFragment, this@LegalFragment)

                    if (!res.isNotSpot) tilLegalDependents.setTextInput(this@LegalFragment, this@LegalFragment) }
                else {
                    tilLegalTIN.hint = string(R.string.tin)

                    if (!res.isNotSpot) { tilLegalDependents.hint = string(R.string.dependents); tilLegalCivilStatus.hint = string(R.string.civilStatus) }

                    oldEntity
                        ?.observe(viewLifecycleOwner) {
                            it?.run {
                                tilLegalTIN.setTextInput(this@LegalFragment, this@LegalFragment, value = tin)

                                if (!res.isNotSpot) {
                                    tilLegalDependents.setTextInput(this@LegalFragment, this@LegalFragment, value = dependents)

                                    txtLegalCivilStatus.apply { setText((adapter as ArrayAdapter<String>).getItem(civilStatus!!)) } }
                            }
                        }
                }

                errorBus
                    .observe(viewLifecycleOwner) {
                        when (it) {
                            Err.TIN -> handleError(tilLegalTIN)
                            Err.DEPENDENTS -> handleError(tilLegalDependents)
                            Err.CIVIL_STATUS -> handleError(tilLegalCivilStatus)
                        }
                    }
            }
        }
    }

    override fun onFocusChange(view: View?, hasFocus: Boolean) { entityViewModel.fieldID = view?.takeIf { hasFocus }?.id ?: 0 }

    override fun afterTextChanged(editable: Editable?) { entityViewModel.setDetail(editable) }

    //region NOT IN USE
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    //endregion NOT IN USE
}