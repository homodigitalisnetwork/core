package pt.hdn.toolbox.entity.profile

import android.annotation.SuppressLint
import android.app.NotificationManager
import androidx.lifecycle.*
import com.bumptech.glide.request.RequestOptions
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.toolbox.entity.util.EntityViewModel
import pt.hdn.toolbox.annotations.Action.Companion.GET
import pt.hdn.toolbox.annotations.Action.Companion.REMOVE
import pt.hdn.toolbox.annotations.Action.Companion.SET
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Notification.Companion.TRANSACTION
import pt.hdn.toolbox.application.Connectivity
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.entity.EntityRequest
import pt.hdn.toolbox.communications.entity.EntityResponse
import pt.hdn.toolbox.entity.util.Entity
import pt.hdn.toolbox.entity.util.EntityDealership
import pt.hdn.toolbox.foreground.ForegroundService
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.isEmpty
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.notifications.CountDownNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.session.Session
import javax.inject.Inject

/**
 * The profile [ViewModel] that inherits from [EntityViewModel] and handles the modification and deletion of the [Entity] in the database
 */
@SuppressLint("StaticFieldLeak")
@HiltViewModel
class ProfileViewModel @Inject constructor(
    @Connectivity private val connectivity: Flow<Boolean>,
    private val notificationManager: NotificationManager,
    private val countDownNotification: CountDownNotification,
    foregroundServiceFlow: Flow<ForegroundService?>,
    requestOptions: RequestOptions,
    photo: Photo,
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : EntityViewModel(requestOptions, photo, dataBus, sessionBus, repo, resources) {

    //region vars
    override val response: Flow<Result<EntityResponse>>
    private var foregroundService: ForegroundService? = null
    private val mutableOldEntity: MutableSharedFlow<EntityRequest> = MutableSharedFlow(1)
    protected lateinit var session: Session
    //endregion vars

    init {
        sessionBus.observe(viewModelScope) { this.session = it!!; request() }

        foregroundServiceFlow.observe(viewModelScope) { this.foregroundService = it }

        connectivity.observe(viewModelScope) { if (it && oldEntity?.isEmpty() == true) request() }

        this.oldEntity = mutableOldEntity
            .flatMapLatest { repo.entity(it) }
            .map { var entity: Entity? = null; it.success { EntityDealership(res, this.entity, session.locale).let { this@ProfileViewModel.entityDealership = it; entity = it.entity } }; entity }
            .flowOn(Default)
            .shareIn(viewModelScope, Lazily, 1)

        this.response = request
            .flatMapLatest { repo.entity(it) }
            .onEach {
                it.success {
                    with(request) {
                        when (action) {
                            REMOVE -> notificationManager.activeNotifications.asSequence().filter { it.id == TRANSACTION }.forEachWith { countDownNotification.finish() }
                            SET -> { entity?.preferencesMap?.let { session.updatePreferences(it) }; foregroundService?.updateBroadcast() }
                        }
                    }
                }
            }
            .flowOn(Default)
    }

    override fun setEntity() {
        launch {
            with(entityDealership) {
                if (validate() == Err.NONE) {
                    request.emit(
                        EntityRequest(
                            action = SET,
                            command = Command.SET,
                            partyUUID = session.partyUUID,
                            deputyUUID = session.deputyUUID,
                            entity = reduce(),
                            photo = if (notNewImage) null else photo,
                            locale = if (isNewBuild) locale else null
                        )
                    )
                }
            }
        }
    }

    override fun removeEntity() {
        launch {
            request.emit(
                EntityRequest(
                    action = REMOVE,
                    command = Command.REMOVE,
                    deputyUUID = session.deputyUUID,
                    partyUUID = session.partyUUID
                )
            )
        }
    }

    private fun request() {
        launch {
            mutableOldEntity.emit(
                EntityRequest(
                    command = Command.GET,
                    deputyUUID = session.deputyUUID,
                    partyUUID = session.partyUUID,
                    photo = photo,
                    action = GET
                )
            )
        }
    }
}