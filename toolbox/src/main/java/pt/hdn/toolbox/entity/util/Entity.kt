package pt.hdn.toolbox.entity.util

import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.Reducible

interface Entity : Cloneable, Reducible<Entity>{
    var firstName: String?
    var middleNames: String?
    var lastName: String?
    var email: String?
    var phone: String?
    var web: String?
    var languages: MutableList<Int>?
    var tin: String?
    var dependents: String?
    var civilStatus: Int?
    var handicapped: Boolean?
    var streetName: String?
    var doorNumber: String?
    var zipCode: String?
    var countryCode: String?
    var position: Position?
    var userName: String?
    var password: String?
    val preferencesMap: MutableMap<String, Any>?

    public override fun clone(): Entity

    @Err fun validate(entity: Entity?, isNotSpot: Boolean): Int {
        return when {
            this == entity -> Err.NO_CHANGE
            firstName.isNullOrBlank() -> Err.FIRST_NAME
            !isNotSpot && middleNames.isNullOrBlank() -> Err.MIDDLE_NAMES
            lastName.isNullOrBlank() -> Err.LAST_NAME
            phone.isNullOrBlank() -> Err.PHONE
            email.isNullOrBlank() -> Err.EMAIL
            isNotSpot && web.isNullOrBlank() -> Err.WEB
            !isNotSpot && languages.isNullOrEmpty() -> Err.LANGUAGES
            tin.isNullOrBlank() -> Err.TIN
            !isNotSpot && dependents.isNullOrBlank() -> Err.DEPENDENTS
            !isNotSpot && civilStatus == null -> Err.CIVIL_STATUS
            !isNotSpot && handicapped == null -> Err.HANDICAPPED
            streetName.isNullOrBlank() -> Err.STREET
            doorNumber.isNullOrBlank() -> Err.DOOR
            zipCode.isNullOrBlank() -> Err.ZIP
            position == null -> Err.POSITION
            countryCode.isNullOrBlank() -> Err.COUNTRY
            entity == null && userName.isNullOrBlank() -> Err.USER_NAME
            !isNotSpot && entity == null && password.isNullOrBlank() -> Err.PASSWORD
            else -> Err.NONE
        }
    }

    override fun reducedBy(other: Entity?): Entity {
        return other
            ?.let {
                DefaultEntity(
                    firstName = takeUnless(firstName, it.firstName),
                    middleNames = takeUnless(middleNames, it.middleNames),
                    lastName = takeUnless(lastName, it.lastName),
                    email = takeUnless(email, it.email),
                    phone = takeUnless(phone, it.phone),
                    web = takeUnless(web, it.web),
                    languages = takeUnless(languages, it.languages),
                    tin = takeUnless(tin, it.tin),
                    dependents = takeUnless(dependents, it.dependents),
                    civilStatus = takeUnless(civilStatus, it.civilStatus),
                    handicapped = takeUnless(handicapped, it.handicapped),
                    streetName = takeUnless(streetName, it.streetName),
                    doorNumber = takeUnless(doorNumber, it.doorNumber),
                    zipCode = takeUnless(zipCode, it.zipCode),
                    position = takeUnless(position, it.position),
                    countryCode = takeUnless(countryCode, it.countryCode),
                    userName = takeUnless(userName, it.userName),
                    password = takeUnless(password, it.password)
                )
            } ?: this
    }
}