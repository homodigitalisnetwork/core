package pt.hdn.toolbox.resources

import android.content.Context
import android.content.res.Configuration
import android.content.res.Configuration.UI_MODE_NIGHT_MASK
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.content.res.TypedArray
import android.graphics.Typeface
import android.graphics.drawable.VectorDrawable
import android.icu.text.Collator
import androidx.annotation.*
import com.google.gson.Gson
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.BuildConfig
import pt.hdn.toolbox.R
import pt.hdn.toolbox.data.IdNameCodeData
import pt.hdn.toolbox.misc.*
import java.time.format.DateTimeFormatter
import java.util.*

open class DefaultResources constructor(
    gson: Gson,
    @ArrayRes appAvailableLocalesId: Int,
    val ISO_HDN_DATE_TIME: DateTimeFormatter,
    override val appUUID: UUID,
    override val appName: String,
    private val context: Context,
    private val configuration: Configuration,
    override val isNotSpot: Boolean,
    @DrawableRes override val logoId: Int
) : Resources {

    //region vars
    private var resources: android.content.res.Resources = context.resources
    final override var locale: Locale = Locale.getDefault(); private set
    final override var collator: Collator = Collator.getInstance(locale); private set
    override var resourcesLocale: String = locale.toString(); set(value) { field = value; setVariables() }
    override var specialities: Map<UUID, UUIDNameMarketData> = mapOf(); set(value) { field = value.asSequence().sortedBy { it.value.name }.toMap() }
    private val labelsMap: Map<String, List<String>>
    private val weekDayLabelsMap: Map<String, List<String>>
    private val monthsPeriodMap: Map<String, Map<Int, String>>
    private val monthsMap: Map<String, Map<Int, String>>
    private val schemasMap: Map<String, Map<Int, String>>
    private val sourcesMap: Map<String, Map<Int, String>>
    private val daysOrdinalMap: Map<String, Map<Int, String>>
    private val daysOfTheWeekMap: Map<String, Map<Int, String>>
    private val daysOfTheWeekPeriodMap: Map<String, Map<Int, String>>
    override val daysOfTheWeek: Map<Int, String>; get() = daysOfTheWeekMap[resourcesLocale]!!
    override val daysOfTheWeekPeriod: Map<Int, String>; get() = daysOfTheWeekPeriodMap[resourcesLocale]!!
    override val daysOrdinal: Map<Int, String>; get() = daysOrdinalMap[resourcesLocale]!!
    override val monthsPeriod: Map<Int, String>; get() = monthsPeriodMap[resourcesLocale]!!
    override val months: Map<Int, String>; get() = monthsMap[resourcesLocale]!!
    override val orientation: Int; get() = resources.configuration.orientation
    override val collectionUUID: UUID = UUID.fromString(BuildConfig.COLLECTION_UUID)
    override val fileProviderAuth: String = "${context.packageName}.fileprovider"
    override val appAvailableLocales: List<String>
    override val languages: List<IdNameCodeData>
    override val schemas: Map<Int, String>; get() = schemasMap[resourcesLocale]!!
    override val sources: Map<Int, String>; get() = sourcesMap[resourcesLocale]!!
    override val font: Typeface
    override val packageName: String = context.packageName
    override val locales: List<IdNameCodeData>
    override val pins: Map<UUID, Pin> by lazy { createPins() }
    override val labels: List<String>; get() = labelsMap[resourcesLocale]!!
    override val weekDayLabels: List<String>; get() = weekDayLabelsMap[resourcesLocale]!!
    override val isDarkModeOn: Boolean; get() = (context.resources.configuration.uiMode and UI_MODE_NIGHT_MASK) == UI_MODE_NIGHT_YES
    //endregion vars

    init {
        this.appAvailableLocales = stringArray(appAvailableLocalesId).toList()
        this.font = resources.getFont(R.font.exo_2)

        with(gson) {
            this@DefaultResources.monthsPeriodMap = from(resources, R.raw.month_period_maps)
            this@DefaultResources.monthsMap = from(resources, R.raw.month_maps)
            this@DefaultResources.daysOrdinalMap = from(resources, R.raw.day_ordinal_maps)
            this@DefaultResources.daysOfTheWeekMap = from(resources, R.raw.dow_maps)
            this@DefaultResources.daysOfTheWeekPeriodMap = from(resources, R.raw.day_period_maps)
            this@DefaultResources.schemasMap = from(resources, R.raw.schema_maps)
            this@DefaultResources.sourcesMap = from(resources, R.raw.source_maps)
            this@DefaultResources.labelsMap = from(resources, R.raw.labels)
            this@DefaultResources.weekDayLabelsMap = from(resources, R.raw.week_days_labels)
            this@DefaultResources.languages = from(resources, R.raw.language_type)
            this@DefaultResources.locales = from(resources, R.raw.locale_type)
        }
    }

    override fun string(format: String, vararg args: Any?): String = format.format(locale, args)

    override fun string(@StringRes id: Int): String = resources.getString(id)

    override fun stringArray(@ArrayRes id: Int): Array<String> = resources.getStringArray(id)

    override fun dimen(@DimenRes id: Int): Int = resources.getDimension(id).toInt()

    override fun color(@ColorRes id: Int): Int = resources.getColor(id, null)

    private fun createPins(): Map<UUID, Pin> {
        val pins = mutableMapOf<UUID ,Pin>()
        val pinsAppUUID: TypedArray = resources.obtainTypedArray(R.array.pinsAppUUID)
        val pinsRes: TypedArray = resources.obtainTypedArray(R.array.pinsRes)

        pinsRes
            .forEachIndexed {
                pinsAppUUID
                    .getString(it)
                    ?.let { appUUID ->
                        pinsRes
                            .getResourceId(it, 0)
                            .takeUnless { it == 0 }
                            ?.let { (resources.getDrawable(it, null) as VectorDrawable).toBitmapDescriptor() }
                            ?.let { pins[UUID.fromString(appUUID)] = Pin(UUID.fromString(appUUID), it) }
                    }
            }

        pinsAppUUID.recycle()
        pinsRes.recycle()

        return pins
    }

    private fun setVariables() {
        this.locale = resourcesLocale.split("_").let { Locale(it[0], it[1]) }
        this.resources = context.createConfigurationContext(configuration.apply { setLocale(this@DefaultResources.locale) }).resources
        this.collator = Collator.getInstance(locale)
    }
}
