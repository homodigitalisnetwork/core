package pt.hdn.toolbox.resources

import android.graphics.Typeface
import android.icu.text.Collator
import androidx.annotation.*
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.data.IdNameCodeData
import pt.hdn.toolbox.misc.Pin
import java.util.*

interface Resources {
    val isNotSpot: Boolean
    val font: Typeface
    val locale: Locale
    val collator: Collator
    val schemas: Map<Int, String>
    val sources: Map<Int, String>
    val appUUID: UUID
    val collectionUUID: UUID
    val packageName: String
    var resourcesLocale: String
    var specialities: Map<UUID, UUIDNameMarketData>
    val appAvailableLocales: List<String>
    val daysOrdinal: Map<Int, String>
    val daysOfTheWeekPeriod: Map<Int, String>
    val daysOfTheWeek: Map<Int, String>
    val locales: List<IdNameCodeData>
    val languages: List<IdNameCodeData>
    val months: Map<Int, String>
    val monthsPeriod: Map<Int, String>
    val appName: String
    val pins: Map<UUID, Pin>
    val labels: List<String>
    val weekDayLabels: List<String>
    val fileProviderAuth: String
    val isDarkModeOn: Boolean
    val orientation: Int
    val logoId: Int

    fun string(format: String, vararg args: Any?): String
    fun string(@StringRes id: Int): String
    fun stringArray(@ArrayRes id: Int): Array<String>
    fun dimen(@DimenRes id: Int): Int
    fun color(@ColorRes id: Int): Int
}