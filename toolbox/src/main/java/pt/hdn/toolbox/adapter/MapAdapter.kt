package pt.hdn.toolbox.adapter

import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.*
import pt.hdn.toolbox.annotations.Mode
import pt.hdn.toolbox.annotations.Mode.Companion.MULTIPLE_SELECTION
import pt.hdn.toolbox.annotations.Mode.Companion.NOT_EXPANDABLE
import pt.hdn.toolbox.annotations.Mode.Companion.SINGLE_SELECTION
import pt.hdn.toolbox.misc.takeUnlessWith
import pt.hdn.toolbox.resources.Resources

/**
 * Abstract base [Adapter] for a [MutableMap] reference that inherits from [LifecycleAdapter] and work in conjunction with [HDNViewHolder].
 * It provides [MutableMap] like functionality to enable synchronization between the list and the [View] that it is attached to and have some degree of behaviour customization.
 * It also provides observable variables
 */
abstract class MapAdapter<VH : HDNViewHolder<ViewBinding, Map.Entry<K, V>>, K, V>(
    ref: MutableMap<K, V>,
    protected val res: Resources,
    @Mode private val mode: Int = SINGLE_SELECTION
) : LifecycleAdapter<VH>(), MutableIterable<MutableMap.MutableEntry<K, V>>, OnViewHolderClickListener, OnViewClickListener {

    //region vars
    protected val selectedPosition: MutableMap<Int, MutableMap.MutableEntry<K, V>> = mutableMapOf()
    private val entries: MutableList<MutableMap.MutableEntry<K, V>> = ref.entries.toMutableList()
    private val mutableSource: MutableSharedFlow<Map<K, V>> = MutableSharedFlow<Map<K, V>>(replay = 1, onBufferOverflow = DROP_OLDEST).apply { tryEmit(ref) }
    private val mutableSize: MutableStateFlow<Int> = MutableStateFlow(ref.size)
//    private val mutableHasAnySelection: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val mutableClick: MutableSharedFlow<Pair<View?, Int>> = MutableSharedFlow(replay = 1, onBufferOverflow = DROP_OLDEST)
    private val mutableHighlighted: MutableStateFlow<Pair<Int, Boolean>> = MutableStateFlow(NO_POSITION to COLLAPSED)
    val source: SharedFlow<Map<K, V>> = mutableSource
    val size: StateFlow<Int> = mutableSize
    val click: Flow<Pair<View?, Int>> = mutableClick
    val highlighted: StateFlow<Pair<Int, Boolean>> = mutableHighlighted
//    val selection: List<MutableMap.MutableEntry<K, V>>; get() = selectedPosition.values.toList()
    val firstSelected: MutableMap.MutableEntry<K, V>?; get() = selectedPosition.values.firstOrNull()
//    val hasAnySelection: StateFlow<Boolean>; get() = mutableHasAnySelection
    var ref: MutableMap<K, V> = ref; set(value) { field = value; selectedPosition.clear(); sourceObservers(); sizeObservers(); highlightObservers(NO_POSITION, COLLAPSED); notifyDataSetChanged() }
    //endregion vars

    private companion object {
        private const val COLLAPSED = false
        private const val EXPANDED = true
    }

    abstract fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return onCreateViewHolder(res, parent, viewType).apply { onViewClickListener = this@MapAdapter; onViewHolderClickListener = this@MapAdapter }
    }

    override fun onViewAttachedToWindow(holder: VH) { super.onViewAttachedToWindow(holder); holder.onViewCreated() }

    override fun onViewDetachedFromWindow(holder: VH) { super.onViewDetachedFromWindow(holder); holder.onViewDestroyed() }

    override fun onBindViewHolder(holder: VH, position: Int) { with(holder) { entries[position].setViewHolder(selectedPosition.containsKey(position)) } }

    override fun onViewClick(position: Int, view: View?) { mutableClick.tryEmit(view to position) }

    override fun onViewHolderClick(position: Int) {
        with(selectedPosition) {
            when {
                mode != MULTIPLE_SELECTION && isEmpty() -> changeState(position, this@MapAdapter.entries[position])
                mode != MULTIPLE_SELECTION &&  containsKey(position)-> changeState(position)
                mode != MULTIPLE_SELECTION -> { changeState(keys.first(), callObs = false); changeState(position, this@MapAdapter.entries[position]) }
                containsKey(position) -> changeState(position)
                else -> changeState(position, this@MapAdapter.entries[position])
            }

//            updateHasAnySelection()
        }
    }

    override fun getItemCount(): Int = ref.size

    override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, V>> = ref.iterator()

//    fun remove(key: K): V? = ref.remove(key)?.also { selectedPosition.cont }

//    fun remove(t: T): Boolean = ref.indexOf(t).takeIf { it > -1 }?.let { removeAt(it); true } ?: false

//    fun removeAt(position: Int): T {
//        return ref.removeAt(position)
//            .also { selectedPosition.remove(position); notifyItemRemoved(position); sourceObservers(); sizeObservers(); highlightObservers(position, COLLAPSED); updateHasAnySelection() }
//    }

    fun removeAt(position: Int): Map.Entry<K, V> {
        return selectedPosition.remove(position)!!.also { ref.remove(it.key); notifyItemRemoved(position); sourceObservers(); sizeObservers(); highlightObservers(position, COLLAPSED)/*; updateHasAnySelection()*/ }
    }

    fun removeSelected(): Map<K, V> {
        return with(selectedPosition) {
            val map: Map<K, V> = values.associate { it.toPair() }

            when (size) {
                0 -> null
                1 -> removeAt(keys.first())
                else -> {
                    keys.forEach { ref.remove(remove(it)!!.key) }

                    notifyDataSetChanged()

                    sourceObservers()

                    sizeObservers()

                    highlightObservers(NO_POSITION, COLLAPSED)

//                    updateHasAnySelection()
                }
            }

            map
        }
    }

    operator fun get(position: Int): MutableMap.MutableEntry<K, V> = entries[position]

    fun add(entry: MutableMap.MutableEntry<K, V>) {
        with(entry) { ref[key] = value }; entries.add(ref.entries.last()); notifyItemInserted(ref.size - 1); sourceObservers(); sizeObservers()
    }

    fun add(pair: Pair<K, V>) {
        with(pair) { ref[first] = second }; entries.add(ref.entries.last()); notifyItemInserted(ref.size - 1); sourceObservers(); sizeObservers()
    }

//    fun addSorted(t: T) { ref.run { add(t); sortWith(compareBy(res.collator) { it.toString() }); indexOf(t) }.let { notifyItemInserted(it); sourceObservers(); sizeObservers() } }

    fun addOrReplaceSelected(pair: Pair<K, V>) { if (selectedPosition.isEmpty()) add(pair) else replaceSelected(pair) }

//    fun addAll(map: Map<K, V>) {
//        map.takeUnlessWith { isEmpty() }?.apply { ref.putAll(this); this@MapAdapter.entries.addAll(this.entries.toList()); notifyItemRangeChanged(ref.size - size, size); sourceObservers(); sizeObservers() }
//    }

    operator fun set(position: Int, t: V) { entries[position].setValue(t); notifyItemChanged(position); sourceObservers() }

//    fun set(position: Int, block: T.() -> Unit) { ref[position].apply(block); notifyItemChanged(position); sourceObservers() }

    fun replaceSelected(vararg pairs: Pair<K, V>): Boolean {
        return with(selectedPosition) {
            if (size != pairs.size) false
            else {
                pairs.forEach { (k, v) -> ref[k] = v }

                clear()

                if (size == 1) notifyItemChanged(keys.first()) else notifyDataSetChanged()

                sourceObservers()

                highlightObservers(NO_POSITION, COLLAPSED)

                true
            }
        }
    }

    fun replaceSelected(vararg entries: MutableMap.MutableEntry<K, V>): Boolean {
        return with(selectedPosition) {
            if (size != entries.size) false
            else {
                entries.forEach { (k, v) -> ref[k] = v }

                clear()

                if (size == 1) notifyItemChanged(keys.first()) else notifyDataSetChanged()

                sourceObservers()

                highlightObservers(NO_POSITION, COLLAPSED)

                true
            }
        }
    }

    @CallSuper
    open fun clear() { ref.clear(); selectedPosition.clear(); notifyDataSetChanged() }

    fun isEmpty(): Boolean = ref.isEmpty()

    fun isNotEmpty(): Boolean = ref.isNotEmpty()

//    fun sort() { ref.sortWith(compareBy(res.collator) { it.toString() }) }

    open fun onExpanded(position: Int) {}

    open fun onCollapsed(position: Int) {}

    private fun changeState(position: Int, entry: MutableMap.MutableEntry<K, V>? = null, callObs: Boolean = true) {
        entry?.let { selectedPosition[position] = it } ?: run { selectedPosition.remove(position); if (mode != NOT_EXPANDABLE) onCollapsed(position) }

        notifyItemChanged(position)

        if (entry != null && mode != NOT_EXPANDABLE) onExpanded(position)

        if (callObs) { highlightObservers(position, entry != null) }
    }

    protected fun sourceObservers() { mutableSource.tryEmit(ref) }

    protected fun sizeObservers() { mutableSize.tryEmit(ref.size) }

    private fun highlightObservers(position: Int, state: Boolean) { mutableHighlighted.tryEmit(position to state) }

//    private fun updateHasAnySelection() { mutableHasAnySelection.tryEmit(selectedPosition.isNotEmpty()) }
}