package pt.hdn.toolbox.adapter

import androidx.annotation.CallSuper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Lifecycle.State.*
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder

/**
 * Abstract [Adapter] class that attach a [Lifecycle] to it by implementing [LifecycleOwner]
 */
abstract class LifecycleAdapter<VH: ViewHolder> : Adapter<VH>(), LifecycleOwner {

    //region vars
    private val lifecycleRegistry: LifecycleRegistry = LifecycleRegistry(this)
    //endregion vars

    init { with(lifecycleRegistry) { currentState = INITIALIZED; currentState = CREATED } }

    override fun getLifecycle(): Lifecycle = lifecycleRegistry

    @CallSuper
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) { with(lifecycleRegistry) { currentState = STARTED; currentState = RESUMED } }

    @CallSuper
    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) { with(lifecycleRegistry) { currentState = STARTED; currentState = CREATED; currentState = DESTROYED } }
}