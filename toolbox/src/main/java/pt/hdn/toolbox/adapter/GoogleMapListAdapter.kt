package pt.hdn.toolbox.adapter

import android.os.Bundle
import androidx.viewbinding.ViewBinding
import pt.hdn.toolbox.annotations.Mode
import pt.hdn.toolbox.annotations.Mode.Companion.SINGLE_SELECTION
import pt.hdn.toolbox.resources.Resources

/**
 * A specialized [ListAdapter] to handle [MapViewHolder]s and its necessities
 */
abstract class GoogleMapListAdapter<VH : MapViewHolder<out ViewBinding, T>, T>(
	ref: MutableList<T>,
	resources: Resources,
	@Mode private val mode: Int = SINGLE_SELECTION
) : ListAdapter<VH, T>(ref, resources, mode) {

	//region vars
	private val pairs: MutableMap<Int, VH> = mutableMapOf()
	//endregion vars

	override fun onBindViewHolder(holder: VH, position: Int) { pairs[position] = holder; super.onBindViewHolder(holder, position) }

	fun onSaveInstanceState(bundle: Bundle) { pairs.forEach { (_, holder) -> holder.onSaveInstanceState(bundle) } }

	fun onResume() { pairs.forEach { (_, holder) -> holder.onResume() } }

	fun onPause() { pairs.forEach { (_, holder) -> holder.onPause() } }

	fun onStop() { pairs.forEach { (_, holder) -> holder.onStop() } }

	fun onLowMemory() { pairs.forEach { (_, holder) -> holder.onLowMemory() } }

	fun onDestroy() { clear() }

	override fun onCollapsed(position: Int) { pairs[position]?.let { clearHolder(it) } }

	override fun clear() { pairs.apply { forEach { (_, holder) -> clearHolder(holder) }; clear() }; super.clear() }

	private fun clearHolder(holder: VH) { with(holder) { onPause(); onStop(); onDestroy() } }
}
