package pt.hdn.toolbox.adapter

import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.lifecycle.*
import androidx.lifecycle.Lifecycle.State.*
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.*
import pt.hdn.toolbox.annotations.Mode
import pt.hdn.toolbox.annotations.Mode.Companion.NOT_EXPANDABLE
import pt.hdn.toolbox.annotations.Mode.Companion.SINGLE_SELECTION
import pt.hdn.toolbox.misc.takeUnlessWith
import pt.hdn.toolbox.resources.Resources

/**
 * Abstract base [Adapter] for a [MutableList] reference that inherits from [LifecycleAdapter] and work in conjunction with [HDNViewHolder].
 * It provides [MutableList] like functionality to enable synchronization between the list and the [View] that it is attached to and have some degree of behaviour customization.
 * It also provides observable variables
 */
abstract class ListAdapter<VH : HDNViewHolder<out ViewBinding, T>, T>(
    ref: MutableList<T>,
    protected val res: Resources,
    @Mode private val mode: Int = SINGLE_SELECTION
) : LifecycleAdapter<VH>(), Iterable<T>, OnViewHolderClickListener, OnViewClickListener {

    //region vars
    protected val selectedPosition: MutableMap<Int, T> = mutableMapOf()
    private val mutableSource: MutableSharedFlow<List<T>> = MutableSharedFlow<List<T>>(replay = 1, onBufferOverflow = DROP_OLDEST).apply { tryEmit(ref) }
    private val mutableSize: MutableStateFlow<Int> = MutableStateFlow(ref.size)
    private val mutableHasAnySelection: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val mutableClick: MutableSharedFlow<Pair<View?, Int>> = MutableSharedFlow(replay = 1, onBufferOverflow = DROP_OLDEST)
    private val mutableHighlighted: MutableStateFlow<Pair<Int, Boolean>> = MutableStateFlow(NO_POSITION to COLLAPSED)
    val source: SharedFlow<List<T>> = mutableSource
    val size: StateFlow<Int> = mutableSize
    val click: Flow<Pair<View?, Int>> = mutableClick
    val highlighted: StateFlow<Pair<Int, Boolean>> = mutableHighlighted
    val selection: List<T>; get() = selectedPosition.values.toList()
    val firstSelected: T?; get() = selectedPosition.values.firstOrNull()
    val hasAnySelection: StateFlow<Boolean>; get() = mutableHasAnySelection
    var ref: MutableList<T> = ref; set(value) { field = value; selectedPosition.clear(); sourceObservers(); sizeObservers(); highlightObservers(NO_POSITION, COLLAPSED); notifyDataSetChanged() }
    //endregion vars

    private companion object {
        private const val COLLAPSED = false
        private const val EXPANDED = true
    }

    abstract fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return onCreateViewHolder(res, parent, viewType).apply { onViewClickListener = this@ListAdapter; onViewHolderClickListener = this@ListAdapter }
    }

    override fun onViewAttachedToWindow(holder: VH) { super.onViewAttachedToWindow(holder); holder.onViewCreated() }

    override fun onViewDetachedFromWindow(holder: VH) { super.onViewDetachedFromWindow(holder); holder.onViewDestroyed() }

    override fun onBindViewHolder(holder: VH, position: Int) { with(holder) { ref[position].setViewHolder(selectedPosition.containsKey(position)) } }

    override fun onViewClick(position: Int, view: View?) { mutableClick.tryEmit(view to position) }

    override fun onViewHolderClick(position: Int) {
        with(selectedPosition) {
            if (mode != NOT_EXPANDABLE) {
                when {
                    mode == SINGLE_SELECTION && isEmpty() -> changeState(position, ref[position])
                    mode == SINGLE_SELECTION && containsKey(position) -> changeState(position)
                    mode == SINGLE_SELECTION -> { changeState(keys.first(), callObs = false); changeState(position, ref[position]) }
                    containsKey(position) -> changeState(position)
                    else -> changeState(position, ref[position])
                }
            }
            else if (!containsKey(position)) { clear(); this[position] = ref[position]; highlightObservers(position, EXPANDED) }

            updateHasAnySelection()
        }
    }

    override fun getItemCount(): Int = ref.size

    override fun iterator(): Iterator<T> = ref.iterator()

//    fun remove(t: T): Boolean = ref.indexOf(t).takeIf { it > -1 }?.let { removeAt(it); true } ?: false

    fun removeAt(position: Int): T {
        return ref.removeAt(position)
            .also { selectedPosition.remove(position); notifyItemRemoved(position); sourceObservers(); sizeObservers(); highlightObservers(position, COLLAPSED); updateHasAnySelection() }
    }

    fun removeSelected(): Collection<T> {
        return with(selectedPosition) {
            when (size) {
                0 -> null
                1 -> removeAt(keys.first())
                else -> {
                    keys.sortedDescending().forEach { ref.removeAt(it); remove(it) }

                    notifyDataSetChanged()

                    sourceObservers()

                    sizeObservers()

                    highlightObservers(NO_POSITION, COLLAPSED)

                    updateHasAnySelection()
                }
            }

            values
        }
    }

    operator fun get(position: Int): T = ref[position]

    fun add(t: T) { ref.add(t); notifyItemInserted(ref.size - 1); sourceObservers(); sizeObservers() }

    fun addSorted(t: T) { ref.run { add(t); sortWith(compareBy(res.collator) { it.toString() }); indexOf(t) }.let { notifyItemInserted(it); sourceObservers(); sizeObservers() } }

    fun addOrReplaceSelected(t: T) { if (selectedPosition.isEmpty()) add(t) else replaceSelected(t) }

    fun addAll(collection: Collection<T>) {
        collection.takeUnlessWith{ isEmpty() }?.apply { ref.addAll(this); notifyItemRangeInserted(ref.size - size, size); sourceObservers(); sizeObservers() }
    }

    operator fun set(position: Int, t: T) { ref[position] = t; notifyItemChanged(position); sourceObservers() }

    fun set(position: Int, block: T.() -> Unit) { ref[position].apply(block); notifyItemChanged(position); sourceObservers() }

    fun replaceSelected(vararg args: T): Boolean {
        return with(selectedPosition) {
            if (size != args.size) false
            else {
                keys.forEachIndexed { index, position -> args[index].let { ref[position] = it; remove(position) } }

                if (size == 1) notifyItemChanged(keys.first()) else notifyDataSetChanged()

                sourceObservers()

                highlightObservers(NO_POSITION, COLLAPSED)

                true
            }
        }
    }

    @CallSuper
    open fun clear() { ref.clear(); selectedPosition.clear(); notifyDataSetChanged() }

    fun isEmpty(): Boolean = ref.isEmpty()

    fun isNotEmpty(): Boolean = ref.isNotEmpty()

//    fun sort() { ref.sortWith(compareBy(res.collator) { it.toString() }) }

    open fun onExpanded(position: Int) {}

    open fun onCollapsed(position: Int) {}

    private fun changeState(position: Int, t: T? = null, callObs: Boolean = true) {
        t?.let { selectedPosition[position] = it } ?: run { selectedPosition.remove(position); onCollapsed(position) }

        notifyItemChanged(position)

        if (t != null) onExpanded(position)

        if (callObs) { highlightObservers(position, t != null) }
    }

    protected fun sourceObservers() { mutableSource.tryEmit(ref) }

    protected fun sizeObservers() { mutableSize.tryEmit(ref.size) }

    private fun highlightObservers(position: Int, state: Boolean) { mutableHighlighted.tryEmit(position to state) }

    private fun updateHasAnySelection() { mutableHasAnySelection.tryEmit(selectedPosition.isNotEmpty()) }
}