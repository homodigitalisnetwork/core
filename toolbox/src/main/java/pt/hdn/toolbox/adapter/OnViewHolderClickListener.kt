package pt.hdn.toolbox.adapter

/**
 * Specialized SAM for [ViewHolder]/[Adapter] usage
 */
fun interface OnViewHolderClickListener {
    fun onViewHolderClick(position: Int)
}