package pt.hdn.toolbox.adapter

import android.view.View

/**
 * Specialized SAM for [ViewHolder]/[Adapter] usage
 */
fun interface OnViewClickListener {
    fun onViewClick(position: Int, view: View?)
}