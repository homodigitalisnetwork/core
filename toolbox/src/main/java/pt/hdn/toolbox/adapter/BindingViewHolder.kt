package pt.hdn.toolbox.adapter

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.annotation.*
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import androidx.viewbinding.ViewBinding
import pt.hdn.toolbox.misc.Keyboard
import pt.hdn.toolbox.misc.getColorAttr
import pt.hdn.toolbox.resources.Resources
import javax.inject.Inject

/**
 * A base [ViewHolder] that inherits from [LifecycleViewHolder]
 * and provides to any child class common functionality.
 * It implements [OnClickListener] to intercept events and forward then on a custom fashion to the listener
 */
abstract class BindingViewHolder<out VB: ViewBinding> constructor(
    protected val res: Resources,
    protected val parent: ViewGroup,
    inflate: (LayoutInflater, ViewGroup, Boolean) -> VB,
    protected val binding: VB = inflate(LayoutInflater.from(parent.context), parent, false)
): LifecycleViewHolder(binding.root), OnClickListener {

    //region vars
    private var mutableKeyboard: Keyboard? = null
    var onViewHolderClickListener: OnViewHolderClickListener? = null
    var onViewClickListener: OnViewClickListener? = null
    var onTouchListener: OnTouchListener? = null
    protected val keyboard: Keyboard; get() = mutableKeyboard!!
    val font: Typeface = res.font
    val context: Context = parent.context
    //endregion vars

    init { itemView.setOnClickListener { onHolderClick() }; this.mutableKeyboard = Keyboard(context.getSystemService(InputMethodManager::class.java), itemView) }

    override fun onClick(view: View?) { onViewClickListener?.takeIf { adapterPosition != NO_POSITION }?.run { onViewClick(adapterPosition, view) } }

    @ColorInt protected fun color(@ColorRes id: Int): Int = context.getColor(id)

    @ColorInt protected fun colorAttr(@AttrRes id: Int): Int = context.getColorAttr(id)

    protected fun toast(@StringRes id: Int) { Toast.makeText(context, string(id), LENGTH_LONG).show() }

    protected fun drawable(@DrawableRes id: Int): Drawable? = context.getDrawable(id)

    protected fun string(format: String, vararg args: Any?): String = res.string(format, args)

    protected fun dimen(@DimenRes id: Int): Int = res.dimen(id)

    protected fun string(@StringRes id: Int): String = res.string(id)

    protected open fun onHolderClick() { onViewHolderClickListener?.takeIf { adapterPosition != NO_POSITION }?.run { onViewHolderClick(adapterPosition) } }
}