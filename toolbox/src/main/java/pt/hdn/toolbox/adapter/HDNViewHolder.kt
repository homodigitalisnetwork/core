package pt.hdn.toolbox.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import pt.hdn.toolbox.resources.Resources

/**
 * A base [ViewHolder] that inherits from [BindingViewHolder] and defines the common functions
 */
abstract class HDNViewHolder<out VB: ViewBinding, in T> constructor(
    resources: Resources,
    parent: ViewGroup,
    inflate: (LayoutInflater, ViewGroup, Boolean) -> VB
) : BindingViewHolder<VB>(resources, parent, inflate), LifecycleOwner {
    abstract fun T.setViewHolder(isExpanded: Boolean)
}