package pt.hdn.toolbox.adapter

import android.view.View
import androidx.annotation.CallSuper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Lifecycle.State.*
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView.ViewHolder

/**
 * Abstract [ViewHolder] class that attach a [Lifecycle] to it by implementing [LifecycleOwner]
 */
abstract class LifecycleViewHolder constructor(
    rootView: View
) : ViewHolder(rootView), LifecycleOwner {

    //region vars
    private val lifecycleRegistry: LifecycleRegistry = LifecycleRegistry(this)
    //endregion vars

    init { with(lifecycleRegistry) { currentState = INITIALIZED; currentState = CREATED } }

    override fun getLifecycle(): Lifecycle = lifecycleRegistry

    @CallSuper
    open fun onViewCreated() { with(lifecycleRegistry) { currentState = STARTED; currentState = RESUMED } }

    @CallSuper
    open fun onViewDestroyed() { with(lifecycleRegistry) { currentState = DESTROYED } }
}