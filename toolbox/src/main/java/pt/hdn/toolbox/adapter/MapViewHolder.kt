package pt.hdn.toolbox.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.viewbinding.ViewBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.views.OnMapCallback

/**
 * Abstract [ViewHolder] class that inherits from [HDNViewHolder]
 * that handles [GoogleMap] initialization and provides the necessary functions structure and map reference for it children
 */
abstract class MapViewHolder<VB: ViewBinding, in T>(
    resources: Resources,
    parent: ViewGroup,
    inflate: (LayoutInflater, ViewGroup, Boolean) -> VB
) : HDNViewHolder<VB, T>(resources, parent, inflate), OnMapCallback {

    //region vars
    protected var googleMap: GoogleMap? = null
    //endregion vars

    init { MapsInitializer.initialize(context) }

    abstract fun GoogleMap.mapReady()

    protected abstract fun VB.saveInstanceState(bundle: Bundle)

    protected abstract fun VB.resume()

    protected abstract fun VB.pause()

    protected abstract fun VB.stop()

    protected abstract fun VB.lowMemory()

    protected abstract fun VB.destroy()

    override fun GoogleMap.onMapReady() { this@MapViewHolder.googleMap = this; mapReady() }

    fun onSaveInstanceState(bundle: Bundle) { googleMap?.let { binding.saveInstanceState(bundle) } }

    fun onResume() { googleMap?.let { binding.resume() } }

    fun onPause() { googleMap?.let { binding.pause() } }

    fun onStop() { googleMap?.let { binding.stop() } }

    fun onLowMemory() { googleMap?.let { binding.lowMemory() } }

    fun onDestroy() { googleMap?.let { it.clear(); binding.destroy(); this.googleMap = null } }
}