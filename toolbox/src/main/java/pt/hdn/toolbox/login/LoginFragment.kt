package pt.hdn.toolbox.login

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.View.*
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.databinding.FragmentLoginBinding
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.observeWith

/**
 * The [Fragment] that handles login
 */
@AndroidEntryPoint
class LoginFragment : BindingFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate), OnClickListener, OnEditorActionListener {

    //region vars
    private val loginViewModel: LoginViewModel by viewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(loginViewModel) {
                connectivity.observe(viewLifecycleOwner) { setAndGetEnable(it).let { btnLoginLogin.isEnabled = it; btnLoginSignup.isEnabled = it } }

                firstResponse
                    .observeWith(viewLifecycleOwner) {
                        success {
                            if (partnerships.size > 1) {
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(R.string.selectPartner)
                                    .setSingleChoiceItems(ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, partnerships), -1) { _, index ->
                                        loginViewModel.setPartnership(index)
                                    }
                                    .show()
                            }
                        }
                        error { code, _ ->
                            barLoginLoading.visibility = GONE

                            setAndGetEnable(true).let { btnLoginLogin.isEnabled = it; btnLoginSignup.isEnabled = it }

                            when(code) {
                                Err.TIMEOUT -> R.string.unableToConnect
                                Err.INVALID -> R.string.invalidPassword
                                Err.CREDENTIALS -> R.string.notRelationship
                                else -> R.string.unknown
                            }.let { toast(it) }
                        }
                    }

                secondResponse
                    .observeWith(viewLifecycleOwner) {
                        barLoginLoading.visibility = GONE

                        setAndGetEnable(true).let { btnLoginLogin.isEnabled = it; btnLoginSignup.isEnabled = it }

                        success { txtLoginPassword.text?.clear(); txtLoginUserName.apply { text?.clear(); requestFocus() } }
                        error { _, _ -> toast(R.string.failLogin) }
                    }

                imgLoginLogo.setImageResource(res.logoId)

                btnLoginLogin.setOnClickListener(this@LoginFragment)

                txtLoginPassword.setOnEditorActionListener(this@LoginFragment)

                if (login) { btnLoginSignup.setOnClickListener(this@LoginFragment); btnLoginShare.setOnClickListener(this@LoginFragment) }
                else { btnLoginSignup.visibility = GONE; btnLoginShare.visibility = GONE }
            }
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean { if (actionId == IME_ACTION_DONE) eval(); return false }

    override fun onClick(view: View?) {
        view?.id
            ?.let {
                when (it) {
                    R.id.btn_loginLogin -> eval()
                    R.id.btn_loginShare -> startActivity(Intent.createChooser(loginViewModel.getSharedIntent(), getString(R.string.shareUsing)))
                    R.id.btn_loginSignup -> navigateTo(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
                    else -> null
                }
            }
    }

    private fun eval() {
        with(binding!!) {
            val userName: String? = txtLoginUserName.text?.toString()
            val password: String? = txtLoginPassword.text?.toString()

            when {
                userName.isNullOrBlank() -> { txtLoginUserName.requestFocus(); toast(R.string.missingUserName) }
                password.isNullOrBlank() -> { txtLoginPassword.requestFocus(); toast(R.string.missingPassword) }
                else -> {
                    loginViewModel.setLogIn(userName, password)

                    barLoginLoading.visibility = VISIBLE

                    loginViewModel.setAndGetEnable(false).let { btnLoginLogin.isEnabled = it; btnLoginSignup.isEnabled = it }
                }
            }
        }
    }
}