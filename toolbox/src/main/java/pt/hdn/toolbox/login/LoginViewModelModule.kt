package pt.hdn.toolbox.login

import androidx.lifecycle.SavedStateHandle
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import pt.hdn.toolbox.annotations.Field.Companion.LOGIN
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class IsLogin

@Module
@InstallIn(ViewModelComponent::class)
object SignatureViewModelModule {

    /**
     * Extracts and provides a behaviour parameter set in a [NavDirections] to [LoginFragment]
     */
    @IsLogin
    @ViewModelScoped
    @Provides
    fun providesLogin(savedStateHandle: SavedStateHandle): Boolean = savedStateHandle.get<Boolean>(LOGIN)!!
}