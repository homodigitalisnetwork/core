package pt.hdn.toolbox.login

import android.content.Intent
import android.content.Intent.*
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.MimeTypes.Companion.Text.PLAIN
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.login.LogInRequest
import pt.hdn.toolbox.communications.login.LogInResponse
import pt.hdn.toolbox.communications.login.SignInResponse
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.value
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.viewmodels.BasicViewModel
import java.util.*
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    @IsLogin val login: Boolean,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : BasicViewModel<CoreRepo>(repo, resources) {

    //region vars
    private val firstRequest: MutableSharedFlow<LogInRequest> =  MutableSharedFlow()
    private val secondRequest: MutableSharedFlow<SignInResponse> =  MutableSharedFlow()
    private var mutableIsEnable: Int = 0
    val firstResponse: SharedFlow<Result<SignInResponse>>
    val secondResponse: Flow<Result<LogInResponse>>
    var isEnable: Boolean = true; private set
    //endregion vars

    init {
        this.firstResponse = firstRequest
            .flatMapLatest { repo.firstLogin(it) }
            .onEach { it.success { if (partnerships.size == 1) { launch { secondRequest.emit(this@success) } } } }
            .shareIn(viewModelScope, Lazily)

        this.secondResponse = secondRequest
            .flatMapLatest { repo.secondLogin(it) }
            .onEach { it.success { res.apply { specialities = this@success.specialities; resourcesLocale = this@success.locale }; sessionBus.send(session) } }
    }

    fun getSharedIntent(): Intent {
        return Intent(ACTION_SEND)
            .setType(PLAIN)
            .putExtra(EXTRA_TITLE, res.appName)
            .putExtra(EXTRA_TEXT, "${string(R.string.shareMessage)}\n\n${string(R.string.playStoreUrl)}${res.packageName}")
    }

    fun setPartnership(index: Int) { firstResponse.value()?.success { launch { secondRequest.emit(this@success.apply { this.index = index }) } } }

    fun setLogIn(userName: String, password: String) {
        launch {
            firstRequest.emit(
                LogInRequest(
                    userName = userName,
                    password = password,
                    command = true,
                    appUUID = res.appUUID
                )
            )
        }
    }

    fun setAndGetEnable(flag: Boolean): Boolean {
        this.mutableIsEnable = (mutableIsEnable + if (flag) 1 else -1).coerceAtMost(0); this.isEnable = mutableIsEnable == 0; return isEnable
    }
}