package pt.hdn.toolbox.session

import android.os.Parcelable
import pt.hdn.toolbox.business.partner.Partner
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.partnerships.util.Partnership
import java.util.*

interface Session: Parcelable {
	val uuid: UUID
	val partnership: Partnership
	val partner: Partner; get() = partnership.partner
	val deputyUUID: UUID; get() = partnership.deputyUUID
	val partyUUID: UUID; get() = partnership.partyUUID
	var location: Boolean; get() = partnership.location; set(value) { partnership.location = value }
	var searchable: Boolean; get() = partnership.searchable; set(value) { partnership.searchable = value }
	val position: Position; get() = partnership.position
	val locale: String; get() = partnership.locale
	val transactionUUID: UUID?; get() = partnership.transactionUUID

	fun updatePreferences(map: Map<String, Any>)
}