package pt.hdn.toolbox.session

import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.partnerships.util.Partnership
import java.util.*

@Parcelize
data class DefaultSession(
	override val uuid: UUID = UUID.randomUUID(),
	override val partnership: Partnership
): Session {
	override fun updatePreferences(map: Map<String, Any>) { partnership.preferences.update(map) }
}