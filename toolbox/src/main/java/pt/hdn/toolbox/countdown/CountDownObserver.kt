package pt.hdn.toolbox.countdown

@DslMarker
annotation class CountDownObserverDsl

/**
 * An observer type DSL for [CountDownSingletonTimer] with the all major events in a timer
 */
@CountDownObserverDsl
class CountDownObserver {

	//region vars
	var tick: ((millisUntilFuture: Long) -> Unit)? = null; private set
	var finish: (() -> Unit)? = null; private set
	var cancel: (() -> Unit)? = null; private set
	var abort: (() -> Unit)? = null; private set
	//endregion vars

	fun tick(tick: (millisUntilFuture: Long) -> Unit) { this.tick = tick }

	fun finish(finish: () -> Unit) { this.finish = finish }

	fun cancel(cancel: () -> Unit) { this.cancel = cancel }

	fun abort(abort: () -> Unit) { this.abort = abort }
}