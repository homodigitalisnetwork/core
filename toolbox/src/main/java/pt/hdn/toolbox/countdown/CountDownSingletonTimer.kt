package pt.hdn.toolbox.countdown

import android.os.*
import android.widget.Chronometer
import pt.hdn.toolbox.annotations.Duration.Companion.x1

/**
 * A countdown [Timer]
 */
object CountDownSingletonTimer {

    //region vars
    private const val MSG: Int = 1
    private val handler: Handler
    var countDownObserver: CountDownObserver? = null
    private var isRunning: Boolean = false
    private var stopTimeInFuture: Long? = null
    private var countDownInterval: Long = 0
    private var millisInFuture: Long = 0
    //endregion vars

    init {
        this.handler = Handler(Looper.getMainLooper()) {
            if (isRunning) {
                val lastTickStart: Long = SystemClock.elapsedRealtime()

                val millisLeft: Long = stopTimeInFuture
                    ?.let { it - SystemClock.elapsedRealtime()  }
                    ?: run { stopTimeInFuture = SystemClock.elapsedRealtime() + millisInFuture + x1; millisInFuture }

                if (millisLeft <= 0) finish()
                else {
                    countDownObserver?.tick?.invoke(millisLeft)

                    val lastTickDuration: Long = SystemClock.elapsedRealtime() - lastTickStart
                    var delay: Long

                    if (millisLeft < countDownInterval) { delay = millisLeft - lastTickDuration; if (delay < 0) delay = 0 }
                    else { delay = countDownInterval - lastTickDuration; while (delay < 0) delay += countDownInterval }

                    it.target.sendMessageDelayed(it.target.obtainMessage(MSG), delay)
                }
            }

            return@Handler false
        }
    }

    fun start(millisInFuture: Long, countDowInternal: Long, countDownObserver: CountDownObserver) {
        if (isRunning) abort()

        if (millisInFuture <= 0) finish()
        else {
            this.countDownInterval = countDowInternal
            this.countDownObserver = countDownObserver
            this.isRunning = true
            this.millisInFuture = millisInFuture
            this.stopTimeInFuture = null

            handler.sendMessage(handler.obtainMessage(MSG))
        }
    }

    fun finish() { exe(countDownObserver?.finish) }

    fun cancel() { exe(countDownObserver?.cancel) }

    fun abort() { exe(countDownObserver?.abort) }

    private fun exe(block: (() -> Unit)?) { this.isRunning = false; handler.removeMessages(MSG); block?.invoke(); countDownObserver = null }
}