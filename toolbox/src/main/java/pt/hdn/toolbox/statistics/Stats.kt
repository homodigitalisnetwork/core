package pt.hdn.toolbox.statistics

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.business.deputy.Deputy

@Parcelize
data class Stats(
	@Expose val efficiency: Float,
	@Expose val consistency: Float,
	@Expose val revenuePerTime: Float,
	@Expose val revenuePerDistance: Float,
	@Expose val revenuePerFuel: Float
) : Parcelable
