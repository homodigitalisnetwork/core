package pt.hdn.toolbox.statistics

import androidx.annotation.FloatRange

object Statistics {
    const val SIGMA_MULTIPLIER: Float = 2.58f
    const val SLOPE: Float = 10f

    fun tScore(value: Float?, moment: Moment, @FloatRange(from = 0.0) sigmaMultiplier: Float = SIGMA_MULTIPLIER, @FloatRange(from = 0.0) slope: Float = SLOPE, reverseScale: Boolean = false): Float {
        return (value
            ?.let {
                if (moment.std == 0f) { if ((it - moment.avg) == 0f) 0.5f else if (reverseScale) 0f else 1f }
                else (if (reverseScale) moment.avg - it else (it - moment.avg)) / (2f * sigmaMultiplier * moment.std) + 0.5f
            } ?: 0.5f) * slope
    }
}