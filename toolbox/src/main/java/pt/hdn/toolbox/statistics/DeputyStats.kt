package pt.hdn.toolbox.statistics

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.business.deputy.Deputy

data class DeputyStats(
	@Expose val deputy: Deputy,
	@Expose val stats: Stats?
)
