package pt.hdn.toolbox.statistics

import com.google.gson.annotations.Expose
import java.math.BigDecimal
import java.time.ZonedDateTime

data class Progression(
    @Expose val timestamp: ZonedDateTime,
    @Expose val revenue: BigDecimal,
    @Expose val revenuePerTime: Float,
    @Expose val revenuePerDistance: Float,
    @Expose val revenuePerFuel: Float
)
