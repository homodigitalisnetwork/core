package pt.hdn.toolbox.statistics

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize

@Parcelize
data class Moment(
	@Expose val avg: Float,
	@Expose val std: Float
): Parcelable
