package pt.hdn.toolbox.statistics

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize

@Parcelize
data class PopulationStats(
	@Expose val revenuePerTimeStats: Moment,
	@Expose val revenuePerDistanceStats: Moment,
	@Expose val revenuePerFuelStats: Moment,
	@Expose val efficiencyStats: Moment,
	@Expose val consistencyStats: Moment
) : Parcelable