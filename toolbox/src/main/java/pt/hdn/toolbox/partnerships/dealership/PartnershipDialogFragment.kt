package pt.hdn.toolbox.partnerships.dealership

import android.os.Bundle
import android.view.View
import android.view.View.*
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.annotations.Err as Err2
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.databinding.DialogDealershipBinding
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.partnerships.PartnershipsViewModel
import pt.hdn.toolbox.spotter.SpotterViewModel
import pt.hdn.toolbox.viewmodels.PartnershipDealershipViewModel

@AndroidEntryPoint
class PartnershipDialogFragment: BindingDialogFragment<DialogDealershipBinding>(DialogDealershipBinding::inflate, 40), OnClickListener {

	//region vars
	private val partnershipDealershipViewModel: PartnershipDealershipViewModel by navGraphViewModels(mapOf(R.id.partnership_graph to PartnershipsViewModel::class, R.id.spotter_graph to SpotterViewModel::class))
	//endregion vars

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		with(binding!!) {
			with(partnershipDealershipViewModel) {
				partnershipResponse
					.observeWith(viewLifecycleOwner) {
						success { toast(R.string.proposalSent); if (navGraph().id == R.id.partnership_graph) popBack() else popBackTo(R.id.spotterDialogFragment) }
						error { _, _ -> toast(R.string.failSendProposal); btnDealershipDialogSubmit.isEnabled = true }
					}

				resumesResponse
					.observeWith(viewLifecycleOwner) {
						success {
							if (resumes.isEmpty()) toast(if (request.renew) R.string.specialityNotAvailable else R.string.noOtherSpecialitiesAvailable)
							else navigateTo(PartnershipDialogFragmentDirections.actionPartnershipDialogFragmentToResumeDialogFragment())
						}
						error { _, _ -> toast(R.string.failExe) }
					}

				dataBus.observe<Contract?>(viewLifecycleOwner, Address.CONTRACT) { it?.let { enquire(it) } ?: run { btnDealershipDialogSubmit.isEnabled = true } }

				errorBus
					.observe(viewLifecycleOwner) {
						btnDealershipDialogSubmit.isEnabled = true

						when (it) {
							Err2.NONE -> navigateTo(signatureDialogFromDialog())
							Err2.NO_CHANGE -> toast(R.string.noChanges)
							else ->  null
						}
					}

				arrayFragmentStateAdapter(this@PartnershipDialogFragment, vpDealershipDialogContainer) {
					onPageSelected { index, lastIndex -> setViewVisibility(R.id.btn_dealershipDialogSubmit, if (index == lastIndex) Visibility.ENABLE else Visibility.GONE) }

					addAll(::TasksFragment, ::RecurrenceFragment, ::ClearanceFragment)
				}

				tabLayoutMediator(viewLifecycleOwner, layoutDealershipDialogDots, vpDealershipDialogContainer)

				btnDealershipDialogRemove.setOnClickListener(this@PartnershipDialogFragment)
				btnDealershipDialogAdd.setOnClickListener(this@PartnershipDialogFragment)
				btnDealershipDialogEdit.setOnClickListener(this@PartnershipDialogFragment)
				btnDealershipDialogSubmit.setOnClickListener(this@PartnershipDialogFragment)
			}
		}
	}

	override fun onClick(view: View?) {
		view?.id
			?.let {
				when (it) {
					R.id.btn_dealershipDialogSubmit -> { binding!!.btnDealershipDialogSubmit.isEnabled = false; partnershipDealershipViewModel.validate() }
					else -> dataBus.send(viewLifecycleOwner, Address.CLICK_1, it)
				}
			}
	}

	private fun setViewVisibility(id: Int, state: Int) {
		with(binding!!) {
			when (id) {
				R.id.bar_dealershipDialogLoading -> barDealershipDialogLoading.visibility =  if (state == Visibility.GONE) GONE else VISIBLE
				R.id.btn_dealershipDialogRemove -> with(btnDealershipDialogRemove) { if (state == Visibility.GONE) hide() else show() }
				R.id.btn_dealershipDialogAdd -> with(btnDealershipDialogAdd) { if (state == Visibility.GONE) hide() else show() }
				R.id.btn_dealershipDialogEdit -> with(btnDealershipDialogEdit) { if (state == Visibility.GONE) hide() else show() }
				R.id.btn_dealershipDialogSubmit -> with(btnDealershipDialogSubmit) { if (state == Visibility.GONE) hide() else show() }
			}
		}
	}
}