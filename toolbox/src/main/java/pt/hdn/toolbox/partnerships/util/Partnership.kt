package pt.hdn.toolbox.partnerships.util

import android.os.Parcelable
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.business.partner.Partner
import pt.hdn.toolbox.communications.message.Message
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.preferences.Preferences
import pt.hdn.toolbox.security.Clearance
import java.time.ZonedDateTime
import java.util.*

interface Partnership: Parcelable, Cloneable {
    var contracts: MutableList<Contract>
    var contract: Contract; get() = contracts.last(); set(value) { contracts.apply { set(lastIndex, value) } }
    var preferences: Preferences; get() = partner.preferences; set(value) { partner.preferences = value }
    var location: Boolean; get() = partner.location; set(value) { partner.location = value }
    var searchable: Boolean; get() = partner.searchable; set(value) { partner.searchable = value }
    val isBuyerSide: Boolean
    val partner: Partner
    val deputyName: String; get() = partner.deputyName
    val partyName: String; get() = partner.partyName
    val deputyUUID: UUID; get() = partner.deputyUUID
    val partyUUID: UUID; get() = partner.partyUUID
    val contractUUID: UUID; get() = contract.uuid
    val recurrence: Recurrence; get() = contract.recurrence
    val clearance: Clearance; get() = partner.clearance
    val start: ZonedDateTime; get() = recurrence.start
    val finish: ZonedDateTime?; get() = recurrence.finish
    val tasks: List<Task>; get() = contract.tasks
    val position: Position; get() = partner.defaultPosition
    val locale: String; get() = partner.locale
    val transactionUUID: UUID?; get() = preferences.transactionUUID

    public override fun clone(): Partnership

    @Err fun validate(partnership: Partnership?): Int { return contract.validate()/*@Err var err: Int = Err.NONE; contracts?.any { it.validate(contract).also { err = it } != Err.NONE }; return err*/ }

    fun message(@Action action: Int, uuid: UUID, partner: Partner? = null): Message
}