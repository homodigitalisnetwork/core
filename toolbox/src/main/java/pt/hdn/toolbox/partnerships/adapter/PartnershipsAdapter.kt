package pt.hdn.toolbox.partnerships.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.adapter.MapAdapter
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.resources.Resources
import java.util.*

class PartnershipsAdapter(
	partnerships: MutableMap<UUID, Partnership>,
//	partnerships: MutableList<Partnership>,
	resources: Resources
) : /*ListAdapter*/MapAdapter<PartnershipViewHolder, UUID, Partnership>(partnerships, resources) {
    override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): PartnershipViewHolder = PartnershipViewHolder(resources, parent)
}