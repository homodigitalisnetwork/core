package pt.hdn.toolbox.partnerships

import androidx.lifecycle.*
import androidx.navigation.NavDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.partnerships.adapter.PartnershipsAdapter
import pt.hdn.toolbox.annotations.Action.Companion.ACCEPT
import pt.hdn.toolbox.annotations.Action.Companion.ADD
import pt.hdn.toolbox.annotations.Action.Companion.ENQUIRE
import pt.hdn.toolbox.annotations.Action.Companion.GET
import pt.hdn.toolbox.annotations.Action.Companion.REJECT
import pt.hdn.toolbox.annotations.Action.Companion.REMOVE
import pt.hdn.toolbox.annotations.Action.Companion.SET
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.communications.bus.*
import pt.hdn.toolbox.communications.partnership.PartnershipsRequest
import pt.hdn.toolbox.communications.partnership.PartnershipsResponse
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.onEachWith
import pt.hdn.toolbox.misc.value
import pt.hdn.toolbox.partnerships.dealership.util.PartnershipDealership
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.viewmodels.PartnershipDealershipViewModel
import java.util.*
import javax.inject.Inject

@HiltViewModel
class PartnershipsViewModel @Inject constructor(
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : PartnershipDealershipViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    val partnershipsAdapter: SharedFlow<PartnershipsAdapter?>
    override val partnershipResponse: Flow<Result<PartnershipsResponse>>
    //endregion vars

    init {
        this.partnershipsAdapter = flowOf(PartnershipsRequest(command = Command.GET, partyUUID = session.partyUUID, isNotSpot = res.isNotSpot, action = GET))
            .flatMapLatest { repo.partnerships(it) }
            .map {
                var adapter: PartnershipsAdapter? = null

                it.success {
                    adapter = res.specialities.let { partnerships?.onEachWith { value.tasks.forEachWith { it[specialityType.uuid]?.let { specialityType = it } } } }?.let { PartnershipsAdapter(it, res) }
                }

                adapter
            }.shareIn(viewModelScope, Lazily, 1)

        dataBus
            .observeWith<Pair<UUID, @Action Int>>(viewModelScope, Address.PROPOSAL) {
                launch {
                    when (second) {
                        REJECT -> PartnershipsRequest(command = Command.REMOVE, partyUUID = session.partyUUID, partnershipUUID = first, action = second)
                        ENQUIRE -> PartnershipsRequest(command = Command.GET, partyUUID = session.partyUUID, partnershipUUID = first, action = second)
                        else -> null
                    }?.let { partnershipRequest.emit(it) }
                }
            }
    }

    fun reject() {
        launch {
            with(partnershipsAdapter.value()!!.firstSelected!!) {
                partnershipRequest.emit(
                    PartnershipsRequest(
                        command = Command.REMOVE,
                        deputyUUID = session.deputyUUID,
                        contractUUID = value.contractUUID,
                        partnershipUUID = key,
                        message = value.message(REJECT, key),
                        action = REMOVE
                    )
                )
            }
        }
    }

    fun accept(signedContract: Contract) {
        launch {
            with(partnershipsAdapter.value()!!.firstSelected!!) {
                partnershipRequest.emit(
                    PartnershipsRequest(
                        command = Command.SET,
                        deputyUUID = session.deputyUUID,
                        contractUUID = value.contractUUID,
                        contract = signedContract,
                        partnershipUUID = key,
                        message = value.message(ACCEPT, key),
                        action = SET
                    )
                )
            }
        }
    }

    fun partnershipDialog(): NavDirections {
        this.partnershipDealership = PartnershipDealership.fromOld(res, partnershipsAdapter.value()!!.firstSelected!!)

        return PartnershipsFragmentDirections.actionPartnershipsFragmentToPartnershipDialogFragment()
    }

    fun signatureDialogFromFragment(): NavDirections {
        return with(partnershipsAdapter.value()!!.firstSelected!!.value) {
            PartnershipsFragmentDirections.actionPartnershipsFragmentToSignatureDialogFragment(isBuyerSide, false, contract)
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------

    init {
        this.partnershipResponse = partnershipRequest
            .flatMapLatest { repo.partnerships(it) }
            .onEach {
                it.success {
                    with(partnershipsAdapter.value()!!) {
                        with(request) request@{
                            when (action) {
                                REMOVE -> removeSelected()
                                GET -> add(partnerships!!.entries.first())
                                ADD -> firstSelected!!.value.contracts.add(contract!!)
                                SET -> firstSelected!!.value.contract = contract!!
                            }
                        }
                    }
                }
            }
    }
}