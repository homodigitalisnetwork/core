package pt.hdn.toolbox.partnerships.adapter

import android.view.ViewGroup
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.annotations.Mode.Companion.NOT_EXPANDABLE
import pt.hdn.toolbox.resources.Resources

class ContractsAdapter(
	contracts: MutableList<Contract>,
	resources: Resources
) : ListAdapter<ContractViewHolder, Contract>(contracts, resources, NOT_EXPANDABLE) {
    override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): ContractViewHolder = ContractViewHolder(resources, parent)
}