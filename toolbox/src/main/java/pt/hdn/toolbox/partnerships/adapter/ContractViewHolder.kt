package pt.hdn.toolbox.partnerships.adapter

import android.view.View.*
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TableLayout
import android.widget.TableRow
import androidx.annotation.StringRes
import com.google.android.material.textview.MaterialTextView
import pt.hdn.contract.annotations.DaysType
import pt.hdn.contract.annotations.MonthType
import pt.hdn.contract.schemas.*
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.databinding.ViewholderContractBinding
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle.MEDIUM
import java.util.*

class ContractViewHolder(
    resources: Resources,
    parent: ViewGroup
) : HDNViewHolder<ViewholderContractBinding, Contract>(resources, parent, ViewholderContractBinding::inflate) {

    //region vars
    private val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(MEDIUM).withLocale(resources.locale)
    private val tinyMargin: Int = dimen(R.dimen.t_size)
    private val smallMargin: Int = dimen(R.dimen.s_size)
    //endregion vars

    override fun Contract.setViewHolder(isExpanded: Boolean) { addTasks(tasks); addRecurrence(recurrence) }

    private fun addTasks(tasks: List<Task>) {
        addDescription(R.string.tasks, 0)

        tasks
            .forEachWith {
                addParameter(R.string.speciality, res.specialities[specialityType.uuid]?.name ?: "Unknown")

                schemas
                    .forEachWith {
                        res.schemas[id]?.let { addParameter(R.string.schema, it) }

                        when (this) {
                            is FixSchema -> addParameter(R.string.value, fix)
                            is RateSchema -> addParameter(R.string.value, rate)
                            is CommissionSchema -> {
                                addParameter(R.string.cut, cut)
                                upperBound?.let { addParameter(R.string.upperBound, it) }
                                lowerBound?.let { addParameter(R.string.lowerBound, it) }
                            }
                            is ObjectiveSchema -> {
                                addParameter(R.string.bonus, bonus)
                                upperBound?.let { addParameter(R.string.upperBound, it) }
                                lowerBound?.let { addParameter(R.string.lowerBound, it) }
                            }
                            is ThresholdSchema -> {
                                addParameter(R.string.bonus, bonus)
                                addParameter(R.string.thresholdType, if (isAbove!!) R.string.aboveLimit else R.string.belowLimit)
                                addParameter(R.string.threshold, threshold)
                            }
                        }
                    }
            }
    }

    private fun addRecurrence(recurrence: Recurrence) {
        with(recurrence) {
            addDescription(R.string.recurrence)

            addParameter(R.string.start, start.format(dateTimeFormatter))

            finish?.let { addParameter(R.string.end, it.format(dateTimeFormatter)) }

            when (monthType) {
                MonthType.MONTHS -> buildString {
                    getMonths()?.forEach { res.months[it - 1]?.let { append(it).append(", ") } }; takeIf { it.length > 1 }?.delete(length - 2, length); addParameter(R.string.on, toString())
                }
                MonthType.PERIOD -> addParameter(R.string.on, res.monthsPeriod[monthsPeriod!!])
            }

            when (daysType) {
                DaysType.DAYS -> buildString {
                    getDays()?.forEach { append(res.daysOrdinal[it]).append(", ") }; takeIf { it.length > 1 }?.delete(length - 2, length); addParameter(R.string.on, toString())
                }
                DaysType.DOW -> buildString {
                    getDow()?.forEach { res.daysOfTheWeek[it]?.let { append(it).append(", ") } }; takeIf { it.length > 1 }?.delete(length - 2, length); addParameter(R.string.on, toString())
                }
                DaysType.PERIOD -> res.daysOfTheWeekPeriod[daysPeriod]?.let { addParameter(R.string.on, it) }
                else -> null
            }
        }
    }

    private fun addDescription(@StringRes idRes: Int, topMargin: Int = tinyMargin) {
        with(MaterialTextView(context)) {
            layoutParams = TableLayout.LayoutParams().apply { setMargins(tinyMargin, topMargin, smallMargin, tinyMargin) }
            text = string(idRes)

            binding.tbContractContainer.addView(this)
        }
    }

    private fun addParameter (@StringRes field: Int, value: BigDecimal?) { addParameter(field, value?.toString()) }

    private fun addParameter (@StringRes field: Int, @StringRes value: Int) { addParameter(field, string(value)) }

    private fun addParameter (@StringRes field: Int, value: String?) {
        with(TableRow(context)) {
            layoutParams = TableLayout.LayoutParams().apply { setMargins(smallMargin, tinyMargin, tinyMargin, 0) }

            with(MaterialTextView(context)) {
                layoutParams = TableRow.LayoutParams().apply { setMargins(0, 0, tinyMargin, 0) }
                textAlignment = TEXT_ALIGNMENT_TEXT_END
                text = string(field)

                addView(this)
            }

            with(MaterialTextView(context)) {
                layoutParams = TableRow.LayoutParams(0, WRAP_CONTENT, 1f).apply { setMargins(tinyMargin, 0, 0, 0) }
                textAlignment = TEXT_ALIGNMENT_TEXT_START
                text = value

                addView(this)
            }

            binding.tbContractContainer.addView(this)
        }
    }
}