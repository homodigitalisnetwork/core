package pt.hdn.toolbox.partnerships.dealership.resume.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.databinding.ViewholderScoreBinding
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.util.Score

class ScoreViewHolder(
    res: Resources,
    parent: ViewGroup
): BindingViewHolder<ViewholderScoreBinding>(res, parent, ViewholderScoreBinding::inflate) {
    fun Score.setViewHolder() { binding.scoreContainer.apply { res = this@ScoreViewHolder.res; data = this@setViewHolder } }
}