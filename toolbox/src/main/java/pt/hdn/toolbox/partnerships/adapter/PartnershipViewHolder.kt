package pt.hdn.toolbox.partnerships.adapter

import android.view.View.*
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.HORIZONTAL
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.databinding.ViewholderPartnershipBinding
import pt.hdn.toolbox.resources.Resources
import java.util.*

class PartnershipViewHolder(
    resources: Resources,
    parent: ViewGroup,
) : HDNViewHolder<ViewholderPartnershipBinding, /*Partnership*/Map.Entry<UUID, Partnership>>(resources, parent, ViewholderPartnershipBinding::inflate) {
    init {
        with(binding) {
            lblPartnershipContract.text = string(R.string.contract)

            recPartnershipsContracts.apply { setHasFixedSize(true); layoutManager = LinearLayoutManager(context, HORIZONTAL, false) }
        }
    }

    override fun /*Partnership*/Map.Entry<UUID, Partnership>.setViewHolder(isExpanded: Boolean) {
        with(binding) {
            with(value) {
                lblPartnershipTitle.text = if (value.isBuyerSide!!) deputyName else partyName

                imgPartnershipWarning.apply { visibility = if (!contract.hasSellerSigned) VISIBLE else GONE; setImageDrawable(drawable(R.drawable.ic_new)) }

                lblPartnershipContract.text = string(R.string.contract)

                buildString {
                    contracts!!.first().tasks.forEach { task -> append(res.specialities[task.specialityType.uuid]?.name ?: "Unknown").append(", ") }

                    takeIf { it.length > 1 }?.delete(length - 2, length)

                    lblPartnershipSubtitle.text = this@buildString.toString()
                }

                layoutPartnershipBody.visibility = if (isExpanded) VISIBLE else GONE

                if (isExpanded) recPartnershipsContracts.adapter = ContractsAdapter(contracts!!, res)
            }
        }
    }
}