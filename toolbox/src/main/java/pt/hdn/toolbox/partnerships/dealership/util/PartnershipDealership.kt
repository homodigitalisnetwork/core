package pt.hdn.toolbox.partnerships.dealership.util

import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.toolbox.annotations.Places
import pt.hdn.toolbox.business.partner.Partner
import pt.hdn.toolbox.communications.bus.ErrorBus
import pt.hdn.toolbox.misc.Dealership
import pt.hdn.toolbox.misc.contains
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.partnerships.dealership.adapter.TasksAdapter
import pt.hdn.toolbox.partnerships.dealership.resume.adapter.ResumeAdapter
import pt.hdn.toolbox.partnerships.dealership.resume.adapter.ResumesAdapter
import pt.hdn.toolbox.partnerships.dealership.resume.util.Resume
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.security.Clearance
import java.time.ZonedDateTime
import java.util.*

/**
 * A [Dealership] type class where the creation, editing, validation and reduction of a [Partnership] object is handle
 */
class PartnershipDealership private constructor(
	private val res: Resources,
	private val ref: Map.Entry<UUID, Partnership>? = null,
	private val clone: Pair<UUID, Partnership>
) : Dealership() {

	//region vars
	lateinit var resumeAdapter: ResumeAdapter
	lateinit var resumesAdapter: ResumesAdapter
	val partnershipUUID: UUID; get() = clone.first
	val partnership: Partnership; get() = clone.second
	val tasksAdapter: TasksAdapter = TasksAdapter(partnership.tasks, res)
	var renew: Boolean = false; private set
	val isNewBuild: Boolean = ref == null
	val partner: Partner; get() = partnership.partner
//	val deputyUUID: String; get() = partnership.deputyUUID
	val recurrence: Recurrence; get() = partnership.recurrence
	val clearance: Clearance; get() = partnership.clearance
	val isStartAfterFinish: Boolean; get() = with(partnership.recurrence) { finish?.let { start.isAfter(it) } == true }
	val startOrNow: ZonedDateTime; get() = partnership.start.coerceAtLeast(ZonedDateTime.now())
	val isBuyerSide: Boolean; get() = partnership.isBuyerSide
	var contract: Contract; get() = partnership.contract; set(value) { partnership.contract = value }
	//endregion vars

	companion object {
		fun fromOld(res: Resources, ref: Map.Entry<UUID, Partnership>): PartnershipDealership = PartnershipDealership(res, ref, with(ref) { key to value.clone() })

		fun fromNew(res: Resources, partnership: Partnership): PartnershipDealership = PartnershipDealership(res, null, UUID.randomUUID() to partnership)
	}

	fun addTasks() { if (renew) tasksAdapter.replaceSelected(resumeAdapter.resume.getTask()) else tasksAdapter.addAll(resumesAdapter.selection.map { it.getTask() }) }

	fun setMonth(isChecked: Boolean, value: Int) { with(recurrence) { (if (isChecked) ::addMonth else ::removeMonth).invoke(value) } }

    fun setMonthPeriod(value: Int) { recurrence.setMonthPeriod(value) }

    fun setDays(isChecked: Boolean, value: Int) { with(recurrence) { (if (isChecked) ::addDay else ::removeDay).invoke(value) } }

    fun setDow(isChecked: Boolean, value: Int) { with(recurrence) { (if (isChecked) ::addDow else ::removeDow).invoke(value) } }

    fun setDayPeriod(value: Int) { recurrence.setDaysPeriod(value) }

	fun setStart(value: ZonedDateTime) { with(recurrence) { start = value.withZoneSameInstant(start.zone); if (isStartAfterFinish) finish = start} }

	fun setFinish(value: ZonedDateTime?) { with(recurrence) { finish = value?.withZoneSameInstant(start.zone) } }

	fun setAccess(@Places place: Int, isChecked: Boolean) { clearance.setPlace(place, isChecked) }

	fun setResumeAdapter(defaultResumes: MutableList<Resume>, renew: Boolean) {
		this.renew = renew

		if (renew) this.resumeAdapter = ResumeAdapter(defaultResumes.first(), res)
		else this.resumesAdapter = ResumesAdapter(defaultResumes.filterNotTo(mutableListOf()) { tasksAdapter.ref.contains { specialityType.uuid == it.speciality.typeUUID!!} }, res)
	}

	suspend fun validate() { errorBus.send(value = partnership.validate(ref?.value)) }
}