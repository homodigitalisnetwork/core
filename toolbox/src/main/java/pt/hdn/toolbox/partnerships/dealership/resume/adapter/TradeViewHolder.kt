package pt.hdn.toolbox.partnerships.dealership.resume.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.databinding.ViewholderTradeBinding
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.dealership.util.SchemaTrade

class TradeViewHolder(
    res: Resources,
    parent: ViewGroup
): BindingViewHolder<ViewholderTradeBinding>(res, parent, ViewholderTradeBinding::inflate) {
    fun SchemaTrade.setViewHolder() { binding.tradeContainer.apply { res = this@TradeViewHolder.res; data = this@setViewHolder } }
}