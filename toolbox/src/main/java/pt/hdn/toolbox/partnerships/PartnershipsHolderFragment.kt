package pt.hdn.toolbox.partnerships

import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentPartnershipsHolderBinding

@AndroidEntryPoint
class PartnershipsHolderFragment: BindingFragment<FragmentPartnershipsHolderBinding>(FragmentPartnershipsHolderBinding::inflate)