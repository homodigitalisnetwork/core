package pt.hdn.toolbox.partnerships.dealership.resume.adapter

import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.HORIZONTAL
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.databinding.ViewholderResumesBinding
import pt.hdn.toolbox.partnerships.dealership.resume.util.Resume
import pt.hdn.toolbox.resources.Resources

class ResumeViewHolder(
    res: Resources,
    parent: ViewGroup
): HDNViewHolder<ViewholderResumesBinding, Resume>(res, parent, ViewholderResumesBinding::inflate) {

    init { binding.recResumesContainer.apply { layoutManager = LinearLayoutManager(context, HORIZONTAL, false); setHasFixedSize(true) } }

    override fun Resume.setViewHolder(isExpanded: Boolean) {
        with(binding) {
            lblResumesTitle.text = speciality.type!!.name

            recResumesContainer.apply { if (isExpanded) { visibility = VISIBLE; adapter = ResumeAdapter(this@setViewHolder, res) } else { visibility = GONE; adapter = null } }
        }
    }

    override fun onViewDestroyed() { super.onViewDestroyed(); binding.recResumesContainer.adapter = null }
}