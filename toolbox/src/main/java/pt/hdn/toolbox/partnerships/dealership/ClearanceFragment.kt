package pt.hdn.toolbox.partnerships.dealership

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.CompoundButton
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.LinearLayout
import androidx.annotation.StringRes
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Places
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentClearanceBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.partnerships.PartnershipsViewModel
import pt.hdn.toolbox.security.Clearance
import pt.hdn.toolbox.spotter.SpotterViewModel
import pt.hdn.toolbox.viewmodels.PartnershipDealershipViewModel

@AndroidEntryPoint
class ClearanceFragment: BindingFragment<FragmentClearanceBinding>(FragmentClearanceBinding::inflate), OnCheckedChangeListener {

	//region vars
	private val partnershipDealershipViewModel: PartnershipDealershipViewModel by navGraphViewModels(mapOf(R.id.partnership_graph to PartnershipsViewModel::class, R.id.spotter_graph to SpotterViewModel::class))
	private var tinyMargin: Int = -1
	private var smallMargin: Int = -1
	private lateinit var places: Array<String>
	//endregion vars

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		with(partnershipDealershipViewModel.partnershipDealership.clearance) {
			with(Clearance) {
				this@ClearanceFragment.tinyMargin = dimen(R.dimen.t_size)
				this@ClearanceFragment.smallMargin = dimen(R.dimen.s_size)
				this@ClearanceFragment.places = stringArray(R.array.clearance_places)

				addClearanceGroupViews(R.string.main, MAIN, main)
				addClearanceGroupViews(R.string.menu, MENU, menu)
				addClearanceGroupViews(R.string.definitions, DEFINITIONS, definitions)

				setViewVisibility()
			}
		}
	}

	override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) { partnershipDealershipViewModel.setAccess(buttonView?.tag as Int, isChecked) }

	private fun addClearanceGroupViews(@StringRes id: Int, ref: List<Int>, accesses: List<Int>) {
		addDescription(tinyMargin, smallMargin, id)

		if (accesses.isEmpty()) { ref.forEach { addSwitch(smallMargin, tinyMargin, it, false) } }
		else {
			var pointer: Int = 0

			accesses
				.iterator()
				.run { pointer = next(); ref.forEach { addSwitch(smallMargin, tinyMargin, it, if (it == pointer) { if (hasNext()) pointer = next(); true } else false) } }
		}
	}

	private fun addDescription(startMargin: Int, otherMargins: Int, @StringRes id: Int) {
		with(MaterialTextView(requireContext())) {
			layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply { setMargins(startMargin, otherMargins, otherMargins, otherMargins) }
			text = string(id)

			binding!!.layoutClearanceAccesses.addView(this)
		}
	}

	private fun addSwitch(sideMargin: Int, otherMargins: Int, @Places place: Int, isChecked: Boolean) {
		with(SwitchMaterial(requireContext())) {
			layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, 0).apply { setMargins(sideMargin, otherMargins, sideMargin, otherMargins) }
			id = View.generateViewId()
			tag = place
			text = places[place]
			setChecked(isChecked)
			setOnCheckedChangeListener(this@ClearanceFragment)

			binding!!.layoutClearanceAccesses.addView(this)
		}
	}

	private fun setViewVisibility() {
		dataBus.send(
			owner = viewLifecycleOwner,
			address = Address.VIEW_1,
			values = arrayOf(
				R.id.btn_dealershipDialogEdit to Visibility.GONE,
				R.id.btn_dealershipDialogRemove to Visibility.GONE,
				R.id.btn_dealershipDialogAdd to Visibility.GONE
			)
		)
	}
}