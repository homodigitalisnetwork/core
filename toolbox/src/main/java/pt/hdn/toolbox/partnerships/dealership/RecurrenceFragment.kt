package pt.hdn.toolbox.partnerships.dealership

import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.CompoundButton.*
import android.widget.DatePicker
import android.widget.DatePicker.OnDateChangedListener
import androidx.core.view.children
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.annotations.Err as Err2
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentRecurrenceBinding
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.partnerships.PartnershipsViewModel
import pt.hdn.toolbox.spotter.SpotterViewModel
import pt.hdn.toolbox.viewmodels.PartnershipDealershipViewModel
import java.time.ZonedDateTime.now

@AndroidEntryPoint
class RecurrenceFragment: BindingFragment<FragmentRecurrenceBinding>(FragmentRecurrenceBinding::inflate), OnCheckedChangeListener, OnDateChangedListener {

	//region vars
	private val partnershipDealershipViewModel: PartnershipDealershipViewModel by navGraphViewModels(mapOf(R.id.partnership_graph to PartnershipsViewModel::class, R.id.spotter_graph to SpotterViewModel::class))
	//endregion vars

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		with(binding!!) {
			with(partnershipDealershipViewModel) {
				with(recurrence) {
					populateChipGroup(xigRecurrenceMonths, res.months, getMonths())
					populateChipGroup(xigRecurrenceMonthsPeriod, res.monthsPeriod, monthsPeriod)
					populateChipGroup(xigRecurrenceDays, res.daysOrdinal, getDays())
					populateChipGroup(xigRecurrenceDow, res.daysOfTheWeek, getDow())
					populateChipGroup(xigRecurrenceDaysPeriod, res.daysOfTheWeekPeriod, daysPeriod)

					dpRecurrenceStart
						.apply {
							with(start) { updateDate(year, monthValue - 1, dayOfMonth) }

							if (partnershipDealership.isNewBuild) { minDate = start.toInstant().toEpochMilli(); setOnDateChangedListener(this@RecurrenceFragment) }
							else isEnabled = false
						}

					ckboxRecurrenceFinish.apply { text = string(R.string.finishing); setOnCheckedChangeListener(this@RecurrenceFragment) }

					finish?.apply { ckboxRecurrenceFinish.isChecked = true; dpRecurrenceFinish.updateDate(year, monthValue - 1, dayOfMonth) }

					dpRecurrenceFinish.apply { minDate = start.coerceAtLeast(now()).toInstant().toEpochMilli(); setOnDateChangedListener(this@RecurrenceFragment) }

					lblRecurrenceMonth.text = string(R.string.on)
					lblRecurrenceDay.text = string(R.string.on)
					lblRecurrenceStart.text = string(R.string.starting)

					errorBus
						.observe(viewLifecycleOwner) {
							when (it) {
								Err2.DAYS, Err2.DOW, Err2.DAYS_PERIOD -> handleError(lblRecurrenceDay)
								Err2.MONTHS, Err2.MONTHS_PERIOD -> handleError(lblRecurrenceMonth)
								Err2.FINISH -> handleError(dpRecurrenceFinish)
							}
						}

					setViewVisibility()
				}
			}
		}
	}

	override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
		buttonView
			?.let {
				with(binding!!) {
					with(partnershipDealershipViewModel.partnershipDealership) {
						if (it.id == R.id.ckbox_recurrenceFinish) {
							with(dpRecurrenceFinish) {
								if (isChecked) {
									visibility = VISIBLE

									startOrNow
										.let {
											setOnDateChangedListener(null)
											updateDate(it.year, it.monthValue - 1, it.dayOfMonth)
											minDate = partnershipDealershipViewModel.setAndGetFinish(it.year, it.monthValue, it.dayOfMonth).toInstant().toEpochMilli()
											setOnDateChangedListener(this@RecurrenceFragment)
										}
								}
								else { visibility = GONE; setFinish(null) }
							}
						} else {
							with(partnershipDealershipViewModel) {
								(it.tag as Int)
									.let { me ->
										when ((it.parent as ChipGroup).id) {
											R.id.xig_recurrenceMonths -> { setMonth(isChecked, me); clearChecks(xigRecurrenceMonthsPeriod) }
											R.id.xig_recurrenceMonthsPeriod -> { setMonthPeriod(me); clearChecks(xigRecurrenceMonths) }
											R.id.xig_recurrenceDays -> { setDays(isChecked, me); clearChecks(xigRecurrenceDaysPeriod); clearChecks(xigRecurrenceDow) }
											R.id.xig_recurrenceDow -> { setDow(isChecked, me); clearChecks(xigRecurrenceDays); clearChecks(xigRecurrenceDaysPeriod) }
											R.id.xig_recurrenceDaysPeriod -> { setDayPeriod(me); clearChecks(xigRecurrenceDays); clearChecks(xigRecurrenceDow) }
										}
									}

								it.isChecked = isChecked
							}
						}
					}
				}
			}
	}

	override fun onDateChanged(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
		view
			?.id
			?.let {
				with(binding!!) {
					with(partnershipDealershipViewModel) {
						when {
//							it == R.id.dp_recurrenceStart -> with(dpRecurrenceStart) {
//								setOnDateChangedListener(null)
//								minDate = setAndGetStart(year, monthOfYear + 1, dayOfMonth).coerceAtLeast(now()).toInstant().toEpochMilli()
//								if (partnershipDealership.isStartAfterFinish) toast(R.string.resetFinishing)
//								setOnDateChangedListener(this@RecurrenceFragment)
//							}
							it == R.id.dp_recurrenceStart -> with(dpRecurrenceFinish) {
								setOnDateChangedListener(null)
								minDate = setAndGetStart(year, monthOfYear - 1, dayOfMonth).coerceAtLeast(now()).toInstant().toEpochMilli()
								if (partnershipDealership.isStartAfterFinish) toast(R.string.resetFinishing)
								setOnDateChangedListener(this@RecurrenceFragment)
							}
//							it == R.id.dp_recurrenceFinish && partnershipDealership.isNewBuild -> with(dpRecurrenceStart) {
//								setOnDateChangedListener(null)
//								maxDate = setAndGetFinish(year, monthOfYear - 1, dayOfMonth).toInstant().toEpochMilli()
//								setOnDateChangedListener(this@RecurrenceFragment)
//							}
							it == R.id.dp_recurrenceFinish -> with(dpRecurrenceStart) {
								setOnDateChangedListener(null)
								maxDate = setAndGetFinish(year, monthOfYear - 1, dayOfMonth).toInstant().toEpochMilli()
								setOnDateChangedListener(this@RecurrenceFragment)
							}
							else -> null
						}
					}
				}
			}
	}

	private fun setViewVisibility() {
		dataBus.send(
			owner = viewLifecycleOwner,
			address = Address.VIEW_1,
			values = arrayOf(
				R.id.btn_dealershipDialogEdit to Visibility.GONE,
				R.id.btn_dealershipDialogRemove to Visibility.GONE,
				R.id.btn_dealershipDialogAdd to Visibility.GONE
			)
		)
	}

	private fun populateChipGroup(chipGroup: ChipGroup, types: Map<Int, String>, valuesChecked: List<Int>?) {
		valuesChecked
			?.iterator()
			?.run {
				var pointer: Int = next()

				types
					.forEachWith {
						Chip(requireContext())
							.apply {
								id = View.generateViewId()
								text = value
								tag = key
								isCheckable = true
								isChecked = (if (key == pointer) { if (hasNext()) pointer = next(); true } else false)
								setOnCheckedChangeListener(this@RecurrenceFragment)

								chipGroup.addView(this)
							}
					}
			} ?: populateChipGroup(chipGroup, types)
	}

	private fun populateChipGroup(chipGroup: ChipGroup, types: Map<Int, String>, valueChecked: Int? = null) {
		types
			.forEachWith {
				Chip(requireContext())
					.apply {
						id = View.generateViewId()
						text = value
						tag = key
						isCheckable = true
						isChecked = valueChecked?.let { it == key } == true
						setOnCheckedChangeListener(this@RecurrenceFragment)

						chipGroup.addView(this)
					}
			}
	}

	private fun clearChecks(chipGroup: ChipGroup) {
		chipGroup
			.children
			.forEachWith { this as Chip; setOnCheckedChangeListener(null); isChecked = false; setOnCheckedChangeListener(this@RecurrenceFragment) }
	}
}