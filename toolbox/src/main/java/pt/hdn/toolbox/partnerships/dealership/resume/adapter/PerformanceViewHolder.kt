package pt.hdn.toolbox.partnerships.dealership.resume.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.databinding.ViewholderPerformanceBinding
import pt.hdn.toolbox.partnerships.dealership.resume.util.Performance
import pt.hdn.toolbox.resources.Resources

class PerformanceViewHolder(
    res: Resources,
    parent: ViewGroup
): BindingViewHolder<ViewholderPerformanceBinding>(res, parent, ViewholderPerformanceBinding::inflate) {
    fun Performance.setViewHolder() { binding.perfPerformanceRadar.apply { res = this@PerformanceViewHolder.res; data = this@setViewHolder } }
}