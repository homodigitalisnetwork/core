package pt.hdn.toolbox.partnerships.dealership.resume.adapter

import android.view.ViewGroup
import pt.hdn.contract.annotations.Market
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.databinding.ViewholderMarketBinding
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.specialities.util.Speciality
import java.math.BigDecimal.ONE

class MarketViewHolder(
    @Market val market: Int,
    res: Resources,
    parent: ViewGroup
) : BindingViewHolder<ViewholderMarketBinding>(res, parent, ViewholderMarketBinding::inflate) {
    fun Speciality.setViewHolder() {
        with(binding) {
            when(market) {
                PERSON -> { R.drawable.ic_person to R.string.dependentMarket }
                SERVICE -> { R.drawable.ic_work to R.string.independentMarket }
                else -> null
            } ?.run { lblMarketTitle.apply { setCompoundDrawablesWithIntrinsicBounds(first, 0,0,0); text = string(second) } }

            when(market) {
//                PERSON -> {
//                    specialityPersonSchemas
//                        .map {
//                            
//                        }
//                }
                SERVICE -> {
                    serviceSchemas!!
                        .map {
                            it.clone()
                                .apply {
                                    (ONE + this@setViewHolder.taxValue)
                                        .let {
                                            when (this) {
                                                is FixSchema -> fix =  fix!! * it
                                                is RateSchema -> rate = rate!! * it
                                                is CommissionSchema -> cut = cut!! * it
                                                is ObjectiveSchema -> bonus = bonus!! * it
                                                is ThresholdSchema -> bonus = bonus!! * it
                                            }
                                        }
                                }
                        }
                }
                else -> null
            }?.let { schemasMarketSchemas.apply { resources = res; data = it } }
        }
    }
}