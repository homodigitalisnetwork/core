package pt.hdn.toolbox.partnerships.dealership.resume.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.annotations.Mode.Companion.MULTIPLE_SELECTION
import pt.hdn.toolbox.partnerships.dealership.resume.util.Resume
import pt.hdn.toolbox.resources.Resources

class ResumesAdapter(
    ref: MutableList<Resume>,
    res: Resources
): ListAdapter<ResumeViewHolder, Resume>(ref, res, MULTIPLE_SELECTION) {
    override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): ResumeViewHolder = ResumeViewHolder(res, parent)
}