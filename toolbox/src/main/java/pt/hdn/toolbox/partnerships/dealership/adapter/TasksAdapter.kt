package pt.hdn.toolbox.partnerships.dealership.adapter

import android.view.ViewGroup
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.resources.Resources

class TasksAdapter(
    ref: List<Task>?,
    resources: Resources
) : ListAdapter<TaskViewHolder, Task>(ref?.mapTo(mutableListOf()) { it.copy() } ?: mutableListOf(), resources) {
    override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): TaskViewHolder = TaskViewHolder(resources, parent)
}