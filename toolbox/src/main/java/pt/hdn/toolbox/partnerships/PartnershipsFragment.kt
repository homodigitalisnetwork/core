package pt.hdn.toolbox.partnerships

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Lifecycle.State.RESUMED
import androidx.lifecycle.Lifecycle.State.STARTED
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.databinding.FragmentViewerBinding
import pt.hdn.toolbox.misc.*
import java.util.*

@AndroidEntryPoint
class PartnershipsFragment : BindingFragment<FragmentViewerBinding>(FragmentViewerBinding::inflate) {

    //region vars
    private val partnershipsViewModel: PartnershipsViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(partnershipsViewModel) {
                lblViewerDisplay.apply { visibility = VISIBLE; setCompoundTopDrawable(R.drawable.ic_partnership) }

                dataBus
                    .using(viewLifecycleOwner) {
                        observe<Contract?>(Address.CONTRACT) { it?.let { accept(it) } }
                        observe<Int>(Address.CLICK_1) {
                            when (it) {
                                R.id.btn_definitionsRemove -> {
                                    MaterialAlertDialogBuilder(requireActivity())
                                        .setTitle(string(R.string.confirmation))
                                        .setMessage(string(R.string.areYouSure))
                                        .setPositiveButton(string(R.string.yes)) { _, _ -> reject() }
                                        .setNegativeButton(string(R.string.no), null)
                                        .show()
                                }
                                R.id.btn_definitionsEdit -> navigateTo(partnershipDialog())
                                R.id.btn_definitionsCheck -> navigateTo(signatureDialogFromFragment())
                            }
                        }
                    }

                partnershipResponse.observe(viewLifecycleOwner) { it.error { _, _ -> toast(R.string.failExe) }; setViewVisibility(false) }

                partnershipsAdapter
                    .observe(viewLifecycleOwner) {
                        barViewerLoading.visibility = GONE

                        it
                            ?.apply {
                                recViewerContainer.apply { setHasFixedSize(true); adapter = it }

                                size.observe(this@observe) { lblViewerDisplay.apply { text = string(R.string.noPartnerships); visibility = if (it == 0) VISIBLE else GONE } }

                                highlighted.observe(this@observe) { (_, _) -> setViewVisibility(false) }

                                viewLifecycleOwner.syncOnResumedObserver { setViewVisibility(true) }
                            }
                            ?: run { lblViewerDisplay.apply { visibility = VISIBLE; setCompoundTopDrawable(R.drawable.ic_sad_face); text = string(R.string.somethingWrong) } }
                    }

                barViewerLoading.visibility = VISIBLE
            }
        }
    }

    private fun setViewVisibility(reset: Boolean) {
        if (reset) {
            dataBus.send(
                owner = viewLifecycleOwner,
                address = Address.VIEW_1,
                values = arrayOf(
                    R.id.btn_definitionsEdit to Visibility.GONE,
                    R.id.btn_definitionsRemove to Visibility.GONE,
                    R.id.btn_definitionsAdd to Visibility.GONE,
                    R.id.btn_definitionsCheck to Visibility.GONE
                )
            )
        }

        dataBus.send(
            owner = viewLifecycleOwner,
            address = Address.VIEW_1,
            values = partnershipsViewModel
                .partnershipsAdapter
                .value()
                ?.firstSelected
                ?.value
                ?.run {
                    arrayOf(
                        R.id.btn_definitionsRemove to Visibility.ENABLE,
                        if (isBuyerSide) R.id.btn_definitionsEdit to Visibility.ENABLE else R.id.btn_definitionsCheck to if (contract.hasSellerSigned) Visibility.GONE else Visibility.ENABLE
                    )
                }
                ?: run {
                    arrayOf(
                        R.id.btn_definitionsCheck to Visibility.GONE,
                        R.id.btn_definitionsRemove to Visibility.GONE,
                        R.id.btn_definitionsEdit to Visibility.GONE
                    )
                }
        )
    }
}