package pt.hdn.toolbox.partnerships.util

import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.contract.annotations.DaysType.Companion.DAYS
import pt.hdn.contract.annotations.MonthType.Companion.PERIOD
import pt.hdn.contract.annotations.MonthsPeriod.Companion.MONTHS_ALL
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.annotations.Field.Companion.ACTION
import pt.hdn.toolbox.annotations.Field.Companion.BUYER
import pt.hdn.toolbox.annotations.Field.Companion.REF
import pt.hdn.toolbox.annotations.Field.Companion.SELLER
import pt.hdn.toolbox.annotations.Field.Companion.TYPE
import pt.hdn.toolbox.annotations.Field.Companion.UUID
import pt.hdn.toolbox.business.partner.Partner
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.communications.message.Message
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.security.Clearance
import java.time.ZonedDateTime
import java.util.*

@Parcelize
data class DefaultPartnership(
    @Expose override var contracts: MutableList<Contract>,
    @Expose override val isBuyerSide: Boolean,
    @Expose override val partner: Partner
) : Partnership {

    companion object {
        fun from(res: Resources, seller: Seller, buyer: Buyer): DefaultPartnership {
            return seller
                .specialityPersonSchemas
                .run {
//                    forEachWith {
//                        when (this) {
//                            is RateSchema -> source = SourceType.TIME
//                            is CommissionSchema -> source = SourceType.INDIVIDUAL_REVENUE
//                            is ObjectiveSchema -> source = SourceType.INDIVIDUAL_REVENUE
//                            is ThresholdSchema -> { source = SourceType.DISTANCE; threshold = Value.NEXT_RADIUS.toBigDecimal(); isAbove = true }
//                        }
//                    }

                    DefaultPartnership(
                        isBuyerSide = true, /*foi adicionado por causa do caminho de partnership*/
                        partner = seller.apply { deputy!!.clearance = Clearance(false) },
                        contracts = mutableListOf(
                            Contract(
                                tasks = mutableListOf(Task(res.specialities[seller.specialityUUID]!!, this)),
                                recurrence = Recurrence(
                                    start = ZonedDateTime.now().plusDays(1L),
                                    monthType = PERIOD,
                                    monthsPeriod = MONTHS_ALL,
                                    daysType = DAYS,
                                    days = mutableListOf(28)
                                ),
                                buyerId = buyer.partyId,
                                buyerDeputyId = buyer.deputyId,
                                sellerId = seller.partyId,
                                sellerDeputyId = seller.deputyId
                            )
                        )
                    )
                }
        }
    }

    override fun clone(): Partnership = copy(contracts = contracts.mapTo(mutableListOf()) { it.clone() })

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Partnership) return false

        if (contracts.toSet() != other.contracts.toSet()) return false
        if (clearance != other.clearance) return false

        return true
    }

    override fun message(@Action action: Int, uuid: UUID, partner: Partner?): Message {
        val ignoreExtraField: Boolean = true

        return Message(
            receiverUUID = partyUUID,
            payload = mutableMapOf<String, Any>()
                .apply {
                    this[TYPE] = false
                    this[ACTION] = action
                    partner?.let { this[REF] = mapOf(BUYER to it.toMap(ignoreExtraField), SELLER to this@DefaultPartnership.partner.toMap(ignoreExtraField)) }
                    this[UUID] = uuid.toString()
                }
            )
    }

    override fun toString(): String = partyName
}