package pt.hdn.toolbox.partnerships.dealership.resume.util

import com.google.gson.annotations.Expose
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.annotations.Resume
import pt.hdn.toolbox.annotations.Resume.Companion.PERFORMANCE
import pt.hdn.toolbox.annotations.Resume.Companion.SCHEMAS
import pt.hdn.toolbox.annotations.Resume.Companion.SCORE
import pt.hdn.toolbox.annotations.Resume.Companion.TRADE
import pt.hdn.toolbox.misc.toInt
import pt.hdn.toolbox.specialities.dealership.util.Price
import pt.hdn.toolbox.specialities.dealership.util.SchemaTrade
import pt.hdn.toolbox.specialities.util.Speciality

data class Resume(
    @Expose val speciality: Speciality,
    @Expose val performance: Performance,
    @Expose private val price: Price
) {

    @Resume fun getViewId(position: Int): Int {
        return when {
            position == 0 -> SCORE
            position == 1 -> PERFORMANCE
            position == 2 && speciality.personSchemas != null -> SCHEMAS
            else -> TRADE
        }
    }

    fun getSize(): Int = 3 + (speciality.personSchemas != null).toInt()

    fun getSchemaTrade(position: Int): SchemaTrade = price[position - (2 + (speciality.personSchemas != null).toInt())]

    fun getTask(): Task = speciality.toTask(PERSON)
}