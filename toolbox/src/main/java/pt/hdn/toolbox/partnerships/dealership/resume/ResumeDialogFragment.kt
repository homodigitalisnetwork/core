package pt.hdn.toolbox.partnerships.dealership.resume

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.View.VISIBLE
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.databinding.DialogResumeBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.partnerships.PartnershipsViewModel
import pt.hdn.toolbox.spotter.SpotterViewModel
import pt.hdn.toolbox.viewmodels.PartnershipDealershipViewModel

@AndroidEntryPoint
class ResumeDialogFragment: BindingDialogFragment<DialogResumeBinding>(DialogResumeBinding::inflate, 80), OnClickListener {

    //region vars
    private val partnershipDealershipViewModel: PartnershipDealershipViewModel by navGraphViewModels(mapOf(R.id.partnership_graph to PartnershipsViewModel::class, R.id.spotter_graph to SpotterViewModel::class))
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(partnershipDealershipViewModel.partnershipDealership) {
                recResumeContainer
                    .apply {
                        setHasFixedSize(true)
                        adapter = if (renew) resumeAdapter else resumesAdapter.also { it.hasAnySelection.observe(viewLifecycleOwner) { btnResumePositive.apply { if (it) show() else hide() } } }
                    }

                btnResumePositive.apply { if (renew) visibility = VISIBLE; setOnClickListener(this@ResumeDialogFragment) }
            }
        }
    }

    override fun onClick(v: View?) { v?.id?.let { if (it == R.id.btn_resumePositive) partnershipDealershipViewModel.addTask(); popBack() } }
}