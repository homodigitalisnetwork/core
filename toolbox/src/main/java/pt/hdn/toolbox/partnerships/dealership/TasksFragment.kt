package pt.hdn.toolbox.partnerships.dealership

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.Visibility
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentTasksBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.partnerships.PartnershipsViewModel
import pt.hdn.toolbox.spotter.SpotterViewModel
import pt.hdn.toolbox.viewmodels.PartnershipDealershipViewModel

@AndroidEntryPoint
class TasksFragment: BindingFragment<FragmentTasksBinding>(FragmentTasksBinding::inflate) {

	//region vars
	private val partnershipDealershipViewModel: PartnershipDealershipViewModel by navGraphViewModels(mapOf(R.id.partnership_graph to PartnershipsViewModel::class, R.id.spotter_graph to SpotterViewModel::class))
	//endregion vars

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		with(binding!!) {
			with(partnershipDealershipViewModel) {
				errorBus.observe(viewLifecycleOwner) { if (it == Err.TYPE_1) handleError(recTasksContainer) }

				dataBus
					.observe<Int>(viewLifecycleOwner, Address.CLICK_1) {
						when (it) {
							R.id.btn_dealershipDialogRemove ->
								MaterialAlertDialogBuilder(requireContext())
									.setTitle(string(R.string.confirmation))
									.setMessage(string(R.string.areYouSure))
									.setPositiveButton(string(R.string.yes)) { dial, position -> removeTask(position); dial.dismiss() }
									.setNegativeButton(string(R.string.no), null)
									.show()
							R.id.btn_dealershipDialogEdit, R.id.btn_dealershipDialogAdd -> {
								dataBus.send(viewLifecycleOwner, Address.VIEW_1, R.id.bar_dealershipDialogLoading to Visibility.ENABLE); getResumes()
							}
						}
					}

				tasksAdapter
					.let {
						recTasksContainer.apply { setHasFixedSize(true); adapter = it }

						it.size.observe(viewLifecycleOwner) { lblTasksDisplay.visibility = if (it == 0) VISIBLE else GONE }

						it.highlighted.observe(viewLifecycleOwner) { (_, _) -> setViewVisibility() }
					}
			}
		}
	}

	private fun setViewVisibility() {
		dataBus.send(
			owner = viewLifecycleOwner,
			address = Address.VIEW_1,
			values = partnershipDealershipViewModel
				.partnershipDealership
				.tasksAdapter
				.firstSelected
				?.run {
					arrayOf(
						R.id.btn_dealershipDialogAdd to Visibility.GONE,
						R.id.btn_dealershipDialogEdit to Visibility.ENABLE,
						R.id.btn_dealershipDialogRemove to Visibility.ENABLE
					)
				} ?: run {
					arrayOf(
						R.id.btn_dealershipDialogEdit to Visibility.GONE,
						R.id.btn_dealershipDialogAdd to Visibility.ENABLE,
						R.id.btn_dealershipDialogRemove to Visibility.ENABLE
					)
				}
		)
	}
}