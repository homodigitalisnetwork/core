package pt.hdn.toolbox.partnerships.dealership.resume.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.viewbinding.ViewBinding
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.annotations.Resume.Companion.PERFORMANCE
import pt.hdn.toolbox.annotations.Resume.Companion.SCHEMAS
import pt.hdn.toolbox.annotations.Resume.Companion.SCORE
import pt.hdn.toolbox.partnerships.dealership.resume.util.Resume
import pt.hdn.toolbox.resources.Resources

class ResumeAdapter(
    val resume: Resume,
    private val res: Resources
) : Adapter<BindingViewHolder<ViewBinding>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<ViewBinding> {
        return when (viewType) {
            SCORE -> ScoreViewHolder(res, parent)
            PERFORMANCE -> PerformanceViewHolder(res, parent)
            SCHEMAS ->MarketViewHolder(PERSON, res, parent)
            else /*TRADES*/ -> TradeViewHolder(res, parent)
        }
    }

    override fun onBindViewHolder(holder: BindingViewHolder<ViewBinding>, position: Int) {
        with(resume) {
            with(holder) {
                when (this) {
                    is ScoreViewHolder -> speciality.score.setViewHolder()
                    is PerformanceViewHolder -> performance.setViewHolder()
                    is MarketViewHolder -> speciality.setViewHolder()
                    is TradeViewHolder -> getSchemaTrade(position).setViewHolder()
                    else -> null
                }
            }
        }
    }

    override fun getItemCount(): Int = resume.getSize()

    override fun getItemViewType(position: Int): Int = resume.getViewId(position)
}