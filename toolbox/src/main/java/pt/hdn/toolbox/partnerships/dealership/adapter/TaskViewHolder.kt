package pt.hdn.toolbox.partnerships.dealership.adapter

import android.view.View.*
import android.view.ViewGroup
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.databinding.ViewholderTaskBinding
import pt.hdn.toolbox.resources.Resources
import java.util.*

class TaskViewHolder(
    resources: Resources,
    parent: ViewGroup
) : HDNViewHolder<ViewholderTaskBinding, Task>(resources, parent, ViewholderTaskBinding::inflate) {

    init { with(binding) { swiTasksExclusivity.text = string(R.string.exclusivity); lblTasksSchemas.text = string(R.string.schemas); lblTasksResponsibilities.text = string(R.string.responsibilities) } }

    override fun Task.setViewHolder(isExpanded: Boolean) {
        with(binding) {
            lblTasksSpeciality.text = res.specialities[specialityType.uuid]?.name ?: string(R.string.unknown)

            layoutTasksBody.visibility = if (isExpanded) VISIBLE else GONE

            if (isExpanded) {
                swiTasksExclusivity.apply { exclusivity.let { visibility = if (it != null) VISIBLE else GONE; isChecked = it ?: false } }

                schemasTasksContainer.apply { resources = res; data = schemas }
            }
        }
    }
}