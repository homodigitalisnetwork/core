package pt.hdn.toolbox.partnerships.dealership.resume.util

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.specialities.util.Speciality
import pt.hdn.toolbox.statistics.PopulationStats
import pt.hdn.toolbox.statistics.Stats

data class Performance(
    @Expose val specialityUUID: String,
    @Expose var stats: Stats? = null,
    @Expose val populationStats: PopulationStats
) {
    //region vars
    var type: UUIDNameMarketData? = null
    //endregion vars
}
