package pt.hdn.toolbox.security

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Places
import pt.hdn.toolbox.annotations.Places.Companion.ACCOUNTING
import pt.hdn.toolbox.annotations.Places.Companion.ASSETS
import pt.hdn.toolbox.annotations.Places.Companion.BALANCE
import pt.hdn.toolbox.annotations.Places.Companion.EXPENDITURES
import pt.hdn.toolbox.annotations.Places.Companion.PARTNERSHIPS
import pt.hdn.toolbox.annotations.Places.Companion.PERFORMANCE
import pt.hdn.toolbox.annotations.Places.Companion.PROFILE
import pt.hdn.toolbox.annotations.Places.Companion.RECURRENCE
import pt.hdn.toolbox.annotations.Places.Companion.REPORT
import pt.hdn.toolbox.annotations.Places.Companion.SHIFT
import pt.hdn.toolbox.annotations.Places.Companion.SINGLE
import pt.hdn.toolbox.annotations.Places.Companion.SPECIALITIES
import pt.hdn.toolbox.annotations.Places.Companion.SPOTTER
import pt.hdn.toolbox.annotations.Places.Companion.TODO
import pt.hdn.toolbox.annotations.Places.Companion.VISIBILITY

@Parcelize
data class Clearance(
    @Expose val main: MutableList<Int>,
    @Expose val menu: MutableList<Int>,
    @Expose val definitions: MutableList<Int>,
    @Expose val exchange: MutableList<Int>
) : Parcelable {

    //region vars
    var toVisibility: Boolean; get() = menu.contains(VISIBILITY); set(value) { handler(menu, VISIBILITY, value) }
    var toPerformance: Boolean; get() = main.contains(PERFORMANCE); set(value) { handler(main, PERFORMANCE, value) }
    var toProfile: Boolean; get() = menu.contains(PROFILE); set(value) { handler(menu, PROFILE, value) }
    var toPartnerships: Boolean; get() = definitions.contains(PARTNERSHIPS); set(value) { handler(definitions, PARTNERSHIPS, value) }
    var toAssets: Boolean; get() = definitions.contains(ASSETS); set(value) { handler(definitions, ASSETS, value) }
    var toExpenditures: Boolean; get() = definitions.contains(EXPENDITURES); set(value) { handler(definitions, EXPENDITURES, value) }
    var toAccounting: Boolean; get() = definitions.contains(ACCOUNTING); set(value) { handler(definitions, ACCOUNTING, value) }
    var toSpecialities: Boolean; get() = definitions.contains(SPECIALITIES); set(value) { handler(definitions, SPECIALITIES, value) }
    var toSpotter: Boolean; get() = main.contains(SPOTTER); set(value) { handler(main, SPOTTER, value) }
    var toTODO: Boolean; get() = main.contains(TODO); set(value) { handler(main, TODO, value) }
    val toReport: Boolean; get() = main.contains(REPORT)
    val toDefinitions: Boolean; get() = definitions.isNotEmpty()
    val toExchange: Boolean; get() = exchange.isNotEmpty()
    val toSingle: Boolean; get() = exchange.contains(SINGLE)
    val toRecurrence: Boolean; get() = exchange.contains(RECURRENCE)
    val toBalance: Boolean; get() = main.contains(BALANCE)
//    val toShift: Boolean = main.contains(SHIFT)
    //endregion vars

    companion object {
        val MAIN: List<Int> = listOf(TODO, REPORT, SHIFT, PERFORMANCE, BALANCE, SPOTTER).sorted()
        val MENU: List<Int> = listOf(VISIBILITY, PROFILE).sorted()
        val DEFINITIONS: List<Int> = listOf(SPECIALITIES, PARTNERSHIPS, ASSETS, EXPENDITURES, ACCOUNTING).sorted()
        val EXCHANGE: List<Int> = listOf(SINGLE, RECURRENCE).sorted()
    }

    constructor(isFounder: Boolean) : this(
        if (isFounder) MAIN.toMutableList() else mutableListOf<Int>(),
        if (isFounder) MENU.toMutableList() else mutableListOf<Int>(),
        if (isFounder) DEFINITIONS.toMutableList() else mutableListOf<Int>(),
        if (isFounder) EXCHANGE.toMutableList() else mutableListOf<Int>()
    )

    fun setPlace(@Places place: Int, isChecked: Boolean) {
        when (place) {
            VISIBILITY -> toVisibility = isChecked
            PERFORMANCE -> toPerformance = isChecked
            PROFILE -> toProfile = isChecked
            PARTNERSHIPS -> toPartnerships = isChecked
            ASSETS -> toAssets = isChecked
            EXPENDITURES -> toExpenditures = isChecked
            ACCOUNTING -> toAccounting = isChecked
            SPECIALITIES -> toSpecialities = isChecked
            SPOTTER -> toSpotter = isChecked
            TODO -> toTODO = isChecked
        }
    }

    private fun handler(list: MutableList<Int>, @Places place: Int, value: Boolean) {
        with(list) { place.let { when {!contains(it) && value -> { add(it); sort() }; contains(it) && !value -> remove(it); else -> null } } }
    }
}