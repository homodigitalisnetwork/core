package pt.hdn.toolbox.recurrences.util

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.ListAdapter
import pt.hdn.toolbox.resources.Resources

class RecurrenceAdapter(
	recurrence: MutableList<Recurrence>,
	resources: Resources
) : ListAdapter<RecurrenceViewHolder, Recurrence>(recurrence, resources) {
	override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): RecurrenceViewHolder = RecurrenceViewHolder(resources, parent)
}