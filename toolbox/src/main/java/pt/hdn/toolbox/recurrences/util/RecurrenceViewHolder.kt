package pt.hdn.toolbox.recurrences.util

import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.View.TEXT_ALIGNMENT_TEXT_START
import android.view.View.TEXT_ALIGNMENT_TEXT_END
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TableLayout
import android.widget.TableRow
import androidx.annotation.StringRes
import com.google.android.material.textview.MaterialTextView
import pt.hdn.contract.schemas.*
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.HDNViewHolder
import pt.hdn.toolbox.databinding.ViewholderRecurrenceBinding
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class RecurrenceViewHolder(
	resources: Resources,
	parent: ViewGroup
) : HDNViewHolder<ViewholderRecurrenceBinding, Recurrence>(resources, parent, ViewholderRecurrenceBinding::inflate) {

	//region vars
	private val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(resources.locale)
	private val tinyMargin: Int = dimen(R.dimen.t_size)
	private val smallMargin: Int = dimen(R.dimen.s_size)
	private val mediumMargin: Int = dimen(R.dimen.m_size)
	private val largeMargin: Int = dimen(R.dimen.l_size)
	//endregion vars

	override fun Recurrence.setViewHolder(isExpanded: Boolean) {
		with(binding) {
			lblRecurrenceDate.text = date
			lblRecurrenceName.text = name
			lblRecurrenceAmount.text = formattedAmount(isBuyerSide, amount)

			layoutRecurrenceBody.visibility = if (isExpanded) VISIBLE else GONE

			if (isExpanded) { tbRecurrenceTasks.removeAllViews(); addTasks(tasks) }
		}
	}

	private fun addTasks(tasks: List<Task>) {
		addDescription(smallMargin, R.string.tasks);

		tasks
			.forEachWith {
				addParameter(mediumMargin, R.string.speciality, specialityType.name)

				schemas
					.forEachWith {
						res.schemas[id]?.let { addParameter(largeMargin, R.string.schema, it) }

						when (this) {
							is FixSchema -> addParameter(largeMargin, R.string.fix, fix)
							is RateSchema -> addParameter(largeMargin, R.string.rate, rate)
							is CommissionSchema -> {
								addParameter(largeMargin, R.string.cut, cut)
								upperBound?.let { addParameter(largeMargin, R.string.upperBound, it) }
								lowerBound?.let { addParameter(largeMargin, R.string.lowerBound, it) }
							}
							is ObjectiveSchema -> {
								addParameter(largeMargin, R.string.bonus, bonus)
								upperBound?.let { addParameter(largeMargin, R.string.upperBound, it) }
								lowerBound?.let { addParameter(largeMargin, R.string.lowerBound, it) }
							}
							is ThresholdSchema -> {
								addParameter(largeMargin, R.string.bonus, bonus)
								addParameter(largeMargin, R.string.thresholdType, if (isAbove!!) R.string.aboveLimit else R.string.belowLimit)
								addParameter(largeMargin, R.string.threshold, threshold)

							}
						}
					}
			}
	}

	private fun addDescription(leftMargin: Int, @StringRes idRes: Int) {
		with(MaterialTextView(context)) {
			layoutParams = TableLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply { setMargins(leftMargin, tinyMargin, tinyMargin, tinyMargin) }
			text = string(idRes)

			binding.tbRecurrenceTasks.addView(this)
		}
	}

	private fun addParameter (leftMargin: Int, @StringRes field: Int, value: BigDecimal?) { addParameter(leftMargin, field, value?.toString()) }

	private fun addParameter (leftMargin: Int, @StringRes field: Int, @StringRes value: Int) { addParameter(leftMargin, field, string(value)) }

	private fun addParameter (leftMargin: Int, @StringRes field: Int, value: String?) {
		with(TableRow(context)) {
			layoutParams = TableLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)

			with(MaterialTextView(context)) {
				layoutParams = TableRow.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply { setMargins(leftMargin, tinyMargin, tinyMargin, tinyMargin) }
				textAlignment = TEXT_ALIGNMENT_TEXT_END
				text = string(field)

				addView(this)
			}

			with(MaterialTextView(context)) {
				layoutParams = TableRow.LayoutParams(0, WRAP_CONTENT, 1f).apply { setMargins(tinyMargin, tinyMargin, tinyMargin, tinyMargin) }
				textAlignment = TEXT_ALIGNMENT_TEXT_START
				text = value

				addView(this)
			}

			binding.tbRecurrenceTasks.addView(this)
		}
	}

	private fun formattedAmount(isBuyerSide: Boolean, bigDecimal: BigDecimal): String {
		return (when { (bigDecimal > ZERO) xor isBuyerSide -> "+ "; bigDecimal == ZERO -> ""; else -> "- " } + "%.2f").format(bigDecimal)
	}
}