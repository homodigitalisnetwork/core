package pt.hdn.toolbox.recurrences

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentViewerBinding
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.setCompoundTopDrawable
import pt.hdn.toolbox.misc.takeUnlessWith

@AndroidEntryPoint
class RecurrencesFragment : BindingFragment<FragmentViewerBinding>(FragmentViewerBinding::inflate) {

    //region vars
    private val recurrencesViewModel: RecurrencesViewModel by viewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(recurrencesViewModel) {
                lblViewerDisplay.apply { visibility = VISIBLE; setCompoundTopDrawable(R.drawable.ic_recurrence) }
                barViewerLoading.visibility = VISIBLE

                recurrenceAdapter
                    .observe(viewLifecycleOwner) {
                        barViewerLoading.visibility = GONE

                        it
                            ?.also {
                                if (it.isEmpty()) { lblViewerDisplay.text = string(R.string.nothingToReport) }
                                else { lblViewerDisplay.visibility = GONE; recViewerContainer.apply { setHasFixedSize(true); adapter = it } }
                            }
                            ?: run { lblViewerDisplay.apply { visibility = VISIBLE; setCompoundTopDrawable(R.drawable.ic_sad_face); text = string(R.string.somethingWrong) } }

                    }
            }
        }
    }
}