package pt.hdn.toolbox.recurrences

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.recurrences.RecurrencesRequest
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.recurrences.util.RecurrenceAdapter
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.viewmodels.CoreViewModel
import javax.inject.Inject

@HiltViewModel
class RecurrencesViewModel @Inject constructor(
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : CoreViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    val recurrenceAdapter: Flow<RecurrenceAdapter?>
    //endregion vars

    init {
        this.recurrenceAdapter = flowOf(RecurrencesRequest(partyUUID = session.partyUUID))
            .flatMapLatest { repo.recurrences(it) }
            .map {
                var adapter: RecurrenceAdapter? = null

                it.success { recurrences.forEachWith { tasks.forEachWith { res.specialities[specialityType.uuid]?.let { specialityType = it } } }; adapter = RecurrenceAdapter(recurrences, res) }

                adapter
            }
            .shareIn(viewModelScope, Lazily, 1)
    }
}