package pt.hdn.toolbox.recurrences.util

import com.google.gson.annotations.Expose
import pt.hdn.contract.util.Task
import java.math.BigDecimal

data class Recurrence(
	@Expose val date: String,
	@Expose val tasks: List<Task>,
	@Expose val amount: BigDecimal,
	@Expose val name: String,
	@Expose val isBuyerSide: Boolean
)