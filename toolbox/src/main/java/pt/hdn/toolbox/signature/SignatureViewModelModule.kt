package pt.hdn.toolbox.signature

import androidx.lifecycle.SavedStateHandle
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import pt.hdn.contract.annotations.SourceType
import pt.hdn.contract.schemas.CommissionSchema
import pt.hdn.contract.schemas.ObjectiveSchema
import pt.hdn.contract.schemas.RateSchema
import pt.hdn.contract.schemas.ThresholdSchema
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.contract.util.Task
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.annotations.Value
import pt.hdn.toolbox.communications.message.DefaultRef
import pt.hdn.toolbox.communications.message.Ref
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.resources.Resources
import java.time.ZonedDateTime
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class IsBuyerSide

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class attachToPermission

@Module
@InstallIn(ViewModelComponent::class)
object SignatureViewModelModule {

    @ViewModelScoped
    @Provides
    fun providesContract(savedStateHandle: SavedStateHandle): Contract = savedStateHandle.get<Contract>(Field.CONTRACT)!!

    @IsBuyerSide
    @ViewModelScoped
    @Provides
    fun providesIsBuyerSide(savedStateHandle: SavedStateHandle): Boolean = savedStateHandle.get<Boolean>(Field.IS_BUYER_SIDE)!!

    @attachToPermission
    @ViewModelScoped
    @Provides
    fun providesAttachToPermission(savedStateHandle: SavedStateHandle): Boolean = savedStateHandle.get<Boolean>(Field.ATTACH_TO_PERMISSION)!!
}