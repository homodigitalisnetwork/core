package pt.hdn.toolbox.signature

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import pt.hdn.contract.util.Contract
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.viewmodels.CoreViewModel
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.communications.bus.*
import pt.hdn.toolbox.communications.signature.SignatureRequest
import pt.hdn.toolbox.communications.signature.SignatureResponse
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import javax.inject.Inject

@HiltViewModel
class SignatureViewModel @Inject constructor(
    @IsBuyerSide private val isBuyerSide: Boolean,
    @attachToPermission private val attachToPermission: Boolean,
    private val contract: Contract,
    sessionBus: SessionBus,
    dataBus: DataBus,
    repo: CoreRepo,
    resources: Resources
) : CoreViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    private val request: MutableSharedFlow<SignatureRequest> = MutableSharedFlow()
    val response: Flow<Result<SignatureResponse>>
    //endregion vars

    init {
        this.response = request
            .flatMapLatest { repo.signature(it) }
            .onEach { it.success { dataBus.send(viewModelScope, Address.CONTRACT, signedContract) } }

        if (attachToPermission) dataBus.observe<Boolean>(viewModelScope, Address.PERMISSION) { if (!it) cancel() }
    }

    fun cancel() { dataBus.send(viewModelScope, Address.CONTRACT, null) }

    fun request(partyPassphrase: String, deputyPassphrase: String) {
        launch {
            request.emit(
                SignatureRequest(
                    deputyUUID = session.deputyUUID,
                    partyUUID = session.partyUUID,
                    contract = contract,
                    partyPassphrase = partyPassphrase,
                    deputyPassphrase = deputyPassphrase,
                    isBuyerSide = isBuyerSide
                )
            )
        }
    }
}