package pt.hdn.toolbox.signature

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.View.*
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.databinding.DialogSignatureBinding
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.observeWith

@AndroidEntryPoint
class SignatureDialogFragment : BindingDialogFragment<DialogSignatureBinding>(DialogSignatureBinding::inflate, 40), OnClickListener, OnEditorActionListener {

    //region vars
    private val signatureViewModel: SignatureViewModel by viewModels()
    //endregion vars

    override fun onStart() { super.onStart(); dialog?.window?.setLayout(MATCH_PARENT, WRAP_CONTENT) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding!!) {
            with(signatureViewModel) {
                connectivity.observe(viewLifecycleOwner) { btnSignaturePositive.apply { isEnabled = it; isClickable = it } }

                response
                    .observeWith(viewLifecycleOwner) {
                        barLoginLoading.visibility = GONE

                        success { toast(R.string.contractSigned); popBack() }
                        error { code, _ -> toast(if (code == Err.PASSPHRASE) R.string.invalidPassphrase else R.string.failExe) }
                    }

                lblSignatureTitle.text = string(R.string.signature)
                if (res.isNotSpot) { tilPartySignaturePassphrase.visibility = VISIBLE; tilPartySignaturePassphrase.hint = string(R.string.companySignature) }
                tilDeputySignaturePassphrase.hint = string(R.string.yourPassphrase)

                txtDeputySignaturePassphrase.setOnEditorActionListener(this@SignatureDialogFragment)
                btnSignaturePositive.setOnClickListener(this@SignatureDialogFragment)
            }
        }
    }

    override fun onBackPressed() { confirmationDialog() }

    override fun onClick(v: View?) { v?.id?.also { when (it) { R.id.btn_signaturePositive -> eval() } } }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean { if (actionId == EditorInfo.IME_ACTION_DONE) eval(); return false }

    private fun eval() {
        with(binding!!) {
            val partyPassphrase: String? = txtPartySignaturePassphrase.text?.toString()
            val deputyPassphrase: String? = txtDeputySignaturePassphrase.text?.toString()

            when {
                res.isNotSpot && partyPassphrase.isNullOrBlank() -> { txtPartySignaturePassphrase.requestFocus(); toast(R.string.missingPassphrase) }
                deputyPassphrase.isNullOrBlank() -> { txtDeputySignaturePassphrase.requestFocus(); toast(R.string.missingPassphrase) }
                else -> {
                    signatureViewModel.request((if (res.isNotSpot) partyPassphrase else deputyPassphrase)!!, deputyPassphrase)

                    barLoginLoading.visibility = VISIBLE
                }
            }
        }
    }

    private fun confirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(string(R.string.confirmation))
            .setMessage(string(R.string.areYouSure))
            .setNegativeButton(string(R.string.no), null)
            .setPositiveButton(string(R.string.yes)) { _, _ -> signatureViewModel.cancel(); popBack() }
            .show()
    }
}