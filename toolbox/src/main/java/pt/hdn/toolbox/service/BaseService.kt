package pt.hdn.toolbox.service

import androidx.annotation.StringRes
import androidx.lifecycle.*
import androidx.savedstate.SavedStateRegistry
import androidx.savedstate.SavedStateRegistryController
import androidx.savedstate.SavedStateRegistryOwner
import pt.hdn.toolbox.resources.Resources
import javax.inject.Inject

abstract class BaseService: LifecycleService(), ViewModelStoreOwner, HasDefaultViewModelProviderFactory, SavedStateRegistryOwner {

    //region vars
    @Inject lateinit var res: Resources
    override val savedStateRegistry: SavedStateRegistry get() = savedStateRegistryController.savedStateRegistry
    private val viewModelStore: ViewModelStore = ViewModelStore()
    private val savedStateRegistryController = SavedStateRegistryController.create(this)
    private val factory: ViewModelProvider.Factory = SavedStateViewModelFactory(application, this, null)
    //endregion vars

    override fun getViewModelStore(): ViewModelStore = viewModelStore

    override fun getDefaultViewModelProviderFactory(): ViewModelProvider.Factory = factory

    fun string(@StringRes id: Int) = res.string(id)
}