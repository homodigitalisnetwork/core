package pt.hdn.toolbox.performance

import com.github.mikephil.charting.formatter.ValueFormatter
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE

class PerformanceValueFormatter(
    private val timestamps: List<ZonedDateTime>
): ValueFormatter() {
    override fun getFormattedValue(value: Float): String = if (value < 0 || value > timestamps.lastIndex) "" else timestamps[value.toInt()].format(ISO_LOCAL_DATE)
}