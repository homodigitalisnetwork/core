package pt.hdn.toolbox.performance

import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.RadarData
import pt.hdn.toolbox.business.deputy.Deputy

interface PerformanceHandler {
    val radarData: RadarData
    val deputies: List<Deputy>
    val lineRevenueData: LineData
    val lineRevenuePerDistanceData: LineData
    val lineRevenuePerTimeData: LineData
    val lineRevenuePerFuelData: LineData
    val timestampValueFormatter: PerformanceValueFormatter
    val labelValueFormatter: AxisLabelValueFormatter
    var index: Int
}