package pt.hdn.toolbox.performance

import com.github.mikephil.charting.formatter.ValueFormatter

class AxisLabelValueFormatter(
    private val labels: List<String>
): ValueFormatter() {

    //region vars
    val size; get() = labels.size
    //endregion vars

    override fun getFormattedValue(value: Float): String = labels[value.toInt()]
}