package pt.hdn.toolbox.performance

import com.github.mikephil.charting.data.*
import pt.hdn.toolbox.business.deputy.Deputy
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.statistics.*
import pt.hdn.toolbox.statistics.Statistics.SIGMA_MULTIPLIER
import pt.hdn.toolbox.statistics.Statistics.SLOPE
import java.time.ZonedDateTime
import java.util.*

class DefaultPerformanceHandler(
    res: Resources,
    deputiesStats: List<DeputyStats>,
    populationStats: PopulationStats,
    progressionsStats: Map<UUID, List<Progression>>? = null
): PerformanceHandler {

    //region vars
    override var index: Int = 0
    private val mutableDeputies: MutableList<Deputy> = mutableListOf()
    private val radarsDataMap: MutableMap<UUID, RadarData> = mutableMapOf()
    private val lineRevenueDataMap: MutableMap<UUID, LineData> = mutableMapOf()
    private val lineRevenuePerDistanceDataMap: MutableMap<UUID, LineData> = mutableMapOf()
    private val lineRevenuePerTimeDataMap: MutableMap<UUID, LineData> = mutableMapOf()
    private val lineRevenuePerFuelDataMap: MutableMap<UUID, LineData> = mutableMapOf()
    private val mutableProgressionTimestamps: MutableList<ZonedDateTime> = mutableListOf()
    override val timestampValueFormatter: PerformanceValueFormatter = PerformanceValueFormatter(mutableProgressionTimestamps)
    override val labelValueFormatter: AxisLabelValueFormatter = AxisLabelValueFormatter(res.labels)
    override val deputies: List<Deputy> = mutableDeputies
    override val radarData: RadarData = radarsDataMap[mutableDeputies[index].uuid]!!
    override val lineRevenueData: LineData = lineRevenueDataMap[mutableDeputies[index].uuid]!!
    override val lineRevenuePerDistanceData: LineData = lineRevenuePerDistanceDataMap[mutableDeputies[index].uuid]!!
    override val lineRevenuePerTimeData: LineData = lineRevenuePerTimeDataMap[mutableDeputies[index].uuid]!!
    override val lineRevenuePerFuelData: LineData = lineRevenuePerFuelDataMap[mutableDeputies[index].uuid]!!
    //endregion vars

    init {
        deputiesStats
            .forEach {
                with(it) {
                    deputy
                        .apply {
                            mutableDeputies.add(this)

                            uuid!!
                                .let { uuid ->
                                    stats
                                        ?.apply {
                                            with(populationStats) {
                                                radarsDataMap[uuid] = compileData(
                                                    tScore(revenuePerTime, revenuePerTimeStats),
                                                    tScore(revenuePerDistance, revenuePerDistanceStats),
                                                    tScore(revenuePerFuel, revenuePerFuelStats),
                                                    tScore(efficiency, efficiencyStats),
                                                    tScore(consistency, consistencyStats)
                                                )
                                            }
                                        }

                                    lineRevenueDataMap[uuid] = LineData(LineDataSet(mutableListOf(), ""))
                                    lineRevenuePerDistanceDataMap[uuid] = LineData(LineDataSet(mutableListOf(), ""))
                                    lineRevenuePerTimeDataMap[uuid] = LineData(LineDataSet(mutableListOf(), ""))
                                    lineRevenuePerFuelDataMap[uuid] = LineData(LineDataSet(mutableListOf(), ""))

                                    progressionsStats
                                        ?.get(uuid)
                                        ?.forEachIndexed { index, progression ->
                                            index
                                                .toFloat()
                                                .let { idx ->
                                                    lineRevenueDataMap[uuid]!!.addEntry(Entry(idx, progression.revenue.toFloat()), 0)
                                                    lineRevenuePerDistanceDataMap[uuid]!!.addEntry(Entry(idx, progression.revenuePerDistance), 0)
                                                    lineRevenuePerTimeDataMap[uuid]!!.addEntry(Entry(idx, progression.revenuePerTime), 0)
                                                    lineRevenuePerFuelDataMap[uuid]!!.addEntry(Entry(idx, progression.revenuePerFuel), 0)
                                                }
                                        }
                                }
                        }
                }
            }
    }

    private fun tScore(value: Float?, moment: Moment): Float = Statistics.tScore(value, moment, SIGMA_MULTIPLIER, SLOPE)

    private fun compileData(vararg data: Float): RadarData = RadarData(RadarDataSet(data.map { RadarEntry(it) }, ""))
}