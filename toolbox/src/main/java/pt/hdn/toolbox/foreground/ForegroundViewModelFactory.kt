package pt.hdn.toolbox.foreground

import android.app.NotificationManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.Factory
import androidx.lifecycle.viewmodel.CreationExtras
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.misc.LocationProvider
import pt.hdn.toolbox.notifications.CountDownNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources

class ForegroundViewModelFactory(
    private val locationProvider: LocationProvider,
    private val countDownNotification: CountDownNotification,
    private val notificationManager: NotificationManager,
    private val dataBus: DataBus,
    private val sessionBus: SessionBus,
    private val repo: CoreRepo,
    private val resources: Resources
): Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
       return  if (modelClass.isAssignableFrom(ForegroundViewModel::class.java)) {
           ForegroundViewModel(
			   locationProvider = locationProvider,
               notificationManager = notificationManager,
			   countDownNotification = countDownNotification,
			   dataBus = dataBus,
			   sessionBus = sessionBus,
			   repo = repo,
			   resources = resources
           ) as T
       }
	   else { throw IllegalArgumentException("Unknown ViewModel class") }
    }
}