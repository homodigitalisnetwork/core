package pt.hdn.toolbox.foreground

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.IntentSenderRequest.Builder
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.lifecycle.viewModelScope
import com.google.android.gms.common.api.ResolvableApiException
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import kotlinx.coroutines.launch
import pt.hdn.toolbox.annotations.*
import pt.hdn.toolbox.annotations.Action.Companion.ACCEPT
import pt.hdn.toolbox.annotations.Action.Companion.CANCEL
import pt.hdn.toolbox.annotations.Action.Companion.REJECT
import pt.hdn.toolbox.annotations.Duration.Companion.x1
import pt.hdn.toolbox.annotations.Duration.Companion.x20
import pt.hdn.toolbox.annotations.Field.Companion.TRANSACTION
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.communications.bus.*
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.notifications.CountDownNotification
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.transaction.util.DefaultTransaction
import pt.hdn.toolbox.viewmodels.CoreViewModel
import java.util.*

class ForegroundViewModel constructor(
    private val locationProvider: LocationProvider,
    private val countDownNotification: CountDownNotification,
    private val sessionBus: SessionBus,
    val notificationManager: NotificationManager,
    dataBus: DataBus,
    repo: CoreRepo,
    resources: Resources
): CoreViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    private var searchableRef: Boolean = false
    private var locationRef: Boolean = session.location
    var onHold: Boolean = session.transactionUUID != null; private set
    var cleanOnTermination: Boolean = true; private set
    private val mutableSearchable: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val mutableLocation: MutableStateFlow<Boolean> = MutableStateFlow(session.location)
    private val searchableToCombine: Flow<Boolean>
    private val locationToCombine: Flow<Boolean>
    val searchable: Flow<Result<Boolean>>
    val location: Flow<Result<Boolean>>
    val settings: Flow<Pair<Boolean, Boolean>>
    val isSearchable: Boolean = session.searchable
    //endregion vars

    init {
        dataBus
            .using(viewModelScope) {
                observe<Unit>(Address.LOGOUT) { this@ForegroundViewModel.cleanOnTermination = false }
                observeWith<Pair<@Action Int, UUID>>(Address.TRANS_COMM) { if (first == CANCEL) { resumeBroadcast(); countDownNotification.cancel(second) } }
                observe<DefaultTransaction>(Address.ENQUIRE) { transaction ->
                    holdBroadcast()

                    with(session) {
                        if (location) { launch { getOneShot().collect { it.success { countDownNotification.start(x20, x1, transaction, this) } } } }
                        else { countDownNotification.start(x20, x1, transaction, position) }
                    }
                }
            }

        this.searchable = mutableSearchable
            .flatMapLatest { repo.searchable(session.deputyUUID, it) }
            .stateIn(viewModelScope, Lazily, Result.Success(false))

        this.searchableToCombine = searchable.map { with(session) { it.success{ searchable = this@success }; searchable } }

        this.location = mutableLocation
            .flatMapLatest { repo.location(session.deputyUUID, it) }
            .stateIn(viewModelScope, Lazily, Result.Success(session.location))

        this.locationToCombine = location.map { with(session) { it.success{ location = this@success }; location } }

        this.settings = searchableToCombine
            .combine(locationToCombine) { s, l -> with(locationProvider) { launch { if (s) startBroadcast(l) else stopBroadcast(true) }; s to l } }
            .distinctUntilChanged { old, new -> !old.first && !new.first }
    }

    suspend fun getOneShot(): Flow<Result<Position>> = locationProvider.getOneShot()

    suspend fun getPermission(context: Context, permission: MutableSharedFlow<String>, config: MutableSharedFlow<IntentSenderRequest>, block: (suspend () -> Unit)? = null) {
        ACCESS_FINE_LOCATION.let { if (checkSelfPermission(context, it) == PERMISSION_GRANTED) { getLocation(config, block) } else permission.emit(it) }
    }

    suspend fun getLocation(config: MutableSharedFlow<IntentSenderRequest>, block: (suspend () -> Unit)? = null) {
        try { locationProvider.checkLocationSettings(); block?.invoke() ?: dataBus.send(Address.PERMISSION, true) }
        catch (e: Exception) { if (e is ResolvableApiException) { try { config.emit(Builder(e.resolution).build()) } catch (sendEx: SendIntentException) { /*do nothing*/ } } }
    }

    suspend fun setLocation(value: Boolean) { this.locationRef = value; if (!onHold) mutableLocation.emit(value) }

    suspend fun setSearchable(value: Boolean) { this.searchableRef = value; if (!onHold) mutableSearchable.emit(value) }

    fun holdBroadcast() { this.onHold = true; launch { locationProvider.stopBroadcast(true) } }

    fun startBroadcast(transactionUUID: UUID, position: Position) { launch { locationProvider.startBroadcast(transactionUUID, position) } }

    fun resumeBroadcast() { this.onHold = false; if (searchableRef) { launch { locationProvider.startBroadcast(locationRef) } } }

    fun updateBroadcast() { if (!onHold && searchableRef && !locationRef) launch { locationProvider.updateBroadcast() } }

    fun terminate() {
        notificationManager
            .forEachWith {
                when (id) {
                    Notification.TRANSACTION -> countDownNotification.finish()
                    Notification.FOREGROUND -> { }
                    else -> notificationManager.cancel(id)
                }
            }

        launch {
            if (cleanOnTermination) {
                if (!onHold) { launch { locationProvider.stopBroadcast(true); repo.searchable(session.deputyUUID, false).collect { logout() } } } else logout()
            }
            else { locationProvider.stopBroadcast(false); logout() }
        }
    }

    /*
    REJECT  existe no when statement por cause dos pending intents na notificação em ENQUIRE
     */

    fun handleTransaction(intent: Intent?) {
        intent
            ?.let {
                it.action
                    ?.toInt()
                    ?.let { action ->
                        countDownNotification.abort()

                        when (action) {
                            ACCEPT /*seller side*/ -> with(it.getParcelableExtra<DefaultTransaction>(TRANSACTION)!!) {
                                holdBroadcast(); dataBus.send(viewModelScope, Address.TRANSACTION, ACCEPT to this)
                            }
                            REJECT /*seller side*/ -> with(it.getParcelableExtra<DefaultTransaction>(TRANSACTION)!!) {
                                resumeBroadcast(); launch { repo.notify(message(action)).collect { it.error { _, _ -> } } }
                            }
                            else -> null
                        }
                    }
            }
    }

    private suspend fun logout() { with(session) { repo.logOut(partyUUID, deputyUUID, cleanOnTermination).collect { it.success { sessionBus.send() }.error { _, _ -> } } } }
}