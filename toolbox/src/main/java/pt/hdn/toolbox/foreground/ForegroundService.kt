package pt.hdn.toolbox.foreground

import android.content.*
import android.location.LocationManager.MODE_CHANGED_ACTION
import android.os.Binder
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.P
import android.os.IBinder
import android.provider.Settings.Secure.getInt
import android.provider.Settings.Secure.LOCATION_MODE
import android.provider.Settings.Secure.LOCATION_MODE_OFF
import androidx.activity.result.IntentSenderRequest
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.notifications.CountDownNotification
import pt.hdn.toolbox.notifications.ForegroundNotification
import pt.hdn.toolbox.service.BaseService
import java.util.*
import javax.inject.Inject

/**
 * The foreground service that inherits from [BaseService] and handles all GPS related activities and all notifications interaction
 */
@AndroidEntryPoint
class ForegroundService : BaseService() {

    //region vars
    @Inject lateinit var foregroundViewModelFactory: ForegroundViewModelFactory
    @Inject lateinit var foregroundNotification: ForegroundNotification
    @Inject lateinit var countDownNotification: CountDownNotification
    val searchable: Flow<Result<Boolean>>; get() = foregroundViewModel.searchable
    val location: Flow<Result<Boolean>>; get() = foregroundViewModel.location
    val isSearchable: Boolean; get() = foregroundViewModel.isSearchable
    private var foregroundBinder: ForegroundBinder = ForegroundBinder()
    private val foregroundViewModel: ForegroundViewModel by viewModels { foregroundViewModelFactory }
    //endregion vars

    override fun onCreate() {
        super.onCreate()

        with(foregroundViewModel) {
            locationGlobalReceiver(this@ForegroundService, IntentFilter(MODE_CHANGED_ACTION)) {
                onReceive {
                    (if (SDK_INT >= P) locationManager.isLocationEnabled else getInt(contentResolver, LOCATION_MODE) != LOCATION_MODE_OFF)
                        .let { if (!it) countDownNotification.finish(); dataBus.send(lifecycleScope, Address.LOCATION, Unit) }
                }
            }

            settings.observeWith(this@ForegroundService) { foregroundNotification.notify(first, second) }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int { foregroundViewModel.handleTransaction(intent); return super.onStartCommand(intent, flags, startId) }

    override fun onBind(intent: Intent): IBinder { super.onBind(intent); return foregroundBinder }

    override fun onUnbind(intent: Intent?): Boolean { terminate(); return super.onUnbind(intent) }

    override fun onTaskRemoved(rootIntent: Intent?) { terminate(); super.onTaskRemoved(rootIntent) }

    /**
     * Gets the latest GPS position of the device once
     */
    suspend fun getOneShot(): Flow<Result<Position>> = foregroundViewModel.getOneShot()

    suspend fun getPermission(permission: MutableSharedFlow<String>, config: MutableSharedFlow<IntentSenderRequest>, block: (suspend () -> Unit)? = null) {
        foregroundViewModel.getPermission(this, permission, config, block)
    }

    suspend fun getLocation(config: MutableSharedFlow<IntentSenderRequest>, block: (suspend () -> Unit)? = null) { foregroundViewModel.getLocation(config, block) }

    suspend fun setSearchable(value: Boolean) { foregroundViewModel.setSearchable(value) }

    suspend fun setLocation(value: Boolean) { foregroundViewModel.setLocation(value) }

    fun startBroadcast(transactionUUID: UUID, position: Position) { foregroundViewModel.startBroadcast(transactionUUID, position) }

    fun updateBroadcast() { foregroundViewModel.updateBroadcast() }

    fun holdBroadcast() { foregroundViewModel.holdBroadcast() }

    fun resumeBroadcast() { foregroundViewModel.resumeBroadcast() }

    private fun terminate() { foregroundViewModel.terminate(); stopForeground(true); stopSelf() }

    inner class ForegroundBinder : Binder() { val foregroundService = this@ForegroundService }
}