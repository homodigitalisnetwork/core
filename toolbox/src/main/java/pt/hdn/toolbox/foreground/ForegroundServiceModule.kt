package pt.hdn.toolbox.foreground

import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Context.BIND_AUTO_CREATE
import android.content.Intent
import android.content.ServiceConnection
import android.location.LocationManager
import android.os.IBinder
import android.widget.RemoteViews
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.components.ServiceComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.hilt.android.scopes.ServiceScoped
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import pt.hdn.toolbox.application.GenericNotificationView
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.misc.LocationProvider
import pt.hdn.toolbox.misc.onServiceConnection
import pt.hdn.toolbox.notifications.*
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ForegroundServiceIntent

@Module
@InstallIn(ActivityRetainedComponent::class)
object ForegroundServiceModule {

    /**
     * Provides an initiation [Intent] for the [ForegroundService]
     */
    @ForegroundServiceIntent
    @ActivityRetainedScoped
    @Provides
    fun providesForegroundServiceIntent(@ApplicationContext context: Context): Intent = Intent(context, ForegroundService::class.java).also { context.startForegroundService(it) }


    /**
     * Provides the [Flow] with the [ForegroundService]
     */
    @ActivityRetainedScoped
    @Provides
    fun providesForeground(@ApplicationContext context: Context, @ForegroundServiceIntent intent: Intent): Flow<ForegroundService?> {
        return with(context) {
            callbackFlow {
                onServiceConnection<ForegroundService.ForegroundBinder> {
                    onServiceConnected { it?.let { trySend(it.foregroundService) } }

                    onServiceDisconnected { trySend(null) }
                }.also { bindService(intent, it, BIND_AUTO_CREATE); awaitClose { unbindService(it) } }
            }
        }
    }
}

@Module
@InstallIn(ServiceComponent::class)
object ForegroundViewModelBuilderModule {

    /**
     * Provides the [ForegroundService] mandatory [RemoteViews] wrap in a [BaseNotification] class
     */
    @ServiceScoped
    @Provides
    fun providesForegroundNotification(
        @ApplicationContext context: Context,
        res: Resources,
        @GenericNotificationView remoteViews: RemoteViews,
        service: Service
    ): ForegroundNotification {
        return ForegroundNotification(service, res, remoteViews, context)
    }

    /**
     * Provides a custom [Factory] for the [ForegroundService] [ViewModel]
     * as a work around for not being able to create the [ViewModel] directly from [Hilt]
     */
    @ServiceScoped
    @Provides
    fun providesForegroundViewModelFactory(
        @ApplicationContext context: Context,
        repo: CoreRepo,
        resources: Resources,
        countDownNotification: CountDownNotification,
        notificationManager: NotificationManager,
        dataBus: DataBus,
        sessionBus: SessionBus,
    ): ForegroundViewModelFactory {
        return ForegroundViewModelFactory(
            locationProvider = LocationProvider(context, repo, sessionBus.flow.value!!.partnership),
            countDownNotification = countDownNotification,
            notificationManager = notificationManager,
            dataBus = dataBus,
            sessionBus = sessionBus,
            repo = repo,
            resources = resources
        )
    }
}

