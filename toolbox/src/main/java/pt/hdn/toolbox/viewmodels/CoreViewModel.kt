package pt.hdn.toolbox.viewmodels

import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources

abstract class CoreViewModel(
	dataBus: DataBus,
	sessionBus: SessionBus,
	repo: CoreRepo,
	resources: Resources
): BaseViewModel<CoreRepo>(dataBus, sessionBus, repo, resources)