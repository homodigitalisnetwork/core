package pt.hdn.toolbox.viewmodels

import androidx.lifecycle.*
import androidx.navigation.NavDirections
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.Recurrence
import pt.hdn.toolbox.annotations.*
import pt.hdn.toolbox.annotations.Action.Companion.ADD
import pt.hdn.toolbox.annotations.Action.Companion.ENQUIRE
import pt.hdn.toolbox.annotations.Field.Companion.ACTION
import pt.hdn.toolbox.annotations.Field.Companion.BUYER
import pt.hdn.toolbox.annotations.Field.Companion.REF
import pt.hdn.toolbox.annotations.Field.Companion.SELLER
import pt.hdn.toolbox.annotations.Field.Companion.TYPE
import pt.hdn.toolbox.annotations.Field.Companion.UUID
import pt.hdn.toolbox.business.buyer.DefaultBuyer
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.communications.bus.*
import pt.hdn.toolbox.communications.message.Message
import pt.hdn.toolbox.communications.resumes.ResumesRequest
import pt.hdn.toolbox.communications.resumes.ResumesResponse
import pt.hdn.toolbox.communications.partnership.PartnershipsRequest
import pt.hdn.toolbox.communications.partnership.PartnershipsResponse
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.takeIfWith
import pt.hdn.toolbox.partnerships.dealership.PartnershipDialogFragmentDirections
import pt.hdn.toolbox.partnerships.dealership.adapter.TasksAdapter
import pt.hdn.toolbox.partnerships.dealership.util.PartnershipDealership
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * A base [ViewModel] for both [SpotterViewModel] and [PartnershipViewModel]
 * with the necessary functionality to create and edit a [Partnership] object
 */
abstract class PartnershipDealershipViewModel(
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : CoreViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    lateinit var partnershipDealership: PartnershipDealership
    protected val resumesRequest: MutableSharedFlow<ResumesRequest> = MutableSharedFlow()
    protected val partnershipRequest: MutableSharedFlow<PartnershipsRequest> = MutableSharedFlow()
    val resumesResponse: Flow<Result<ResumesResponse>>
    val errorBus: ErrorBus; get() = partnershipDealership.errorBus
    val recurrence: Recurrence; get() = partnershipDealership.recurrence
    val tasksAdapter: TasksAdapter; get() = partnershipDealership.tasksAdapter
    abstract val partnershipResponse: Flow<Result<PartnershipsResponse>>
    //endregion vars

    init {
        this.resumesResponse = resumesRequest
            .flatMapLatest { repo.resumes(it) }
            .onEach {
                it.success {
                    resumes
                        .takeIfWith { isNotEmpty() }
                        ?.run { forEach { with(it.speciality) { type = res.specialities[typeUUID] } }; partnershipDealership.setResumeAdapter(this, request.renew) }
                }
            }
            .shareIn(viewModelScope, Lazily)
    }

    fun addTask() { partnershipDealership.addTasks() }

    fun getResumes() {
        with(partnershipDealership) {
            launch {
                resumesRequest.emit(
                    ResumesRequest(
                        deputyUUID = session.deputyUUID,
                        partyUUID = session.partyUUID,
                        specialityUUID = tasksAdapter.firstSelected?.specialityType?.uuid
                    )
                )
            }
        }
    }

    /**
     * Set and get [Contract]'s starting date using the provided day, month and year parameters
     */
    fun setAndGetStart(year: Int, month: Int, day: Int): ZonedDateTime {
        return ZonedDateTime.ofLocal(LocalDateTime.of(year, month, day, 0, 0, 0), ZoneId.systemDefault(), null)
            .also { partnershipDealership.setStart(it) }
    }

    /**
     * Set and get [Contract]'s finishing date using the provided day, month and year parameters
     */
    fun setAndGetFinish(year: Int, month: Int, day: Int): ZonedDateTime {
        return ZonedDateTime.ofLocal(LocalDateTime.of(year, month, day, 0, 0, 0), ZoneId.systemDefault(), null)
            .also { partnershipDealership.setFinish(it) }
    }

    fun setMonth(isChecked: Boolean, value: Int) { partnershipDealership.setMonth(isChecked, value) }

    fun setMonthPeriod(value: Int) { partnershipDealership.setMonthPeriod(value) }

    fun setDays(isChecked: Boolean, value: Int) { partnershipDealership.setDays(isChecked, value) }

    fun setDow(isChecked: Boolean, value: Int) { partnershipDealership.setDow(isChecked, value) }

    fun setDayPeriod(value: Int) { partnershipDealership.setDayPeriod(value) }

    fun setAccess(@Places place: Int, isChecked: Boolean) { partnershipDealership.setAccess(place, isChecked) }

    fun removeTask(position: Int) { partnershipDealership.tasksAdapter.removeAt(position) }

    fun validate() { launch { partnershipDealership.validate() } }

    fun signatureDialogFromDialog(): NavDirections {
        return with(partnershipDealership) {
            PartnershipDialogFragmentDirections.actionPartnershipDialogFragmentToSignatureDialogFragment(isBuyerSide, false, contract)
        }
    }

    fun enquire(signedContract: Contract) {
        with(partnershipDealership) {
            contract = signedContract

            launch {
                partnershipRequest
                    .emit(
                        PartnershipsRequest(
                            command = Command.ADD,
                            partnership = partnership,
                            deputyUUID = session.deputyUUID,
                            partyUUID = session.partyUUID,
                            message = partnership.message(ENQUIRE, partnershipUUID, DefaultBuyer.from(session.partnership)),
                            action = ADD
                        )
                    )
            }
        }
    }
}