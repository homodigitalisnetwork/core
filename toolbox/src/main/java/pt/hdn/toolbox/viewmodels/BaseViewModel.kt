package pt.hdn.toolbox.viewmodels

import androidx.lifecycle.viewModelScope
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.session.Session

abstract class BaseViewModel<T: CoreRepo>(
	val dataBus: DataBus,
	sessionBus: SessionBus,
	repo: T,
	resources: Resources
): BasicViewModel<T>(repo, resources) {

	//region vars
	protected lateinit var session: Session
	//endregion vars

	init { sessionBus.observe(viewModelScope) { it?.let { this.session = it} } }
}