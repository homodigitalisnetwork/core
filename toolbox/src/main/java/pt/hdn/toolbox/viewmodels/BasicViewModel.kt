package pt.hdn.toolbox.viewmodels

import androidx.annotation.ArrayRes
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources

abstract class BasicViewModel<T: CoreRepo>(
    protected val repo: T,
    protected val res: Resources
 ): ViewModel() {
    fun string(@StringRes id: Int) = res.string(id)

    fun string(format: String, vararg args: Any) = res.string(format, args)

    fun stringArray(@ArrayRes id: Int): Array<String> = res.stringArray(id)
}