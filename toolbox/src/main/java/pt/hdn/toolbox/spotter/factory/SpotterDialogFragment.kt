package pt.hdn.toolbox.spotter.factory

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.View.VISIBLE
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.toolbox.R
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.databinding.DialogResumeBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.observeWith
import pt.hdn.toolbox.misc.onScrollListener
import pt.hdn.toolbox.spotter.SpotterViewModel
import pt.hdn.toolbox.spotter.factory.adapter.IntroViewHolder
import pt.hdn.toolbox.spotter.factory.adapter.SellerAdapter

/**
 * A [DialogFragment] that inherits from [BindingDialogFragment] that presents the selected specialist information
 */
@AndroidEntryPoint
class SpotterDialogFragment: BindingDialogFragment<DialogResumeBinding>(DialogResumeBinding::inflate, 80), OnClickListener {

    //region vars
    private val spotterViewModel: SpotterViewModel by navGraphViewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(spotterViewModel) {
                recResumeContainer
                    .apply {
                        setHasFixedSize(true)
                        adapter = sellerAdapter

                        addOnScrollListener(
                            onScrollListener {
                                onScrolled { rec, _, _ ->
                                    with(rec.layoutManager as LinearLayoutManager) {
                                        if (findLastVisibleItemPosition() == (itemCount - 1) && !btnResumePositive.isOrWillBeShown) btnResumePositive.show()
                                    }
                                }
                            }
                        )
                    }

                btnResumePositive.apply { visibility = VISIBLE; setOnClickListener(this@SpotterDialogFragment) }

                photoResponse
//                    .observe(viewLifecycleOwner) { it.success { (binding?.recResumeContainer?.getChildAt(0) as? IntroViewHolder?)?.setImage() } }
                    .observe(viewLifecycleOwner) { it.success { binding?.recResumeContainer?.getChildAt(0) } }
            }
        }
    }

    override fun onClick(v: View?) {
        v?.id
            ?.let {
                with(spotterViewModel) {
                    when (it) {
                        R.id.btn_resumePositive -> {
                            clearMarkers()

                            inMarketType
                                .let {
                                    when {
                                        it == (PERSON + SERVICE) -> navigateTo(selectorDialog())
                                        (it and PERSON) == PERSON -> { addPartnership(); navigateTo(SpotterDialogFragmentDirections.actionSpotterDialogFragmentToPartnershipDialogFragment()) }
                                        (it and SERVICE) == SERVICE -> { enquire(); popBack() } }
                                }
                        }
                    }
                }
            }
    }
}