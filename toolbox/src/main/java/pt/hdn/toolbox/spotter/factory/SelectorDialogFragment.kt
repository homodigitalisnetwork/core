package pt.hdn.toolbox.spotter.factory

import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.R
import pt.hdn.toolbox.binding.BindingDialogFragment
import pt.hdn.toolbox.databinding.DialogSelectorBinding
import pt.hdn.toolbox.misc.navGraphViewModels
import pt.hdn.toolbox.spotter.SpotterViewModel

/**
 * A [DialogFragment] that inherits from [BindingDialogFragment] and is task to sort if the user want to hire the service or the person
 */
@AndroidEntryPoint
class SelectorDialogFragment: BindingDialogFragment<DialogSelectorBinding>(DialogSelectorBinding::inflate, 40), OnClickListener {

    //region vars
    private val spotterViewModel: SpotterViewModel by navGraphViewModels()
    //endregion vars

    override fun onStart() { super.onStart(); dialog?.window?.setLayout(MATCH_PARENT, WRAP_CONTENT) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding!!) {
            btnSelectorPerson.setOnClickListener(this@SelectorDialogFragment)
            btnSelectorService.setOnClickListener(this@SelectorDialogFragment)

            lblSelectorTitle.text = string(R.string.wish)
            lblSelectorPersonQuestion.text = string(R.string.wirePerson)
            lblSelectorServiceQuestion.text = string(R.string.requestService)
        }
    }

    override fun onClick(v: View?) {
        v?.id
            ?.let {
                with(spotterViewModel) {
                    when(it){
                        R.id.btn_selectorPerson -> { addPartnership(); navigateTo(SelectorDialogFragmentDirections.actionSelectorDialogFragmentToPartnershipDialogFragment()) }
                        R.id.btn_selectorService -> { enquire(); popBackTo(R.id.spotterDialogFragment) } }
                }
            }
    }
}