package pt.hdn.toolbox.spotter.factory.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.viewbinding.ViewBinding
import pt.hdn.contract.annotations.Market.Companion.PERSON
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.annotations.Seller.Companion.INTRO
import pt.hdn.toolbox.annotations.Seller.Companion.PERSON_MARKET
import pt.hdn.toolbox.annotations.Seller.Companion.SCORE
import pt.hdn.toolbox.annotations.Seller.Companion.SERVICE_MARKET
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.misc.toInt
import pt.hdn.toolbox.partnerships.dealership.resume.adapter.MarketViewHolder
import pt.hdn.toolbox.partnerships.dealership.resume.adapter.PerformanceViewHolder
import pt.hdn.toolbox.partnerships.dealership.resume.adapter.ScoreViewHolder
import pt.hdn.toolbox.resources.Resources

/**
 * An [Adapter] class that translates a [Seller] information into ViewHolders for any [View] that the adapter is attach to
 */
class SellerAdapter(
    val seller: Seller,
    private val res: Resources
) : Adapter<BindingViewHolder<ViewBinding>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<ViewBinding> {
        return when (viewType) {
            INTRO -> IntroViewHolder(res, parent)
            SCORE -> ScoreViewHolder(res, parent)
            PERSON_MARKET -> MarketViewHolder(PERSON, res, parent)
            else /*SERVICE_MARKET*/ -> MarketViewHolder(SERVICE, res, parent)
        }
    }

    override fun onBindViewHolder(holder: BindingViewHolder<ViewBinding>, position: Int) {
        with(seller) seller@{
            with(holder) {
                when (this) {
                    is IntroViewHolder -> this@seller.setViewHolder()
                    is ScoreViewHolder -> specialityScore.setViewHolder()
                    is MarketViewHolder -> speciality!!.setViewHolder()
                    else -> null
                }
            }
        }
    }

    override fun getItemCount(): Int = with(seller) { 1 + (speciality?.score != null).toInt() + (speciality?.personSchemas != null).toInt() + (speciality?.serviceSchemas != null).toInt() }

    override fun getItemViewType(position: Int): Int {
        return with(seller) {
            when (position) {
                0 -> INTRO
                (0 + (speciality?.score != null).toInt()) -> SCORE
//                (0 + (speciality?.score != null).toInt() + (speciality?.personSchemas != null).toInt()) -> PERSON_MARKET
                (0 + (speciality?.score != null).toInt() + /*(speciality?.personSchemas != null).toInt() + */(speciality?.serviceSchemas != null).toInt()) -> SERVICE_MARKET
                else -> -1
            }
        }
    }
}