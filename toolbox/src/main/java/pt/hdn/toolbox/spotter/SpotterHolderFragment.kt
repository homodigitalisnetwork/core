package pt.hdn.toolbox.spotter

import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.databinding.FragmentSpotterHolderBinding

/**
 * The [Fragment] holder of Spotter [NavGraoh]. A solution created so that the [NavGraph] could exists in a fragmented but scalable ecosystem
 */
@AndroidEntryPoint
class SpotterHolderFragment: BindingFragment<FragmentSpotterHolderBinding>(FragmentSpotterHolderBinding::inflate)