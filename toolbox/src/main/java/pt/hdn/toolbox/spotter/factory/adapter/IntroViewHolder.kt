package pt.hdn.toolbox.spotter.factory.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.databinding.ViewholderIntroBinding
import pt.hdn.toolbox.resources.Resources

/**
 * A specialize [ViewHolder] that inherits from [BindingViewHolder]
 * and shows the photo, name and spoken languages of the seller
 */
class IntroViewHolder(
    res: Resources,
    parent: ViewGroup
): BindingViewHolder<ViewholderIntroBinding>(res, parent, ViewholderIntroBinding::inflate) {
    fun Seller.setViewHolder() { binding.introContainer.apply { resources = res; data = this@setViewHolder } }

    fun setImage() { binding.introContainer.setPhoto() }
}