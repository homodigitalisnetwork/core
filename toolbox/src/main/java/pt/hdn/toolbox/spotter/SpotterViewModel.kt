package pt.hdn.toolbox.spotter

import android.annotation.SuppressLint
import android.content.Context
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import androidx.activity.result.IntentSenderRequest
import androidx.lifecycle.*
import androidx.navigation.NavDirections
import com.google.android.gms.maps.UiSettings
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.CameraPosition.fromLatLngZoom
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Eagerly
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.contract.annotations.Market.Companion.SERVICE
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.annotations.Action.Companion.ENQUIRE
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.foreground.ForegroundService
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.partnership.PartnershipsResponse
import pt.hdn.toolbox.communications.photo.PhotoRequest
import pt.hdn.toolbox.communications.photo.PhotoResponse
import pt.hdn.toolbox.communications.spotter.SearchRequest
import pt.hdn.toolbox.communications.spotter.SearchResponse
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.partnerships.dealership.util.PartnershipDealership
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.spotter.factory.SpotterDialogFragmentDirections
import pt.hdn.toolbox.spotter.factory.adapter.SellerAdapter
import pt.hdn.toolbox.spotter.util.SpotterFactory
import pt.hdn.toolbox.viewmodels.PartnershipDealershipViewModel
import java.util.*
import javax.inject.Inject

/**
 * A [ViewModel] that inherits from [PartnershipDealershipViewModel] so that it can provide dependent worker hiring functionality
 */
@SuppressLint("StaticFieldLeak")
@HiltViewModel
class SpotterViewModel @Inject constructor(
    private val spotterFactory: SpotterFactory,
    foregroundServiceFlow: Flow<ForegroundService?>,
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : PartnershipDealershipViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    private var page: Int = 0
    private var foregroundService: ForegroundService?  = null
    lateinit var uiSettings: UiSettings
    var isBottomSheetOpen: Boolean = false; set(value) { field = value; with(uiSettings) { value.not().let { isZoomGesturesEnabled = it; isScrollGesturesEnabled = it }  } }
    var cameraPosition: CameraPosition = fromLatLngZoom(spotterFactory.location, 13f)
    var seller: Seller?; get() = spotterFactory.seller; set(value) { spotterFactory.seller = value; value?.run { photo ?: run { getPhoto(deputyUUID) } } }
    var position: Position; get() = spotterFactory.position; set(value) { spotterFactory.position = value }
    var range: Double; get() = spotterFactory.range; set(value) { spotterFactory.range = value }
    private val searchRequest: MutableSharedFlow<SearchRequest> = MutableSharedFlow()
    private val mutableConfig: MutableSharedFlow<IntentSenderRequest> = MutableSharedFlow()
    private val mutablePermission: MutableSharedFlow<String> = MutableSharedFlow()
    private val positionRequest: MutableSharedFlow<ForegroundService> = MutableSharedFlow()
    private val photoRequest: MutableSharedFlow<PhotoRequest> = MutableSharedFlow()
    val sellerAdapter: SellerAdapter; get() = spotterFactory.sellerAdapter
    val inMarketType: Int; get() = spotterFactory.inMarketType
    val location: LatLng; get() = spotterFactory.location
    val searchResponse: Flow<SearchResponse?>
    val config: Flow<IntentSenderRequest> = mutableConfig
    val permission: Flow<String> = mutablePermission
    val photoResponse: Flow<Result<PhotoResponse>>
    val positionResponse: Flow<Result<Position>>
    override val partnershipResponse: Flow<Result<PartnershipsResponse>>
//    private val performanceRequest: MutableSharedFlow<PerformanceRequest> = MutableSharedFlow()
//    val performanceResponse: Flow<PerformanceHandler?>
    //endregion vars

    init {
        foregroundServiceFlow.observe(viewModelScope) { this.foregroundService = it }

        this.searchResponse = searchRequest
            .flatMapLatest { repo.search(it) }
            .map { var searchResponse: SearchResponse? = null; it.success { searchResponse = SearchResponse(sellers, spotterFactory.markers) }; searchResponse }

//        this.performanceResponse = performanceRequest
//            .flatMapLatest { repo.performance(it)  }
//            .map {
//                var performanceHandler: PerformanceHandler? = null
//
//                it.success {
//                    this@SpotterViewModel.performanceHandler = DefaultPerformanceHandler(res, deputiesStats, populationStats)
//
//                    performanceHandler = this@SpotterViewModel.performanceHandler
//                }
//
//                performanceHandler
//            }

        this.positionResponse = positionRequest
            .flatMapLatest { it.getOneShot() }
            .onEach { it.success { clearMarkers(); spotterFactory.position = this } }

        this.photoResponse = photoRequest
            .flatMapLatest { repo.photo(it) }
            .onEach { it.success { seller?.photo = photo } }
            .shareIn(viewModelScope, Lazily)
    }

//    fun getPerformance() {
//        launch {
//            with(seller!!) {
//                performanceRequest.emit(
//                    PerformanceRequest(
//                        uuid = party!!.uuid!!,
//                        specialityUUID = speciality!!.typeUUID!!,
//                        isDeputy = true
//                    )
//                )
//            }
//        }
//    }

    /**
     * Creates an [ArrayAdapter] for the speciality [Spinner]
     * NOTE: Only the independent market is available,
     * in which, requires to filter out all specialities that are available only to the dependent market
     */
    fun getSpecialitiesAdapter(context: Context): SpinnerAdapter {
        return ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, res.specialities.values.filter { (it.market and SERVICE) == SERVICE }.toList())
    }

    fun getPermission() { foregroundService?.run { launch { getPermission(mutablePermission, mutableConfig) { positionRequest.emit(this@run) } } } }

    fun getLocation() { foregroundService?.run { launch { getLocation(mutableConfig) { positionRequest.emit(this@run) } } } }

    fun getOneShot() { foregroundService?.run { launch { positionRequest.emit(this@run) } } }

    /**
     * Creates a [Dealership] object for a [Partnership]
     */
    fun addPartnership() { this.partnershipDealership = PartnershipDealership.fromNew(res, spotterFactory.partnership) }

    /**
     * Sets the speciality type being search
     */
    fun setType(type: UUIDNameMarketData) { spotterFactory.type = type }

    fun getSearch() {
        clearMarkers()

        launch {
            searchRequest.emit(
                SearchRequest(
                    deputyUUID = session.deputyUUID,
                    partyUUID = session.partyUUID,
                    specialityUUID = spotterFactory.typeUUID,
                    range = range.toFloat(),
                    position = position
                )
            )
        }
    }

    fun clearMarkers() { with(spotterFactory) { clearMarkers(); this@SpotterViewModel.cameraPosition = fromLatLngZoom(location, 13f) } }

    /**
     * Gets the [NavDirections] to the [DialogFragment] responsible to sort out if the user wants to hire the person or its services
     */
    fun selectorDialog(): NavDirections = SpotterDialogFragmentDirections.actionSpotterDialogFragmentToSelectorDialogFragment()

    /**
     * Initiate the transaction process by making the user temporary unavailable if it is visible on any market
     * and signal to whom is responsible in launching the transaction [NavGraph] via [DataBus]
     */
    fun enquire() { foregroundService?.holdBroadcast(); dataBus.send(viewModelScope, Address.TRANSACTION, ENQUIRE to spotterFactory.transaction) }

    /**
     * It requests the [ViewPager2] holder to lock scrolling functionality. It is necessary when you have a nested swipeable view in a swipeable view
     */
    fun increasePageable() { dataBus.send(viewModelScope, Address.PAGEABLE, ++page) }

    /**
     * It requests the [ViewPager2] holder to unlock scrolling functionality. It is necessary when you have a nested swipeable view in a swipeable view
     */
    fun decreasePageable() { dataBus.send(viewModelScope, Address.PAGEABLE, (page - 1).coerceAtLeast(0).also { this.page = it }) }

    /**
     * Request the specialist photo using the provided [UUID]
     */
    private fun getPhoto(deputyUUID: UUID) { launch { photoRequest.emit(PhotoRequest(deputyUUID)) } }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------

    init { this.partnershipResponse = partnershipRequest.flatMapLatest { repo.partnerships(it) } }
}