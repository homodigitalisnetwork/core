package pt.hdn.toolbox.spotter.util

import com.google.android.gms.maps.CameraUpdateFactory.newLatLng
import com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil.computeOffset
import com.google.maps.android.ktx.addCircle
import com.google.maps.android.ktx.addMarker
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Value.Companion.RANGE_OFFSET
import pt.hdn.toolbox.annotations.Value.Companion.SQRT2
import pt.hdn.toolbox.resources.Resources

/**
 * A class that manages a [Marker]-[Circle] combo in a [MapView]
 */
class SearchTarget(
    private val googleMap: GoogleMap,
    res: Resources,
    ll: LatLng
) {

    //region vars
    private val circle: Circle
    private val marker: Marker
    var position: LatLng; get() = marker.position; set(value) { with(marker) { position = value; alpha = 1f }; circle.center = value; focus() }
    var radius: Double; get() = circle.radius; set(value) { circle.radius = value; focus() }
    //endregion vars

    init {
        with(googleMap) {
            this@SearchTarget.circle = addCircle {
                center(ll)
                clickable(false)
                strokeColor(res.color(R.color.hdn))
                strokePattern(mutableListOf(Dash(10f), Gap(8f)))
                radius(RANGE_OFFSET)
                zIndex(0f)
            }

            this@SearchTarget.marker = addMarker {
                position(ll)
                draggable(true)
                icon(res.pins.values.first().bitmapDescriptor)
            }!!
        }
    }

    override fun equals(other: Any?): Boolean = when (other) { is Marker -> marker == other; else -> super.equals(other) }

    /**
     * Centers the circle on the marker and bright the marker
     */
    fun onMarkerDragStart() { with(marker) { circle.center = position; alpha = 1f } }

    /**
     * Centers the circle on the marker
     */
    fun onMarkerDrag() { circle.center = marker.position }

    fun dim() { marker.alpha = 0.5f }

    fun bright() { marker.alpha = 1f }

    /**
     * Centers and zooms the marker on the [GoogleMap]
     */
    fun focus() {
        with(circle) {
            (radius * SQRT2)
                .let { googleMap.animateCamera(newLatLngBounds(LatLngBounds(computeOffset(center, it, 225.0), computeOffset(center, it, 45.0)), 100)) }
        }
    }

    /**
     * Centers the marker on the [GoogleMap]
     */
    fun center(marker: Marker? = null) { googleMap.animateCamera(newLatLng((marker ?: this.marker).position)) }
}