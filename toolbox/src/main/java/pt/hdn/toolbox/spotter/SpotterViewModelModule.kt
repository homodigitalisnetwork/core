package pt.hdn.toolbox.spotter

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.buyer.DefaultBuyer
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.spotter.util.SpotterFactory

@Module
@InstallIn(ViewModelComponent::class)
object SpotterViewModelModule {

    /**
     * Provides the [Buyer] object built from the in session [Partnership] that will be used throughout a transaction
     */
    @ViewModelScoped
    @Provides
    fun providesBuyer(sessionBus: SessionBus): Buyer = DefaultBuyer.from(sessionBus.flow.value!!.partnership)

    /**
     * Provides a [SpotterFactory] for [SpotterViewModel]
     */
    @ViewModelScoped
    @Provides
    fun providesSpotterFactory(res: Resources, buyer: Buyer): SpotterFactory = SpotterFactory(res, buyer)
}