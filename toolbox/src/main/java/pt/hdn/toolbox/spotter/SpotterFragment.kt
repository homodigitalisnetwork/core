package pt.hdn.toolbox.spotter

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.os.Bundle
import android.view.View
import android.view.View.*
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts.StartIntentSenderForResult
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.lifecycle.Lifecycle.State.RESUMED
import com.google.android.gms.maps.CameraUpdateFactory.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.*
import com.google.android.material.slider.Slider
import com.google.android.material.slider.Slider.OnChangeListener
import com.google.maps.android.ktx.addMarker
import dagger.hilt.android.AndroidEntryPoint
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Value.Companion.RANGE_MAX
import pt.hdn.toolbox.annotations.Value.Companion.RANGE_OFFSET
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.databinding.FragmentSpotterBinding
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.spotter.util.SearchTarget
import pt.hdn.toolbox.views.OnMapCallback

/**
 * A [Fragment] that inherits from [BindingFragment] and handles the search for a specialist
 */
@AndroidEntryPoint
class SpotterFragment : BindingFragment<FragmentSpotterBinding>(FragmentSpotterBinding::inflate), OnClickListener, OnChangeListener, OnItemSelectedListener, OnMapCallback, OnMarkerClickListener, OnMarkerDragListener, OnCameraIdleListener {

    //region vars
    private val spotterViewModel: SpotterViewModel by navGraphViewModels()
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>
    private lateinit var searchTarget: SearchTarget
    private var googleMap: GoogleMap? = null
    private lateinit var requestPermission: ActivityResultLauncher<String>
    private lateinit var requestPosition: ActivityResultLauncher<IntentSenderRequest>
    //endregion vars

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.requestPermission = registerForActivityResult(RequestPermission()) { if (it) spotterViewModel.getLocation() else binding?.barSpotterLoading?.visibility = GONE }
        this.requestPosition = registerForActivityResult(StartIntentSenderForResult()) {
            if (it.resultCode == RESULT_OK) spotterViewModel.getOneShot() else  binding?.barSpotterLoading?.visibility = GONE
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(spotterViewModel) {
                this@SpotterFragment.bottomSheetBehavior = from(frlSpotterSearch)
                    .apply {
                        addBottomSheetCallback(
                            onBottomSheetCallback {
                                onStateChanged { _, newState ->
                                    when (newState) {
                                        STATE_DRAGGING -> increasePageable()
                                        STATE_SETTLING -> decreasePageable()
                                        STATE_EXPANDED -> isBottomSheetOpen = true
                                        STATE_COLLAPSED -> isBottomSheetOpen = false
                                    }
                                }
                            }
                        )

                        state = if (isBottomSheetOpen) STATE_EXPANDED else STATE_COLLAPSED
                    }

                lblSpotterQuestion.text = string(R.string.whatSpeciality)
                btnSpotterGPS.setOnClickListener(this@SpotterFragment)
                btnSpotterSearch.setOnClickListener(this@SpotterFragment)

                sldSpotterRange
                    .apply {
                        valueFrom = RANGE_OFFSET.toFloat()
                        valueTo = (RANGE_MAX + RANGE_OFFSET).toFloat()
                        value = RANGE_OFFSET.toFloat()
                        addOnChangeListener(this@SpotterFragment)
                    }

                spnSpotterSpeciality.apply { onItemSelectedListener = this@SpotterFragment; adapter = getSpecialitiesAdapter(requireContext()) }

                mapSpotterMap
                    .apply {
                        isIntercepting.observe(viewLifecycleOwner) { if (it) increasePageable() else decreasePageable() }

                        onCreate(null)
                        onResume()
                        getMapAsyncWithContext(this@SpotterFragment)
                    }

                    connectivity.observe(viewLifecycleOwner, state = RESUMED) { btnSpotterSearch.apply { isEnabled = it; isClickable = it } }
            }
        }
    }

    /**
     * Google recommended overrides when using a [MapView]
     */
    override fun onResume() { super.onResume(); binding!!.mapSpotterMap.onResume() }

    override fun onPause() { binding?.mapSpotterMap?.onPause(); super.onPause() }

    override fun onStop() { binding?.mapSpotterMap?.onStop(); super.onStop() }

    override fun onSaveInstanceState(outState: Bundle) { binding?.mapSpotterMap?.onSaveInstanceState(outState); super.onSaveInstanceState(outState) }

    override fun onDestroyView() { this.googleMap = googleMap?.run { clear(); null }; binding?.mapSpotterMap?.onDestroy(); super.onDestroyView() }

    override fun onLowMemory() { binding?.mapSpotterMap?.onLowMemory(); super.onLowMemory() }

    override fun onClick(view: View?) {
        view
            ?.id
            ?.let {
                with(spotterViewModel) {
                    when (it) {
                        R.id.btn_spotterGPS -> { getPermission(); binding!!.barSpotterLoading.visibility = VISIBLE }
                        R.id.btn_spotterSearch -> with(bottomSheetBehavior) {
                            when (state) {
                                STATE_EXPANDED -> { search(); increasePageable(); state = STATE_COLLAPSED }
                                STATE_COLLAPSED -> { increasePageable(); state = STATE_EXPANDED }
                            }
                        }
                    }
                }
            }
    }

    /**
     * Function that handles the [Slider] value change.
     * NOTE: No other solution was found to avoid the "RestrictedApi"
     */
    @SuppressLint("RestrictedApi")
    override fun onValueChange(slider: Slider, value: Float, fromUser: Boolean) { if (fromUser) { value.toDouble().let { searchTarget.radius = it; spotterViewModel.range = it } } }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        (parent?.adapter as? ArrayAdapter<UUIDNameMarketData>)?.getItem(position)?.let { spotterViewModel.setType(it) }
    }

    /**
     * Given it is being used a [EnhanceMapView] instead of a regular [MapView] the [GoogleMap] object is passed as context rather that the usual parameter.
     * The function set all dependencies related wth [GoogleMap] object
     */
    override fun GoogleMap.onMapReady() {
        with(spotterViewModel) {
            this@SpotterFragment.googleMap = this@onMapReady

            this.uiSettings = this@onMapReady.uiSettings.apply { isRotateGesturesEnabled = false; isTiltGesturesEnabled = false; isMapToolbarEnabled = false }

            mapType = MAP_TYPE_NORMAL

            setMapStyle(loadRawResourceStyle(requireContext(), if (res.isDarkModeOn) R.raw.map_styling_dark else R.raw.map_styling_light))
            setOnMarkerClickListener(this@SpotterFragment)
            setOnMarkerDragListener(this@SpotterFragment)
            setOnCameraIdleListener(this@SpotterFragment)

            this@SpotterFragment.searchTarget = SearchTarget(this@onMapReady, res, location)

            moveCamera(newCameraPosition(cameraPosition))

            config.observe(viewLifecycleOwner) { requestPosition.launch(it) }

            permission.observe(viewLifecycleOwner) { requestPermission.launch(it) }

            positionResponse
                .observeWith(viewLifecycleOwner) {
                    success { searchTarget.position = toLatLng(); toast(R.string.dragForPrecision) }
                    error { _, _ -> toast(R.string.failLocation) }; binding!!.barSpotterLoading.visibility = GONE
                }

            searchResponse
                .observe(viewLifecycleOwner) {
                    binding!!.barSpotterLoading.visibility = GONE

                    searchTarget.focus()

                    when {
                        it?.sellers == null -> { searchTarget.bright(); toast(R.string.failToGetSpecialists) }
                        it.sellers.isEmpty() -> { searchTarget.bright(); toast(R.string.noSpecialists) }
                        else -> {
                            searchTarget.dim()

                            it.sellers
                                .forEach { seller ->
                                    addMarker {
                                        position(seller.position!!.toLatLng()); icon(res.pins.run { getOrDefault(seller.partyAppUUID, values.first()) }.bitmapDescriptor); zIndex(2f)
                                    }?.apply { tag = seller; it.markers.add(this) }
                                }
                        }
                    }
                }
        }
    }

    override fun onMarkerDragStart(marker: Marker) { with(spotterViewModel) { increasePageable(); clearMarkers(); searchTarget.onMarkerDragStart() } }

    override fun onMarkerDrag(marker: Marker) { searchTarget.onMarkerDrag() }

    override fun onMarkerDragEnd(marker: Marker) { with(spotterViewModel) { decreasePageable(); position = Position.from(marker.position); searchTarget.focus() } }

    override fun onMarkerClick(marker: Marker): Boolean {
        if (searchTarget.equals(marker)) searchTarget.focus()
        else {
            searchTarget.center(marker)

            spotterViewModel.seller = marker.tag as Seller

            navigateTo(SpotterFragmentDirections.actionSpotterFragmentToSpotterDialogFragment())
        }

        return true
    }

    override fun onCameraIdle() { googleMap?.let { spotterViewModel.cameraPosition = it.cameraPosition } }

    /**
     * Executes a search for specialists given the speciality, location and range
     */
    private fun search() { binding!!.barSpotterLoading.visibility = VISIBLE; spotterViewModel.getSearch() }

    //region NOT IN USE
    override fun onNothingSelected(p0: AdapterView<*>?) { }
    //endregion NOT IN USE
}