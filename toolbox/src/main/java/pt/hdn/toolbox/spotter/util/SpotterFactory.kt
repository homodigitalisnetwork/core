package pt.hdn.toolbox.spotter.util

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import pt.hdn.contract.annotations.Market
import pt.hdn.contract.annotations.Market.Companion.NONE
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.annotations.Value.Companion.RANGE_OFFSET
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.seller.Seller
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.partnerships.util.DefaultPartnership
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.spotter.factory.adapter.SellerAdapter
import pt.hdn.toolbox.transaction.util.DefaultTransaction
import pt.hdn.toolbox.transaction.util.Transaction
import java.util.*

/**
 * A factory that produces a [Transaction] or a [Partnership], depending if the user wants to hire a service or the person
 */
class SpotterFactory(
    private val res: Resources,
    val buyer: Buyer
) {
    //region vars
    val location: LatLng; get() = buyer.position!!.toLatLng()
    val markers: MutableList<Marker> = mutableListOf()
    val transaction: Transaction; get() = DefaultTransaction.from(res, seller!!, buyer)
    val partnership: Partnership; get() = DefaultPartnership.from(res, seller!!, buyer)
    val typeUUID: UUID; get() = type.uuid
    @Market val inMarketType: Int; get() = seller?.inMarketType ?: NONE
    var range: Double = RANGE_OFFSET
    var position: Position; get() = buyer.position!!; set(value) { buyer.position = value }
    var seller: Seller? = null; set(value) { field = value; value?.let { this.sellerAdapter = SellerAdapter(it, res) } }
    lateinit var type: UUIDNameMarketData
    lateinit var sellerAdapter: SellerAdapter; private set
    //endregion vars

    fun clearMarkers() { markers.apply { forEachWith { remove() }; clear() } }
}