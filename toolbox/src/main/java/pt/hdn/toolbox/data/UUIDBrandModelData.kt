package pt.hdn.toolbox.data

import com.google.gson.annotations.Expose

data class UUIDBrandModelData(
    @Expose val uuid: String,
    @Expose val type: Int,
    @Expose val brand: String,
    @Expose val model: String
) {
    override fun toString() = "$brand $model"
}