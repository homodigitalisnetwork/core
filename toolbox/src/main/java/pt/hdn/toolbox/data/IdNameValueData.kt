package pt.hdn.toolbox.data

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdNameValueData(
    @Expose var id: Int = -1,
    @Expose var value: String = "",
    @Expose var name: String = ""
) : Parcelable {
    override fun toString() = name
}