package pt.hdn.toolbox.report.adapter.exchange

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.misc.WindowBounds
import pt.hdn.toolbox.report.adapter.ReportData
import java.util.*

data class ExchangeData(
    @Expose override val uuid: UUID,
    @Expose val windowBounds: WindowBounds,
    @Expose val order: List<UUID>,
    @Expose val trades: Map<Long, Map<UUID, List<SpecialityTrade>>>
) : ReportData
