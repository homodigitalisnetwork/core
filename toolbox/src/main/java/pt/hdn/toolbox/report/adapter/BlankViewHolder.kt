package pt.hdn.toolbox.report.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.databinding.ViewholderBlankBinding
import pt.hdn.toolbox.resources.Resources

class BlankViewHolder(
    resources: Resources,
    parent: ViewGroup
) : ReportViewHolder<ViewholderBlankBinding>(resources, parent, ViewholderBlankBinding::inflate) { override fun Report.setViewHolder() { } }