package pt.hdn.toolbox.report.adapter.exchange

import com.google.gson.annotations.Expose
import java.math.BigDecimal

data class SpecialityTrade(
    @Expose val isBuyerSide: Boolean,
    @Expose val amount: BigDecimal,
    @Expose val volume: Int
)
