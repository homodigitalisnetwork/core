package pt.hdn.toolbox.report.adapter.activity

import android.view.ViewGroup
import pt.hdn.toolbox.databinding.ViewholderActivityBinding
import pt.hdn.toolbox.report.adapter.Report
import pt.hdn.toolbox.report.adapter.ReportData
import pt.hdn.toolbox.report.adapter.ReportViewHolder
import pt.hdn.toolbox.resources.Resources

class ActivityViewHolder(
    resources: Resources,
    parent: ViewGroup
) : ReportViewHolder<ViewholderActivityBinding>(resources, parent, ViewholderActivityBinding::inflate) {
    override fun Report.setViewHolder() { this as ActivityDataSet; binding.chartActivityChart.apply { res = this@ActivityViewHolder.res; dataSet = this@setViewHolder } }
}