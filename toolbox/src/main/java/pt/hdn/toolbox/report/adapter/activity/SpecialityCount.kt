package pt.hdn.toolbox.report.adapter.activity

import com.google.gson.annotations.Expose
import java.util.*

data class SpecialityCount(
    @Expose val specialityUUID: UUID,
    @Expose val count: Int
)
