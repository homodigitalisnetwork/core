package pt.hdn.toolbox.report.adapter.exchange

import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import pt.hdn.toolbox.misc.forEachDay
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.report.adapter.Report
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.util.*

class ExchangeDataSet(
    exchangeData: ExchangeData,
    res: Resources
) : Report {

    //region vars
    val revenueDataSet: BarDataSet
    val costDataSet: BarDataSet
    var weekCost: BigDecimal; private set
    var weekRevenue: BigDecimal; private set
    val order: List<UUID> = exchangeData.order
    //endregion vars

    init {
        with(exchangeData) {
            val revenueEntries: MutableList<BarEntry> = mutableListOf()
            val costEntries: MutableList<BarEntry> = mutableListOf()

            this@ExchangeDataSet.weekCost = ZERO
            this@ExchangeDataSet.weekRevenue = ZERO

            windowBounds
                .forEachDay {
                    order
                        .foldIndexed(Pair(FloatArray(order.size), FloatArray(order.size))) { index, acc, specialityUUID ->
                            trades[it]
                                ?.get(specialityUUID)
                                ?.forEachWith { with(acc) { if (isBuyerSide) { weekCost += amount; first } else { weekRevenue += amount; second } }[index] = amount.toFloat() }

                            acc
                        }
                        .apply { it.toFloat().let { costEntries.add(BarEntry(it, first)); revenueEntries.add(BarEntry(it, second)) } }
                }

            this@ExchangeDataSet.revenueDataSet = BarDataSet(revenueEntries, null)
            this@ExchangeDataSet.costDataSet = BarDataSet(costEntries, null)
                .apply {
                    if (order.size == 1) this.label = res.specialities[order.first()]?.name ?: "Unknown"
                    else this.stackLabels = order.map { res.specialities[it]?.name ?: "Unknown" }.toTypedArray()
                }
        }
    }
}
