package pt.hdn.toolbox.report.adapter.activity

import com.google.gson.annotations.Expose
import pt.hdn.toolbox.misc.WindowBounds
import pt.hdn.toolbox.report.adapter.ReportData
import java.util.*

data class ActivityData(
    @Expose override val uuid: UUID,
    @Expose val activity: List<SpecialityCount>
) : ReportData
