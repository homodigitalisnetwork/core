package pt.hdn.toolbox.report.adapter.exchange

import android.view.ViewGroup
import pt.hdn.toolbox.databinding.ViewholderExchangeBinding
import pt.hdn.toolbox.report.adapter.Report
import pt.hdn.toolbox.report.adapter.ReportData
import pt.hdn.toolbox.report.adapter.ReportViewHolder
import pt.hdn.toolbox.resources.Resources

class ExchangeViewHolder(
    resources: Resources,
    parent: ViewGroup
) : ReportViewHolder<ViewholderExchangeBinding>(resources, parent, ViewholderExchangeBinding::inflate) {
    override fun Report.setViewHolder() { this as ExchangeDataSet; binding.chartExchangeChart.apply { res = this@ExchangeViewHolder.res; dataSet = this@setViewHolder } }
}