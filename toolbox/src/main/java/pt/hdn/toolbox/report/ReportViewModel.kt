package pt.hdn.toolbox.report

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Eagerly
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.report.ReportRequest
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.misc.launch
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.report.adapter.ReportAdapter
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.misc.WindowBounds
import pt.hdn.toolbox.report.adapter.activity.ActivityData
import pt.hdn.toolbox.report.adapter.activity.ActivityDataSet
import pt.hdn.toolbox.report.adapter.exchange.ExchangeData
import pt.hdn.toolbox.report.adapter.exchange.ExchangeDataSet
import pt.hdn.toolbox.viewmodels.CoreViewModel
import javax.inject.Inject

@HiltViewModel
class ReportViewModel @Inject constructor(
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : CoreViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    private val request: MutableSharedFlow<ReportRequest> = MutableSharedFlow()
    val response: Flow<ReportAdapter?>
    //endregion vars

    init {
        this.response = request
            .flatMapLatest { repo.report(it) }
            .map {
                var adapter: ReportAdapter? = null

                it.success {
                    adapter = ReportAdapter(datas.mapNotNull { when (it) {is ActivityData -> ActivityDataSet(it, res); is ExchangeData -> ExchangeDataSet(it, res); else -> null; } }, res)
                }

                adapter
            }
            .flowOn(Default)
            .shareIn(viewModelScope, Eagerly, 1)
    }

    fun refresh() { launch { request.emit(ReportRequest(partyUUID = session.partyUUID, appUUID = res.appUUID, windowBounds = WindowBounds.currentWeek())) } }
}