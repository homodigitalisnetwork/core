package pt.hdn.toolbox.report.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.viewbinding.ViewBinding
import pt.hdn.toolbox.report.adapter.activity.ActivityDataSet
import pt.hdn.toolbox.report.adapter.activity.ActivityViewHolder
import pt.hdn.toolbox.report.adapter.exchange.ExchangeDataSet
import pt.hdn.toolbox.report.adapter.exchange.ExchangeViewHolder
import pt.hdn.toolbox.resources.Resources

class ReportAdapter(
    private val ref: List<Report>,
    private val res: Resources
): Adapter<ReportViewHolder<ViewBinding>>() {

    //region vars
    //endregion vars

    private companion object {
        private const val ELSE = -1
        private const val ACTIVITY = 0
        private const val EXCHANGE = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportViewHolder<ViewBinding> {
        return when (viewType) {ACTIVITY -> ActivityViewHolder(res, parent); EXCHANGE -> ExchangeViewHolder(res, parent); else -> BlankViewHolder(res, parent) }
    }

    override fun onBindViewHolder(holder: ReportViewHolder<ViewBinding>, position: Int) { with(holder) { ref[position].setViewHolder() } }

    override fun getItemViewType(position: Int): Int { return when(ref[position]) {is ActivityDataSet -> ACTIVITY; is ExchangeDataSet -> EXCHANGE; else -> ELSE } }

    override fun getItemCount(): Int = ref.size
}