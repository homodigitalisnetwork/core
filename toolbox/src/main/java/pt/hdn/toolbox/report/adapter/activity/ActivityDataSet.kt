package pt.hdn.toolbox.report.adapter.activity

import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import pt.hdn.toolbox.report.adapter.Report
import pt.hdn.toolbox.resources.Resources

class ActivityDataSet(
    activityData: ActivityData,
    res: Resources
) : Report {

    //region vars
    val dataSet: PieDataSet
    //endregion vars

    init { this.dataSet = PieDataSet(activityData.activity.map { (specialityUUID, count) -> PieEntry(count.toFloat(), res.specialities[specialityUUID]?.name) }, null) }

    fun isEmpty(): Boolean = dataSet.values.isEmpty()
}
