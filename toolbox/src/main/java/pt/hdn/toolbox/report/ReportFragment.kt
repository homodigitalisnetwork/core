package pt.hdn.toolbox.report

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle.State.RESUMED
import androidx.lifecycle.Lifecycle.State.STARTED
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers.Main
import pt.hdn.toolbox.R
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.databinding.FragmentViewerBinding
import pt.hdn.toolbox.misc.observe
import pt.hdn.toolbox.misc.setCompoundTopDrawable
import pt.hdn.toolbox.misc.takeUnlessWith

@AndroidEntryPoint
class ReportFragment : BindingFragment<FragmentViewerBinding>(FragmentViewerBinding::inflate) {

    //region vars
    private val reportViewModel: ReportViewModel by viewModels()
    //endregion vars

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding!!) {
            with(reportViewModel) {
                lblViewerDisplay.apply { visibility = VISIBLE; setCompoundTopDrawable(R.drawable.ic_report) }

                dataBus.observe<Unit>(viewLifecycleOwner, Address.UPDATE, state = STARTED) { barViewerLoading.visibility = VISIBLE; refresh() }

                response
                    .observe(viewLifecycleOwner, state = RESUMED) {
                        barViewerLoading.visibility = GONE

                        it
                            ?.apply { lblViewerDisplay.visibility = GONE; recViewerContainer.takeUnlessWith { adapter === this@apply }?.apply { setHasFixedSize(true); adapter = it } }
                            ?: run { lblViewerDisplay.apply { setCompoundTopDrawable(R.drawable.ic_sad_face); text = string(R.string.somethingWrong) } }
                    }
            }
        }
    }
}