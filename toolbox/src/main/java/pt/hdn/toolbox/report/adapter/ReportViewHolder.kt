package pt.hdn.toolbox.report.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import pt.hdn.toolbox.adapter.BindingViewHolder
import pt.hdn.toolbox.resources.Resources

abstract class ReportViewHolder<out VB : ViewBinding>(
    resources: Resources,
    parent: ViewGroup,
    inflate: (LayoutInflater, ViewGroup, Boolean) -> VB,
) : BindingViewHolder<VB>(resources, parent, inflate) {
    abstract fun Report.setViewHolder()
}