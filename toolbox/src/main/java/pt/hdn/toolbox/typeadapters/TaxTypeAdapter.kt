package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.economics.DefaultTax
import pt.hdn.toolbox.economics.Tax
import java.lang.reflect.Type

class TaxTypeAdapter: JsonSerializer<Tax>, JsonDeserializer<Tax> {
    override fun serialize(src: Tax?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { context?.serialize(it) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Tax? = context?.deserialize(json, DefaultTax::class.java)
}