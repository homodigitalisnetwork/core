package pt.hdn.toolbox.typeadapters

import com.google.android.gms.maps.model.LatLng
import com.google.gson.*
import kotlinx.serialization.json.Json
import pt.hdn.toolbox.annotations.Field.Companion.LAT
import pt.hdn.toolbox.annotations.Field.Companion.LON
import java.lang.reflect.Type
import java.time.LocalDate
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME

class LatLngTypeAdapter : JsonSerializer<LatLng?>, JsonDeserializer<LatLng?> {
    override fun serialize(src: LatLng?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? {
        return src?.let { JsonObject().apply { addProperty(LAT, it.latitude); addProperty(LON, it.longitude) } }
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): LatLng? = json?.asJsonObject?.run { LatLng(this[LAT].asDouble, this[LON].asDouble) }
}