package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import java.lang.reflect.Type
import java.time.LocalDate
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME

class LocalDateTypeAdapter : JsonSerializer<LocalDate?>, JsonDeserializer<LocalDate?> {
    override fun serialize(src: LocalDate?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { JsonPrimitive(it.format(ISO_LOCAL_DATE_TIME)) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): LocalDate? = json?.asString?.let { LocalDate.parse(it) }
}