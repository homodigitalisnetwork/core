package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.partnerships.util.DefaultPartnership
import pt.hdn.toolbox.partnerships.util.Partnership
import java.lang.reflect.Type

class PartnershipTypeAdapter: JsonSerializer<Partnership?>, JsonDeserializer<Partnership> {
    override fun serialize(src: Partnership?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { context?.serialize(it) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Partnership? = context?.deserialize(json, DefaultPartnership::class.java)
}