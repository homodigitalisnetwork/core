package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.business.seller.DefaultSeller
import pt.hdn.toolbox.business.seller.Seller
import java.lang.reflect.Type

class SellerTypeAdapter: JsonDeserializer<Seller> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Seller? = context?.deserialize(json, DefaultSeller::class.java)
}