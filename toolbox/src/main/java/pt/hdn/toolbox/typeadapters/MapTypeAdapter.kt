package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import java.lang.reflect.Type

class MapTypeAdapter<K, V>: JsonDeserializer<Map<K, V>> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Map<K, V>? = context?.deserialize(json, typeOfT)
}