package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.business.deputy.Deputy
import pt.hdn.toolbox.business.party.DefaultParty
import pt.hdn.toolbox.business.party.Party
import java.lang.reflect.Type

class PartyTypeAdapter: JsonSerializer<Party?>, JsonDeserializer<Party> {
    override fun serialize(src: Party?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { context?.serialize(it) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Party? = context?.deserialize(json, DefaultParty::class.java)
}