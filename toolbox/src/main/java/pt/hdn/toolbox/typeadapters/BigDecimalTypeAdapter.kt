package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import java.lang.reflect.Type
import java.math.BigDecimal

class BigDecimalTypeAdapter: JsonSerializer<BigDecimal?>, JsonDeserializer<BigDecimal?> {
    override fun serialize(src: BigDecimal?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { JsonPrimitive(it.toString()) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): BigDecimal? = json?.asString?.let { BigDecimal(it) }
}