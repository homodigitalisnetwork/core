package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.specialities.util.DefaultSpeciality
import pt.hdn.toolbox.specialities.util.Speciality
import java.lang.reflect.Type

class SpecialityTypeAdapter: JsonSerializer<Speciality>, JsonDeserializer<Speciality> {
    override fun serialize(src: Speciality?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { context?.serialize(it) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Speciality? = context?.deserialize(json, DefaultSpeciality::class.java)
}