package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.buyer.DefaultBuyer
import java.lang.reflect.Type

class BuyerTypeAdapter: JsonDeserializer<Buyer> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Buyer? = context?.deserialize(json, DefaultBuyer::class.java)
}