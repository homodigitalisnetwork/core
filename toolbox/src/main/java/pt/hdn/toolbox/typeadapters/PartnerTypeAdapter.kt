package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.business.partner.DefaultPartner
import pt.hdn.toolbox.business.partner.Partner
import java.lang.reflect.Type

class PartnerTypeAdapter: JsonSerializer<Partner?>, JsonDeserializer<Partner> {
    override fun serialize(src: Partner?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { context?.serialize(it) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Partner? = context?.deserialize(json, DefaultPartner::class.java)
}