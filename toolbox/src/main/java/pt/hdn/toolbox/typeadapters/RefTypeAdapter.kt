package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.business.buyer.Buyer
import pt.hdn.toolbox.business.buyer.DefaultBuyer
import pt.hdn.toolbox.communications.message.DefaultRef
import pt.hdn.toolbox.communications.message.Ref
import java.lang.reflect.Type

class RefTypeAdapter: JsonDeserializer<Ref> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Ref? = context?.deserialize(json, DefaultRef::class.java)
}