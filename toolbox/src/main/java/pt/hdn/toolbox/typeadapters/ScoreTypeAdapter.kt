package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.specialities.util.Score
import java.lang.reflect.Type

class ScoreTypeAdapter: JsonDeserializer<Score?> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Score? = context?.deserialize(json, typeOfT)
}