package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.entity.util.DefaultEntity
import pt.hdn.toolbox.entity.util.Entity
import java.lang.reflect.Type

class EntityTypeAdapter: JsonSerializer<Entity?>, JsonDeserializer<Entity> {
    override fun serialize(src: Entity?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { context?.serialize(it) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Entity? = context?.deserialize(json, DefaultEntity::class.java)
}