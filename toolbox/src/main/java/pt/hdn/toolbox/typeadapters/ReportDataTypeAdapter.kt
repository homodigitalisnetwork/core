package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.annotations.Field.Companion.UUID
import pt.hdn.toolbox.annotations.Report.Companion.ACTIVITY
import pt.hdn.toolbox.annotations.Report.Companion.EXCHANGE
import pt.hdn.toolbox.report.adapter.ReportData
import pt.hdn.toolbox.report.adapter.activity.ActivityData
import pt.hdn.toolbox.report.adapter.exchange.ExchangeData
import java.lang.reflect.Type

class ReportDataTypeAdapter: JsonDeserializer<ReportData?> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ReportData? {
        return json
            ?.asJsonObject
            ?.run { when (this[UUID].asString) { ACTIVITY -> ActivityData::class.java; EXCHANGE -> ExchangeData::class.java; else -> null }?.let { context?.deserialize(this, it) } }
    }
}