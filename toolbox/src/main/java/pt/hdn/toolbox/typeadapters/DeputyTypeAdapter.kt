package pt.hdn.toolbox.typeadapters

import com.google.gson.*
import pt.hdn.toolbox.business.deputy.DefaultDeputy
import pt.hdn.toolbox.business.deputy.Deputy
import java.lang.reflect.Type
import java.math.BigDecimal

class DeputyTypeAdapter: JsonSerializer<Deputy?>, JsonDeserializer<Deputy> {
    override fun serialize(src: Deputy?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? = src?.let { context?.serialize(it) }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Deputy? = context?.deserialize(json, DefaultDeputy::class.java)
}