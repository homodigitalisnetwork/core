package pt.hdn.toolbox.misc

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.IntentFilter
import android.content.ServiceConnection
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.Bitmap.Config.ARGB_8888
import android.graphics.Canvas
import android.graphics.drawable.VectorDrawable
import android.location.Address
import android.location.Location
import android.os.CountDownTimer
import android.os.IBinder
import android.os.Looper
import android.service.notification.StatusBarNotification
import android.text.TextWatcher
import android.util.TypedValue
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo.IME_ACTION_NEXT
import android.widget.RemoteViews
import android.widget.TextView
import androidx.annotation.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.*
import androidx.lifecycle.*
import androidx.lifecycle.Lifecycle.State
import androidx.lifecycle.Lifecycle.State.*
import androidx.lifecycle.Lifecycle.Event
import androidx.lifecycle.Lifecycle.Event.*
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import androidx.viewpager2.widget.ViewPager2
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.Tab
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.database.*
import com.google.firebase.database.ktx.getValue
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import pt.hdn.toolbox.annotations.Field.Companion.LAT
import pt.hdn.toolbox.annotations.Field.Companion.LON
import pt.hdn.toolbox.annotations.Level
import pt.hdn.toolbox.annotations.Side
import pt.hdn.toolbox.countdown.CountDownObserver
import pt.hdn.toolbox.messaging.BaseMessagingService
import pt.hdn.toolbox.service.BaseService
import pt.hdn.toolbox.transaction.util.Observer
import pt.hdn.toolbox.transaction.util.Topic
import pt.hdn.toolbox.views.ArrayFragmentStateAdapter
import java.io.*
import java.lang.reflect.Type
import java.time.ZonedDateTime
import java.util.*
import java.util.regex.Pattern
import kotlin.OptIn
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.coroutines.cancellation.CancellationException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.experimental.ExperimentalTypeInference
import kotlin.reflect.KClass
import kotlin.text.Charsets.UTF_8

typealias StringArray = Array<String>

//fun String.isNumber(): Boolean {
//    return Pattern
//        .compile("[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*")
//        .matcher(this)
//        .matches()
//}

fun CharSequence.isNumber(): Boolean {
    return Pattern
        .compile("[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*")
        .matcher(this)
        .matches()
}

fun Boolean.toInt() = if (this) 1 else 0

fun Long.toElapsedTime(): String {
    var hours: Long = 0
    var minutes: Long = 0
    var seconds: Long = this

    if (seconds >= 3600) { hours = this / 3600; seconds -= hours * 3600 }

    if (seconds >= 60) { minutes = seconds / 60; seconds -= minutes * 60 }

    return if (hours > 0) "%dh%dm".format(hours, minutes) else "%dm%ds".format(minutes, seconds)
}

inline fun onLocation(block: pt.hdn.toolbox.misc.Location.() -> Unit): LocationCallback = Location().apply(block).build()

inline fun onValueEvent(block: ValueEvent.() -> Unit): ValueEventListener = ValueEvent().apply(block).build()

inline fun onChildEvent(block: ChildEvent.() -> Unit): ChildEventListener = ChildEvent().apply(block).build()

@ExperimentalCoroutinesApi
suspend inline fun <reified T> DatabaseReference.awaitOnce(): T {
    return suspendCancellableCoroutine { cont ->
        addListenerForSingleValueEvent(
            onValueEvent {
                onDataChange { it.getValue<T>()?.let { cont.resume(it as T) }; cont.cancel(Exception("The return value was null")) }

                onCancelled { cont.resumeWithException(it.toException()) }
            }
        )
    }
}

@ExperimentalCoroutinesApi
suspend inline fun <reified T> DatabaseReference.awaitOnce(crossinline block: (snapshot: DataSnapshot) -> T): T {
    return suspendCancellableCoroutine { cont ->
        addListenerForSingleValueEvent(
            onValueEvent {
                onDataChange { block(it)?.let { cont.resume(it) }; cont.cancel(Exception("The return value was null")) }

                onCancelled { cont.resumeWithException(it.toException()) }
            }
        )
    }
}

@ExperimentalCoroutinesApi
suspend inline fun <reified T> DatabaseReference.flowChild(crossinline block: (snapshot: DataSnapshot) -> T?): Flow<T> {
    return callbackFlow {
        onChildEvent { onChildAdded { block(it)?.let { trySend(it) } }; onCancelled { close(it.toException()) } }
            .let { addChildEventListener(it); awaitClose { removeEventListener(it) } }
    }
}

@ExperimentalCoroutinesApi
suspend inline fun <reified T> DatabaseReference.flow(): Flow<T> {
    return callbackFlow {
        onValueEvent { onDataChange { it.getValue<T>()?.let { trySend(it) } }; onCancelled { close(it.toException()) } }
            .let { addValueEventListener(it); awaitClose { removeEventListener(it) } }
    }
}

@ExperimentalCoroutinesApi
suspend inline fun <reified T> DatabaseReference.flow(crossinline block: (snapshot: DataSnapshot) -> T?): Flow<T> {
    return callbackFlow {
        onValueEvent { onDataChange { block(it)?.let { trySend(it) } }; onCancelled { close(it.toException()) } }
            .let { addValueEventListener(it); awaitClose { removeEventListener(it) } }
    }
}

@ExperimentalCoroutinesApi
suspend inline fun <reified T> DatabaseReference.flowOnce(crossinline block: (snapshot: DataSnapshot) -> T?): Flow<T> {
    return callbackFlow {
        addListenerForSingleValueEvent(onValueEvent { onDataChange { block(it)?.let { trySend(it) }; close() }; onCancelled { close(it.toException()) } }); awaitClose { }
    }
}

@ExperimentalCoroutinesApi
suspend inline fun <reified T> DatabaseReference.flowOnce(): Flow<T> {
    return callbackFlow {
        addListenerForSingleValueEvent(onValueEvent { onDataChange { it.getValue<T>()?.let { trySend(it) }; close() }; onCancelled { close(it.toException()) } }); awaitClose { }
    }
}

@ExperimentalCoroutinesApi
suspend inline fun <reified T> Query.flowOnce(crossinline block: (snapshot: DataSnapshot) -> T?): Flow<T> {
    return callbackFlow {
        addListenerForSingleValueEvent(onValueEvent { onDataChange { block(it)?.let { trySend(it) }; close() }; onCancelled { close(it.toException()) } }); awaitClose { }
    }
}

suspend fun FusedLocationProviderClient.awaitOnce(locationRequest: LocationRequest): Position {
    return suspendCancellableCoroutine { cont ->
        onLocation {
            onLocationResult { removeLocationUpdates(this); cont.resume(Position.from(it)) }

            onLocationAvailability { if (!it.isLocationAvailable) { removeLocationUpdates(this); cont.resumeWithException(Exception("Location provider has become unavailable")) } }
        }
            .let {
                try { requestLocationUpdates(locationRequest, it, Looper.getMainLooper()).addOnCanceledListener { cont.cancel() }.addOnFailureListener { cont.resumeWithException(it) } }
                catch (e: SecurityException) { cont.resumeWithException(e) }
            }
    }
}

fun FusedLocationProviderClient.locationFlow(locationRequest: LocationRequest): Flow<Position> {
    return callbackFlow {
        onLocation {
            onLocationResult { trySend(Position.from(it)) }

            onLocationAvailability { if (!it.isLocationAvailable) { close(Exception("Location provider has become unavailable")) } }
        }.let {
            try {
                requestLocationUpdates(locationRequest, it, Looper.getMainLooper())
                    .addOnCanceledListener { close(CancellationException("The task was cancelled", null)) }
                    .addOnFailureListener { close(it) }
            }
            catch (e: SecurityException) { close(e) }

            awaitClose { removeLocationUpdates(it) }
        }
    }
}

inline fun <T : Closeable?, R> T.useWith(block: T.() -> R): R {
    var exception: Throwable? = null

    try { return block() }
    catch (e: Throwable) { exception = e; throw e }
    finally { when { this == null -> null; exception == null -> close(); else -> try { close() } catch (closeException: Throwable) { } } }
}

@OptIn(ExperimentalTypeInference::class)
fun <T> superFlow(@BuilderInference block: suspend ProducerScope<T>.() -> Unit): Flow<T> = channelFlow(block).flowOn(IO)

fun <T> SharedFlow<T>.value(): T? = replayCache.firstOrNull()

fun <T> SharedFlow<T>.isEmpty(): Boolean = replayCache.isEmpty()

inline fun <T> Flow<T>.onEachWith (crossinline action: suspend T.() -> Unit): Flow<T> = transform { it.action(); return@transform emit(it) }

inline fun <T, R> Flow<T>.mapWith(crossinline transform: suspend T.() -> R): Flow<R> = transform { value -> return@transform emit(value.transform()) }

inline fun <T> Flow<T>.filterWith(crossinline predicate: suspend T.() -> Boolean): Flow<T> = transform { value -> if (value.predicate()) return@transform emit(value) }

suspend inline fun <T> Flow<T>.collectWith(crossinline action: T.() -> Unit): Unit = collect { value -> value.action() }

inline fun <reified T> Flow<T>.observeWith(owner: LifecycleOwner, context: CoroutineContext = EmptyCoroutineContext, state: State = if (owner is Service) STARTED else CREATED, crossinline collector: T.() -> Unit) {
    observe(owner, context, state) { it.collector() }
}

inline fun <reified T> Flow<T>.observe(owner: LifecycleOwner, context: CoroutineContext = EmptyCoroutineContext, state: State = if (owner is Service) STARTED else CREATED, crossinline collector: CoroutineScope.(value: T) -> Unit) {
    with(owner) { lifecycleScope.launch(context) { repeatOnLifecycle(state) { collect { this.collector(it) } } } }
}

inline fun <reified T> Flow<T>.observeWith(scope: CoroutineScope, context: CoroutineContext = EmptyCoroutineContext, crossinline collector: T.() -> Unit) = observe(scope, context) { it.collector() }

inline fun <reified T> Flow<T>.observe(scope: CoroutineScope, context: CoroutineContext = EmptyCoroutineContext, collector: FlowCollector<T>) = scope.launch(context) { collect(collector) }

@OptIn(ExperimentalTypeInference::class)
inline fun <T, R> Flow<T>.flatMapLatestWith(@BuilderInference crossinline transform: suspend T.() -> Flow<R>): Flow<R> = transformLatest { emitAll(it.transform()) }

inline fun <T> Array<out T>.findWith(predicate: T.() -> Boolean): T? = firstOrNull(predicate)

inline fun <T> Array<out T>.forEachWith(action: T.() -> Unit) { for (element in this) element.action() }

inline fun <T> Array<out T>.anyWith(predicate: T.() -> Boolean): Boolean { for (element in this) if (element.predicate()) return true; return false }

inline fun <K, V, M : Map<out K, V>> M.onEachWith(action: Map.Entry<K, V>.() -> Unit): M = apply { for (element in this) element.action() }

inline fun <K, V> Map<out K, V>.forEachWith(action: Map.Entry<K, V>.() -> Unit) { for (element in this) element.action() }

inline fun <K, V> Map<out K, V>.filterKeysNot(predicate: (K) -> Boolean): Map<K, V> = linkedMapOf<K, V>().also { forEachWith { if (!predicate(key)) { it[key] = value } } }

infix fun <K, V: Reducible<V>> MutableMap<out K, out V>.reducedBy(other: MutableMap<K, V>): MutableMap<K, V>? {
    return entries.fold(linkedMapOf<K, V>()) { acc, (k, v) -> other[k]?.let { v reducedBy it }?.let { acc[k] = it }; acc }.takeUnless { it.isEmpty() }
}

fun <K, V> MutableMap<in K, in V>.putAll(entries: Sequence<Map.Entry<K, V>>) { for ((key, value) in entries) { put(key, value) } }

fun <K, V, M : MutableMap<in K, in V>> Sequence<Map.Entry<K, V>>.toMap(destination: M): M = destination.apply { putAll(this@toMap) }

fun <K, V> Sequence<Map.Entry<K, V>>.toMap(): Map<K, V> = toMap(LinkedHashMap<K, V>())

fun <T> Sequence<T>.filterWith(predicate: T.() -> Boolean): Sequence<T> = filter { it.predicate() }

inline fun <T> Sequence<T>.forEachWith(action: T.() -> Unit) { for (element in this) action(element) }

inline fun <T> Sequence<T>.onEachWith(crossinline action: T.() -> Unit): Sequence<T> = map { it.action(); it }

inline fun <T> Iterable<T>.contains(predicate: T.() -> Boolean): Boolean { for (element in this) if (predicate(element)) return true; return false }

inline fun <T> Iterable<T>.forEachWith(action: T.() -> Unit) { for (element in this) action(element) }

inline fun <T> Iterable<T>.sumOf(selector: (T) -> Float): Float { var sum: Float = 0f; for (element in this) { sum += selector(element) }; return sum }

inline fun <T, C : Iterable<T>> C.onEachWith(action: T.() -> Unit): C = apply { for (element in this) action(element) }

inline fun <T, R> Iterable<T>.mapWith(transform: T.() -> R): List<R> = mapToWith(ArrayList(if (this is Collection<*>) size else 10), transform)

inline fun <T, R, C : MutableCollection<in R>> Iterable<T>.mapToWith(destination: C, transform: T.() -> R): C = destination.also { for (item in this) { it.add(item.run(transform)) } }

inline fun <reified T> Gson.from(resources: Resources, @RawRes id: Int): T = fromJson(resources.openRawResource(id).bufferedReader(), typeToken<T>())

inline fun <reified T> Gson.from(json: String): T = fromJson(json, typeToken<T>())

inline fun <reified T> Gson.from(`in`: JsonReader): T = fromJson(`in`, typeToken<T>())

inline fun <reified T> typeToken(): Type = object : TypeToken<T>() {}.type

inline fun TypedArray.forEachIndexed(block: (index: Int) -> Unit) { for (index: Int in 0 until length()) { block(index) } }

fun AppCompatTextView.setCompoundTopDrawable(@DrawableRes id: Int) { setCompoundDrawablesWithIntrinsicBounds(0, id, 0, 0) }

fun ZonedDateTime.coerceAtLeast(minimumValue: ZonedDateTime): ZonedDateTime = if (this.isBefore(minimumValue)) minimumValue else this

inline fun NotificationManager.find(predicate: (StatusBarNotification) -> Boolean): StatusBarNotification? = activeNotifications.firstOrNull(predicate)

inline fun NotificationManager.findWith(predicate: StatusBarNotification.() -> Boolean): StatusBarNotification? = activeNotifications.findWith(predicate)

inline fun NotificationManager.any(predicate: (StatusBarNotification) -> Boolean): Boolean = activeNotifications.any(predicate)

inline fun NotificationManager.forEachWith(action: StatusBarNotification.() -> Unit) { activeNotifications.forEachWith(action) }

inline fun LifecycleOwner.syncObserve(crossinline block: (source: LifecycleOwner, event: Event) -> Unit) { lifecycle.addObserver(LifecycleEventObserver { source, event -> block(source, event) }) }

inline fun LifecycleOwner.syncOnResumedObserver(crossinline block: () -> Unit) { lifecycle.addObserver(LifecycleEventObserver { _, event -> if (event == ON_RESUME) block() }) }

inline fun LifecycleOwner.asyncObserve(state: State = RESUMED, crossinline block: suspend CoroutineScope.() -> Unit) { lifecycleScope.launch { repeatOnLifecycle(state) { block() } } }

fun Location.toLatLng(): LatLng = LatLng(latitude, longitude)

fun Address.toLatLng(): LatLng = LatLng(latitude, longitude)

fun Address.toPosition(): Position = Position(latitude, longitude)

fun LatLng.toMap(): Map<String, Double> = mapOf(LAT to latitude, LON to longitude)

fun TextInputLayout.setTextInput(onFocusChangeListener: OnFocusChangeListener, textWatcher: TextWatcher, hint: String? = null, value: String? = null) {
    hint?.let { this.hint = it }

    value
        ?.let {
            isHintAnimationEnabled = false
            editText?.apply { setText(it); this.onFocusChangeListener = onFocusChangeListener; addTextChangedListener(textWatcher) }
            isHintAnimationEnabled = true
        }
        ?: run { editText?.apply { this.onFocusChangeListener = onFocusChangeListener; addTextChangedListener(textWatcher) } }
}

fun TextInputLayout.setTextInput(value: String? = null) { isHintAnimationEnabled = false; editText?.apply { setText(value) }; isHintAnimationEnabled = true }

inline fun TextView.onNextAction(crossinline block: () -> Unit) {
    setOnEditorActionListener { _, actionId, _ -> return@setOnEditorActionListener if (actionId == IME_ACTION_NEXT) { block(); true } else false }
}

fun ViewPager2.swipeNext() { adapter?.let { if (currentItem < it.itemCount) currentItem += 1 } }

fun tabLayoutMediator(lifecycleOwner: LifecycleOwner, tabLayout: TabLayout, viewPager2: ViewPager2, block: (Tab.(position: Int) -> Unit)? = null) {
    TabLayoutMediator(tabLayout, viewPager2) { tab, position -> block?.let { tab.it(position) } ?: run { tab.view.isClickable = false } }
        .apply { lifecycleOwner.syncObserve { _, event -> when (event) { ON_START -> attach(); ON_STOP -> detach(); else -> null } } }
}

inline fun <T> T.takeUnlessWith(predicate: T.() -> Boolean): T? = if (!this.predicate()) this else null

inline fun <T> T.takeIfWith(predicate: T.() -> Boolean): T? = if (this.predicate()) this else null

inline fun onBottomSheetCallback(supplier: BottomSheet.() -> Unit): BottomSheetCallback = BottomSheet().apply(supplier).build()

inline fun onScrollListener(supplier: OnScroll.() -> Unit): OnScrollListener = OnScroll().apply(supplier).build()

inline fun <T: IBinder> onServiceConnection(supplier: OnServiceConnection<T>.() -> Unit): ServiceConnection = OnServiceConnection<T>().apply(supplier).build()

inline fun arrayFragmentStateAdapter(fragment: Fragment, viewPager2: ViewPager2, supplier: ArrayFragmentAdapter.() -> Unit): ArrayFragmentStateAdapter {
    return ArrayFragmentAdapter(fragment, viewPager2).apply(supplier).build()
}

inline fun arrayFragmentStateAdapter(fragmentActivity: FragmentActivity, viewPager2: ViewPager2, supplier: ArrayFragmentAdapter.() -> Unit): ArrayFragmentStateAdapter {
    return ArrayFragmentAdapter(fragmentActivity, viewPager2).apply(supplier).build()
}

fun notificationChannel(id: String, name: CharSequence, importance: Int, config: NotificationChannel.() -> Unit): NotificationChannel = NotificationChannel(id, name, importance).apply(config)

inline fun topic(@Level left: Int, @Level right: Int, leftInclusive: Boolean, rightInclusive: Boolean, callOnce: Boolean, @Side callOnSide: Int, executeOnAborting: Boolean, obs: Observer, supplier: Topic.() -> Unit): Topic {
    return Topic(left, right, leftInclusive, rightInclusive, callOnce, callOnSide, executeOnAborting, obs).apply(supplier)
}

//fun countDownTimer(millisInFuture: Long, millisInterval: Long, supplier: CountDown.() -> Unit): CountDownTimer = CountDown(millisInFuture, millisInterval).apply(supplier).build()

fun VectorDrawable.toBitmapDescriptor(): BitmapDescriptor? = run { Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, ARGB_8888)?.let { setBounds(0, 0, intrinsicWidth, intrinsicHeight); draw(Canvas(it)); BitmapDescriptorFactory.fromBitmap(it) } }

fun Context.getColorAttr(@AttrRes id: Int): Int = TypedValue().also { theme.resolveAttribute(id, it, true) }.data

fun GoogleMap.setOnMarkerDragListener(supplier: OnMarkerDrag.() -> Unit) { setOnMarkerDragListener(OnMarkerDrag().apply(supplier).build()) }

inline fun WindowBounds.forEachDay(action: (day: Long) -> Unit) { for (day: Long in 0 until days) action(day) }

inline fun locationGlobalReceiver(lifecycleService: LifecycleService, intentFilter: IntentFilter, supplier : LocationGlobalReceiver.() -> Unit) { LocationGlobalReceiver(lifecycleService, intentFilter).apply(supplier).build() }

inline fun countDownObserver(supplier: CountDownObserver.() -> Unit): CountDownObserver = CountDownObserver().apply(supplier)

inline fun remoteViews(packageName: String, @LayoutRes layoutId: Int, block: RemoteViews.() -> Unit): RemoteViews = RemoteViews(packageName, layoutId).apply(block)

fun ViewModel.launch(context: CoroutineContext = EmptyCoroutineContext, block: suspend CoroutineScope.() -> Unit): Job = viewModelScope.launch(context = context, block = block)

fun Fragment.launch(context: CoroutineContext = EmptyCoroutineContext, block: suspend CoroutineScope.() -> Unit): Job = viewLifecycleOwner.lifecycleScope.launch(context = context, block = block)

@MainThread
inline fun <reified VM : ViewModel> Fragment.navGraphViewModels(@IdRes navGraphId: Int? = null, noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null): Lazy<VM> {
    val backStackEntry by lazy { with(findNavController()) { getBackStackEntry(navGraphId ?: graph.id) } }
    val storeProducer: () -> ViewModelStore = { backStackEntry.viewModelStore }
    return createViewModelLazy(viewModelClass = VM::class, storeProducer =  storeProducer, factoryProducer = { factoryProducer?.invoke() ?: defaultViewModelProviderFactory })
}

@MainThread
fun <VM : ViewModel> Fragment.navGraphViewModels(map: Map<Int, KClass<out VM>>, @IdRes navGraphId: Int? = null, factoryProducer: (() -> ViewModelProvider.Factory)? = null): Lazy<VM> {
    val vmClassProducer: () -> KClass<out VM> = { map[navGraphId ?: findNavController().graph.id] ?: throw Resources.NotFoundException("Unable to find graph") }
    val backStackEntry by lazy { with(findNavController()) { getBackStackEntry(navGraphId ?: graph.id) } }
    val storeProducer: () -> ViewModelStore = { backStackEntry.viewModelStore }
    return ViewModelLazy(vmClassProducer, storeProducer, factoryProducer ?: { defaultViewModelProviderFactory })
}

@MainThread
fun <VM : ViewModel> Fragment.navGraphViewModels(vararg pairs: Pair<Int, KClass<out VM>>, @IdRes navGraphId: Int? = null, factoryProducer: (() -> ViewModelProvider.Factory)? = null): Lazy<VM> {
    return navGraphViewModels(pairs.toMap(), navGraphId, factoryProducer)
}

@MainThread
inline fun <reified VM : ViewModel> Fragment.navHostViewModels(@IdRes navHostId: Int, noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null): Lazy<VM> {
    val backStackEntry by lazy { (childFragmentManager.findFragmentById(navHostId) as NavHostFragment).navController.let { it.getBackStackEntry(it.graph.id) } }
    val storeProducer: () -> ViewModelStore = { backStackEntry.viewModelStore }
    return createViewModelLazy(viewModelClass = VM::class, storeProducer =  storeProducer, factoryProducer = factoryProducer ?: { defaultViewModelProviderFactory })
}

@MainThread
inline fun <reified  VM : ViewModel> Fragment.navHolderViewModel(noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null): Lazy<VM> {
    val storeProducer: () -> ViewModelStore = { (parentFragment?.parentFragment ?: requireActivity()).viewModelStore }
    return createViewModelLazy(viewModelClass = VM::class, storeProducer =  storeProducer, factoryProducer = factoryProducer ?: { defaultViewModelProviderFactory })
}

@MainThread
inline fun <reified VM : ViewModel> BaseService.viewModels(noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null): Lazy<VM> {
    return ViewModelLazy(VM::class, { viewModelStore }, factoryProducer ?: { defaultViewModelProviderFactory })
}

@MainThread
fun <VM : ViewModel> Fragment.viewModels(map: Map<KClass<out Fragment>, KClass<out VM>>): Lazy<VM> {
    val vmClassProducer: () -> KClass<out VM> = { parentFragment?.let { map[it::class] } ?: throw Resources.NotFoundException("Unable to find the view model") }
    return ViewModelLazy(vmClassProducer, { (parentFragment ?: this).viewModelStore }, { defaultViewModelProviderFactory })
}

@MainThread
inline fun <reified VM : ViewModel> BaseMessagingService.viewModels(noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null): Lazy<VM> {
    return ViewModelLazy(VM::class, { viewModelStore }, factoryProducer ?: { defaultViewModelProviderFactory })
}