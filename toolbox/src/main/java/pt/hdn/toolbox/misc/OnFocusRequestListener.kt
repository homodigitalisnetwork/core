package pt.hdn.toolbox.misc

interface OnFocusRequestListener {
	fun onFocusRequest(index: Int)
}