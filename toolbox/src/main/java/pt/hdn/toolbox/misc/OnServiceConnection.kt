package pt.hdn.toolbox.misc

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback

@DslMarker
annotation class OnServiceConnectionDsl

@OnServiceConnectionDsl
class OnServiceConnection<T: IBinder> {

	//region vars
	var onServiceConnected: ((iBinder: T?) -> Unit)? = null; private set
	var onServiceDisconnected: (() -> Unit)? = null; private set
	//endregion vars

	fun onServiceConnected(onServiceConnected: (iBinder: T?) -> Unit) { this.onServiceConnected = onServiceConnected }

	fun onServiceDisconnected(onServiceDisconnected: () -> Unit) { this.onServiceDisconnected = onServiceDisconnected }

	fun build(): ServiceConnection {
		return object : ServiceConnection {
			override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) { onServiceConnected?.invoke(p1 as T?) }

			override fun onServiceDisconnected(p0: ComponentName?) { onServiceDisconnected?.invoke() }
		}
	}
}