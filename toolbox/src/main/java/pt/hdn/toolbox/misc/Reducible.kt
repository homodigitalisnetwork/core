package pt.hdn.toolbox.misc

interface Reducible<T> {
    infix fun reducedBy(other: T?): T?

    fun <E> takeUnless(a: E?, b: E?): E? = a.takeUnless { a == b }
}