package pt.hdn.toolbox.misc

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager

abstract class LocalBroadcastReceiver(context: Context): BroadcastReceiver() {

    //region vars
    private var isRegister = false
    private val localBroadcastManager: LocalBroadcastManager = LocalBroadcastManager.getInstance(context)
    //endregion vars

    fun sendBroadcast(intent: Intent): Boolean = localBroadcastManager.sendBroadcast(intent)

    fun register(intentFilter: IntentFilter) { if (!isRegister) { localBroadcastManager.registerReceiver(this, intentFilter); this.isRegister = true } }

    fun unregister(): Boolean = isRegister && selfUnregister()

    private fun selfUnregister(): Boolean = localBroadcastManager.unregisterReceiver(this).let { this.isRegister = false; true }
}