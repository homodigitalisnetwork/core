package pt.hdn.toolbox.misc

import com.github.mikephil.charting.formatter.ValueFormatter
import pt.hdn.toolbox.annotations.Misc.Companion.PATTERN
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Class that inherits from [ValueFormatter] and translates indexes to [LocalDate] from an initial [LocalDate]
 */
class DateValueFormatter(
    private val lowerBound: LocalDate
) : ValueFormatter() {

    //region vars
    private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(PATTERN)
    //endregion vars

    override fun getFormattedValue(value: Float): String = lowerBound.plusDays(value.toLong()).format(formatter)
}