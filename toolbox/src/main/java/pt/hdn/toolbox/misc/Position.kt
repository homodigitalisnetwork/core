package pt.hdn.toolbox.misc

import android.location.Address
import android.location.Location
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.Expose
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.annotations.Field.Companion.LAT
import pt.hdn.toolbox.annotations.Field.Companion.LON

@Parcelize
data class Position(
    @Expose var lat: Double = 0.0,
    @Expose var lon: Double = 0.0
) : Parcelable, Cloneable {
    companion object {
        fun from(location: Location) = with(location) { Position(latitude, longitude) }

        fun from(address: Address) = with(address) { Position(latitude, longitude) }

        fun from(ll: LatLng) = with(ll) { Position(latitude, longitude) }

        fun from(map: Map<String, Double>) = Position(map[LAT]!!, map[LON]!!)
    }

    public override fun clone(): Position = copy()

    fun toMap(): Map<String, Double> = mapOf(LAT to lat, LON to lon)

    fun toLatLng(): LatLng = LatLng(lat, lon)
}