package pt.hdn.toolbox.misc

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import androidx.lifecycle.Lifecycle.Event.ON_START
import androidx.lifecycle.Lifecycle.Event.ON_STOP
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleService

/**
 * A [BroadcastReceiver] inherited abstract class that attaches to a [LifecycleOwner] and manages itself
 */
abstract class GlobalBroadcastReceiver(
    lifecycleOwner: LifecycleOwner,
    private val context: Context,
    private val intentFilter: IntentFilter
): BroadcastReceiver() {

    //region vars
    private var isRegister = false
    //endregion vars

    constructor(lifecycleService: LifecycleService, intentFilter: IntentFilter): this(lifecycleService, lifecycleService.application, intentFilter)

    init {
    	lifecycleOwner
            .lifecycle
            .addObserver(
                LifecycleEventObserver { _, event ->
                    when (event) {
                        ON_START -> if (!isRegister) context.registerReceiver(this, intentFilter)?.also { this.isRegister = true }
                        ON_STOP -> isRegister && context.let { it.unregisterReceiver(this); this.isRegister = false; true }
                        else -> null
                    }
                }
            )
    }
}
