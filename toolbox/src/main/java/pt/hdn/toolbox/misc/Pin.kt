package pt.hdn.toolbox.misc

import com.google.android.gms.maps.model.BitmapDescriptor
import java.util.*

data class Pin(
    val appUUID: UUID,
    val bitmapDescriptor: BitmapDescriptor
)