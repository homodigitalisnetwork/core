package pt.hdn.toolbox.misc

import android.content.Context
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.google.maps.android.SphericalUtil.computeDistanceBetween
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.tasks.await
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.annotations.Command
import pt.hdn.toolbox.annotations.Duration.Companion.x1
import pt.hdn.toolbox.annotations.Duration.Companion.x10
import pt.hdn.toolbox.annotations.Duration.Companion.x20
import pt.hdn.toolbox.annotations.Duration.Companion.x5
import pt.hdn.toolbox.annotations.Err
import pt.hdn.toolbox.annotations.State
import pt.hdn.toolbox.annotations.State.Companion.IDLE
import pt.hdn.toolbox.annotations.State.Companion.RUNNING
import pt.hdn.toolbox.annotations.State.Companion.TO_DB
import pt.hdn.toolbox.annotations.State.Companion.TO_FB
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.partnerships.util.Partnership
import pt.hdn.toolbox.communications.broadcast.BroadcastRequest
import pt.hdn.toolbox.communications.error
import pt.hdn.toolbox.communications.success
import java.util.*

/**
 * Class that manages all GPS data communication to SQL or Firebase and
 */
class LocationProvider constructor(
    context: Context,
    private val repo: CoreRepo,
    private val partnership: Partnership
) {

    //region vars
    val isUsingGPS: Boolean; get() = state > IDLE
    private val fusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    private val settingsClient: SettingsClient = LocationServices.getSettingsClient(context)
    private val locationRequest: LocationRequest = LocationRequest.create().apply { interval = x20; fastestInterval = x10; priority = PRIORITY_HIGH_ACCURACY }
    private val oneShotLocationRequest: LocationRequest = LocationRequest.create().apply { interval = x5; fastestInterval = x1; priority = PRIORITY_HIGH_ACCURACY }
    private val locationSettingsRequest: LocationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).setAlwaysShow(true).build()
    @State private var state = IDLE
    private var broadcastJob: Job? = null
    private var hasBroadcast: Boolean = false
    //endregion vars

    suspend fun getOneShot(): Flow<Result<Position>> {
        return flow {
            try { emit(Result.Success(fusedLocationProviderClient.awaitOnce(oneShotLocationRequest))) }
            catch (e: Exception) { emit(Result.Error(Err.LOCATION, e)) }
        }.flowOn(Default)
    }

    suspend fun startBroadcast(location: Boolean) {
        when (state) {
            IDLE -> if (location) broadcastGPS() else broadcastStatic()
            RUNNING -> if (location) broadcastGPS()
            TO_DB -> if (!location) broadcastStatic()
            TO_FB -> { stopBroadcast(false); if (location) broadcastGPS() else broadcastStatic() }
        }
    }

    suspend fun startBroadcast(transactionUUID: UUID, position: Position) {
        coroutineScope {
            this@LocationProvider.broadcastJob = launch(Default) {
                fusedLocationProviderClient
                    .locationFlow(locationRequest)
                    .flatMapLatest { repo.position(transactionUUID, it, computeDistanceBetween(it.toLatLng(), position.toLatLng())) }
                    .collect { it.success { this@LocationProvider.state = TO_FB }.error { _, _ -> } }
            }
        }
    }

    suspend fun stopBroadcast(cleanUp: Boolean) { if (state != IDLE) { broadcastCleanUp(cleanUp) } }

    suspend fun updateBroadcast() { if (state == TO_DB) broadcast(partnership.position) }

    suspend fun checkLocationSettings(): LocationSettingsResponse = settingsClient.checkLocationSettings(locationSettingsRequest).await()

    private suspend fun broadcastCleanUp(cleanUp: Boolean) {
        broadcastJob?.cancel()

        if (cleanUp && hasBroadcast) {
            repo.broadcast(BroadcastRequest(Command.REMOVE, partnership.deputyUUID, partnership.partyUUID)).collect { it.success { this@LocationProvider.hasBroadcast = false } }
        }

        this.state = IDLE
    }

    private suspend fun broadcastStatic() { broadcastJob?.cancel(); broadcast(partnership.position); this.state = RUNNING }

    private suspend fun broadcastGPS() {
        coroutineScope {
            this@LocationProvider.broadcastJob = launch(Default) {
                fusedLocationProviderClient.locationFlow(locationRequest).collect { broadcast(it); this@LocationProvider.state = TO_DB }
            }
        }
    }

    private suspend fun broadcast(position: Position) {
        repo
            .broadcast(BroadcastRequest(command = Command.SET, deputyUUID = partnership.deputyUUID, partyUUID = partnership.partyUUID, position = position))
            .collect { it.success { hasBroadcast = true } }
    }
}