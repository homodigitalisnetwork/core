package pt.hdn.toolbox.misc

import android.view.View
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT

class Keyboard(
    private val inputMethodManager: InputMethodManager,
    private val root: View
) {

    fun show() { inputMethodManager.showSoftInput(root, SHOW_IMPLICIT) }

    fun hide() { inputMethodManager.hideSoftInputFromWindow(root.windowToken, 0) }
}