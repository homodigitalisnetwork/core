package pt.hdn.toolbox.misc

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewbinding.ViewBinding
import androidx.viewpager2.widget.ViewPager2
import pt.hdn.toolbox.binding.BindingFragment
import pt.hdn.toolbox.views.ArrayFragmentStateAdapter
import pt.hdn.toolbox.views.ArrayFragmentStateAdapter.Invoker

/**
 *  A DSL builder type to build an [ArrayFragmentStateAdapter] that are used with [ViewPager2]s
 */
@DslMarker
annotation class ArrayFragmentAdapterDsl

@ArrayFragmentAdapterDsl
class ArrayFragmentAdapter(
	private val fragmentManager: FragmentManager,
	private val viewPager2: ViewPager2,
	private val lifecycle: Lifecycle
) {

	//region vars
	private var onPageScrolled: ((position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit)? = null
	private var onPageSelected: ((index: Int, lastIndex: Int) -> Unit)? = null
	private var onPageScrollStateChanged: ((state: Int) -> Unit)? = null
	private val invokers: MutableList<Invoker> = mutableListOf()
	//endregion vars

	constructor(fragment: Fragment, viewPager2: ViewPager2): this(fragment.childFragmentManager, viewPager2, fragment.lifecycle)

	constructor(fragmentActivity: FragmentActivity, viewPager2: ViewPager2): this(fragmentActivity.supportFragmentManager, viewPager2, fragmentActivity.lifecycle)

	fun onPageScrolled(onPageScrolled: (position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit) { this.onPageScrolled = onPageScrolled }

	fun onPageSelected(onPageSelected: (index: Int, lastIndex: Int) -> Unit) { this.onPageSelected = onPageSelected }

	fun onPageScrollStateChanged(onPageScrollStateChanged: (state: Int) -> Unit) { this.onPageScrollStateChanged = onPageScrollStateChanged }

	fun add(supplier: () -> BindingFragment<ViewBinding>) { invokers.add(Invoker(supplier)) }

	fun add(invoker: Invoker) { invokers.add(invoker) }

	fun addAll(vararg suppliers: () -> BindingFragment<ViewBinding>) { invokers.addAll(suppliers.map { Invoker(it) }) }

	fun build(): ArrayFragmentStateAdapter {
		val supplier: (OnPageChanged.() -> Unit)? = if ((onPageScrolled ?: onPageSelected ?: onPageScrollStateChanged) != null) {
			fun OnPageChanged.() {
				this@ArrayFragmentAdapter.onPageScrolled?.let { onPageScrolled(it) }
				this@ArrayFragmentAdapter.onPageSelected?.let { onPageSelected(it) }
				this@ArrayFragmentAdapter.onPageScrollStateChanged?.let { onPageScrollStateChanged(it) }
			}
		} else null

		return ArrayFragmentStateAdapter(fragmentManager, lifecycle, viewPager2, supplier).apply { addAll(invokers); initiate() }
	}
}