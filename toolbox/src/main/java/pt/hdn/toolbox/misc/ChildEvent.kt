package pt.hdn.toolbox.misc

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

@DslMarker
annotation class ChildEventListenerDsl

/**
 * A DSL builder type to create [ChildEventListener] objects used in to listen nodes in [Firebase]
 */
@ChildEventListenerDsl
class ChildEvent {

	//region vars
	var onChildAdded: ((snapshot: DataSnapshot) -> Unit)? = null; private set
	var onChildChanged: ((snapshot: DataSnapshot, previousChildName: String?) -> Unit)? = null; private set
	var onChildRemoved: ((snapshot: DataSnapshot) -> Unit)? = null; private set
	var onChildMoved: ((snapshot: DataSnapshot, previousChildName: String?) -> Unit)? = null; private set
	var onCancelled: ((error: DatabaseError) -> Unit)? = null; private set
	//endregion vars

	fun onChildAdded(onChildAdded: (snapshot: DataSnapshot) -> Unit) { this.onChildAdded = onChildAdded }

	fun onChildChanged(onChildChanged: (snapshot: DataSnapshot, previousChildName: String?) -> Unit) { this.onChildChanged = onChildChanged }

	fun onChildRemoved(onChildRemoved: (snapshot: DataSnapshot) -> Unit) { this.onChildRemoved = onChildRemoved }

	fun onChildMoved(onChildMoved: (snapshot: DataSnapshot, previousChildName: String?) -> Unit) { this.onChildMoved = onChildMoved }

	fun onCancelled(onCancelled: (error: DatabaseError) -> Unit) { this.onCancelled = onCancelled }

	fun build(): ChildEventListener {
		return object : ChildEventListener {
			override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) { onChildAdded?.invoke(snapshot) }

			override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) { onChildChanged?.invoke(snapshot, previousChildName) }

			override fun onChildRemoved(snapshot: DataSnapshot) { onChildRemoved?.invoke(snapshot) }

			override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) { onChildMoved?.invoke(snapshot, previousChildName) }

			override fun onCancelled(error: DatabaseError) { onCancelled?.invoke(error) }
		}
	}
}