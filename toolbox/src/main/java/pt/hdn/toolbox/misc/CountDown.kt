package pt.hdn.toolbox.misc

import android.os.CountDownTimer

@DslMarker
annotation class CountDownDsl


@CountDownDsl
class CountDown(
    private val millisInFuture: Long,
    private val countDownInterval: Long
) {

    //region vars
    private var finish: (() -> Unit)? = null
    private var tick: (() -> Unit)? = null
    //endregion vars

    fun onFinish(block: () -> Unit) { this.finish = block }

    fun onTick(block: () -> Unit) { this.tick = block }

    fun build(): CountDownTimer = object : CountDownTimer(millisInFuture, countDownInterval) {
        override fun onTick(millisUntilFinished: Long) { tick?.invoke() }

        override fun onFinish() { finish?.invoke() }
    }
}