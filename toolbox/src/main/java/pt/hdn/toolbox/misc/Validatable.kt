package pt.hdn.toolbox.misc

import pt.hdn.toolbox.annotations.Err

interface Validatable<T> {
    @Err infix fun validatedBy(other: T?): Int
}