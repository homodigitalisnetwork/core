package pt.hdn.toolbox.misc

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback

@DslMarker
annotation class BottomSheetCallbackDsl

/**
 * A DSL builder type to generate [BottomSheetCallback] for [Layout]s with bottomSheetBehaviour
 */
@BottomSheetCallbackDsl
class BottomSheet {

	//region vars
	private var onStateChanged: ((bottomSheet: View, newState: Int) -> Unit)? = null; private set
	private var onSlide: ((bottomSheet: View, slideOffset: Float) -> Unit)? = null; private set
	//endregion vars

	fun onStateChanged(block: (bottomSheet: View, newState: Int) -> Unit) { this.onStateChanged = block }

	fun onSlide(block: (bottomSheet: View, slideOffset: Float) -> Unit) { this.onSlide = block }

	fun build(): BottomSheetCallback {
		return object : BottomSheetCallback() {
			override fun onStateChanged(bottomSheet: View, newState: Int) { onStateChanged?.invoke(bottomSheet, newState) }

			override fun onSlide(bottomSheet: View, slideOffset: Float) { onSlide?.invoke(bottomSheet, slideOffset) }
		}
	}
}