package pt.hdn.toolbox.misc

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener

@DslMarker
annotation class OnScrollListenerDsl

@OnScrollListenerDsl
class OnScroll {

	//region vars
	var onScrollStateChanged: ((recyclerView: RecyclerView, newState: Int) -> Unit)? = null; private set
	var onScrolled: ((recyclerView: RecyclerView, dx: Int, dy: Int) -> Unit)? = null; private set
	//endregion vars

	fun onScrollStateChanged(onScrollStateChanged: (recyclerView: RecyclerView, newState: Int) -> Unit) { this.onScrollStateChanged = onScrollStateChanged }

	fun onScrolled(onScrolled: (recyclerView: RecyclerView, dx: Int, dy: Int) -> Unit) { this.onScrolled = onScrolled }

	fun build(): OnScrollListener {
		return object : OnScrollListener() {
			override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) { onScrollStateChanged?.invoke(recyclerView, newState) }

			override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) { onScrolled?.invoke(recyclerView, dx, dy) }
		}
	}
}