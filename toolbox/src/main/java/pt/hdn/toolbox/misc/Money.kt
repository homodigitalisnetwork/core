package pt.hdn.toolbox.misc

import android.icu.math.BigDecimal
import android.icu.math.BigDecimal.ROUND_HALF_UP
import java.util.*

class Money(locale: Locale, amount: Double) {

    //region vars
    val currency: Currency = Currency.getInstance(locale)
    val amount: BigDecimal = BigDecimal.valueOf(amount).setScale(2, ROUND_HALF_UP)
    //endregion vars
}