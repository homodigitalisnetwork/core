package pt.hdn.toolbox.misc

import android.content.Context
import android.graphics.Bitmap.CompressFormat.JPEG
import android.graphics.Bitmap.createBitmap
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.decodeFile
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Parcelable
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface.*
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Media
import pt.hdn.toolbox.annotations.Misc
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*

@Parcelize
class Photo(
    var uri: Uri? = null,
    private var isThumbnail: Boolean = false
) : Parcelable {

    //region vars
    var file: File? = uri?.run { File(path!!) }; set(value) { file?.takeIf { it.exists() }?.delete(); field = value }
    //endregion vars

    companion object {
        fun from(context: Context, authority: String, directory: File, name: String = UUID.randomUUID().toString()): Photo {
            return Photo().apply { this.file = File(directory, "$name${Misc.JPEG_SUFFIX}").also { this.uri = FileProvider.getUriForFile(context, authority, it) } }
        }

        fun from(uriString: String): Photo = Photo(Uri.parse(uriString))
    }

    fun exists(): Boolean = file?.exists() == true

    fun delete() { this.file = file?.takeIf { it.exists() }?.run { delete(); this@Photo.uri = null; null } }

    fun toThumbnail(): Boolean {
        var sampleSize = 1
        var scale: Double

        if (isThumbnail) return true

        file
            ?.let { file ->
                BitmapFactory
                    .Options()
                    .apply{ inJustDecodeBounds = true; decodeFile(file.absolutePath, this) }
                    .takeIfWith { outWidth != -1 && outHeight != -1 }
                    ?.run {
                        scale = outWidth.coerceAtLeast(outHeight).toDouble() / Media.THUMBNAIL_SIZE

                        if (scale in 0.9..1.1) { this@Photo.isThumbnail = true; return true }

                        while (scale > sampleSize) { sampleSize *= 2 }

                        BitmapFactory
                            .Options()
                            .run {
                                inSampleSize = sampleSize

                                decodeFile(file.absolutePath, this)
                                    ?.run {
                                        when (ExifInterface(file.path).getAttributeInt(TAG_ORIENTATION, ORIENTATION_NORMAL)) {
                                            ORIENTATION_ROTATE_90 -> 90f
                                            ORIENTATION_ROTATE_180 -> 180f
                                            ORIENTATION_ROTATE_270 -> 270f
                                            else -> 0f
                                        }.let { me ->
                                            createBitmap(this, 0, 0, width, height, Matrix().apply { postRotate(me) }, true)
                                                ?.run {
                                                    ByteArrayOutputStream()
                                                        .takeIf { compress(JPEG, 100, it) }
                                                        ?.use { FileOutputStream(file).use { me -> me.write(it.toByteArray()) }; this@Photo.isThumbnail = true; return true }
                                                }
                                        }
                                    }
                            }
                    }
            }

        return false
    }
}
