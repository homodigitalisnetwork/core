package pt.hdn.toolbox.misc

import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener
import com.google.android.gms.maps.model.Marker

@DslMarker
annotation class OnMarkerDragDsl

@OnMarkerDragDsl
class OnMarkerDrag {

	//region vars
	var onMarkerDrag: ((marker: Marker) -> Unit)? = null; private set
	var onMarkerDragEnd: ((marker: Marker) -> Unit)? = null; private set
	var onMarkerDragStart: ((marker: Marker) -> Unit)? = null; private set
	//endregion vars

	fun onMarkerDrag(onMarkerDrag: (marker: Marker) -> Unit) { this.onMarkerDrag = onMarkerDrag }

	fun onMarkerDragEnd(onMarkerDragEnd: (marker: Marker) -> Unit) { this.onMarkerDragEnd = onMarkerDragEnd }

	fun onMarkerDragStart(onMarkerDragStart: (marker: Marker) -> Unit) { this.onMarkerDragStart = onMarkerDragStart }

	fun build(): OnMarkerDragListener {
		return object : OnMarkerDragListener {
			override fun onMarkerDrag(p0: Marker) { onMarkerDrag?.invoke(p0) }

			override fun onMarkerDragEnd(p0: Marker) { onMarkerDragEnd?.invoke(p0) }

			override fun onMarkerDragStart(p0: Marker) { onMarkerDragStart?.invoke(p0) }
		}
	}
}