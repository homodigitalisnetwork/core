package pt.hdn.toolbox.misc

import com.google.gson.annotations.Expose
import java.time.Clock
import java.time.DayOfWeek.MONDAY
import java.time.DayOfWeek.SUNDAY
import java.time.LocalDateTime
import java.time.LocalTime.MAX
import java.time.LocalTime.MIN
import java.time.temporal.ChronoUnit.DAYS
import java.time.temporal.TemporalAdjusters.previousOrSame
import java.time.temporal.TemporalAdjusters.nextOrSame

data class WindowBounds (
    @Expose val lowerBound: LocalDateTime,
    @Expose val upperBound: LocalDateTime
) {

    //region vars
    val days: Long; get() =  DAYS.between(lowerBound, upperBound) + 1L
    //endregion vars

    companion object {
        fun create(delta: Long = 30): WindowBounds = with(LocalDateTime.now(Clock.systemUTC())) { WindowBounds(minusDays(delta).with(MIN), with(MAX).withNano(0)) }
        fun currentWeek(): WindowBounds = with(LocalDateTime.now(Clock.systemUTC())) { WindowBounds(with(previousOrSame(MONDAY)), with(nextOrSame(SUNDAY))) }
    }
}
