package pt.hdn.toolbox.misc

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

@DslMarker
annotation class ValueEventListenerDsl

@ValueEventListenerDsl
class ValueEvent {

	//region vars
	var onDataChange: ((snapshot: DataSnapshot) -> Unit)? = null; private set
	var onCancelled: ((error: DatabaseError) -> Unit)? = null; private set
	//endregion vars

	fun onDataChange(onDataChange: (snapshot: DataSnapshot) -> Unit) { this.onDataChange = onDataChange }

	fun onCancelled(onCancelled: (error: DatabaseError) -> Unit) { this.onCancelled = onCancelled }

	fun build(): ValueEventListener {
		return object : ValueEventListener {
			override fun onDataChange(snapshot: DataSnapshot) { onDataChange?.invoke(snapshot) }

			override fun onCancelled(error: DatabaseError) { onCancelled?.invoke(error) }
		}
	}
}