package pt.hdn.toolbox.misc

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import androidx.lifecycle.LifecycleService

@DslMarker
annotation class LocationGlobalReceiverDsl

/**
 * A DSL builder type to create [GlobalBroadcastReceiver]
 */
@LocationGlobalReceiverDsl
class LocationGlobalReceiver(
	private val lifecycleService: LifecycleService,
	private val intentFilter: IntentFilter
) {

	//region vars
	val locationManager: LocationManager = lifecycleService.application.getSystemService(LocationManager::class.java)
	var onReceive: ((Intent?) -> Unit)? = null; private set
	//endregion vars

	fun onReceive(onReceive: (Intent?) -> Unit) { this.onReceive = onReceive }

	fun build(): GlobalBroadcastReceiver {
		return object : GlobalBroadcastReceiver(lifecycleService, intentFilter) { override fun onReceive(context: Context?, intent: Intent?) { onReceive?.invoke(intent) } }
	}
}