package pt.hdn.toolbox.misc

import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback

@DslMarker
annotation class OnPageChangeCallbackDsl

@OnPageChangeCallbackDsl
class OnPageChanged(
	private val viewPager: ViewPager2
) {

	//region vars
	var onPageScrolled: ((position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit)? = null; private set
	var onPageSelected: ((index: Int, lastIndex: Int) -> Unit)? = null; private set
	var onPageScrollStateChanged: ((state: Int) -> Unit)? = null; private set
	//endregion vars

	fun onPageScrolled(onPageScrolled: (position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit) { this.onPageScrolled = onPageScrolled }

	fun onPageSelected(onPageSelected: (index: Int, lastIndex: Int) -> Unit) { this.onPageSelected = onPageSelected }

	fun onPageScrollStateChanged(onPageScrollStateChanged: (state: Int) -> Unit) { this.onPageScrollStateChanged = onPageScrollStateChanged }

	fun build(): OnPageChangeCallback {
		return object : OnPageChangeCallback() {
			override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { onPageScrolled?.invoke(position, positionOffset, positionOffsetPixels) }

			override fun onPageSelected(position: Int) { onPageSelected?.invoke(position, (viewPager.adapter?.itemCount ?: 0) - 1) }

			override fun onPageScrollStateChanged(state: Int) { onPageScrollStateChanged?.invoke(state) }
		}
	}
}