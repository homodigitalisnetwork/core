package pt.hdn.toolbox.misc

import android.app.Service
import androidx.lifecycle.Lifecycle.State
import androidx.lifecycle.Lifecycle.State.RESUMED
import androidx.lifecycle.Lifecycle.State.STARTED
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import pt.hdn.toolbox.annotations.Address
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.observe
import pt.hdn.toolbox.communications.bus.observeWith
import javax.inject.Inject

@DslMarker
annotation class DataBusScopeObserverDsl

@DslMarker
annotation class DataBusOwnerObserverDsl

/**
 * A DSL to reduce boilerplate when adding multiple observers to the [DataBus] at the same time.
 */
@DataBusScopeObserverDsl
class DataBusScopeObservers(
	val dataBus: DataBus,
	val scope: CoroutineScope
) {
	inline fun <reified R: Any?> observe(@Address address: Int, flowCollector: FlowCollector<R>): Job = dataBus.observe(scope, address, flowCollector =  flowCollector)

	inline fun <reified R: Any?> observeWith(@Address address: Int, crossinline block: R.() -> Unit): Job = dataBus.observeWith(scope, address, block = block)
}

/**
 * A DSL to reduce boilerplate when adding multiple observers to the [DataBus] at the same time.
 */
@DataBusOwnerObserverDsl
class DataBusLifecycleObservers(
	val dataBus: DataBus,
	val owner: LifecycleOwner
) {
	inline fun <reified R: Any?> observe(@Address address: Int, state: State = if (owner is Service) STARTED else RESUMED, crossinline block: CoroutineScope.(value: R) -> Unit): Job {
		return dataBus.observe(owner, address, state = state, block = block)
	}

	inline fun <reified R: Any?> observeWith(@Address address: Int, state: State = if (owner is Service) STARTED else RESUMED, crossinline block: R.() -> Unit): Job {
		return dataBus.observeWith(owner, address, state = state, block = block)
	}
}