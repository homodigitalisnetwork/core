package pt.hdn.toolbox.misc

import android.location.Location
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult

@DslMarker
annotation class LocationCallbackDsl

/**
 * A DSL builder type to create [LocationCallback] objects
 */
@LocationCallbackDsl
class Location {

	//region vars
	var onLocationResult: (LocationCallback.(location: Location) -> Unit)? = null; private set
	var onLocationAvailability: (LocationCallback.(locationAvailability: LocationAvailability) -> Unit)? = null; private set
	//endregion vars

	fun onLocationResult(onLocationResult: LocationCallback.(location: Location) -> Unit) { this.onLocationResult = onLocationResult }

	fun onLocationAvailability(onLocationAvailability: LocationCallback.(locationAvailability: LocationAvailability) -> Unit) { this.onLocationAvailability = onLocationAvailability }

	fun build(): LocationCallback {
		return object : LocationCallback() {
			override fun onLocationResult(p0: LocationResult) { p0.lastLocation?.let { onLocationResult?.invoke(this, it) } }

			override fun onLocationAvailability(p0: LocationAvailability) { onLocationAvailability?.invoke(this, p0) }
		}
	}
}