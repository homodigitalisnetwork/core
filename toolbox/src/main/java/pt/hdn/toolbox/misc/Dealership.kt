package pt.hdn.toolbox.misc

import pt.hdn.toolbox.communications.bus.ErrorBus

/**
 * An abstract class that rose from the need to create, edit, validate and reduce mutable objects
 * like a user object (on registry an object is created and needs to be validate and later on it can be edited, validate and reduce if the database is normalized).
 * Like a car dealership, you can buy a new car or repair if broken.
 */
abstract class Dealership {
    val errorBus: ErrorBus = ErrorBus()
}