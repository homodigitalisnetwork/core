package pt.hdn.toolbox.singles.adapter

import android.graphics.drawable.VectorDrawable
import android.os.Bundle
import android.text.format.DateUtils
import android.view.View.*
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TableLayout
import android.widget.TableRow
import androidx.annotation.StringRes
import com.google.android.gms.maps.CameraUpdateFactory.newLatLngZoom
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL
import com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle
import com.google.android.material.textview.MaterialTextView
import com.google.maps.android.ktx.addMarker
import pt.hdn.contract.schemas.*
import pt.hdn.toolbox.R
import pt.hdn.toolbox.adapter.MapViewHolder
import pt.hdn.toolbox.annotations.Action.Companion.ABORT
import pt.hdn.toolbox.annotations.Action.Companion.CANCEL
import pt.hdn.toolbox.annotations.Action.Companion.CONCLUDE
import pt.hdn.toolbox.databinding.ViewholderSingleBinding
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.toBitmapDescriptor
import pt.hdn.toolbox.resources.Resources
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle.MEDIUM

class SingleViewHolder(
    resources: Resources,
    parent: ViewGroup
) : MapViewHolder<ViewholderSingleBinding, Single>(resources, parent, ViewholderSingleBinding::inflate) {

    //region vars
    private val tinyMargin: Int = dimen(R.dimen.t_size)
    private val smallMargin: Int = dimen(R.dimen.s_size)
    private lateinit var position: Position
    //endregion vars

    init {
        with(binding) {
            lblSingleSpecialityField.text = string(R.string.speciality)
            lblSingleDurationField.text = string(R.string.duration)
            lblSingleExchangeField.text = string(R.string.value)
            lblSingleTaxField.text = string(R.string.tax)
        }
    }

    override fun Single.setViewHolder(isExpanded: Boolean) {
        with(binding) {
            this@SingleViewHolder.position = position

            lblSingleDate.text = date
            lblSingleAmount.text = formattedAmount(isBuyerSide, amount?.let { it + if (isBuyerSide) tax ?: ZERO else ZERO })

            when (status) {
                CANCEL -> R.drawable.ic_cancel
                ABORT -> R.drawable.ic_abort
                CONCLUDE -> R.drawable.ic_completed
                else -> R.drawable.ic_question
            }.let { imgSingleStatus.setImageResource(it) }

            layoutSingleBody.visibility = if (isExpanded) VISIBLE else GONE

            if (isExpanded) {
                lblSingleSpecialityValue.text = type!!.name
                lblSingleDurationValue.text = null
                rowSingleDuration.visibility = GONE
                lblSingleExchangeValue.text = null
                rowSingleExchange.visibility = GONE
                lblSingleTaxValue.text = null
                rowSingleTax.visibility = GONE

//                addSchemas(schemas)

                if (status == ABORT || status == CONCLUDE) {
                    chrono?.let { lblSingleDurationValue.text = DateUtils.formatElapsedTime(it.elapsedTimeSeconds); rowSingleDuration.visibility = VISIBLE }
                    amount?.let { lblSingleExchangeValue.text = formattedAmount(isBuyerSide, it); rowSingleExchange.visibility = VISIBLE }
                    if (isBuyerSide) tax?.let { lblSingleTaxValue.text = "- %.2f".format(it); rowSingleTax.visibility = VISIBLE }
                }

                mapSinglePosition.apply { onCreate(null); onResume(); getMapAsyncWithContext(this@SingleViewHolder) }
            }
        }
    }

    override fun GoogleMap.mapReady() {
        uiSettings.apply { isMapToolbarEnabled = false; setAllGesturesEnabled(false) }

        mapType = MAP_TYPE_NORMAL

        setMapStyle(loadRawResourceStyle(context, if (res.isDarkModeOn) R.raw.map_styling_dark else R.raw.map_styling_light))

        this@SingleViewHolder.position.toLatLng()
            .let {
                addMarker { draggable(false); position(it); icon((drawable(R.drawable.ic_mini_pin) as VectorDrawable).toBitmapDescriptor()) }

                moveCamera(newLatLngZoom(it, 13f))
            }
    }

    override fun ViewholderSingleBinding.saveInstanceState(bundle: Bundle) { mapSinglePosition.onSaveInstanceState(bundle) }

    override fun ViewholderSingleBinding.resume() { mapSinglePosition.onResume() }

    override fun ViewholderSingleBinding.pause() { mapSinglePosition.onPause() }

    override fun ViewholderSingleBinding.stop() { mapSinglePosition.onStop() }

    override fun ViewholderSingleBinding.lowMemory() { mapSinglePosition.onLowMemory() }

    override fun ViewholderSingleBinding.destroy() { mapSinglePosition.onDestroy() }

//    private fun addSchemas(schemas: List<Schema>) {
//        binding.tbSingleSchema.removeAllViews()
//
//        schemas
//            .forEachWith {
//                when (this) {
//                    is FixSchema -> { addDescription(string(R.string.fix)); addParameter(R.string.fix, fix) }
//                    is RateSchema -> { addDescription(string(R.string.rate)); addParameter(R.string.rate, rate) }
//                    is CommissionSchema -> {
//                        addDescription(string(R.string.commission))
//                        addParameter(R.string.cut, cut)
//                        upperBound?.let { addParameter(R.string.upperBound, it) }
//                        lowerBound?.let { addParameter(R.string.lowerBound, it) }
//                    }
//                    is ObjectiveSchema -> {
//                        addDescription(string(R.string.objective))
//                        addParameter(R.string.bonus, bonus)
//                        upperBound?.let { addParameter(R.string.upperBound, it) }
//                        lowerBound?.let { addParameter(R.string.lowerBound, it) }
//                    }
//                    is ThresholdSchema -> {
//                        addDescription(string(R.string.threshold))
//                        addParameter(R.string.bonus, bonus)
//                        addParameter(R.string.thresholdType, if (isAbove!!) R.string.aboveLimit else R.string.belowLimit)
//                        addParameter(R.string.threshold, threshold)
//
//                    }
//                }
//            }
//    }
//
//    private fun addDescription(value: String) {
//        MaterialTextView(context)
//            .apply {
//                layoutParams = TableLayout.LayoutParams().apply { setMargins(tinyMargin, tinyMargin, tinyMargin, 0) }
//                text = value
//
//                binding.tbSingleSchema.addView(this)
//            }
//    }
//
//    private fun addParameter (@StringRes field: Int, value: BigDecimal?) { addParameter(field, value?.toString()) }
//
//    private fun addParameter (@StringRes field: Int, @StringRes value: Int) { addParameter(field, string(value)) }
//
//    private fun addParameter (@StringRes field: Int, value: String?) {
//        TableRow(context)
//            .apply {
//                layoutParams = TableLayout.LayoutParams().apply { setMargins(smallMargin, tinyMargin, tinyMargin, 0) }
//
//                MaterialTextView(context)
//                    .apply {
//                        layoutParams = TableRow.LayoutParams().apply { setMargins(0, 0, tinyMargin, 0) }
//                        textAlignment = TEXT_ALIGNMENT_TEXT_END
//                        text = string(field)
//
//                        addView(this)
//                    }
//
//                MaterialTextView(context)
//                    .apply {
//                        layoutParams = TableRow.LayoutParams().apply { setMargins(tinyMargin, 0, 0, 0) }
//                        textAlignment = TEXT_ALIGNMENT_TEXT_START
//                        text = value
//
//                        addView(this)
//                    }
//
//                binding.tbSingleSchema.addView(this)
//            }
//    }

    private fun formattedAmount(isBuyerSide: Boolean, value: BigDecimal?): String? {
        return value?.let { (when { (it > ZERO) xor isBuyerSide -> "+ "; it == ZERO -> ""; else -> "- " } + "%.2f").format(it) }
    }
}