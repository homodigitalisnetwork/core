package pt.hdn.toolbox.singles.adapter

import android.view.ViewGroup
import pt.hdn.toolbox.adapter.GoogleMapListAdapter
import pt.hdn.toolbox.resources.Resources

class SinglesAdapter(
	singles: MutableList<Single>,
	resources: Resources
) : GoogleMapListAdapter<SingleViewHolder, Single>(singles, resources) {
	override fun onCreateViewHolder(resources: Resources, parent: ViewGroup, viewType: Int): SingleViewHolder = SingleViewHolder(resources, parent)
}
