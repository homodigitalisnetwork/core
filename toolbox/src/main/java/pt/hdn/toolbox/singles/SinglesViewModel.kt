package pt.hdn.toolbox.singles

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import pt.hdn.toolbox.communications.bus.DataBus
import pt.hdn.toolbox.communications.bus.SessionBus
import pt.hdn.toolbox.communications.singles.SinglesRequest
import pt.hdn.toolbox.communications.success
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.repo.CoreRepo
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.singles.adapter.SinglesAdapter
import pt.hdn.toolbox.viewmodels.CoreViewModel
import javax.inject.Inject

@HiltViewModel
class SinglesViewModel @Inject constructor(
    dataBus: DataBus,
    sessionBus: SessionBus,
    repo: CoreRepo,
    resources: Resources
) : CoreViewModel(dataBus, sessionBus, repo, resources) {

    //region vars
    val singlesAdapter: Flow<SinglesAdapter?>
    //endregion vars

    init {
        this.singlesAdapter = flowOf(SinglesRequest(partyUUID = session.partyUUID))
            .flatMapLatest { repo.singles(it) }
            .map { var adapter: SinglesAdapter? = null; it.success { singles.forEachWith { type = res.specialities[typeUUID] }; adapter = SinglesAdapter(singles, res) }; adapter }
            .shareIn(viewModelScope, Lazily, 1)
    }
}