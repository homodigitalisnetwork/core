package pt.hdn.toolbox.singles.adapter

import com.google.gson.annotations.Expose
import pt.hdn.contract.schemas.Schema
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.annotations.Action
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.transaction.util.Chrono
import java.math.BigDecimal
import java.util.*

data class Single (
	@Expose val typeUUID: UUID,
	@Expose val position: Position,
	@Expose val isBuyerSide: Boolean,
	@Expose val chrono: Chrono?,
	@Expose val schemas: MutableList<Schema>,
	@Action @Expose val status: Int,
	@Expose val amount: BigDecimal?,
	@Expose val tax: BigDecimal?,
	@Expose val date: String,
	var type: UUIDNameMarketData? = null
)