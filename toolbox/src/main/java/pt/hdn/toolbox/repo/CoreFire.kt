package pt.hdn.toolbox.repo

import android.net.Uri
import android.util.Log
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.database.DatabaseException
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageReference
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.tasks.await
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.annotations.*
import pt.hdn.toolbox.annotations.Field.Companion.MARKET
import pt.hdn.toolbox.annotations.Field.Companion.NAME
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.annotations.FirebasePath.Companion.ABORT
import pt.hdn.toolbox.annotations.FirebasePath.Companion.ACKNOWLEDGE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.APPS
import pt.hdn.toolbox.annotations.FirebasePath.Companion.BUYER
import pt.hdn.toolbox.annotations.FirebasePath.Companion.CATALOGUE_LOCALE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.CATALOGUE_SPECIALITIES
import pt.hdn.toolbox.annotations.FirebasePath.Companion.CHATS
import pt.hdn.toolbox.annotations.FirebasePath.Companion.CHRONO
import pt.hdn.toolbox.annotations.FirebasePath.Companion.CONTRACT
import pt.hdn.toolbox.annotations.FirebasePath.Companion.DEPUTIES
import pt.hdn.toolbox.annotations.FirebasePath.Companion.DISTANCE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.KILL
import pt.hdn.toolbox.annotations.FirebasePath.Companion.LEVEL
import pt.hdn.toolbox.annotations.FirebasePath.Companion.LOCALE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.LOCATION
import pt.hdn.toolbox.annotations.FirebasePath.Companion.LOGIN
import pt.hdn.toolbox.annotations.FirebasePath.Companion.NOTIFICATIONS
import pt.hdn.toolbox.annotations.FirebasePath.Companion.PAUSE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.PAUSE_LENGTH
import pt.hdn.toolbox.annotations.FirebasePath.Companion.POSITION
import pt.hdn.toolbox.annotations.FirebasePath.Companion.RING
import pt.hdn.toolbox.annotations.FirebasePath.Companion.SEARCHABLE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.SELLER
import pt.hdn.toolbox.annotations.FirebasePath.Companion.START
import pt.hdn.toolbox.annotations.FirebasePath.Companion.STOP
import pt.hdn.toolbox.annotations.FirebasePath.Companion.THUMBNAILS
import pt.hdn.toolbox.annotations.FirebasePath.Companion.TRANSACTIONS
import pt.hdn.toolbox.annotations.FirebasePath.Companion.USERS
import pt.hdn.toolbox.annotations.FirebasePath.Companion.VARIABLES
import pt.hdn.toolbox.chat.ChatMessage
import pt.hdn.toolbox.chat.DefaultChatMessage
import pt.hdn.toolbox.communications.message.Message
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.misc.*
import pt.hdn.toolbox.preferences.DefaultPreferences
import pt.hdn.toolbox.preferences.Preferences
import pt.hdn.toolbox.transaction.util.DefaultTransaction
import java.util.*

open class CoreFire constructor(
    protected val ref: DatabaseReference,
    protected val auth: FirebaseAuth,
    protected val stg: StorageReference,
    protected val appUUID: UUID,
    protected val gson: Gson
) {

    //region vars
    //endregion vars

    fun exceptionHandler(e: Throwable): Result<Nothing> {
        Log.e("Fire", "exceptionHandler: ", e)

        return when(e) {
            is DatabaseException -> Result.Error(Err.FIREBASE, e)
            is FirebaseAuthInvalidCredentialsException -> Result.Error(Err.CREDENTIALS, e)
            is StorageException -> Result.Error(Err.FIRESTORE, e)
            else -> Result.Unknown
        }
    }

    suspend fun signIn(token: String): AuthResult = auth.signInWithCustomToken(token).await()

    suspend fun dbAvailableLocales(): List<String> = ref.child(CATALOGUE_LOCALE).awaitOnce()

    suspend fun deviceToken(): String = Firebase.messaging.token.await()

    suspend fun deputyPreferences(deputyUUID: UUID): Preferences = ref.child(USERS).child(deputyUUID).awaitOnce<DefaultPreferences>().apply { appUUID = this@CoreFire.appUUID }

    suspend fun partyPreferences(partyUUID: UUID, preferences: Preferences, isNotSpot: Boolean) { ref.child(USERS).child(partyUUID).setValue(preferences.toMap(appUUID, isNotSpot)).await() }

    suspend fun specialities(sessionLocale: String): Map<UUID, UUIDNameMarketData> {
        return ref
            .child(CATALOGUE_SPECIALITIES)
            .child(sessionLocale)
            .awaitOnce {
                it
                    .children
                    .associateBy({ UUID.fromString(it.key!!) }) {
                        with(it.value as Map<String, Any?>) {
                            UUIDNameMarketData(uuid = UUID.fromString(this[Field.UUID] as String), name = this[NAME] as String, market = (this[MARKET] as Long).toInt())
                        }
                    }
            }
    }

    suspend fun session(partyUUID: UUID, deputyUUID: UUID, sessionUUID: UUID, deviceToken: String) {
        val deputyUUIDString: String = deputyUUID.toString()

        buildMap<String, Any> {
            this[path(deputyUUID, DEPUTIES, deputyUUIDString)] = deviceToken; this[path(deputyUUID, LOGIN)] = sessionUUID.toString()

            if (partyUUID != deputyUUID) { this[path(partyUUID, DEPUTIES, deputyUUIDString)] = deviceToken }
        }.let { ref.child(VARIABLES).updateChildren(it) }
    }

    suspend fun notify(message: Message) { ref.child(NOTIFICATIONS).child(message.receiverUUID).setValue(message.payload).await() }

    suspend fun searchable(deputyUUID: UUID, searchable: Boolean) { ref.child(USERS).child(deputyUUID).child(APPS).child(appUUID).child(SEARCHABLE).setValue(searchable).await() }

    suspend fun locationSource(deputyUUID: UUID, location: Boolean) { ref.child(USERS).child(deputyUUID).child(APPS).child(appUUID).child(LOCATION).setValue(location).await() }

    suspend fun settings(deputyUUID: UUID, searchable: Boolean?, location: Boolean?) {
        buildMap<String, Any> { searchable?.let { this[path(deputyUUID, SEARCHABLE)] = it }; location?.let { this[path(deputyUUID, LOCATION)] = it } }
            .let { ref.child(USERS).updateChildren(it).await() }
    }

    suspend fun signOut(partyUUID: UUID, deputyUUID: UUID, cleanUp: Boolean) {
        val deputyUUIDString: String = deputyUUID.toString()

        if (cleanUp) {
            buildMap<String, Any?> {
                this[path(deputyUUID, DEPUTIES, deputyUUIDString)] = null
                this[path(deputyUUID, LOGIN)] = ""
                if (partyUUID != deputyUUID) this[path(partyUUID, DEPUTIES, deputyUUIDString)] = null
            }.let { ref.child(VARIABLES).updateChildren(it).await() }
        }

        auth.signOut()
    }

    suspend fun putPhoto(partyUUID: UUID, photoUri: Uri) { stg.child(THUMBNAILS).child(partyUUID).child("$partyUUID${Misc.JPEG_SUFFIX}").putFile(photoUri).await() }

    suspend fun getPhoto(uuid: UUID, photo: Photo) { stg.child(THUMBNAILS).child(uuid).child("$uuid${Misc.JPEG_SUFFIX}").getFile(photo.file!!).await() }

    suspend fun remove(partyUUID: UUID) { ref.child(KILL).child(partyUUID).setValue(mapOf(FirebasePath.UUID to appUUID.toString())).await() }

    suspend fun recover(partyUUID: UUID, transactionUUID: UUID): DefaultTransaction { return ref.child(TRANSACTIONS).child(transactionUUID).awaitOnce { DefaultTransaction.from(it, gson, partyUUID) } }

    suspend fun locale(partyUUID: UUID, locale: String) { ref.child(USERS).child(partyUUID).child(LOCALE).setValue(locale).await() }

    suspend fun updatePreferences(partyUUID: UUID, preferences: Map<String, Any>) { ref.child(USERS).child(partyUUID).updateChildren(preferences).await() }

    suspend fun abortListener(transactionUUID: UUID): Flow<Boolean> = ref.child(TRANSACTIONS).child(transactionUUID).child(ABORT).flow()

    suspend fun levelListener(transactionUUID: UUID): Flow<Int> = ref.child(TRANSACTIONS).child(transactionUUID).child(LEVEL).flow()

    suspend fun ringListener(transactionUUID: UUID): Flow<Int> = ref.child(TRANSACTIONS).child(transactionUUID).child(RING).flow()

    suspend fun chatOverwatch(transactionUUID: UUID, deputyUUID: UUID): Flow<ChatMessage> {
        return ref.child(CHATS).child(transactionUUID).flowChild { DefaultChatMessage.from(it, deputyUUID) }
    }

    suspend fun chatListenOnce(transactionUUID: UUID, deputyUUID: UUID, key: String? = null): Flow<List<ChatMessage>> {
        return ref
            .child(CHATS)
            .child(transactionUUID)
            .run { key?.let { orderByKey().startAfter(it) } ?: this }
            .flowOnce { buildList { it.takeIf { it.hasChildren() }?.children?.forEach { add(DefaultChatMessage.from(it, deputyUUID)) } } }
    }

    suspend fun contractListener(transactionUUID: UUID): Flow<Contract> {
        return ref.child(TRANSACTIONS).child(transactionUUID).child(CONTRACT).flow { it.getValue<String>()?.let { gson.from(it) } }
    }

    suspend fun level(transactionUUID: UUID, @Level level: Int) { ref.child(TRANSACTIONS).child(transactionUUID).child(LEVEL).setValue(level).await() }

    suspend fun message(transactionUUID: UUID, chatMessage: ChatMessage) { ref.child(CHATS).child(transactionUUID).push().setValue(chatMessage.toMap()).await() }

    suspend fun positionListener(transactionUUID: UUID): Flow<Position> = ref.child(TRANSACTIONS).child(transactionUUID).child(POSITION).flow()

    suspend fun position(transactionUUID: UUID, position: Position, distance: Double) {
        ref.child(TRANSACTIONS).child(transactionUUID).updateChildren(buildMap { this[POSITION] = position.toMap(); this[DISTANCE] = distance }).await()
    }

    suspend fun abort(transactionUUID: UUID) { ref.child(TRANSACTIONS).child(transactionUUID).child(ABORT).setValue(true).await() }

    suspend fun start(transactionUUID: UUID, start: Long) { ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).child(START).setValue(start).await() }

    suspend fun startListener(transactionUUID: UUID): Flow<Long> = ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).child(START).flow()

    suspend fun pause(transactionUUID: UUID, pause: Long) { ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).child(PAUSE).setValue(pause).await() }

    suspend fun pauseListener(transactionUUID: UUID): Flow<Long> = ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).child(PAUSE).flow()

    suspend fun resume(transactionUUID: UUID, pauseLength: Long) {
        ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).updateChildren(mapOf<String, Long?>(PAUSE to null, PAUSE_LENGTH to pauseLength)).await()
    }

    suspend fun resumeListener(transactionUUID: UUID): Flow<Long> = ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).child(PAUSE_LENGTH).flow()

    suspend fun stop(transactionUUID: UUID, stop: Long) { ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).child(STOP).setValue(stop).await() }

    suspend fun stopListener(transactionUUID: UUID): Flow<Long> = ref.child(TRANSACTIONS).child(transactionUUID).child(CHRONO).child(STOP).flow()

    suspend fun ring(transactionUUID: UUID, count: Int) { ref.child(TRANSACTIONS).child(transactionUUID).child(RING).setValue(count).await() }

    suspend fun acknowledge(transactionUUID: UUID, isBuyerSide: Boolean) {
        ref.child(TRANSACTIONS).child(transactionUUID).child(ACKNOWLEDGE).child(if (isBuyerSide) BUYER else SELLER).setValue(true)
    }

    suspend fun token(deputyUUID: UUID, token: String) { ref.child(VARIABLES).child(deputyUUID).child(appUUID).child(DEPUTIES).child(deputyUUID).setValue(token).await() }

    private fun path(prefix: UUID, vararg suffix: String): String {
        return buildString { append("/"); append(prefix); append("/"); append(APPS); append("/"); append(appUUID); suffix.forEach { append("/"); append(it) } }
    }

    protected fun DatabaseReference.child(uuid: UUID): DatabaseReference = child(uuid.toString())

    protected fun StorageReference.child(uuid: UUID): StorageReference = child(uuid.toString())
}