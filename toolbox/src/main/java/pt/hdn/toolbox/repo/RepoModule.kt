package pt.hdn.toolbox.repo

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.CookieJar
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import pt.hdn.toolbox.BuildConfig.API_VERSION
import pt.hdn.toolbox.application.CoreGson
import pt.hdn.toolbox.resources.Resources
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.net.CookieManager
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PhotosDir

@Module
@InstallIn(SingletonComponent::class)
object RepoModule {

    //region vars
    private const val CORE_URL = "https://core.hdnet.pt/$API_VERSION/"
    //endregion vars

    @Singleton
    @Provides
    fun providesCookieJar(): CookieJar = JavaNetCookieJar(CookieManager()/*.apply { setCookiePolicy(ACCEPT_ALL) }*/)

    @Singleton
    @Provides
    fun providesOkHttpClient(cookieJar: CookieJar): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(BODY))
            .cookieJar(cookieJar)
            .build()
    }

    @Singleton
    @Provides
    fun providesCoreAPI(@CoreGson gson: Gson, okHttpClient: OkHttpClient): CoreAPI {
        return Retrofit
            .Builder()
            .client(okHttpClient)
            .baseUrl(CORE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(CoreAPI::class.java)
    }

    @Singleton
    @Provides
    fun providesCoreSQL(api: CoreAPI): CoreSQL = CoreSQL(api)

    @Singleton
    @Provides
    fun providesDatabaseReference(): DatabaseReference = Firebase.database.reference

    @Singleton
    @Provides
    fun providesFirebaseAuth(): FirebaseAuth = Firebase.auth

    @Singleton
    @Provides
    fun providesFirebaseStorage(): StorageReference = Firebase.storage.reference

    @Singleton
    @Provides
    fun providesCoreFire(ref: DatabaseReference, auth: FirebaseAuth, stg: StorageReference, resources: Resources, @CoreGson gson: Gson): CoreFire = CoreFire(ref, auth, stg, resources.appUUID, gson)

    @Singleton
    @Provides
    fun providesCoreRepo(@ApplicationContext context: Context, sql: CoreSQL, fire: CoreFire, resources: Resources, @PhotosDir picsDir: File): CoreRepo = CoreRepo(sql, fire, context, resources, picsDir)
}