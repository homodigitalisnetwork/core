package pt.hdn.toolbox.repo

import pt.hdn.toolbox.communications.broadcast.BroadcastRequest
import pt.hdn.toolbox.communications.resumes.ResumesRequest
import pt.hdn.toolbox.communications.resumes.ResumesResponse
import pt.hdn.toolbox.communications.generic.Response
import pt.hdn.toolbox.communications.login.LogInRequest
import pt.hdn.toolbox.communications.login.SignInResponse
import pt.hdn.toolbox.communications.market.PriceRequest
import pt.hdn.toolbox.communications.partnership.PartnershipsRequest
import pt.hdn.toolbox.communications.partnership.PartnershipsResponse
import pt.hdn.toolbox.communications.entity.EntityRequest
import pt.hdn.toolbox.communications.entity.EntityResponse
import pt.hdn.toolbox.communications.market.PriceResponse
import pt.hdn.toolbox.communications.performance.PerformanceRequest
import pt.hdn.toolbox.communications.performance.PerformanceResponse
import pt.hdn.toolbox.communications.recurrences.RecurrencesRequest
import pt.hdn.toolbox.communications.recurrences.RecurrencesResponse
import pt.hdn.toolbox.communications.report.ReportRequest
import pt.hdn.toolbox.communications.report.ReportResponse
import pt.hdn.toolbox.communications.resources.ResourcesRequest
import pt.hdn.toolbox.communications.resources.ResourcesResponse
import pt.hdn.toolbox.communications.signature.SignatureRequest
import pt.hdn.toolbox.communications.signature.SignatureResponse
import pt.hdn.toolbox.communications.signout.SignOutResponse
import pt.hdn.toolbox.communications.singles.SinglesRequest
import pt.hdn.toolbox.communications.singles.SinglesResponse
import pt.hdn.toolbox.communications.specialities.SpecialitiesRequest
import pt.hdn.toolbox.communications.specialities.SpecialitiesResponse
import pt.hdn.toolbox.communications.spotter.SearchRequest
import pt.hdn.toolbox.communications.spotter.SellersResponse
import pt.hdn.toolbox.communications.transaction.TransactionRequest
import retrofit2.http.Body
import retrofit2.http.POST

private const val BROADCAST = "broadcast"
private const val ENTITY = "entity"
private const val PRICE = "price"
private const val PARTNERSHIPS = "partnerships"
private const val PERFORMANCE = "performance"
private const val PING ="ping"
private const val RECURRENCES = "recurrences"
private const val REPORT = "report"
private const val RESOURCES = "resources"
private const val RESUMES = "resumes"
private const val SELLERS = "sellers"
private const val SIGNATURE = "signature"
private const val SIGNIN = "signin"
private const val SIGNOUT = "signout"
private const val SINGLES = "singles"
private const val SPECIALITIES = "specialities"
private const val TRANSACTION = "transaction"

interface CoreAPI {
    @POST(BROADCAST) suspend fun broadcast(@Body broadcastRequest: BroadcastRequest): Response
    @POST(ENTITY) suspend fun entity(@Body entityRequest: EntityRequest): EntityResponse
    @POST(PARTNERSHIPS) suspend fun partnerships(@Body partnershipsRequest: PartnershipsRequest): PartnershipsResponse
    @POST(PERFORMANCE) suspend fun performance(@Body performanceRequest: PerformanceRequest): PerformanceResponse
    @POST(PING) suspend fun ping()
    @POST(PRICE) suspend fun price(@Body priceRequest: PriceRequest): PriceResponse
    @POST(RECURRENCES) suspend fun recurrences(@Body recurrencesRequest: RecurrencesRequest): RecurrencesResponse
    @POST(REPORT) suspend fun report(@Body reportRequest: ReportRequest): ReportResponse
    @POST(RESOURCES) suspend fun resources(@Body resourcesRequest: ResourcesRequest): ResourcesResponse
    @POST(RESUMES) suspend fun resumes(@Body resumesRequest: ResumesRequest): ResumesResponse
    @POST(SELLERS) suspend fun sellers(@Body searchRequest: SearchRequest): SellersResponse
    @POST(SIGNATURE) suspend fun signature(@Body signatureRequest: SignatureRequest): SignatureResponse
    @POST(SIGNIN) suspend fun signIn(@Body logInRequest: LogInRequest): SignInResponse
    @POST(SIGNOUT) suspend fun signOut(): SignOutResponse
    @POST(SINGLES) suspend fun singles(@Body singlesRequest: SinglesRequest): SinglesResponse
    @POST(SPECIALITIES) suspend fun specialities(@Body specialitiesRequest: SpecialitiesRequest): SpecialitiesResponse
    @POST(TRANSACTION) suspend fun transaction(@Body transactionRequest: TransactionRequest): Response
}