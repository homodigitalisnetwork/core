package pt.hdn.toolbox.repo

import android.util.Log
import pt.hdn.toolbox.annotations.Err
import retrofit2.HttpException
import pt.hdn.toolbox.communications.Result
import pt.hdn.toolbox.communications.broadcast.BroadcastRequest
import pt.hdn.toolbox.communications.resumes.ResumesRequest
import pt.hdn.toolbox.communications.resumes.ResumesResponse
import pt.hdn.toolbox.communications.generic.Response
import pt.hdn.toolbox.communications.login.LogInRequest
import pt.hdn.toolbox.communications.login.SignInResponse
import pt.hdn.toolbox.communications.market.PriceRequest
import pt.hdn.toolbox.communications.partnership.PartnershipsRequest
import pt.hdn.toolbox.communications.partnership.PartnershipsResponse
import pt.hdn.toolbox.communications.entity.EntityRequest
import pt.hdn.toolbox.communications.entity.EntityResponse
import pt.hdn.toolbox.communications.market.PriceResponse
import pt.hdn.toolbox.communications.performance.PerformanceRequest
import pt.hdn.toolbox.communications.performance.PerformanceResponse
import pt.hdn.toolbox.communications.recurrences.RecurrencesRequest
import pt.hdn.toolbox.communications.recurrences.RecurrencesResponse
import pt.hdn.toolbox.communications.report.ReportRequest
import pt.hdn.toolbox.communications.report.ReportResponse
import pt.hdn.toolbox.communications.resources.ResourcesRequest
import pt.hdn.toolbox.communications.resources.ResourcesResponse
import pt.hdn.toolbox.communications.signature.SignatureRequest
import pt.hdn.toolbox.communications.signature.SignatureResponse
import pt.hdn.toolbox.communications.signout.SignOutResponse
import pt.hdn.toolbox.communications.singles.SinglesRequest
import pt.hdn.toolbox.communications.singles.SinglesResponse
import pt.hdn.toolbox.communications.specialities.SpecialitiesRequest
import pt.hdn.toolbox.communications.specialities.SpecialitiesResponse
import pt.hdn.toolbox.communications.spotter.SearchRequest
import pt.hdn.toolbox.communications.spotter.SellersResponse
import pt.hdn.toolbox.communications.transaction.TransactionRequest
import java.net.SocketTimeoutException

open class CoreSQL constructor(
    private val api: CoreAPI
) {

    fun exceptionHandler(throwable: Throwable): Result<Nothing> {
        Log.e("SQL", "exceptionHandler: throwable", throwable)

        return when (throwable) {
            is HttpException -> /*throwable.response()?.errorBody()?.run { Result.Error(charStream()) } ?: Result.Unknown*/ Result.Error(throwable.code(), throwable)
            is SocketTimeoutException -> Result.Error(Err.TIMEOUT, throwable)
            else -> Result.Unknown
        }
    }

    suspend fun broadcast(broadcastRequest: BroadcastRequest): Response = api.broadcast(broadcastRequest)
    suspend fun entity(entityRequest: EntityRequest): EntityResponse = api.entity(entityRequest)
    suspend fun price(priceRequest: PriceRequest): PriceResponse = api.price(priceRequest)
    suspend fun partnerships(partnershipsRequest: PartnershipsRequest): PartnershipsResponse = api.partnerships(partnershipsRequest)
    suspend fun performance(performanceRequest: PerformanceRequest): PerformanceResponse = api.performance(performanceRequest)
    suspend fun ping() = api.ping()
    suspend fun recurrences(recurrencesRequest: RecurrencesRequest): RecurrencesResponse = api.recurrences(recurrencesRequest)
    suspend fun report(reportRequest: ReportRequest): ReportResponse = api.report(reportRequest)
    suspend fun resources(resourcesRequest: ResourcesRequest): ResourcesResponse = api.resources(resourcesRequest)
    suspend fun resumes(resumesRequest: ResumesRequest): ResumesResponse = api.resumes(resumesRequest)
    suspend fun sellers(searchRequest: SearchRequest): SellersResponse = api.sellers(searchRequest)
    suspend fun signature(signatureRequest: SignatureRequest): SignatureResponse = api.signature(signatureRequest)
    suspend fun signIn(logInRequest: LogInRequest): SignInResponse = api.signIn(logInRequest)
    suspend fun signOut(): SignOutResponse = api.signOut()
    suspend fun singles(singlesRequest: SinglesRequest): SinglesResponse = api.singles(singlesRequest)
    suspend fun specialities(specialitiesRequest: SpecialitiesRequest): SpecialitiesResponse = api.specialities(specialitiesRequest)
    suspend fun transaction(transactionRequest: TransactionRequest): Response = api.transaction(transactionRequest)
}