package pt.hdn.toolbox.repo

import android.content.Context
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import pt.hdn.contract.util.Contract
import pt.hdn.contract.util.UUIDNameMarketData
import pt.hdn.toolbox.annotations.*
import pt.hdn.toolbox.annotations.Action.Companion.GET
import pt.hdn.toolbox.annotations.Action.Companion.REMOVE
import pt.hdn.toolbox.annotations.Action.Companion.SET
import pt.hdn.toolbox.annotations.FirebasePath.Companion.LOCALE
import pt.hdn.toolbox.annotations.Misc.Companion.DEFAULT_LOCALE
import pt.hdn.toolbox.chat.ChatMessage
import pt.hdn.toolbox.communications.*
import pt.hdn.toolbox.communications.broadcast.BroadcastRequest
import pt.hdn.toolbox.communications.resumes.ResumesRequest
import pt.hdn.toolbox.communications.resumes.ResumesResponse
import pt.hdn.toolbox.communications.generic.Response
import pt.hdn.toolbox.communications.login.LogInRequest
import pt.hdn.toolbox.communications.login.LogInResponse
import pt.hdn.toolbox.communications.login.SignInResponse
import pt.hdn.toolbox.communications.market.PriceRequest
import pt.hdn.toolbox.communications.market.PriceResponse
import pt.hdn.toolbox.communications.partnership.PartnershipsRequest
import pt.hdn.toolbox.communications.partnership.PartnershipsResponse
import pt.hdn.toolbox.communications.entity.EntityRequest
import pt.hdn.toolbox.communications.entity.EntityResponse
import pt.hdn.toolbox.communications.message.Message
import pt.hdn.toolbox.communications.performance.PerformanceRequest
import pt.hdn.toolbox.communications.performance.PerformanceResponse
import pt.hdn.toolbox.communications.photo.PhotoRequest
import pt.hdn.toolbox.communications.photo.PhotoResponse
import pt.hdn.toolbox.communications.recurrences.RecurrencesRequest
import pt.hdn.toolbox.communications.recurrences.RecurrencesResponse
import pt.hdn.toolbox.communications.report.ReportRequest
import pt.hdn.toolbox.communications.report.ReportResponse
import pt.hdn.toolbox.communications.resources.ResourcesRequest
import pt.hdn.toolbox.communications.resources.ResourcesResponse
import pt.hdn.toolbox.communications.signature.SignatureRequest
import pt.hdn.toolbox.communications.signature.SignatureResponse
import pt.hdn.toolbox.communications.signout.SignOutResponse
import pt.hdn.toolbox.communications.singles.SinglesRequest
import pt.hdn.toolbox.communications.singles.SinglesResponse
import pt.hdn.toolbox.communications.specialities.SpecialitiesRequest
import pt.hdn.toolbox.communications.specialities.SpecialitiesResponse
import pt.hdn.toolbox.communications.spotter.SearchRequest
import pt.hdn.toolbox.communications.spotter.SellersResponse
import pt.hdn.toolbox.communications.transaction.TransactionRequest
import pt.hdn.toolbox.misc.Photo
import pt.hdn.toolbox.misc.Position
import pt.hdn.toolbox.misc.forEachWith
import pt.hdn.toolbox.misc.superFlow
import pt.hdn.toolbox.preferences.Preferences
import pt.hdn.toolbox.resources.Resources
import pt.hdn.toolbox.session.DefaultSession
import pt.hdn.toolbox.transaction.util.DefaultTransaction
import java.io.File
import java.util.*

open class CoreRepo constructor(
    private val sql: CoreSQL,
    private val fire: CoreFire,
    protected val context: Context,
    protected val res: Resources,
    protected val picsDir: File
) {

    //region APPLICATION
    suspend fun resources(resourcesRequest: ResourcesRequest): Flow<Result<ResourcesResponse>> {
        return superFlow { try { send(Result.Success(sql.resources(resourcesRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }
    //endregion APPLICATION

    //region FOREGROUND VIEW MODEL
    suspend fun searchable(deputyUUID: UUID, searchable: Boolean): Flow<Result<Boolean>> {
        return superFlow { try { fire.searchable(deputyUUID, searchable); send(Result.Success(searchable)) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun location(deputyUUID: UUID, location: Boolean): Flow<Result<Boolean>> {
        return superFlow { try { fire.locationSource(deputyUUID, location); send(Result.Success(location)) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun settings(deputyUUID: UUID, state: Pair<Boolean?, Boolean?>): Flow<Result<Pair<Boolean?, Boolean?>>> {
        return superFlow { try { with(state) { fire.settings(deputyUUID, first, second); send(Result.Success(state)) } } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun logOut(partyUUID: UUID, deputyUUID: UUID, cleanUp: Boolean): Flow<Result<SignOutResponse>> {
        return superFlow {
            try { fire.signOut(partyUUID, deputyUUID, cleanUp); try { send(Result.Success(sql.signOut())) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
            catch (e: Exception) { send(fire.exceptionHandler(e)) }
        }
    }
    //endregion FOREGROUND VIEW MODEL

    //region HOME VIEW MODEL
    suspend fun recover(partyUUID: UUID, transactionUUID: UUID): Flow<Result<DefaultTransaction>> {
        return superFlow { try { send(Result.Success(fire.recover(partyUUID, transactionUUID))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }
    //endregion HOME VIEW MODEL

    //region LOCATION PROVIDER
    suspend fun broadcast(broadcastRequest: BroadcastRequest): Flow<Result<Response>> {
        return superFlow { try { send(Result.Success(sql.broadcast(broadcastRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }

    suspend fun position(transactionUUID: UUID, position: Position, distance: Double): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.position(transactionUUID, position, distance))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }
    //endregion LOCATION PROVIDER

    //region LOGIN VIEW MODEL
    suspend fun firstLogin(logInRequest: LogInRequest): Flow<Result<SignInResponse>> {
        return superFlow { try { send(Result.Success(sql.signIn(logInRequest).apply { request = logInRequest })) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }

    suspend fun secondLogin(signInResponse: SignInResponse): Flow<Result<LogInResponse>> {
        return superFlow {
            try {
                with(signInResponse) {
                    fire.signIn(token!!)

                    partnership
                        .let {
                            try {
                                val fireDeviceToken: Deferred<String> = async { fire.deviceToken() }

                                val fireDBAvailableLocales: Deferred<List<String>> = async { fire.dbAvailableLocales() }

                                val deputyPreferences: Preferences = fire.deputyPreferences(it.deputyUUID)

                                val resourcesLocale: String = with(deputyPreferences) {
                                    if (res.appAvailableLocales.toMutableList().apply { retainAll(fireDBAvailableLocales.await()) }.contains(locale)) locale else DEFAULT_LOCALE
                                }

                                val fireSpecialities: Deferred<Map<UUID, UUIDNameMarketData>> = async { fire.specialities(resourcesLocale) }

                                fire.session(it.partyUUID, it.deputyUUID, uuid, fireDeviceToken.await())

                                send(Result.Success(LogInResponse(fireSpecialities.await(), resourcesLocale, DefaultSession(uuid, it.apply { preferences = deputyPreferences }))))
                            } catch (e: Exception) {
                                fire.signOut(it.partyUUID, it.deputyUUID, true)

                                throw e
                            }
                        }
                }
            } catch (e: Exception) {
                sql.signOut()

                send(fire.exceptionHandler(e).takeUnless { it is Result.Unknown } ?: sql.exceptionHandler(e))
            }
        }
    }
    //endregion LOGIN VIEW MODEL

    //region MESSAGING VIEW MODEL
    suspend fun token(deputyUUID: UUID, appUUID: UUID, token: String): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.token(deputyUUID, token))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }
    //endregion MESSAGING VIEW MODEL

    //region PARTNERSHIPS VIEW MODEL
    suspend fun partnerships(partnershipsRequest: PartnershipsRequest): Flow<Result<PartnershipsResponse>> {
        return superFlow {
            with(partnershipsRequest) {
                try {
                    val partnershipsResponse = sql.partnerships(this).apply { request = partnershipsRequest }

                    try { message?.let { partnershipsResponse.partnershipUUID?.let { me -> it.updateUUID(me) }; fire.notify(it) }; send(Result.Success(partnershipsResponse)) }
                    catch (e: Exception) { send(fire.exceptionHandler(e)) }
                }
                catch (e: Exception) { send(sql.exceptionHandler(e)) }
            }
        }
    }

    suspend fun resumes(resumesRequest: ResumesRequest): Flow<Result<ResumesResponse>> {
        return superFlow {
            try { send(Result.Success(sql.resumes(resumesRequest).apply { request = resumesRequest })) } catch (e: Exception) { send(sql.exceptionHandler(e)) }
        }
    }
    //endregion PARTNERSHIPS VIEW MODEL

    //region PROFILE VIEW MODEL
    suspend fun entity(entityRequest: EntityRequest): Flow<Result<EntityResponse>> {
        return superFlow {
            with(entityRequest) {
                try {
                    when (action) {
                        REMOVE -> {
                            try { fire.remove(partyUUID!!) } catch (e: Exception) { send(fire.exceptionHandler(e)); return@superFlow }

                            send(Result.Success(sql.entity(this).apply { request = entityRequest }))
                        }
                        GET -> {
                            val sqlEntity = async { sql.entity(entityRequest) }

                            try { fire.getPhoto(partyUUID!!, photo!!); send(Result.Success(sqlEntity.await().apply { request = entityRequest })) } catch (e: Exception) { send(fire.exceptionHandler(e)) }
                        }
                        SET -> {
                            entity?.let { sql.entity(this) }

                            try {
                                entity
                                    ?.preferencesMap
                                    ?.apply { locale?.let { this[LOCALE] = it }; launch { fire.updatePreferences(partyUUID!!, this@apply) } }
                                    ?: locale?.let { launch { fire.locale(partyUUID!!, it) } }

                                photo?.let { launch { fire.putPhoto(partyUUID!!, it.uri!!) } }

                                send(Result.Success(EntityResponse(isSuccessful = true).apply { request = entityRequest }))
                            }
                            catch (e: Exception) { send(fire.exceptionHandler(e)) }
                        }
                    }
                }
                catch (e: Exception) { send(sql.exceptionHandler(e)) }
            }
        }
    }
    //endregion PROFILE VIEW MODEL

    //region RECURRENCES VIEW MODEL
    suspend fun recurrences(recurrencesRequest: RecurrencesRequest): Flow<Result<RecurrencesResponse>> {
        return superFlow { try { send(Result.Success(sql.recurrences(recurrencesRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }
    //endregion RECURRENCES VIEW MODEL

    //region REGISTER VIEW MODEL
    suspend fun register(entityRequest: EntityRequest): Flow<Result<LogInResponse>> {
        return superFlow {
            try {
                with(sql.entity(entityRequest)) {
                    try {
                        fire.signIn(token!!)

                        try {
                            launch { fire.putPhoto(partnership!!.partyUUID, entityRequest.photoUri) }

                            launch { fire.partyPreferences(partnership!!.deputyUUID, entityRequest.preferences!!, res.isNotSpot) }

                            val  fireDeviceToken = async { fire.deviceToken() }

                            val fireLocales = async { fire.dbAvailableLocales() }

                            val resourcesLocale = if (res.appAvailableLocales.toMutableList().apply { retainAll(fireLocales.await()) }.contains(entityRequest.preferences!!.locale)) entityRequest.preferences.locale else DEFAULT_LOCALE

                            val fireSpecialities = async { fire.specialities(resourcesLocale) }

                            fire.session(partnership!!.partyUUID, partnership!!.deputyUUID, sessionUUID!!, fireDeviceToken.await())

                            send(Result.Success(LogInResponse(fireSpecialities.await(), resourcesLocale, DefaultSession(sessionUUID, partnership.apply { partner.preferences = entityRequest.preferences }))))
                        }
                        catch (e: Exception) { fire.signOut(partnership!!.partyUUID, partnership!!.deputyUUID, true); throw e }
                    }
                    catch (e: Exception) { sql.entity(EntityRequest(command = Command.ROLLBACK, partyUUID = partnership!!.partyUUID)); send(fire.exceptionHandler(e)) }
                }
            }
            catch (e: Exception) { send(sql.exceptionHandler(e)) }
        }
    }
    //endregion REGISTER VIEW MODEL

    //region REPORT VIEW MODEL
    suspend fun report(reportRequest: ReportRequest): Flow<Result<ReportResponse>> {
        return superFlow { try { send(Result.Success(sql.report(reportRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }

    suspend fun performance(performanceRequest: PerformanceRequest): Flow<Result<PerformanceResponse>> {
        return superFlow { try { send(Result.Success(sql.performance(performanceRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }
    //endregion REPORT VIEW MODEL

    //region SIGNATURE VIEW MODEL
    suspend fun signature(signatureRequest: SignatureRequest): Flow<Result<SignatureResponse>> {
        return superFlow { try { send(Result.Success(sql.signature(signatureRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }
    //endregion SIGNATURE VIEW MODEL

    //region SINGLE VIEW MODEL
    suspend fun singles(singlesRequest: SinglesRequest): Flow<Result<SinglesResponse>> {
        return superFlow { try { send(Result.Success(sql.singles(singlesRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }
    //endregion SINGLE VIEW MODEL

    //region SPECIALITIES VIEW MODEL
    suspend fun specialities(specialitiesRequest: SpecialitiesRequest): Flow<Result<SpecialitiesResponse>> {
        return superFlow {
            try { send(Result.Success(sql.specialities(specialitiesRequest).apply { request = specialitiesRequest })) } catch (e: Exception) { send(sql.exceptionHandler(e)) }
        }
    }

    suspend fun price(priceRequest: PriceRequest): Flow<Result<PriceResponse>> {
        return superFlow { try { send(Result.Success(sql.price(priceRequest))) } catch (e: Exception) { send(sql.exceptionHandler(e)) } }
    }
    //endregion SPECIALITIES VIEW MODEL

    //region SPOTTER VIEW MODEL
    /**
     * Fetch the available specialists for a given speciality on search area
     */
    suspend fun search(searchRequest: SearchRequest): Flow<Result<SellersResponse>> {
        return superFlow {
//            sql.sellers(searchRequest)
//                .apply {
//                    try {
//                        CoroutineExceptionHandler { _, _ ->  }
//                            .let { me ->
//                                supervisorScope { sellers.forEachWith { launch(me) { Photo.from(context, res.fileProviderAuth, picsDir).let { fire.getPhoto(deputyUUID, it); photo = it } } } }
//
//                                send(Result.Success(this))
//                            }
//                    }
//                    catch (e: Exception) { send(fire.exceptionHandler(e)) }
//                }

            try { send(Result.Success(sql.sellers(searchRequest))) }
            catch (e: Exception) { send(sql.exceptionHandler(e)) }
        }
    }

    /**
     * Fetch the selected specialist photo and saves it on a dedicated directory
     */
    suspend fun photo(pictureRequest: PhotoRequest): Flow<Result<PhotoResponse>> {
        return superFlow {
            with(pictureRequest) {
                try { Photo.from(context, res.fileProviderAuth, picsDir).let { fire.getPhoto(deputyUUID, it); send(Result.Success(PhotoResponse(it))) } }
                catch (e: Exception) { send(fire.exceptionHandler(e)) }
            }
        }
    }
    //endregion SPOTTER VIEW MODEL

    //region TRANSACTION MANAGER
    suspend fun transaction(transactionRequest: TransactionRequest): Flow<Result<Response>> {
        return superFlow {
            with(transactionRequest) {
                try {
                    val response = sql.transaction(this)

                    try { fire.acknowledge(transactionUUID, isBuyerSide); send(Result.Success(response)) } catch (e: Exception) { send(fire.exceptionHandler(e)) }
                }
                catch (e: Exception) { send(sql.exceptionHandler(e)) }
            }
        }
    }

    suspend fun abort(transactionUUID: UUID): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.abort(transactionUUID))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun start(transactionUUID: UUID, start: Long): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.start(transactionUUID, start))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun pause(transactionUUID: UUID, pause: Long): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.pause(transactionUUID, pause))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun resume(transactionUUID: UUID, pauseLength: Long): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.resume(transactionUUID, pauseLength))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun stop(transactionUUID: UUID, stop: Long): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.stop(transactionUUID, stop))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun ring(transactionUUID: UUID, count: Int): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.ring(transactionUUID, count))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun startListener(transactionUUID: UUID): Flow<Result<Long>> = fire.startListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun pauseListener(transactionUUID: UUID): Flow<Result<Long>> = fire.pauseListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun resumeListener(transactionUUID: UUID): Flow<Result<Long>> = fire.resumeListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun stopListener(transactionUUID: UUID): Flow<Result<Long>> = fire.stopListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun abortListener(transactionUUID: UUID): Flow<Result<Boolean>> = fire.abortListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun positionListener(transactionUUID: UUID): Flow<Result<Position>> = fire.positionListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun levelListener(transactionUUID: UUID): Flow<Result<Int>> = fire.levelListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun level(transactionUUID: UUID, @Level level: Int): Flow<Result<Unit>> = superFlow {
        try { send(Result.Success(fire.level(transactionUUID, level))) } catch (e: Exception) { send(fire.exceptionHandler(e)) }
    }
    //endregion TRANSACTION MANAGER

    //region TRANSACTION VIEW MODEL
    suspend fun notify(message: Message): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.notify(message))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }

    suspend fun ringListener(transactionUUID: UUID): Flow<Result<Int>> = fire.ringListener(transactionUUID).map{ Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }

    suspend fun contractListener(transactionUUID: UUID): Flow<Result<Contract>> {
        return fire.contractListener(transactionUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }
    }

    suspend fun chatOverwatch(transactionUUID: UUID, deputyUUID: UUID): Flow<Result<ChatMessage?>> {
        return fire.chatOverwatch(transactionUUID, deputyUUID).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }
    }

    suspend fun chatListenOnce(transactionUUID: UUID, deputyUUID: UUID, key: String? = null): Flow<Result<List<ChatMessage>>> {
        return fire.chatListenOnce(transactionUUID, deputyUUID, key).map { Result.Success(it) }.catch { Result.Error(Err.FIREBASE, it) }
    }

    suspend fun message(transactionUUID: UUID, chatMessage: ChatMessage): Flow<Result<Unit>> {
        return superFlow { try { send(Result.Success(fire.message(transactionUUID, chatMessage))) } catch (e: Exception) { send(fire.exceptionHandler(e)) } }
    }
    //endregion TRANSACTION VIEW MODEL
}
