package pt.hdn.toolbox.preferences

import com.google.firebase.database.Exclude
import kotlinx.parcelize.Parcelize
import pt.hdn.toolbox.annotations.Field
import pt.hdn.toolbox.annotations.FirebasePath.Companion.APPS
import pt.hdn.toolbox.annotations.FirebasePath.Companion.LOCALE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.LOCATION
import pt.hdn.toolbox.annotations.FirebasePath.Companion.NAME
import pt.hdn.toolbox.annotations.FirebasePath.Companion.POSITION
import pt.hdn.toolbox.annotations.FirebasePath.Companion.RATING
import pt.hdn.toolbox.annotations.FirebasePath.Companion.SEARCHABLE
import pt.hdn.toolbox.annotations.FirebasePath.Companion.SHIFT
import pt.hdn.toolbox.annotations.FirebasePath.Companion.TRANSACTION
import pt.hdn.toolbox.misc.Position
import java.util.*

@Parcelize
data class DefaultPreferences(
    override var name: String = "",
    override var position: Position? = null,
    override var locale: String = "",
    override var rating: Float = 5f,
    @Exclude override var transactionUUID: UUID? = null,
    @Exclude override var shiftUUID: UUID? = null,
    @Exclude override var location: Boolean = false,
    @Exclude override var searchable: Boolean = false
) : Preferences {

    //region vars
    var apps: Map<String, Any>? = null
    @Exclude var appUUID: UUID? = null; set(value) {
        apps
            ?.let {
                value
                    ?.toString()
                    ?.let { me ->
                        with(it[me] as Map<String, Any>) {
                            this@DefaultPreferences.transactionUUID = this[TRANSACTION]?.let { UUID.fromString(it as String) }
                            this@DefaultPreferences.shiftUUID = this[SHIFT]?.let { UUID.fromString(it as String) }
                            this@DefaultPreferences.location = this[LOCATION] as? Boolean ?: false
                            this@DefaultPreferences.searchable = this[SEARCHABLE] as Boolean
                        }
                    }
            }
    }
    //endregion vars

    override fun update(map: Map<String, Any>) {
        with(map) {
            this[Field.POSITION]?.let { position = Position.from(it as Map<String, Double>) }
            this[Field.NAME]?.let { name = it as String }
            this[Field.LOCALE]?.let { locale = it as String }
        }
    }

    override fun toMap(appUUID: UUID, isNotSpot: Boolean): Map<String, Any> {
        return buildMap {
            this[APPS] = mapOf(appUUID.toString() to buildMap<String, Any> { this[SEARCHABLE] = searchable; location.takeUnless { isNotSpot }?.let { this[LOCATION] =  it } })
            this[NAME] = name
            this[RATING] = rating
            position?.let { this[POSITION] = it.toMap() }
            this[LOCALE] = locale
        }
    }
}
