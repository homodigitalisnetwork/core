package pt.hdn.toolbox.preferences

import android.os.Parcelable
import pt.hdn.toolbox.misc.Position
import java.util.*

interface Preferences: Parcelable {
    var name: String
    var position: Position?
    var locale: String
    var rating: Float
    var transactionUUID: UUID?
    var shiftUUID: UUID?
    var location: Boolean
    var searchable: Boolean

    fun update(map: Map<String, Any>)

    fun toMap(appUUID: UUID, isNotSpot: Boolean): Map<String, Any>
}